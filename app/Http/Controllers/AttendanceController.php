<?php

namespace App\Http\Controllers;

//use Illuminate\Contracts\Session\Session;
use App\Helper\AppHelper;
use Illuminate\Http\Request;
use App\attendancemodel;
//use DB;
use Excel;
//use Maatwebsite\Excel\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use function MongoDB\BSON\toJSON;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AttendanceController extends Controller
{
    public function index(){
        return view('attendance.index');
    }
    public function attendance_setup(){
        $data=DB::table('attendance_setup')->get();
        return view('attendance.attendance_setup',compact('data'));
    }

    public function settings_store(Request $request){
        $data=DB::table('attendance_setup')->insert([
            'shiftName' =>$request->shiftName,
            'entry_time' =>$request->entry_time,
            'max_entry_time'=>$request->max_entry_time,
            'break_time'=>$request->break_time,
            'exit_time'  =>$request->exit_time,
            'created_at' =>Carbon::now()->toDateTimeString(),
            'updated_at' =>Carbon::now()->toDateTimeString(),
        ]);
        Session::flash('message', 'Shift information has been successfully created.');
        return redirect()->back();
    }

    public function settings_update(Request $request){
        $id=$request->settings_id;
        $data=DB::table('attendance_setup')->WHERE('id',$id)->update([
            'shiftName' =>$request->shiftName,
            'entry_time' =>$request->entry_time,
            'max_entry_time'=>$request->max_entry_time,
            'break_time'=>$request->break_time,
            'exit_time'  =>$request->exit_time,
            'created_at' =>Carbon::now()->toDateTimeString(),
            'updated_at' =>Carbon::now()->toDateTimeString(),
        ]);
        Session::flash('message', 'Shift information has been successfully updated.');
        return redirect()->back();
    }
    
    public function settings_delete(Request $request){
        $id=$request->settings_id;
        $check=DB::table('employees')->where(['empShiftId'=>$id])->get();
        if(count($check)){
            Session::flash('failedMessage',"Can't delete. There are already some employees in this shift." );
        }
        else {
            $data=DB::table('attendance_setup')->where(['id'=>$id])->delete();
            Session::flash('message', 'Shift information has been successfully deleted. ');
        }
        return redirect()->back();
    }

    public function attendance_bonus(){
        $data=DB::table('tbattendance_bonus')->get();
        return view('settings.attendance_bonus.index',compact('data'));
    }


    public function attendance_bonus_store(Request $request){
        $user=Auth::user();
        $current_time = Carbon::now()->toDateTimeString();
        $data=DB::table('tbattendance_bonus')->insert([
            'bTitle' =>$request->bTitle,
            'bAmount' =>$request->bAmount,
            'createdBy'=>$user->id,
            'created_at' =>$current_time,
            'updated_at' =>$current_time,
        ]);
        if($data){
            Session::flash('message', 'Attendance Bonus Category has been successfully created.');
            return redirect()->back();
        }
    }

    public function attendance_bonus_update(Request $request){
        $user=Auth::user();
        $id=$request->bId;
        $current_time = Carbon::now()->toDateTimeString();
        $data=DB::table('tbattendance_bonus')->WHERE('id',$id)->update([
            'bTitle' =>$request->bTitle,
            'bAmount' =>$request->bAmount,
            'createdBy'=>$user->id,
            'updated_at' =>$current_time,
        ]);
        if($data){
            Session::flash('message', 'Attendance Bonus Category has been successfully updated.');
            return redirect()->back();
        }
    }


    public function attendance_bonus_delete(Request $request){
        $id=$request->bId;

        $check=DB::table('employees')->where(['empAttBonusId'=>$id])->get();
        if(count($check)){
            Session::flash('fmessage',"Error! There are already some employees using this resource." );
        }
        else {
            $data=DB::table('tbattendance_bonus')->where(['id'=>$id])->delete();
            Session::flash('message', 'Attendance Bonus Category has been successfully deleted. ');
        }
        
        return redirect()->back();
    }

    public function csv_upload(Request $request){
        $this->validate($request,[
            'upload-file'=>['required']
        ]);
        $file=$request->file('upload-file');
        $current_time = Carbon::now()->toDateTimeString();
        $csv_file=time()."_".$request->file('upload-file')->getClientOriginalName();
        $file->move(base_path('/file/'),time()."_".$file->getClientOriginalName());
//        return $csv_file;
        $store_scv=DB::table('attendance_file')->insert([
            'attachment' =>$csv_file,
            'date' =>$request->cur_date,
            'comment'=>$request->comment,
            'created_at' =>$current_time,
            'updated_at' =>$current_time,
        ]);

        if($store_scv){
            Session::flash('message', 'File upload Successful!');
            return redirect('csv_view');
        }else{
            Session::flash('message', 'File format is not matched.');

        }
    }
    public function csv_show(){
        $data=DB::table('attendance_file')->latest()->take(1)->get();
        return view('attendance.csv_view',compact('data'));
    }

    public function store(Request $request){
        $file_name=DB::table('attendance_file')->where(['id'=>$request->hidden_id])->first()->attachment;
        $path=base_path()."/file/$file_name";
//        $path=base_path()."/file/1564892829_test-1.xls";
//        return $file_name;
        $data=Excel::load($path, function($reader){})->get();
//        return $data;
        if(!empty($data) && $data->count()){
            foreach ($data as $key => $value) {
                $insert[] = [
                    'device_name' => $value->device_name,
                    'door' => $value->door,
                    'emp_no'=>$value->emp_no,
                    'card_no'=>$value->card_no,
                    'time'=>$value->time,
                    'event_explanation'=>$value->event_explanation,
                    'attendance_file_id'=>$request->hidden_id

                ];
            }

            if(!empty($insert)){
                $insert_data=collect($insert);
                $chunks = $insert_data->chunk(500);
                foreach ($chunks as $chunk)
                {
                    $insertData=DB::table('attendance_machine_data')->insert($chunk->toArray());
                }

                if ($insertData) {
                    Session::flash('success', 'Your Data has successfully imported');
                }
                else {
                    Session::flash('error', 'Error inserting the data..');
                    return back();
                }
            }
        }

        $additional_card_check=DB::table('tbtemp_employee_proxy')->count();
        if($additional_card_check){
            $att=DB::select("SELECT attendance_machine_data.card_no, tbtemp_employee_proxy.proxy_number AS proxy_number, MIN(TIME(attendance_machine_data.time)) as in_time, MAX(TIME(attendance_machine_data.time)) as out_time, DATE(attendance_machine_data.time) as date FROM attendance_machine_data JOIN tbtemp_employee_proxy ON tbtemp_employee_proxy.proxy_number=attendance_machine_data.card_no GROUP BY attendance_machine_data.card_no, DATE(attendance_machine_data.time) HAVING attendance_machine_data.card_no is not null");
            $ipids=[];
            foreach ($att as $key => $value) {
                $emp_id=$value->proxy_number;
                if($emp_id!=null){
                    $checkEmp = DB::table('temporary_card_attendance')->where(['empCardNumber' => $emp_id, 'date' => $value->date])->get();
                    if(count($checkEmp)){
                        $id = $checkEmp[0]->id;
                        $upids[]=$id;
                        $ddate=$value->date;
                        $otime=$value->out_time;
                        if($checkEmp[0]->in_time>$otime)
                        {
                            $temp=$checkEmp[0]->in_time;
                            $checkEmp[0]->in_time=$otime;
                            $otime=$temp;
                        }
                        $upInsert[]=[
                            'id'=>$id,
                            'empCardNumber'=>$emp_id,
                            'in_time'=>$checkEmp[0]->in_time,
                            'out_time'=>$otime,
                            'date'=>$ddate,

                        ];
                    }
                    else{
                        $seInsert[] = [
                            'empCardNumber' => $emp_id,
                            'in_time' => $value->in_time,
                            'out_time' => $value->out_time,
                            'date' => $value->date,
                        ];
                    }
                }
            }

            if(!empty($upids))
            {
                DB::table('temporary_card_attendance')->whereIn('id',$upids)->delete();

                DB::table('temporary_card_attendance')->insert($upInsert);


            }

            if (!empty($seInsert)) {
                DB::table('temporary_card_attendance')->insert($seInsert);
            }




        }

        $att=DB::select("SELECT attendance_setup.max_entry_time,attendance_setup.exit_time,attendance_machine_data.card_no, employees.id AS emp_id, MIN(TIME(attendance_machine_data.time)) as in_time, MAX(TIME(attendance_machine_data.time)) as out_time, DATE(attendance_machine_data.time) as date FROM attendance_machine_data 
        JOIN employees ON employees.empCardNumber=attendance_machine_data.card_no
        LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id
        GROUP BY attendance_machine_data.card_no, DATE(attendance_machine_data.time) HAVING attendance_machine_data.card_no is not null");
        $ids=[];
        foreach ($att as $key => $value) {
            $emp_id=$value->emp_id;
            if($emp_id!=null){
                $checkEmp = DB::table('attendance')->where(['emp_id' => $emp_id, 'date' => $value->date])->get();
                if(count($checkEmp)){
                    $id = $checkEmp[0]->id;
                    $ids[]=$id;
                    $ddate=$value->date;
                    $otime=$value->out_time;
                    if($checkEmp[0]->in_time>$otime)
                    {
                        $temp=$checkEmp[0]->in_time;
                        $checkEmp[0]->in_time=$otime;
                        $otime=$temp;
                    }
                    $updateInsert[]=[
                        'id'=>$id,
                        'emp_id'=>$emp_id,
                        'in_time'=>$checkEmp[0]->in_time,
                        'out_time'=>$otime,
                        'date'=>$ddate,
                        'max_entry'=>$value->max_entry_time,
                        'exit_times'=>$value->exit_time,
                    ];
                }
                else{
                    $secondInsert[] = [
                        'emp_id' => $emp_id,
                        'in_time' => $value->in_time,
                        'out_time' => $value->out_time,
                        'date' => $value->date,
                        'max_entry'=>$value->max_entry_time,
                        'exit_times'=>$value->exit_time,
                    ];
                }
            }
        }
        if(!empty($ids))
        {
            DB::table('attendance')->whereIn('id',$ids)->delete();
            DB::table('attendance')->insert($updateInsert);
        }

        if (!empty($secondInsert)) {
//            return $secondInsert;
            DB::table('attendance')->insert($secondInsert);
        }
        DB::table('attendance_machine_data')->truncate();

        return "KK";

        $data=DB::table('attendance_file')->take(5)->latest()->get();
        return view('attendance.csv_view',compact('data'));

    }



    public function attendance_report_daily()
    {
        return view('attendance.status');
    }

    public function absentStatus($date){
        $applications=DB::select("SELECT employees.id,employees.empFirstName,employees.empLastName,
                          employees.employeeId, designations.designation FROM employees, designations 
                          where employees.empDesignationId = designations.id AND employees.empAccStatus='1' AND employees.id not in 
                          (SELECT emp_id FROM attendance GROUP BY date, emp_id HAVING date='".$date."')
                           AND employees.id NOT IN 
                          (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date and status='1');");

        if(count($applications)){
            return view('attendance.statusData',compact('applications','date'));
        }
        else{
            return "";
        }

    }



    public function filesView(){
        $data=DB::table('attendance_file')->latest()->get();
        return view('attendance.fileIndex',compact('data'));

    }

    public function showDelete($id){
        $data=DB::table('attendance_file')->where(['id'=>$id])->first();
//        return $data;
        return view('attendance.modal.deleteForm',compact('data'));
    }

    public function destroyFile(Request $request,$id){
        $path=base_path()."/file/".DB::table('attendance_file')->where(['id'=>$id])->first()->attachment;
        if(file_exists($path)){
            unlink($path);
        }
        $dlt=DB::table('attendance_file')->where(['id'=>$id])->delete();
        if($dlt){
            Session::flash('message', 'File delete successful!');
        }
        $data=DB::table('attendance_file')->latest()->get();
        return view('attendance.fileIndex',compact('data'));

    }

    public function addEmployee($id,$date){
        $data=DB::table('employees')->where(['id'=>$id])->first();
        return view('attendance.modal.attendanceData',compact('data','date'));

    }

    public function storeAttendance(Request $request){
        $this->validate($request,[
            'in_time'=>'required',
            'out_time'=>'required',
        ]);
        $up=DB::table('attendance')->insert([
            'emp_id'=>$request->emp_id,
            'in_time'=>$request->in_time,
            'out_time'=>$request->out_time,
            'date'=>$request->date,

        ]);
        if($up){
            Session::flash('message','Attendance data inserted');
        }
        return redirect('/attendance/present/status');

    }

    public function presentStatus(){
        return view('report.attendance.daily_attendance_report');

    }
    public function presentAttendance($date){
        $data=DB::select("SELECT attendance.id as aid, employees.id,employees.empFirstName,
              employees.empLastName,employees.employeeId, designations.designation, attendance.in_time,
              attendance.out_time,attendance.date FROM employees, designations, 
              attendance where employees.empDesignationId=designations.id and employees.id=attendance.emp_id
              AND employees.empAccStatus='1' 
              AND attendance.date='$date'");
        $absentData=DB::select("SELECT employees.id,employees.empFirstName, employees.empLastName,employees.employeeId, designations.designation FROM employees, designations where employees.empDesignationId=designations.id AND employees.empAccStatus='1' and employees.id not in (SELECT emp_id FROM attendance where date='$date') AND employees.id NOT IN 
                          (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date and status='1')");

        $allData=array_merge($data,$absentData);
        return $allData;

    }

    public function dataAttendance($data){
        $ddata=explode(',',$data);
        $date=$ddata[0];
        $id=$ddata[1];
        $time=DB::table('attendance')->where('emp_id','=',$id)->where('date','=',$date)->select('id','in_time','out_time')->first();
        return view('attendance.editDataAttendance',compact('time'));

    }

    public function addEditAttendance($id, $date){
        $checkExist=DB::table('attendance')->where(['emp_id'=>$id,'date'=>$date])->get();
        if(count($checkExist)) {
            $data=DB::table('employees')
                ->join('designations','employees.empDesignationId','=','designations.id')
                ->join('attendance','employees.id','=','attendance.emp_id')
                ->select('employees.id as eid','attendance.id as aid','employees.empFirstName',
                    'employees.empLastName','employees.employeeId','designations.designation','attendance.date',
                    'attendance.in_time','attendance.out_time')
                ->where(['attendance.date'=>$date,'attendance.emp_id'=>$id])
                ->first();

            return view('attendance.modal.attendanceUpdate',compact('data'));

        }
        else{
            $data=DB::table('employees')
                ->join('designations','employees.empDesignationId','=','designations.id')
                ->select('employees.id','employees.empFirstName','employees.empLastName','employees.employeeId','designations.designation')
                ->where(['employees.id'=>$id])
                ->first();
//            $data=$employee;
            return view('attendance.modal.attendanceData',compact('data','date'));
        }

    }

    public function updateAttendance(Request $request,$id){
        if($id==0){
            $this->validate($request,[
                'emp_id'=>'required',
                'date'=>'required',
                'stime'=>'required',
                'etime'=>'required',
            ]);

            $k=DB::table('attendance')->where('id','=',$request->id)->update([
                'emp_id'=>$request->emp_id,
                'in_time'=>$request->stime,
                'out_time'=>$request->etime,
                'date'=>$request->date,
            ]);
            if($k){
               $empdetails=DB::table('employees')->where('id','=',$request->emp_id)->first();

                $str = DB::table('notifications')->insert([
                    'type'=>'Manual Attendance Update',
                    'message'=>"$empdetails->empFirstName $empdetails->empLastName's ($empdetails->employeeId) attendance has been changed.",
                    'notification_by'=>Auth::user()->id,
                    'notification_to'=>$request->emp_id,
                    'created_at'=>Carbon::now('Asia/Dhaka')->toDateTimeString(),
                    'updated_at'=>Carbon::now('Asia/Dhaka')->toDateTimeString(),
                ]);
                DB::table('tbactivity_logs_history')->insert([
                    'acType'=>'Manual Attendance Update',
                    'details'=>"$empdetails->empFirstName $empdetails->empLastName's ($empdetails->employeeId) attendance has been updated.",
                    'createdBy'=>Auth::user()->id,
                    'created_at'=>Carbon::now('Asia/Dhaka')->toDateTimeString(),
                    'updated_at'=>Carbon::now('Asia/Dhaka')->toDateTimeString(),
                ]);
                Session::flash('success','Attendance data updated');

            }

            return \redirect()->back();

        }
//        return $request->all();
        $this->validate($request,[
            'emp_id'=>'required',
            'in_time'=>'required',
            'out_time'=>'required',
            'date'=>'required',
        ]);
        $up=DB::table('attendance')->where(['id'=>$id])->update([
            'emp_id'=>$request->emp_id,
            'in_time'=>$request->in_time,
            'out_time'=>$request->out_time,
            'date'=>$request->date,

        ]);
        if($up){
            Session::flash('success','Attendance data updated');
        }
        return redirect()->back();
    }

    public function createAttendance(){
        $department=DB::table('departments')->select('id','departmentName')->get(); 
        $section=DB::table('employees')->select('empSection')
        ->where('empSection','!=',NULL)
        ->groupBy('empSection')
        ->get(); 
        $shift=DB::table('attendance_setup')->get();
        return view('attendance.createAttendance',compact('department','section','shift'));
    }

    public function editAttendance(){
        $department=DB::table('departments')->select('id','departmentName')->get(); 
        $section=DB::table('employees')->select('empSection')
        ->where('empSection','!=',NULL)
        ->groupBy('empSection')
        ->get(); 
        $employees=DB::table('employees')->where('empAccStatus','=',1)->get(['empFirstName','employeeId','id']);
        return view('attendance.editAttendance',compact('employees','department','section'));

    }

    public function deleteAttendance(){
        $department=DB::table('departments')->select('id','departmentName')->get();
        $employees=DB::table('employees')->where('empAccStatus','=',1)->get(['empFirstName','employeeId','id']);
        return view('attendance.delete_attendance',compact('employees','department'));

    }

    public function deleteAttendanceProccess1(Request $request){
        $myarray=implode(',',$request->emp_id);
        $att_record=DB::SELECT("SELECT attendance.id,attendance.emp_id,attendance.in_time,attendance.out_time,attendance.date,employees.empFirstName,employees.empLastName,employees.employeeId,employees.empCardNumber
        FROM attendance
        JOIN employees ON employees.id=attendance.emp_id 
        WHERE attendance.emp_id IN ($myarray) 
        AND `date` = '$request->date'
        ");
        return view('attendance.delete_attendance_view',compact('att_record'));
    }

    public function confirm_delete($id){
       $att=DB::table('attendance')->join('employees','employees.id','=','attendance.emp_id')->select('attendance.*','employees.empFirstName','employees.empLastName','employees.employeeId','employees.empCardNumber')->where(['attendance.id'=>$id])->first();
        //dd($att);
        DB::table('notifications')->insert([
            'type'=>'Manual Absent ',
            'message'=>"$att->empFirstName $att->empLastName's ($att->employeeId) attendance status changed to absent.",
            'notification_by'=>Auth::user()->id,
            'notification_to'=>$att->emp_id,
            'created_at'=>Carbon::now('Asia/Dhaka')->toDateTimeString(),
            'updated_at'=>Carbon::now('Asia/Dhaka')->toDateTimeString(),
        ]);
        DB::table('tbactivity_logs_history')->insert([
            'acType'=>'Manual Attendance Update',
            'details'=>"$att->empFirstName $att->empLastName's ($att->employeeId) attendance status changed to absent.",
            'createdBy'=>Auth::user()->id,
            'created_at'=>Carbon::now('Asia/Dhaka')->toDateTimeString(),
            'updated_at'=>Carbon::now('Asia/Dhaka')->toDateTimeString(),
        ]);
        $att_del=DB::table('attendance')->where(['id'=>$id])->delete();
        if($att_del){
            Session::flash('success','Attendance record has been successfully deleted for this date.');
            return back(); 
        }
      
         //$employees=DB::table('employees')->where('empAccStatus','=',1)->get(['empFirstName','employeeId','id']);
    }

    public function manualAttendance(Request $request){
        $employeeID[]=explode(' #',$request->employee);
        if(isset($employeeID[0][1])){
            $request->emp_id=$employeeID[0][1];
        }
        else{
            Session::flash('message','Employee Name #ID Not found. Follow the exact format suggested.');
            return Redirect::back();
        }
        $request->emp_id=DB::table('employees')->where('employeeId','=',$request->emp_id)->first()->id;
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->createAttendance();
        }
        $this->validate($request,[
            'employee'=>'required',
            'date'=>'required',
            'stime'=>'required',
        ]);
        $empIDCheck=DB::table('employees')->where('id','=',$request->emp_id)->count();
        $empdetails=DB::table('employees')
        ->where('employees.id','=',$request->emp_id)
        ->first();
        $shift=DB::table('attendance_setup')->where('id',$request->shift_id)->first();
        if($empIDCheck==0){
            Session::flash('message','No employee found with the given employee id. Please check again.');
            return Redirect::back();
        }
        if($request->etime==null){
            $request->etime=$request->stime;
        }
        $check=DB::table('attendance')->where(['emp_id'=>$request->emp_id,'date'=>Carbon::parse($request->date)->toDateString()])->get();
        if(count($check)<1) {
            $input[] = [
                'emp_id' => $request->emp_id,
                'date' => Carbon::parse($request->date)->toDateString(),
                'in_time' => $request->stime,
                'out_time' => $request->etime,
                'max_entry' =>$shift->max_entry_time,
                'exit_times' =>$shift->exit_time,
            ];
            DB::table('notifications')->insert([
            'type'=>'Manual Attendance',
            'message'=>"$empdetails->empFirstName $empdetails->empLastName's ($empdetails->employeeId) manual attendance has been recorded.",
            'notification_by'=>Auth::user()->id,
            'notification_to'=>$request->emp_id,
            'created_at'=>Carbon::now('Asia/Dhaka')->toDateTimeString(),
            'updated_at'=>Carbon::now('Asia/Dhaka')->toDateTimeString(),
            ]);
            DB::table('tbactivity_logs_history')->insert([
                'acType'=>'Manual Attendance',
                'details'=>"$empdetails->empFirstName $empdetails->empLastName's ($empdetails->employeeId) manual attendance has been recorded.",
                'createdBy'=>Auth::user()->id,
                'created_at'=>Carbon::now('Asia/Dhaka')->toDateTimeString(),
                'updated_at'=>Carbon::now('Asia/Dhaka')->toDateTimeString(),
            ]);
        }
        else{
            Session::flash('error','Failed!! Given employee attendance record already exist for '.Carbon::parse($request->date)->toDateString().'. Update Attendance.');
            return redirect(route('attendance.manual.edit'));
        }
        if(!empty($input)) {
            $k = DB::table('attendance')->insert($input);
            if ($k) {
                Session::flash('success', 'Attendance Information Successfully Added');
            }
        }
        return redirect()->back();
    }

    public function manual_store_date(){
        $employees=DB::table('employees')->where('empAccStatus','=',1)->get(['empFirstName','employeeId','id']);
        $department=DB::table('departments')->select('id','departmentName')->get(); 
        $section=DB::table('employees')->select('empSection')
        ->where('empSection','!=',NULL)
        ->groupBy('empSection')
        ->get(); 
        return view('attendance.manual_attendance_date_wise',compact('employees','department','section'));
    }

    public function manual_store_data(Request $request){
        $start_date=Carbon::parse($request->date)->toDateString();
        $end_date=Carbon::parse($request->endDate)->toDateString();
        $shift=DB::table('attendance_setup')->get();
        if($start_date<=$end_date){
            $employee=DB::table('employees')
                ->leftJoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftJoin('designations','employees.empDesignationId','=','designations.id')
                ->where('employees.id','=',$request->emp_id)
                ->select('employees.empFirstName','employees.empLastName','employees.employeeId','employees.id',
                    'departments.departmentName','designations.designation')
                ->first();
            $start_date=Carbon::parse($request->date)->toDateString();
            $end_date=Carbon::parse($request->endDate)->addDay(1)->toDateString();
            return view('attendance.manual_attendance_date_wise_date',compact('employee','start_date','end_date','shift'));

        }
        else{
            Session::flash('error','Start Date Can Not Be Greater Than End Date');
            return \redirect()->back();
        }

    }

    public function editAttendanceDate(){
        $employees=DB::table('employees')->where('empAccStatus','=',1)->get(['empFirstName','employeeId','id']);
        return view('attendance.edit_attendance_date_wise', compact('employees'));
    }

    public function showEditAttendanceDate(Request $request){
        $start_date=Carbon::parse($request->date)->toDateString();
        $end_date=Carbon::parse($request->endDate)->toDateString();
        if($start_date<=$end_date){
            $employee=DB::table('employees')
                ->leftJoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftJoin('designations','employees.empDesignationId','=','designations.id')
                ->where('employees.id','=',$request->emp_id)
                ->select('employees.empFirstName','employees.empLastName','employees.employeeId','employees.id',
                    'departments.departmentName','designations.designation')
                ->first();
            $start_date=Carbon::parse($request->date)->toDateString();
            $end_date=Carbon::parse($request->endDate)->addDay(1)->toDateString();
            return view('attendance.edit_attendance_date_wise_date',compact('employee','start_date','end_date'));

        }
        else{
            Session::flash('error','Start Date Can Not Be Greater Than End Date');
            return \redirect()->back();
        }

    }

    public function edit_store_date_store(Request $request){
//        return $request->all();
        if(isset($request->ids)) {
            $data_count = count($request->ids);
            for ($i = 0; $i < $data_count; $i++) {
                if($request->in_time[$i]!=null) {
                    if($request->out_time[$i]==null){
                        $out_time=$request->in_time[$i];
                    }
                    else{
                        $out_time=$request->out_time[$i];

                    }
                    DB::table('attendance')->where('id', '=', $request->ids[$i])
                        ->where('date', '=', $request->dates[$i])
                        ->where('emp_id','=', $request->emp_id)->update([
                            'in_time' => $request->in_time[$i],
                            'out_time' => $out_time,
                        ]);
                }

            }
        }

        Session::flash('success', 'Attendance Data Inserted/Updated Successfully');
        return \redirect(route('attendance.manual.show_edit_date_wise'));
    }

    public function manual_attendance(){
        return view('attendance.manual_attendance');
    }

    public function manual_store_date_store(Request $request){
        $shift=DB::table('attendance_setup')->where('id',$request->shift_id)->first();
        if(isset($request->date)) {
            $data_count = count($request->date);
            $data = [];
            $update = 0;
            for ($i = 0; $i < $data_count; $i++) {
                if ($request->out_time[$i] == null) {
                    $out_time = $request->in_time[$i];

                } elseif ($request->out_time[$i] < $request->in_time[$i]) {
                    $out_time = $request->in_time[$i];

                } else {
                    $out_time = $request->out_time[$i];

                }
                if ($request->update[$i] == 0) {
                    if ($request->in_time[$i] != null) {
                        $data[] = [
                            'emp_id' => $request->emp_id,
                            'date' => $request->date[$i],
                            'in_time' => $request->in_time[$i],
                            'out_time' => $out_time,
                            'max_entry' => $shift->max_entry_time,
                            'exit_times' => $shift->exit_time,
                        ];
                    }
                }
                if ($request->update[$i] == 1) {
                    $update = 1;
                    DB::table('attendance')->where('emp_id', '=', $request->emp_id)
                        ->where('date', '=', $request->date[$i])->update([
                            'in_time' => $request->in_time[$i],
                            'out_time' => $out_time,
                            'date' => $request->date[$i],
                            'emp_id' => $request->emp_id,
                            'max_entry' => $shift->max_entry_time,
                            'exit_times' => $shift->exit_time,
                        ]);


                    //                return $id;
                    //                return $request->all();
                }

            }
            $empdetails = DB::table('employees')->where('id', '=', $request->emp_id)->first();
            if (!empty($data)) {
                DB::table('attendance')->insert($data);

                DB::table('tbactivity_logs_history')->insert([
                    'acType' => 'Manual Attendance',
                    'details' => "$empdetails->empFirstName $empdetails->empLastName's ($empdetails->employeeId) attendance has been changed.",
                    'createdBy' => Auth::user()->id,
                    'created_at' => Carbon::now('Asia/Dhaka')->toDateTimeString(),
                    'updated_at' => Carbon::now('Asia/Dhaka')->toDateTimeString(),
                ]);

                Session::flash('success', 'Attendance Data Inserted/Updated Successfully');
                return \redirect(route('attendance.manual_store_date'));
            } elseif (empty($data) && $update == 0) {
                Session::flash('error', 'Data Insertion Failed No Data Found');
                return \redirect(route('attendance.manual_store_date'));
            }


            DB::table('notifications')->insert([
                'type' => 'Manual Attendance Edit',
                'message' => "$empdetails->empFirstName $empdetails->empLastName's ($empdetails->employeeId) attendance has been changed.",
                'notification_by' => Auth::user()->id,
                'notification_to' => $request->emp_id,
                'created_at' => Carbon::now('Asia/Dhaka')->toDateTimeString(),
                'updated_at' => Carbon::now('Asia/Dhaka')->toDateTimeString(),
            ]);

            DB::table('tbactivity_logs_history')->insert([
                'acType' => 'Manual Attendance',
                'details' => "$empdetails->empFirstName $empdetails->empLastName's ($empdetails->employeeId) attendance has been changed.",
                'createdBy' => Auth::user()->id,
                'created_at' => Carbon::now('Asia/Dhaka')->toDateTimeString(),
                'updated_at' => Carbon::now('Asia/Dhaka')->toDateTimeString(),
            ]);

            Session::flash('success', 'Attendance Data Inserted/Updated Successfully');
            return \redirect(route('attendance.manual_store_date'));


        }
        else{
            return \redirect(route('attendance.manual_store_date'));
        }
    }

    public static function late_time(){
        $late_time=DB::table('attendance_setup')->
        select('max_entry_time')->first();
        return $late_time->max_entry_time;
    }

    public static function out_time(){
        $out_time=DB::table('attendance_setup')->
        select('exit_time')->first();
        return $out_time->exit_time;
    }

    //delete attendance date wise
    public function delete_attendance_date_wise(Request $request){
      $data=DB::table('attendance')->where('date',$request->date)->delete();
      Session::flash('success_one', 'Attendance Data has been deleted Successfully');
      return back();
    }


    //delete attendance date wise department
    public function delete_attendance_date_wise_dept(Request $request){   
      $data=DB::table('employees')
      ->join('attendance','attendance.emp_id','=','employees.id')
      ->where('empDepartmentId',$request->dept_id)
      ->where('attendance.date',$request->date)
      ->select('attendance.id','attendance.date','attendance.emp_id')
      ->get();
      return view('attendance.del_date_dept_att',compact('data'));
    }

    //delete attendance date and department confirm
    public function delete_attendance_date_wise_dept_confirm(Request $request){
        $myarray=implode(',',$request->id);
        DB::SELECT("DELETE  FROM `attendance` WHERE `id` IN ($myarray)");
        Session::flash('success_two', 'Attendance Data has been deleted Successfully');
        return redirect()->route('attendance.manual.delete');
    }

    //department wise menual attendance 
    public function manualAttendancedept(Request $request){
       $date=date('Y-m-d',strtotime($request->date)); 
       $data=DB::table('employees')
       ->where('empDepartmentId',$request->dept_id)
       ->select('id')
       ->get();
       $coll=[];
       foreach($data as $emp){
        $coll[]=$emp->id;
       }
       $myarray=implode(',',$coll);
       DB::SELECT("DELETE  FROM `attendance` WHERE `emp_id` IN ($myarray) AND date='$date' ");
    
       $data_two=DB::table('employees')
       ->where('empDepartmentId',$request->dept_id)
       ->select('employees.id')
       ->get();
       $shift=DB::table('attendance_setup')->where('id',$request->shift_id)->first();
       foreach($data_two as $emp){
        DB::table('attendance')->insert([
         'emp_id' =>$emp->id,
         'in_time' =>$request->stime,
         'out_time' =>$request->etime,
         'max_entry' =>$shift->max_entry_time,
         'exit_times' =>$shift->exit_time,
         'date' =>Carbon::parse($request->date)->toDateString(),
        ]);
       }
       Session::flash('success', 'Attendance Information Successfully Added');
       return back();
    }



    public function manualAttendancesection(Request $request){
        $date=date('Y-m-d',strtotime($request->date)); 
        $data=DB::table('employees')
        ->where('empSection',$request->section_id)
        ->select('id')
        ->get();
        $coll=[];
        foreach($data as $emp){
         $coll[]=$emp->id;
        }
        $myarray=implode(',',$coll);
        DB::SELECT("DELETE  FROM `attendance` WHERE `emp_id` IN ($myarray) AND date='$date' ");
     
        $data_two=DB::table('employees')
        ->where('empSection',$request->section_id)
        ->select('employees.id')
        ->get();
        $shift=DB::table('attendance_setup')->where('id',$request->shift_id)->first();
        foreach($data_two as $emp){
         DB::table('attendance')->insert([
          'emp_id' =>$emp->id,
          'in_time' =>$request->stime,
          'out_time' =>$request->etime,
          'max_entry' =>$shift->max_entry_time,
          'exit_times' =>$shift->exit_time,
          'date' =>Carbon::parse($request->date)->toDateString(),
         ]);
        }
        Session::flash('success', 'Attendance Information Successfully Added');
        return back();
    }


    public function updateAttendancedept(Request $request){
        $data=DB::table('employees')
        ->join('attendance','employees.id','=','attendance.emp_id')
        ->where('empDepartmentId',$request->dept_id)
        ->where('date',Carbon::parse($request->date)->toDateString())
        ->select('empFirstName','employeeId','attendance.emp_id','in_time','out_time','date')
        ->get();
        return view('attendance.dept_datewise_edit',compact('data'));
    }

    public function updateAttendancesection(Request $request){
        $data=DB::table('employees')
        ->join('attendance','employees.id','=','attendance.emp_id')
        ->where('empSection',$request->section_id)
        ->where('date',Carbon::parse($request->date)->toDateString())
        ->select('empFirstName','employeeId','attendance.emp_id','in_time','out_time','date')
        ->get();
        return view('attendance.dept_datewise_edit',compact('data'));
    }

    public function updateattdept(Request $request){
        $date=$request->date;
        $emp_id = $request->emp_id;
        $in_time = $request->in_time;
        $out_time = $request->out_time;
      foreach($emp_id as $key => $n ) 
      {
         $update=DB::table('attendance')->where('emp_id',$emp_id[$key])->where('date',$date)->update([
            'in_time' =>$in_time[$key],
            'out_time' =>$out_time[$key],
         ]);
      }	    
      Session::flash('success', 'Attendance Information has been Successfully Update');
      return back();  
    }

    public function manual_dept_section_datewise_data(Request $request){
       $start=date('Y-m-d',strtotime($request->date)); 
       $end=date('Y-m-d',strtotime($request->endDate)); 
       if($request->dept_id){
        $data=DB::table('employees')
        ->where('empDepartmentId',$request->dept_id)
        ->select('id','empFirstName','employeeId')
        ->get();
       }else{
        $data=DB::table('employees')
        ->where('empSection',$request->section_id)
        // ->where('date',Carbon::parse($request->date)->toDateString())
        ->select('id','empFirstName','employeeId')
        ->get(); 
       }        
        return view('attendance.dept_section_date_wise_attendance',compact('start','end','data'));
    }

    public function manual_dept_section_datewise_data_store(Request $request){
        
  
    //    for($i=0;$i<count($request->att_date);$i++){
    //      DB::table('attendance')->insert([
    //          'emp_id'=>$request->btn_emp_id,
    //          'date' =>$request->att_date[$i],
    //          'in_time'=>$request->in_time[$i],
    //          'out_time'=>$request->out_time[$i],
    //      ]);

    //    }  
    //    Session::flash('success', 'Attendance Information has been Successfully Save');
    //    return back();  
    }





}