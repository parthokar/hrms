<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use Redirect;
use Session;
use Carbon\Carbon;
use App\off_day;
use stdClass;
use auth;
use Excel;
use DateTime;


class PayrollController extends Controller
{

   //bonus settings
   public function bonus_settings(){
     $work_groups=DB::table('employees')
     ->select('work_group')
     ->groupBy('work_group')
     ->get();  
     $list=DB::table('festival_bonus_settings')->get();
     return view('payroll.bonus_settings',compact('work_groups','list'));
   } 

   public function bonus_settings_edit($id){
        $data=DB::table('festival_bonus_settings')
        ->where('id',$id)
        ->first(); 
        $work_groups=DB::table('employees')
        ->select('work_group')
        ->groupBy('work_group')
        ->get(); 
       return view('payroll.bonus_settings_edit',compact('data','work_groups'));
   }

   //bonus settings store
   public function bonus_settings_store(Request $request){
        DB::table('festival_bonus_settings')->insert([
            'work_group' =>$request->work_group,
            'fest_title' =>$request->fest_title,
            'fest_start' =>date('Y-m-d',strtotime($request->fest_start)),
            'condition_year' =>$request->condition_year,
            'condition_month' =>$request->condition_month,
            'year_percent' =>$request->year_percent,
            'month_percent' =>$request->month_percent,
        ]);
        Session::flash('message', 'Settings Store Successful!');
        return back();
   }
    //bonus settings store
    public function bonus_settings_update(Request $request){
        $id=$request->id;
        DB::table('festival_bonus_settings')->where('id',$id)->update([
            'work_group' =>$request->work_group,
            'fest_title' =>$request->fest_title,
            'fest_start' =>date('Y-m-d',strtotime($request->fest_start)),
            'condition_year' =>$request->condition_year,
            'condition_month' =>$request->condition_month,
            'year_percent' =>$request->year_percent,
            'month_percent' =>$request->month_percent,
        ]);
        Session::flash('message', 'Settings Update Successful!');
        return redirect()->route('display_bonus_settings');
   }
    //Payroll geade view page
    public function payroll_grade(){
        return view('payroll.grade');
    }
    //Payroll geade data store
    public function payroll_grade_store(Request $request){
        $current_time = Carbon::now()->toDateTimeString();
        $payroll_grade=DB::table('payroll_grade')->insert([
            'grade_name' =>$request->grade_name,
            'basic_salary' =>$request->basic_amount,
            'house_rant' =>$request->house_amount,
            'transport' =>$request->transport_amount,
            'medical' =>$request->medical_amount,
            'food' =>$request->food_amount,
            'others' =>$request->others_amount,
            'total_salary'=>$request->basic_amount+$request->house_amount+$request->transport_amount+$request->medical_amount+$request->food_amount+$request->others_amount,
            'created_by' =>$request->user_name,
            'modified_by' =>$request->user_name,
            'created_at' =>$current_time,
            'updated_at' =>$current_time,
        ]);
        if($payroll_grade){
            Session::flash('message', 'Grade Store Successful!');
            return redirect('grade');
        }
    }
    //Payroll grade data view method
    public function payroll_grade_show(){
        $data=DB::table('payroll_grade')->paginate(20);
        return view('payroll.grade_show',compact('data'));
    }
    //id wise grade show
    public function show($id){
        $data=DB::table('payroll_grade')->WHERE('id',$id)->get();
        return response()->json($data);
    }
    //Payroll grade data update method
    public function payroll_grade_update(Request $request){
        $id=$request->grade_id;
        $check=DB::table('grade_history')->WHERE('id',$id)->count();
        if($check==0){
            $selectPrevious=DB::table('payroll_grade')->WHERE('id',$id)->first();
            $recordinsert=DB::table('grade_history')->insert([
                'gradeId' =>$selectPrevious->id,
                'basicSalary' =>$selectPrevious->basic_salary,
                'houseRent' =>$selectPrevious->house_rant,
                'medical' =>$selectPrevious->medical,
                'transport' =>$selectPrevious->transport,
                'food' =>$selectPrevious->food,
                'month' =>date('Y-m'),
                'created_at' =>Carbon::now()->toDateTimeString(),
                'updated_at' =>Carbon::now()->toDateTimeString()
            ]);
        }else{
            $check=DB::table('grade_history')->WHERE('id',$id)->delete();
            $selectPrevious=DB::table('payroll_grade')->WHERE('id',$id)->first();
            $recordinsert=DB::table('grade_history')->insert([
                'gradeId' =>$selectPrevious->id,
                'basicSalary' =>$selectPrevious->basic_salary,
                'houseRent' =>$selectPrevious->house_rant,
                'medical' =>$selectPrevious->medical,
                'transport' =>$selectPrevious->transport,
                'food' =>$selectPrevious->food,
                'month' =>date('Y-m'),
                'created_at' =>Carbon::now()->toDateTimeString(),
                'updated_at' =>Carbon::now()->toDateTimeString()
            ]);
        }
        $data=DB::table('payroll_grade')->WHERE('id',$id)->update([
            'grade_name' =>$request->grade_name,
            'basic_salary' =>$request->basic_amount,
            'house_rant' =>$request->house_amount,
            'transport' =>$request->transport_amount,
            'medical' =>$request->medical_amount,
            'food' =>$request->food_amount,
            'others' =>$request->others_amount,
            'total_salary' =>$request->basic_amount+$request->house_amount+$request->transport_amount+$request->medical_amount+$request->food_amount+$request->others_amount,
            'created_by' =>$request->user_name,
            'modified_by' =>$request->user_name,
            'created_at' =>Carbon::now()->toDateTimeString(),
            'updated_at' =>Carbon::now()->toDateTimeString(),
        ]);
            Session::flash('message', 'Grade Update Successful!');
            return redirect()->back();
    }
    //Payroll grade data delete method
    public function grade_delete($id){

        $grade_id=DB::table('payroll_salary')->where(['grade_id'=>$id])->get();
        if(count($grade_id)){
            Session::flash('failedMessage',"Deletion failed. There are already some employees under this grade." );
            return back();

        }else{

            $data=DB::table('payroll_grade')->WHERE('id',$id)->delete();
            if($data){
                Session::flash('message', 'Grade Delete Successful!');
                return redirect()->back();
            }
        }
    }
    
    //Payroll salary view page
    public function payroll_salary(){
        $data=DB::table('payroll_grade')->latest()->get();
        $data1=DB::table('payroll_grade')->where('id','=',session('gradeId'))->first();

        return view('payroll.salary',compact('data','data1'));
    }

    //Normal Bonus store multiple employee
    public function bonus_emloyee(Request $request){
        $bonus=$request->bonus_amount_employees;
        $emp_id=$request->emp_ids;
        $date=date('Y-m');
        $current_time = Carbon::now()->toDateTimeString();
        for($i=0;$i<count($request->bonus_amount_employees);$i++){
            $data=DB::table('employees_bonus')->insert([
                'emp_id' => $request->emp_ids[$i],
                'emp_bonus' => $bonus[$i],
                'date' => $date,
                'created_at' =>$current_time,
                'updated_at' =>$current_time,
            ]);
        }
            Session::flash('message', 'Bonus added Successful!');
            return redirect()->back();
    }
    //employee bonus settings view
    public function employee_bonus_view(){
        return view('payroll.bonus');
    }
    //increment bonus view page
    public function incrementBonusview(){
        $designation=DB::table('designations')->get();
        $department=DB::table('departments')->get();
        return view('payroll.empincrement',compact('designation','department'));
    }

    //increment bonus designation id wise employee
    public function incdesignationidwise($id){

    }

    //increment bonus designation all employee
    public function incdesignationall(){
      $alldesignation=DB::SELECT("SELECT employees.id,employees.empFirstName,employees.empLastName,employees.empJoiningDate,designations.designation,payroll_salary.total_employee_salary as gross_salary,employees.probation_period,NOW() as current_month,TIMESTAMPDIFF(MONTH,empJoiningDate,NOW())+1 as month_duration FROM employees
      LEFT JOIN payroll_salary ON payroll_salary.emp_id = employees.id
      LEFT JOIN  designations ON employees.empDesignationId=designations.id 
      WHERE TIMESTAMPDIFF(MONTH,empJoiningDate,NOW())+1 >employees.probation_period");
      return response()->json($alldesignation);
    }

    //employee wise gross show
    public function empGrossShow($id){
        $data=DB::SELECT("SELECT * FROM `payroll_salary` WHERE emp_id=$id");
        return response()->json($data);
    }
    //attendance bonus view page
    public function attendanceBonuspage(){
        $designation=DB::table('designations')->get();
        $department=DB::table('departments')->get();
        return view('payroll.empattendance',compact('designation','department'));
    }
    //festival bonus view page
    public function FestivalBonuspage(){
        $work_groups=DB::table('employees')
        ->select('work_group')
        ->groupBy('work_group')
        ->get(); 
        $fest_dates=DB::table('festival_bonus_settings')
        ->groupBy('fest_start')
        ->get();
        return view('payroll.empfestival',compact('work_groups','fest_dates'));
    }

    //designation wise employee bonus store for festival bonus
    public function designationBonusStore(Request $request){
        $i=0;
        foreach($request->emp_id as $key=>$value)
        {
            $empid = $value;
                $empgross =  $_POST['total_salary'][$key];
                $emp_total_percent=$_POST['percent'][$key];
                $emp_total_amount=$_POST['amount'][$key];
                $amount = $_POST['amount'][$key];
                $totals_salary=$_POST['total_salary'][$key];
            $emp_bonus = (int)$totals_salary/100*(int)$emp_total_percent;
            $insert[]=[
                'emp_id' =>$empid,
                'emp_gross' =>$empgross,
                'emp_bonus' =>$emp_bonus,
                'emp_amount' =>$amount,
                'emp_total_percent' =>$emp_total_percent,
                'emp_total_amount' =>$emp_total_amount,
                'bonus_given_id' =>auth()->user()->id,
                'bonus_title' =>$request->festival_title,
                'month' =>date('Y-m'),
                'created_at' =>Carbon::now()->toDateTimeString(),
                'updated_at' =>Carbon::now()->toDateTimeString(),
            ];
        }
        if($i%550==0){
            if(!empty($insert)){
                $insertData = DB::table('festival_bonus')->insert($insert);
                unset($insert);
            }
        }
        $i++;
        Session::flash('fbonus', 'Festival Bonus added successful!');
        return redirect()->back();
    }
    //attendance bonus store
    public function attdebonusstore(Request $request){
        $i=0;
        foreach($request->emp_id as $key=>$value)
        {
            $empid = $value;
            $empgross =  $_POST['total_salary'][$key];
                $emp_total_percent=$_POST['percent'][$key];
                $emp_total_amount=$_POST['amount'][$key];
                $amount = $_POST['amount'][$key];
            $emp_bonus = (int)$_POST['total_salary'][$key]/100*(int)$emp_total_percent;
            $insert[]=[
                'bonus_id' =>$empid,
                'emp_gross' =>$empgross,
                'bonus_amount' =>$amount,
                'bonus_percent' =>$emp_bonus,
                'emp_total_percent' =>$emp_total_percent,
                'emp_total_amount' =>$emp_total_amount,
               'bonus_given_id' =>auth()->user()->id,
                'month' =>date('Y-m'),
                'created_at' =>Carbon::now()->toDateTimeString(),
                'updated_at' =>Carbon::now()->toDateTimeString(),
            ];
        }
        if($i%550==0){
            if(!empty($insert)){
                $insertData = DB::table('attendance_bonus')->insert($insert);
                unset($insert);
            }
        }
        $i++;
        Session::flash('abonus', 'Attendance Bonus added successful!');
        return redirect()->back();
    }


    //increment or year employee multiple bonus store
    public function incBonusStore(Request $request){
        $i=0;
        foreach($request->emp_id as $key=>$value)
        {
            $empid = $value;

                $percent=$_POST['percent'][$key];
                $amount=$_POST['amount'][$key];
            $total_salary = $_POST['total_salary'][$key];
            $percent_bonus= (int)$_POST['total_salary'][$key]/100*(int)$percent;
            $insert[]=[
                'emp_id' =>$empid,
                'emp_gross' =>$_POST['total_salary'][$key],
                'emp_bonus' =>$percent_bonus,
                'emp_amount' =>$amount,
                'emp_total_percent' =>$percent,
                'emp_total_amount' =>$amount,
                'bonus_given_id' =>auth()->user()->id,
                'date' =>date('Y-m'),
                'created_at' =>Carbon::now()->toDateTimeString(),
                'updated_at' =>Carbon::now()->toDateTimeString(),
            ];
        }
        if($i%550==0){
            if(!empty($insert)){
                $insertData = DB::table('employees_bonus')->insert($insert);
                unset($insert);
            }
        }
        $i++;

      if($insertData){
          for($i=0;$i<count($request->emp_id);$i++){
              $id=$request->emp_id[$i];
                  $percent=$request->percent[$i];
                  $amount=$request->amount[$i];
              $total_salary=$request->total_salary[$i];
              $percent_wise=$total_salary/100*$percent;
              $amount_wise=$amount;
              $totals=$percent_wise+$amount_wise;
              $update=DB::table('payroll_salary')->where('emp_id',$id)->update([
                 'total_employee_salary' =>$totals+$total_salary
            ]);
          }
      }

        Session::flash('inbonus', 'Increment Bonus added successful!');
        return redirect()->back();

    }


    //Attendance Bonus store multiple employee
    public function AttendanceBonus(Request $request){
        $data=DB::table('attendance_bonus')->insert([
           'bonus_id' =>$request->emp_ids,
           'emp_gross' =>$request->gross,
           'bonus_amount' =>$request->amount,
           'bonus_percent' =>$request->gross/100*$request->percent,
           'emp_total_percent' =>$request->percent,
           'emp_total_amount' =>$request->amount,
           'bonus_given_id' =>auth::user()->id,
           'month' =>date('Y-m'),
           'created_at' =>Carbon::now()->toDateTimeString(),
           'updated_at' =>Carbon::now()->toDateTimeString(),
        ]);
        if($data){
            Session::flash('attbonus', 'Bonus Added successful');
            return redirect()->back();
        }
     }



    //payslip generate view
    public function payslipGenerate(){
        $department =DB::table('departments')->select('id','departmentName')->get();
        $section    =DB::table('employees')->select('empSection')->whereNotNull('empSection')->groupBy('empSection')->get();
        $employee   =DB::table('employees')->where('is_three_shift',0)->select('id','empFirstName','empLastName','employeeId')->get();
        return view('report.payroll.payslip.index',compact('department','section','employee'));
    }
    //payslip report
    public function payslipReportemployees(Request $request){

         if($request->type=='dept_wise_data'){
            if($request->payslip_option=='bangla'){
                $dt= $request->payslip_generate_report."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $data=DB::table('tb_salary_history')
                ->where('dates',$request->payslip_generate_report)
                ->where('department_id',$request->department_id)
                ->where(function ($query) use ($dt){
                    $query->where('date_of_discontinuation','=',null);
                    $query->orWhere('date_of_discontinuation','>=',$dt);
                })
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history.status',0)
                ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                ->select('tb_salary_history.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                ->orderBy('employees.employeeId','ASC')
                ->paginate(500);
                return view('report.payroll.payslip.payslip_bangla_pdf',compact('data'));
            }else{
                $dt= $request->payslip_generate_report."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $data=DB::table('tb_salary_history')
                ->where('dates',$request->payslip_generate_report)
                ->where('department_id',$request->department_id)
                ->where(function ($query) use ($dt){
                    $query->where('date_of_discontinuation','=',null);
                    $query->orWhere('date_of_discontinuation','>=',$dt);
                })
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history.status',0)
                    ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                    ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                    ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                    ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                    ->select('tb_salary_history.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                    ->orderBy('employees.employeeId','ASC')
                    ->paginate(500);
                return view('report.payroll.payslip.payslippdf',compact('data'));
            }
         }


         if($request->type=='section_wise_data'){
            $dt= $request->payslip_generate_report."-28";
            $dt= Carbon::parse($dt)->endOfMonth();
            $dt= Carbon::parse($dt)->toDateString();
            if($request->payslip_option=='bangla'){
                $data=DB::table('tb_salary_history')
                ->where('dates',$request->payslip_generate_report)
                ->where('section_id',$request->section_id)
                ->where(function ($query) use ($dt){
                    $query->where('date_of_discontinuation','=',null);
                    $query->orWhere('date_of_discontinuation','>=',$dt);
                })
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history.status',0)
                    ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                    ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                    ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                    ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                    ->select('tb_salary_history.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                    ->orderBy('employees.employeeId','ASC')
                    ->paginate(500);
                return view('report.payroll.payslip.payslip_bangla_pdf',compact('data'));
            }else{
                $dt= $request->payslip_generate_report."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $data=DB::table('tb_salary_history')
                ->where('dates',$request->payslip_generate_report)
                ->where('section_id',$request->section_id)
                ->where(function ($query) use ($dt){
                    $query->where('date_of_discontinuation','=',null);
                    $query->orWhere('date_of_discontinuation','>=',$dt);
                })
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history.status',0)
                    ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                    ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                    ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                    ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                    ->select('tb_salary_history.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                    ->orderBy('employees.employeeId','ASC')
                    ->paginate(500);
                return view('report.payroll.payslip.payslippdf',compact('data'));
            }
         }

         if($request->type=='emp_wise_data'){
            $dt= $request->payslip_generate_report."-28";
            $dt= Carbon::parse($dt)->endOfMonth();
            $dt= Carbon::parse($dt)->toDateString();
            if($request->payslip_option=='bangla'){
                $month=$request->payslip_generate_report;
                $data=DB::table('tb_salary_history')
                ->where('dates',$request->payslip_generate_report)
                ->where(function ($query) use ($dt){
                    $query->where('date_of_discontinuation','=',null);
                    $query->orWhere('date_of_discontinuation','>=',$dt);
                })
                ->where('tb_salary_history.emp_id','=',$request->emp_id)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history.status',0)
                    ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                    ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                    ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                    ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                    ->select('tb_salary_history.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                    ->orderBy('employees.employeeId','ASC')
                    ->paginate(500);
                return view('report.payroll.payslip.payslip_bangla_pdf',compact('data'));
            }else{
                $month=$request->payslip_generate_report;
                $dt= $request->payslip_generate_report."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $data=DB::table('tb_salary_history')
                ->where('dates',$request->payslip_generate_report)
                ->where(function ($query) use ($dt){
                    $query->where('date_of_discontinuation','=',null);
                    $query->orWhere('date_of_discontinuation','>',$dt);
                })
                ->where('tb_salary_history.emp_id','=',$request->emp_id)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history.status',0)
                    ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                    ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                    ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                    ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                    ->select('tb_salary_history.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                    ->orderBy('employees.employeeId','ASC')
                    ->paginate(500);
                return view('report.payroll.payslip.payslippdf',compact('data'));
            }

         }

         if($request->type=='month_wise_data'){
            if($request->payslip_option=='bangla'){
                $month=$request->payslip_generate_report;
                $dt= $request->payslip_generate_report."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                   $data=DB::table('tb_salary_history')
                        ->where('dates',$request->payslip_generate_report)
                       ->where(function ($query) use ($dt){
                           $query->where('date_of_discontinuation','=',null);
                           $query->orWhere('date_of_discontinuation','>',$dt);
                       })
                        ->where('employees.empJoiningDate','<=',$dt)
                        ->where('tb_salary_history.status',0)
                       ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                       ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                       ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                       ->leftjoin('departments','tb_salary_history.department_id','=','departments.id')
                       ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                       ->select('tb_salary_history.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                       ->orderBy('departments.id','ASC')
                       ->orderBy('employees.employeeId','ASC')
                       ->paginate(500);
                   return view('report.payroll.payslip.payslip_bangla_pdf',compact('data'));
               }else{
                $month=$request->payslip_generate_report;
                $dt= $request->payslip_generate_report."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                   $data=DB::table('tb_salary_history')
                        ->where('dates',$request->payslip_generate_report)
                       ->where(function ($query) use ($dt){
                           $query->where('date_of_discontinuation','=',null);
                           $query->orWhere('date_of_discontinuation','>',$dt);
                       })
                        ->where('employees.empJoiningDate','<=',$dt)
                        ->where('tb_salary_history.status',0)
                       ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                       ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                       ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                       ->leftjoin('departments','tb_salary_history.department_id','=','departments.id')
                       ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                       ->select('tb_salary_history.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                       ->orderBy('departments.id','ASC')
                       ->orderBy('employees.employeeId','ASC')
                       ->paginate(500);
                //    return count($data);
                   return view('report.payroll.payslip.payslippdf',compact('data'));
               }
            }


         }
        //production bonus view
       public function ProductionBonus(){
          return view('payroll.production');
       }
       //production bonus single employee wise store
       public function ProductionBonusStore(Request $request){
           $month=date('Y-m');
               $data=DB::table('tb_production_bonus')->insert([
                  'emp_id' =>$request->emp_id,
                  'production_amount' =>$request->production_amount,
                  'month' =>$month,
                  'created_at' =>Carbon::now()->toDateTimeString(),
                  'updated_at' =>Carbon::now()->toDateTimeString(),
               ]);
           Session::flash('productionamount', 'Production Bonus Added successful');
           return redirect()->back();
         }

         //production bonus workgroup wise view
         public function ProductionBonusWorkgroup(Request $request){
           $group=$request->work_group_employee;
           $data=DB::table('employees')
           ->leftjoin('payroll_salary','payroll_salary.emp_id','=','employees.id')
           ->select('employees.id','empFirstName','empLastName','employeeId','work_group','payroll_salary.total_employee_salary')
           ->where('work_group',$group)
           ->where('empAccStatus','=',1)
           ->groupBy('employees.id')
           ->get();
           return view('payroll.production_bonus_work_group',compact('data'));
         }

         //production bonus work group wise multiple store
         public function ProductionBonusWorkgroupStore(Request $request){
             if(isset($_POST['amountt_btn'])){
                 $emp=$request->emp_id;
                 $month=array(date('Y-m'));
                 $check=DB::table('tb_production_bonus')->whereIn('emp_id',$emp)->whereIn('month',$month)->count();
                 if($check>0){
                     $delete=DB::table('tb_production_bonus')->whereIn('emp_id',$emp)->whereIn('month',$month)->delete();
                     $amountt=0;
                     foreach($request->emp_id as $key=>$value)
                     {
                         $empid = $value;
                         $amount=$_POST['emp_amount'][$key];
                         $insert[]=[
                             'emp_id' =>$empid,
                             'production_amount' =>$amount,
                             'month' =>date('Y-m'),
                             'created_at' =>Carbon::now()->toDateTimeString(),
                             'updated_at' =>Carbon::now()->toDateTimeString(),
                         ];
                     }
                     if($amountt%550==0){
                         if(!empty($insert)){
                             $insertData = DB::table('tb_production_bonus')->insert($insert);
                             unset($insert);
                         }
                     }
                     $amountt++;
                 }
                 else{
                     $amountt=0;
                     foreach($request->emp_id as $key=>$value)
                     {
                         $empid = $value;
                         $amount=$_POST['emp_amount'][$key];
                         $insert[]=[
                             'emp_id' =>$empid,
                             'production_amount' =>$amount,
                             'month' =>date('Y-m'),
                             'created_at' =>Carbon::now()->toDateTimeString(),
                             'updated_at' =>Carbon::now()->toDateTimeString(),
                         ];
                     }
                     if($amountt%550==0){
                         if(!empty($insert)){
                             $insertData = DB::table('tb_production_bonus')->insert($insert);
                             unset($insert);
                         }
                     }
                     $amountt++;
                 }
             }
             if(isset($_POST['percentt_btn'])){
                 $emp=$request->emp_id;
                 $month=array(date('Y-m'));
                 $check=DB::table('tb_production_bonus')->whereIn('emp_id',$emp)->whereIn('month',$month)->count();
                 if($check>0){
                     $delete=DB::table('tb_production_bonus')->whereIn('emp_id',$emp)->whereIn('month',$month)->delete();
                     $amountt=0;
                     foreach($request->emp_id as $key=>$value)
                     {
                         $empid = $value;
                         $percent=$_POST['emp_percent'][$key];
                         $total_salary = $_POST['emp_gross'][$key];
                         $deduction_salary=$total_salary-1100;
                         $emp_bonus=$deduction_salary/100*$percent;
                         $insert[]=[
                             'emp_id' =>$empid,
                             'production_percent' =>$emp_bonus,
                             'production_percent_total' =>$percent,
                             'month' =>date('Y-m'),
                             'created_at' =>Carbon::now()->toDateTimeString(),
                             'updated_at' =>Carbon::now()->toDateTimeString(),
                         ];
                     }
                     if($amountt%550==0){
                         if(!empty($insert)){
                             $insertData = DB::table('tb_production_bonus')->insert($insert);
                             unset($insert);
                         }
                     }
                     $amountt++;
                 }
                 else{
                     $amountt=0;
                     foreach($request->emp_id as $key=>$value)
                     {
                         $empid = $value;
                         $percent=$_POST['emp_percent'][$key];
                         $total_salary = $_POST['emp_gross'][$key];
                         $deduction_salary=$total_salary-1100;
                         $emp_bonus=$deduction_salary/100*$percent;
                         $insert[]=[
                             'emp_id' =>$empid,
                             'production_percent' =>$emp_bonus,
                             'production_percent_total' =>$percent,
                             'month' =>date('Y-m'),
                             'created_at' =>Carbon::now()->toDateTimeString(),
                             'updated_at' =>Carbon::now()->toDateTimeString(),
                         ];
                     }
                     if($amountt%550==0){
                         if(!empty($insert)){
                             $insertData = DB::table('tb_production_bonus')->insert($insert);
                             unset($insert);
                         }
                     }
                     $amountt++;
                 }
             }
             Session::flash('production_bonus_work_group', 'Production Bonus Added successful');
             return redirect()->route('production.view');
         }


        //festival bonus work group wise
        public function fest_bonus_store(Request $request){
            $yearsss=date('Y',strtotime($request->fest_date));
            $monthsss=date('m',strtotime($request->fest_date));
            $group=$request->work_group_employee;
            $festival_condition=DB::table('festival_bonus_settings')
            ->where('work_group',$group)
            ->where('fest_start',$request->fest_date)
            ->first();
            if($festival_condition==''){
                Session::flash('error', 'Date Not Match for work group');
                return redirect()->route('festival.view');
            }
            DB::table('festival_bonus')
            ->where('work_group',$group)
            ->whereYear('fest_date',$yearsss)
            ->whereMonth('fest_date',$monthsss)
            ->delete();
            $data=DB::table('employees')
            ->where('employees.work_group',$group)
            ->where('employees.empAccStatus',1)
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('payroll_salary','payroll_salary.emp_id','=','employees.id')
            ->select('employees.id as em_id','empJoiningDate','work_group','payroll_salary.total_employee_salary','empDepartmentId as dept_id','empSection')
            ->get();
            $insert_data = []; 
            foreach($data as $item){
                $join_date = new DateTime($item->empJoiningDate);
                $festival_date = new DateTime($festival_condition->fest_start);
                $interval = $join_date->diff($festival_date);
                $year=$interval->y;
                $month=$interval->m;
                if($year>=$festival_condition->condition_year) {
                    $total_bonus=$item->total_employee_salary/100*$festival_condition->year_percent; 
                }
                elseif($month>=$festival_condition->condition_month){
                    $total_bonus=$item->total_employee_salary/100*$festival_condition->month_percent;  
                }
                else{
                    $total_bonus=0; 
                } 
                $insert=[
                    'work_group'=>$group,
                    'emp_id' =>$item->em_id,
                    'dept_id' =>$item->dept_id,
                    'section_id' =>$item->empSection,
                    'emp_gross' =>$item->total_employee_salary,
                    'emp_bonus' =>$total_bonus,
                    'bonus_given_id' =>auth()->user()->id,
                    'bonus_title' =>$festival_condition->fest_title,
                    'fest_date' =>$festival_condition->fest_start,
                    'month' =>date('Y-m-01',strtotime($festival_condition->fest_start)),
                    'created_at' =>Carbon::now()->toDateTimeString(),
                    'updated_at' =>Carbon::now()->toDateTimeString(),
                ];
                $insert_data[]=$insert;
            }
            $insert_data = collect($insert_data); 
            $chunks = $insert_data->chunk(3000);
            foreach ($chunks as $chunk)
            {
                \DB::table('festival_bonus')->insert($chunk->toArray());
            }
            Session::flash('festival_bonus_work_group', 'Festival Bonus Added successful');
            return redirect()->route('festival.view');
        }

        //production bonus report view
        public function ProductionBonusreport(){
            return view('report.payroll.bonus.production_bonus');
        }

        //production bonus report show
        public function ProductionBonusreportShow(Request $request){
            $month=$request->year_bonus_month;
            $group=$request->work_group;
            $month_name=DB::table('tb_production_bonus')
                ->leftjoin('employees','tb_production_bonus.emp_id','=','employees.id')
                ->select('tb_production_bonus.created_at')
                ->where('month',$month)
                ->where('employees.work_group',$group)
                ->first();
            $data=DB::table('tb_production_bonus')
            ->leftjoin('employees','tb_production_bonus.emp_id','=','employees.id')
            ->select('tb_production_bonus.*','employees.empFirstName','employees.empLastName','employees.employeeId','employees.work_group')
            ->where('month',$month)
            ->where('employees.work_group',$group)
            ->paginate(200);
            return view('report.payroll.bonus.production_bonus_show',compact('data','month_name'));
        }


        //festival bonus report show
        public function festivalreportworkgroup(Request $request){
            $title=$request->title_id;
            $p_mode=$request->p_mode;
            $pp_mode=$request->pp_mode;
            $count=DB::table('festival_bonus')
            ->where('bonus_title',$title)
            ->count();
            if($count>0){
                if($request->dept_id==0){
                    $bonus_title=DB::table('festival_bonus')
                    ->select('bonus_title','month')
                    ->where('bonus_title',$request->title_id)
                    ->first();
                    $deprtment=DB::table('departments')
                    ->select('departments.id','departmentName')
                    ->groupBy('departments.id')
                    ->get();    
                    if($request->dept_excel=='dept_excel'){
                        Excel::create("Bonus", function($excel) use ($bonus_title,$deprtment,$title,$p_mode,$pp_mode) {
                            $name=Auth::user()->name;
                            $excel->setTitle("Bonus Sheet");
                            $excel->setCreator($name);
                            $excel->setDescription('Bonus Sheet');
                            $excel->sheet('Sheet 1', function ($sheet) use ($bonus_title,$deprtment,$title,$p_mode,$pp_mode) {
                                $sheet->loadView('report.payroll.bonus.department_all_excel')
                                    ->with('bonus_title',$bonus_title)
                                    ->with('deprtment',$deprtment)
                                    ->with('title',$title)
                                    ->with('p_mode',$p_mode)
                                    ->with('pp_mode',$pp_mode);
                            });
                        })->download('xlsx');
                    } 
                  return view('report.payroll.bonus.festival_bonus_show_all_dept',compact('bonus_title','deprtment','title','p_mode','pp_mode'));
                }
                $bonus_title=DB::table('festival_bonus')
                ->select('bonus_title','month')
                ->where('dept_id',$request->dept_id)
                ->where('bonus_title',$request->title_id)
                ->first();
                $deprtment=DB::table('departments')
                ->where('id',$request->dept_id)
                ->select('departmentName')
                ->first();
                if($p_mode=='All'){
                    $data=DB::table('festival_bonus')
                    ->join('employees','festival_bonus.emp_id','=','employees.id')
                    ->leftjoin('designations','employees.empDesignationId','=','designations.id')
                    ->select('festival_bonus.*','employees.empFirstName','employees.empLastName','employees.employeeId','employees.work_group','designations.designation','empJoiningDate','payment_mode','bank_account')
                    ->where('dept_id',$request->dept_id)
                    ->where('bonus_title',$request->title_id)
                    ->where('emp_bonus','>',0)
                    ->orderBy('employeeId','ASC')
                    ->get();
                }else{
                    $data=DB::table('festival_bonus')
                    ->join('employees','festival_bonus.emp_id','=','employees.id')
                    ->leftjoin('designations','employees.empDesignationId','=','designations.id')
                    ->select('festival_bonus.*','employees.empFirstName','employees.empLastName','employees.employeeId','employees.work_group','designations.designation','empJoiningDate','payment_mode','bank_account')
                    ->where('dept_id',$request->dept_id)
                    ->where('bonus_title',$request->title_id)
                    ->where('payment_mode',$p_mode)
                    ->where('emp_bonus','>',0)
                    ->orderBy('employeeId','ASC')
                    ->get();
                }
             
                if($request->dept_excel=='dept_excel'){
                    Excel::create("Bonus", function($excel) use ($data,$bonus_title,$deprtment,$p_mode,$pp_mode) {
                        $name=Auth::user()->name;
                        $excel->setTitle("Bonus Sheet");
                        $excel->setCreator($name);
                        $excel->setDescription('Bonus Sheet');
                        $excel->sheet('Sheet 1', function ($sheet) use ($data,$bonus_title,$deprtment,$p_mode,$pp_mode) {
                            $sheet->loadView('report.payroll.bonus.department_single_excel')
                                ->with('data',$data)
                                ->with('bonus_title',$bonus_title)
                                ->with('deprtment',$deprtment)
                                ->with('pp_mode',$pp_mode)
                                ->with('p_mode',$p_mode);
                        });
                    })->download('xlsx');
                }
                return view('report.payroll.bonus.festival_bonus_show',compact('data','bonus_title','deprtment','p_mode','pp_mode'));
            }else{
                return back();
            }
        }
        public function festivalreportworkgroupsection(Request $request){
            $title=$request->title_id;
            $p_mode=$request->p_mode;
            $pp_mode=$request->pp_mode;
            $count=DB::table('festival_bonus')
            ->where('bonus_title',$title)
            ->count();
            if($count>0){
                if($request->sections_id=='als'){
                    $bonus_title=DB::table('festival_bonus')
                    ->select('bonus_title','month')
                    ->where('bonus_title',$request->title_id)
                    ->first();
                    $section=DB::table('festival_bonus')
                    ->select('section_id')
                    ->where('bonus_title',$request->title_id)
                    ->groupBy('section_id')
                    ->get();     
                    if($request->section_excel=='section_excel'){
                        Excel::create("Bonus", function($excel) use ($bonus_title,$section,$title,$p_mode,$pp_mode) {
                            $name=Auth::user()->name;
                            $excel->setTitle("Bonus Sheet");
                            $excel->setCreator($name);
                            $excel->setDescription('Bonus Sheet');
                            $excel->sheet('Sheet 1', function ($sheet) use ($bonus_title,$section,$title,$p_mode,$pp_mode) {
                                $sheet->loadView('report.payroll.bonus.section_all_excel')
                                    ->with('bonus_title',$bonus_title)
                                    ->with('section',$section)
                                    ->with('p_mode',$p_mode)
                                    ->with('pp_mode',$pp_mode)
                                    ->with('title',$title);
                            });
                        })->download('xlsx');
                    } 
                  return view('report.payroll.bonus.festival_bonus_show_all_section',compact('bonus_title','section','title','p_mode','pp_mode'));
            }else{
                $bonus_title=DB::table('festival_bonus')
                ->select('bonus_title','month')
                ->where('section_id',$request->sections_id)
                ->where('bonus_title',$request->title_id)
                ->first();
                $section=DB::table('festival_bonus')
                ->where('section_id',$request->sections_id)
                ->where('bonus_title',$request->title_id)
                ->select('section_id')
                ->first();
                if($p_mode=='All'){
                    $data=DB::table('festival_bonus')
                    ->join('employees','festival_bonus.emp_id','=','employees.id')
                    ->leftjoin('designations','employees.empDesignationId','=','designations.id')
                    ->select('festival_bonus.*','employees.empFirstName','employees.empLastName','employees.employeeId','employees.work_group','designations.designation','empJoiningDate','payment_mode','bank_account')
                    ->where('section_id',$request->sections_id)
                    ->where('bonus_title',$request->title_id)
                    ->where('emp_bonus','>',0)
                    ->orderBy('employeeId','ASC')
                    ->get();
                }else{
                    $data=DB::table('festival_bonus')
                    ->join('employees','festival_bonus.emp_id','=','employees.id')
                    ->leftjoin('designations','employees.empDesignationId','=','designations.id')
                    ->select('festival_bonus.*','employees.empFirstName','employees.empLastName','employees.employeeId','employees.work_group','designations.designation','empJoiningDate','payment_mode','bank_account')
                    ->where('section_id',$request->sections_id)
                    ->where('bonus_title',$request->title_id)
                    ->where('emp_bonus','>',0)
                    ->where('payment_mode','=',$p_mode)
                    ->orderBy('employeeId','ASC')
                    ->get();
                }
        
                    if($request->section_excel=='section_excel'){
                        Excel::create("Bonus", function($excel) use ($data,$bonus_title,$section,$pp_mode) {
                            $name=Auth::user()->name;
                            $excel->setTitle("Bonus Sheet");
                            $excel->setCreator($name);
                            $excel->setDescription('Bonus Sheet');
                            $excel->sheet('Sheet 1', function ($sheet) use ($data,$bonus_title,$section,$pp_mode) {
                                $sheet->loadView('report.payroll.bonus.section_single_excel')
                                    ->with('data',$data)
                                    ->with('bonus_title',$bonus_title)
                                    ->with('pp_mode',$pp_mode)
                                    ->with('section',$section);
                            });
                        })->download('xlsx');
                    } 
                    return view('report.payroll.bonus.festival_bonus_show_section',compact('data','bonus_title','section','pp_mode'));
               }
            }else{
                return back();
            }
        }

        //festival bonus report show
        public function festivalreportworkgroupsummary(Request $request){
            if($request->dept_id_summary=='dept_all'){
                $bonus_title=DB::table('festival_bonus')
                ->select('bonus_title','month')
                ->where('bonus_title',$request->title_id)
                ->groupBy('dept_id')
                ->first();
                $data=DB::SELECT("SELECT COUNT(emp_id) as man_power,departments.departmentName,SUM(emp_bonus) as total,SUM(emp_gross) as tot_gross 
                        FROM festival_bonus
                        LEFT JOIN departments ON festival_bonus.dept_id=departments.id
                        WHERE emp_bonus>0 AND bonus_title='$request->title_id'
                        GROUP BY dept_id");
              }
            else{
                $bonus_title=DB::table('festival_bonus')
                ->select('bonus_title','month')
                ->where('dept_id',$request->dept_id_summary)
                ->where('bonus_title',$request->title_id)
                ->first();
                $data=DB::SELECT("SELECT COUNT(emp_id) as man_power,departments.departmentName,SUM(emp_bonus) as total,SUM(emp_gross) as tot_gross 
                        FROM festival_bonus
                        LEFT JOIN departments ON festival_bonus.dept_id=departments.id
                        WHERE emp_bonus>0 AND dept_id='$request->dept_id_summary' AND bonus_title='$request->title_id'
                        GROUP BY dept_id");
            }
            if($request->dept_excel_summary=='dept_excel_summary'){
                Excel::create("Bonus", function($excel) use ($data,$bonus_title) {
                    $name=Auth::user()->name;
                    $excel->setTitle("Bonus Sheet");
                    $excel->setCreator($name);
                    $excel->setDescription('Bonus Sheet');
                    $excel->sheet('Sheet 1', function ($sheet) use ($data,$bonus_title) {
                        $sheet->loadView('report.payroll.bonus.summary_department_excel')
                            ->with('data',$data)
                            ->with('bonus_title',$bonus_title);
                    });
                })->download('xlsx');
            }
            return view('report.payroll.bonus.festival_bonus_show_summary',compact('data','bonus_title'));
        }



        public function festivalreportworkgroupsectionsummary(Request $request){
            if($request->section_id_summary=='section_all'){
                $bonus_title=DB::table('festival_bonus')
                ->select('bonus_title','month')
                ->where('bonus_title',$request->title_id)
                ->groupBy('section_id')
                ->first();
                $data=DB::SELECT("SELECT COUNT(emp_id) as man_power,section_id,SUM(emp_bonus) as total,SUM(emp_gross) as tot_gross 
                        FROM festival_bonus
                        LEFT JOIN departments ON festival_bonus.dept_id=departments.id
                        WHERE emp_bonus>0 AND bonus_title='$request->title_id'
                        GROUP BY section_id");
              }
            else{
                $bonus_title=DB::table('festival_bonus')
                ->select('bonus_title','month')
                ->where('section_id',$request->section_id_summary)
                ->where('bonus_title',$request->title_id)
                ->first();
                $data=DB::SELECT("SELECT COUNT(emp_id) as man_power,section_id,SUM(emp_bonus) as total,SUM(emp_gross) as tot_gross 
                        FROM festival_bonus
                        LEFT JOIN departments ON festival_bonus.dept_id=departments.id
                        WHERE emp_bonus>0 AND section_id='$request->section_id_summary' AND bonus_title='$request->title_id'
                        GROUP BY section_id");
            }
            if($request->section_excel_summary=='section_excel_summary'){
                Excel::create("Bonus", function($excel) use ($data,$bonus_title) {
                    $name=Auth::user()->name;
                    $excel->setTitle("Bonus Sheet");
                    $excel->setCreator($name);
                    $excel->setDescription('Bonus Sheet');
                    $excel->sheet('Sheet 1', function ($sheet) use ($data,$bonus_title) {
                        $sheet->loadView('report.payroll.bonus.summary_section_excel')
                            ->with('data',$data)
                            ->with('bonus_title',$bonus_title);
                    });
                })->download('xlsx');
            }
            return view('report.payroll.bonus.festival_bonus_show_summary_section',compact('data','bonus_title'));
        }


        //payslip resigned employee
        public function payslipGenerateResignedEmployee(){
            $department=DB::table('employees')
            ->where('employees.date_of_discontinuation','!=',null)      
            ->where('employees.discon_type','=',2)
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->select('departments.id as dept_id','departments.departmentName')
            ->groupBy('employees.empDepartmentId')
            ->get();

            $section=DB::table('employees')
            ->select('empSection')
            ->where('employees.date_of_discontinuation','!=',null)      
            ->where('employees.discon_type','=',2)
            ->whereNotNull('empSection')
            ->groupBy('empSection')
            ->get();

           $employee=DB::table('employees')
           ->where('employees.date_of_discontinuation','!=',null)      
           ->where('employees.discon_type','=',2)
           ->select('id','empFirstName','empLastName','employeeId')
           ->get();
            return view('report.payroll.payslip.resigned_employee',compact('department','section','employee'));
        }

        //payslip resigned employee show
        public function payslipGenerateResignedEmployeeshow(Request $request){
            if($request->type=='dept_wise_data'){
                if($request->payslip_option=='bangla'){
                    $dt= $request->payslip_generate_report."-28";
                    $dt= Carbon::parse($dt)->endOfMonth();
                    $dt= Carbon::parse($dt)->toDateString();
                    $ft= $request->payslip_generate_report."-28";
                    $ft= Carbon::parse($ft)->firstOfMonth();
                    $ft= Carbon::parse($ft)->toDateString();
                    $data=DB::table('tb_salary_history')
                    ->where('dates',$request->payslip_generate_report)
                    ->where('department_id',$request->department_id)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.date_of_discontinuation','<=',$dt)
                    ->where('employees.date_of_discontinuation','>=',$ft)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.empJoiningDate','<=',$dt)
                    ->where('employees.discon_type','=',2)
                    ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                    ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                    ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                    ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                    ->select('tb_salary_history.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                    ->orderBy('employees.employeeId','ASC')
                    ->paginate(500);
                    return view('report.payroll.payslip.payslip_bangla_pdf',compact('data'));
                }else{
                    $dt= $request->payslip_generate_report."-28";
                    $dt= Carbon::parse($dt)->endOfMonth();
                    $dt= Carbon::parse($dt)->toDateString();
                    $ft= $request->payslip_generate_report."-28";
                    $ft= Carbon::parse($ft)->firstOfMonth();
                    $ft= Carbon::parse($ft)->toDateString();
                    $data=DB::table('tb_salary_history')
                    ->where('dates',$request->payslip_generate_report)
                    ->where('department_id',$request->department_id)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.date_of_discontinuation','<=',$dt)
                    ->where('employees.date_of_discontinuation','>=',$ft)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.empJoiningDate','<=',$dt)
                    ->where('employees.discon_type','=',2)
                        ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                        ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                        ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                        ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                        ->select('tb_salary_history.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                        ->orderBy('employees.employeeId','ASC')
                        ->paginate(500);
                    return view('report.payroll.payslip.payslippdf',compact('data'));
                }
             }
    
    
             if($request->type=='section_wise_data'){
                $dt= $request->payslip_generate_report."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $ft= $request->payslip_generate_report."-28";
                $ft= Carbon::parse($ft)->firstOfMonth();
                $ft= Carbon::parse($ft)->toDateString();
                if($request->payslip_option=='bangla'){
                    $data=DB::table('tb_salary_history')
                    ->where('dates',$request->payslip_generate_report)
                    ->where('section_id',$request->section_id)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.date_of_discontinuation','<=',$dt)
                    ->where('employees.date_of_discontinuation','>=',$ft)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.empJoiningDate','<=',$dt)
                    ->where('employees.discon_type','=',2)
                        ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                        ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                        ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                        ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                        ->select('tb_salary_history.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                        ->orderBy('employees.employeeId','ASC')
                        ->paginate(500);
                    return view('report.payroll.payslip.payslip_bangla_pdf',compact('data'));
                }else{
                    $dt= $request->payslip_generate_report."-28";
                    $dt= Carbon::parse($dt)->endOfMonth();
                    $dt= Carbon::parse($dt)->toDateString();
                    $ft= $request->payslip_generate_report."-28";
                    $ft= Carbon::parse($ft)->firstOfMonth();
                    $ft= Carbon::parse($ft)->toDateString();
                    $data=DB::table('tb_salary_history')
                    ->where('dates',$request->payslip_generate_report)
                    ->where('section_id',$request->section_id)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.date_of_discontinuation','<=',$dt)
                    ->where('employees.date_of_discontinuation','>=',$ft)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.empJoiningDate','<=',$dt)
                    ->where('employees.discon_type','=',2)
                        ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                        ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                        ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                        ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                        ->select('tb_salary_history.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                        ->orderBy('employees.employeeId','ASC')
                        ->paginate(500);
                    return view('report.payroll.payslip.payslippdf',compact('data'));
                }
             }
    
             if($request->type=='emp_wise_data'){
                $dt= $request->payslip_generate_report."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $ft= $request->payslip_generate_report."-28";
                $ft= Carbon::parse($ft)->firstOfMonth();
                $ft= Carbon::parse($ft)->toDateString();
                if($request->payslip_option=='bangla'){
                    $month=$request->payslip_generate_report;
                    $data=DB::table('tb_salary_history')
                    ->where('dates',$request->payslip_generate_report)
                    ->where('tb_salary_history.emp_id','=',$request->emp_id)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.date_of_discontinuation','<=',$dt)
                    ->where('employees.date_of_discontinuation','>=',$ft)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.empJoiningDate','<=',$dt)
                    ->where('employees.discon_type','=',2)
                        ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                        ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                        ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                        ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                        ->select('tb_salary_history.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                        ->orderBy('employees.employeeId','ASC')
                        ->paginate(500);
                    return view('report.payroll.payslip.payslip_bangla_pdf',compact('data'));
                }else{
                    $dt= $request->payslip_generate_report."-28";
                    $dt= Carbon::parse($dt)->endOfMonth();
                    $dt= Carbon::parse($dt)->toDateString();
                    $ft= $request->payslip_generate_report."-28";
                    $ft= Carbon::parse($ft)->firstOfMonth();
                    $ft= Carbon::parse($ft)->toDateString();
                    $data=DB::table('tb_salary_history')
                    ->where('dates',$request->payslip_generate_report)
                    ->where('tb_salary_history.emp_id','=',$request->emp_id)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.date_of_discontinuation','<=',$dt)
                    ->where('employees.date_of_discontinuation','>=',$ft)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.empJoiningDate','<=',$dt)
                    ->where('employees.discon_type','=',2)
                        ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                        ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                        ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                        ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                        ->select('tb_salary_history.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                        ->orderBy('employees.employeeId','ASC')
                        ->paginate(500);
                    return view('report.payroll.payslip.payslippdf',compact('data'));
                }
    
             }
    
             if($request->type=='month_wise_data'){
                if($request->payslip_option=='bangla'){
                    $dt= $request->payslip_generate_report."-28";
                    $dt= Carbon::parse($dt)->endOfMonth();
                    $dt= Carbon::parse($dt)->toDateString();
                    $ft= $request->payslip_generate_report."-28";
                    $ft= Carbon::parse($ft)->firstOfMonth();
                    $ft= Carbon::parse($ft)->toDateString();
                       $data=DB::table('tb_salary_history')
                            ->where('dates',$request->payslip_generate_report)
                            ->where('employees.date_of_discontinuation','!=',null)
                            ->where('employees.date_of_discontinuation','<=',$dt)
                            ->where('employees.date_of_discontinuation','>=',$ft)
                            ->where('employees.date_of_discontinuation','!=',null)
                            ->where('employees.empJoiningDate','<=',$dt)
                            ->where('employees.discon_type','=',2)
                           ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                           ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                           ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                           ->leftjoin('departments','tb_salary_history.department_id','=','departments.id')
                           ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                           ->select('tb_salary_history.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                           ->orderBy('departments.id','ASC')
                           ->orderBy('employees.employeeId','ASC')
                           ->paginate(500);
                       return view('report.payroll.payslip.payslip_bangla_pdf',compact('data'));
                   }else{
                    $dt= $request->payslip_generate_report."-28";
                    $dt= Carbon::parse($dt)->endOfMonth();
                    $dt= Carbon::parse($dt)->toDateString();
                    $ft= $request->payslip_generate_report."-28";
                    $ft= Carbon::parse($ft)->firstOfMonth();
                    $ft= Carbon::parse($ft)->toDateString();
                       $data=DB::table('tb_salary_history')
                            ->where('dates',$request->payslip_generate_report)
                            ->where('employees.date_of_discontinuation','!=',null)
                            ->where('employees.date_of_discontinuation','<=',$dt)
                            ->where('employees.date_of_discontinuation','>=',$ft)
                            ->where('employees.date_of_discontinuation','!=',null)
                            ->where('employees.empJoiningDate','<=',$dt)
                            ->where('employees.discon_type','=',2)
                           ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                           ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                           ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                           ->leftjoin('departments','tb_salary_history.department_id','=','departments.id')
                           ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                           ->select('tb_salary_history.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                           ->orderBy('departments.id','ASC')
                           ->orderBy('employees.employeeId','ASC')
                           ->paginate(500);
                    //    return count($data);
                       return view('report.payroll.payslip.payslippdf',compact('data'));
                   }
                }
        }


        //payslip lefty employee
        public function payslipGenerateLeftyEmployee(){
            $department=DB::table('employees')
            ->where('employees.date_of_discontinuation','!=',null)      
            ->where('employees.discon_type','=',1)
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->select('departments.id as dept_id','departments.departmentName')
            ->groupBy('employees.empDepartmentId')
            ->get();

            $section=DB::table('employees')
            ->select('empSection')
            ->where('employees.date_of_discontinuation','!=',null)      
            ->where('employees.discon_type','=',1)
            ->whereNotNull('empSection')
            ->groupBy('empSection')
            ->get();

           $employee=DB::table('employees')
           ->where('employees.date_of_discontinuation','!=',null)      
           ->where('employees.discon_type','=',1)
           ->select('id','empFirstName','empLastName','employeeId')
           ->get();
            return view('report.payroll.payslip.lefty_employee',compact('department','section','employee'));
        }


        //lefty employee payslip show
        public function payslipGenerateLeftyEmployeeshow(Request $request){
            if($request->type=='dept_wise_data'){
                if($request->payslip_option=='bangla'){
                    $dt= $request->payslip_generate_report."-28";
                    $dt= Carbon::parse($dt)->endOfMonth();
                    $dt= Carbon::parse($dt)->toDateString();
                    $ft= $request->payslip_generate_report."-28";
                    $ft= Carbon::parse($ft)->firstOfMonth();
                    $ft= Carbon::parse($ft)->toDateString();
                    $data=DB::table('tb_salary_history')
                    ->where('dates',$request->payslip_generate_report)
                    ->where('department_id',$request->department_id)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.date_of_discontinuation','<=',$dt)
                    ->where('employees.date_of_discontinuation','>=',$ft)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.empJoiningDate','<=',$dt)
                    ->where('employees.discon_type','=',1)
                    ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                    ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                    ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                    ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                    ->select('tb_salary_history.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                    ->orderBy('employees.employeeId','ASC')
                    ->paginate(500);
                    return view('report.payroll.payslip.payslip_bangla_pdf',compact('data'));
                }else{
                    $dt= $request->payslip_generate_report."-28";
                    $dt= Carbon::parse($dt)->endOfMonth();
                    $dt= Carbon::parse($dt)->toDateString();
                    $ft= $request->payslip_generate_report."-28";
                    $ft= Carbon::parse($ft)->firstOfMonth();
                    $ft= Carbon::parse($ft)->toDateString();
                    $data=DB::table('tb_salary_history')
                    ->where('dates',$request->payslip_generate_report)
                    ->where('department_id',$request->department_id)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.date_of_discontinuation','<=',$dt)
                    ->where('employees.date_of_discontinuation','>=',$ft)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.empJoiningDate','<=',$dt)
                    ->where('employees.discon_type','=',1)
                        ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                        ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                        ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                        ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                        ->select('tb_salary_history.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                        ->orderBy('employees.employeeId','ASC')
                        ->paginate(500);
                    return view('report.payroll.payslip.payslippdf',compact('data'));
                }
             }
    
    
             if($request->type=='section_wise_data'){
                $dt= $request->payslip_generate_report."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $ft= $request->payslip_generate_report."-28";
                $ft= Carbon::parse($ft)->firstOfMonth();
                $ft= Carbon::parse($ft)->toDateString();
                if($request->payslip_option=='bangla'){
                    $data=DB::table('tb_salary_history')
                    ->where('dates',$request->payslip_generate_report)
                    ->where('section_id',$request->section_id)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.date_of_discontinuation','<=',$dt)
                    ->where('employees.date_of_discontinuation','>=',$ft)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.empJoiningDate','<=',$dt)
                    ->where('employees.discon_type','=',1)
                        ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                        ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                        ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                        ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                        ->select('tb_salary_history.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                        ->orderBy('employees.employeeId','ASC')
                        ->paginate(500);
                    return view('report.payroll.payslip.payslip_bangla_pdf',compact('data'));
                }else{
                    $dt= $request->payslip_generate_report."-28";
                    $dt= Carbon::parse($dt)->endOfMonth();
                    $dt= Carbon::parse($dt)->toDateString();
                    $ft= $request->payslip_generate_report."-28";
                    $ft= Carbon::parse($ft)->firstOfMonth();
                    $ft= Carbon::parse($ft)->toDateString();
                    $data=DB::table('tb_salary_history')
                    ->where('dates',$request->payslip_generate_report)
                    ->where('section_id',$request->section_id)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.date_of_discontinuation','<=',$dt)
                    ->where('employees.date_of_discontinuation','>=',$ft)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.empJoiningDate','<=',$dt)
                    ->where('employees.discon_type','=',1)
                        ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                        ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                        ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                        ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                        ->select('tb_salary_history.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                        ->orderBy('employees.employeeId','ASC')
                        ->paginate(500);
                    return view('report.payroll.payslip.payslippdf',compact('data'));
                }
             }
    
             if($request->type=='emp_wise_data'){
                $dt= $request->payslip_generate_report."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $ft= $request->payslip_generate_report."-28";
                $ft= Carbon::parse($ft)->firstOfMonth();
                $ft= Carbon::parse($ft)->toDateString();
                if($request->payslip_option=='bangla'){
                    $month=$request->payslip_generate_report;
                    $data=DB::table('tb_salary_history')
                    ->where('dates',$request->payslip_generate_report)
                    ->where('tb_salary_history.emp_id','=',$request->emp_id)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.date_of_discontinuation','<=',$dt)
                    ->where('employees.date_of_discontinuation','>=',$ft)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.empJoiningDate','<=',$dt)
                    ->where('employees.discon_type','=',1)
                        ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                        ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                        ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                        ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                        ->select('tb_salary_history.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                        ->orderBy('employees.employeeId','ASC')
                        ->paginate(500);
                    return view('report.payroll.payslip.payslip_bangla_pdf',compact('data'));
                }else{
                    $dt= $request->payslip_generate_report."-28";
                    $dt= Carbon::parse($dt)->endOfMonth();
                    $dt= Carbon::parse($dt)->toDateString();
                    $ft= $request->payslip_generate_report."-28";
                    $ft= Carbon::parse($ft)->firstOfMonth();
                    $ft= Carbon::parse($ft)->toDateString();
                    $data=DB::table('tb_salary_history')
                    ->where('dates',$request->payslip_generate_report)
                    ->where('tb_salary_history.emp_id','=',$request->emp_id)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.date_of_discontinuation','<=',$dt)
                    ->where('employees.date_of_discontinuation','>=',$ft)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.empJoiningDate','<=',$dt)
                    ->where('employees.discon_type','=',1)
                        ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                        ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                        ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                        ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                        ->select('tb_salary_history.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                        ->orderBy('employees.employeeId','ASC')
                        ->paginate(500);
                    return view('report.payroll.payslip.payslippdf',compact('data'));
                }
    
             }
    
             if($request->type=='month_wise_data'){
                if($request->payslip_option=='bangla'){
                    $dt= $request->payslip_generate_report."-28";
                    $dt= Carbon::parse($dt)->endOfMonth();
                    $dt= Carbon::parse($dt)->toDateString();
                    $ft= $request->payslip_generate_report."-28";
                    $ft= Carbon::parse($ft)->firstOfMonth();
                    $ft= Carbon::parse($ft)->toDateString();
                       $data=DB::table('tb_salary_history')
                            ->where('dates',$request->payslip_generate_report)
                            ->where('employees.date_of_discontinuation','!=',null)
                            ->where('employees.date_of_discontinuation','<=',$dt)
                            ->where('employees.date_of_discontinuation','>=',$ft)
                            ->where('employees.date_of_discontinuation','!=',null)
                            ->where('employees.empJoiningDate','<=',$dt)
                            ->where('employees.discon_type','=',1)
                           ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                           ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                           ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                           ->leftjoin('departments','tb_salary_history.department_id','=','departments.id')
                           ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                           ->select('tb_salary_history.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                           ->orderBy('departments.id','ASC')
                           ->orderBy('employees.employeeId','ASC')
                           ->paginate(500);
                       return view('report.payroll.payslip.payslip_bangla_pdf',compact('data'));
                   }else{
                    $dt= $request->payslip_generate_report."-28";
                    $dt= Carbon::parse($dt)->endOfMonth();
                    $dt= Carbon::parse($dt)->toDateString();
                    $ft= $request->payslip_generate_report."-28";
                    $ft= Carbon::parse($ft)->firstOfMonth();
                    $ft= Carbon::parse($ft)->toDateString();
                       $data=DB::table('tb_salary_history')
                            ->where('dates',$request->payslip_generate_report)
                            ->where('employees.date_of_discontinuation','!=',null)
                            ->where('employees.date_of_discontinuation','<=',$dt)
                            ->where('employees.date_of_discontinuation','>=',$ft)
                            ->where('employees.date_of_discontinuation','!=',null)
                            ->where('employees.empJoiningDate','<=',$dt)
                            ->where('employees.discon_type','=',1)
                           ->leftJoin('employees','tb_salary_history.emp_id', '=', 'employees.id')
                           ->leftJoin('payroll_grade','tb_salary_history.grade_id', '=', 'payroll_grade.id')
                           ->leftJoin('designations','tb_salary_history.designation_id', '=', 'designations.id')
                           ->leftjoin('departments','tb_salary_history.department_id','=','departments.id')
                           ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                           ->select('tb_salary_history.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                           ->orderBy('departments.id','ASC')
                           ->orderBy('employees.employeeId','ASC')
                           ->paginate(500);
                    //    return count($data);
                       return view('report.payroll.payslip.payslippdf',compact('data'));
                   }
                }
          }

}

