<?php

namespace App\Http\Controllers;

use App\Helper\AppHelper;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PHPUnit\Framework\Constraint\Count;
use Redirect;
use Session;
use PDF;
use mPDF;
use Carbon\Carbon;
use Excel;
use stdClass;


class ReportController extends Controller
{
    //Start Employee Reports Area 
    public function employeeReportDashboard(){
        return view('report.employee.index');
    }

    public function search_employee(){
        $employees = DB::table('employees')
            ->select(DB::raw("CONCAT(empFirstName,' ',empLastName,' , ID : ',employeeId) AS full_name, id"))
            ->pluck('full_name','id')->all();
//         return $employees;
        return view('report.employee.employee_search',compact('employees'));
    }

    public function showEmployee(Request $request){
        return redirect(route('employee.show',$request->empId));
    }

    public function employeeListReport(){
        $units=DB::table('units')->distinct()->groupBy('name')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $skill_level=DB::table('employees')->whereNotNull('skill_level')->distinct()->get(['skill_level']);
        $designations=DB::table('designations')->get(['designation','id']);
        return view('report.employee.employee_list',compact('units','departments','floors','sections','designations','skill_level'));
    }

    public function floor_line($id)
    {

        $lines=DB::table('tblines')->where('floor_id','=',$id)->pluck('line_no','id')->all();
        return view('report.floor_line',compact('lines'));
    }

    public function viewEmployeeListReport(Request $request){

        $query=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName, payroll_salary.basic_salary, payroll_salary.house_rant, payroll_salary.medical, payroll_salary.transport, payroll_salary.food, payroll_salary.other, payroll_salary.total_employee_salaryFROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id LEFT JOIN payroll_salary ON payroll_salary.emp_id=employees.id WHERE employees.id>0 ";

        if (($request->unitId)!=0) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (($request->reportType)!="0") {
            $query=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName, attendance_setup.max_entry_time AS max_entry_time, attendance_setup.shiftName AS shiftName , payroll_salary.basic_salary, payroll_salary.house_rant, payroll_salary.medical, payroll_salary.transport, payroll_salary.food, payroll_salary.other, payroll_salary.total_employee_salary, tbattendance_bonus.bTitle FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id  LEFT JOIN tbattendance_bonus ON employees.empAttBonusId=tbattendance_bonus.id LEFT JOIN payroll_salary ON payroll_salary.emp_id=employees.id WHERE employees.id>0 ";

            if (($request->unitId)!=0) {
                $query.=" AND employees.unit_id='".$request->unitId."'";
            }

            if (($request->floorId)!=0) {
                $query.=" AND employees.floor_id='".$request->floorId."'";
            }

            if (($request->line_id)!=0) {
                $query.=" AND employees.line_id='".$request->line_id."'";
            }

            if (!empty($request->sectionName)) {
                $query.=" AND employees.empSection='".$request->sectionName."'";
            }

            if (($request->departmentId)!=0) {
                $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
            }

            if (($request->designationId)!=0) {
                $query.=" AND employees.empDesignationId='".$request->designationId."'";
            }

            if (($request->empGenderId)!=0) {
                $query.=" AND employees.empGenderId='".$request->empGenderId."'";
            }

            if (!empty($request->skill_level)) {
                $query.=" AND employees.skill_level='".$request->skill_level."'";
            }

            
            $gType="employees.".$request->reportType." ".$request->reportOrder;
            $query.=" AND employees.empAccStatus='".$request->accStatus."'  ORDER BY employees.empSection ";
            // $query.=" GROUP BY 'unit_id'";
//            return $query;

            $employees= DB::select(DB::raw($query));


            if (($request->viewType)=="Preview") {
                return view('report.employee.view_employee_list',compact('employees','request'));
            }elseif(($request->viewType)=="Generate PDF"){
                $pagesize=$request->pagesize;
                $pageOrientation=$request->pageOrientation;
                return view('print/employee_list',compact('employees','request'));

                // $pdf=PDF::loadView('report.pdf.employee_list_pdf',compact('employees','request'));
                // $pdf->setPaper($pagesize, $pageOrientation);
                // $name=time()."_employee_list.pdf";
                // return $pdf->download($name);
            }elseif(($request->viewType)=="GenerateExcel"){
                $excelName=time()."_employee_list";
                Excel::create("$excelName", function($excel) use ($employees, $request) {

                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Employee List");


                    // Chain the setters
                    $excel->setCreator($name);

                    $excel->setDescription('Employee List');

                    $excel->sheet('Sheet 1', function ($sheet) use ($employees, $request) {
                        $sheet->loadView('report.excel.employee_list_excel')
                            ->with('employees',$employees)
                            ->with('request',$request);
                    });

                })->download('xlsx');

            }else{
                Session::flash('error_message', "Invalid Request");
                return view('404');
            }
        }elseif(($request->reportType)=="1"||($request->reportType)=="2") {

            $query=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName, payroll_salary.basic_salary, payroll_salary.house_rant, payroll_salary.medical, payroll_salary.transport, payroll_salary.food, payroll_salary.other, payroll_salary.total_employee_salary FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id LEFT JOIN payroll_salary ON payroll_salary.emp_id=employees.id  WHERE employees.id>0 ";

            if (($request->unitId)!=0) {
                $query.=" AND employees.unit_id='".$request->unitId."'";
            }

            if (($request->floorId)!=0) {
                $query.=" AND employees.floor_id='".$request->floorId."'";
            }

            if (($request->line_id)!=0) {
                $query.=" AND employees.line_id='".$request->line_id."'";
            }

            if (!empty($request->sectionName)) {
                $query.=" AND employees.empSection='".$request->sectionName."'";
            }

            if (($request->departmentId)!=0) {
                $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
            }

            if (($request->designationId)!=0) {
                $query.=" AND employees.empDesignationId='".$request->designationId."'";
            }

            if (($request->empGenderId)!=0) {
                $query.=" AND employees.empGenderId='".$request->empGenderId."'";
            }

            $query.=" AND employees.empAccStatus='".$request->accStatus."'  ORDER BY employees.line_id ASC";
            // $query.=" GROUP BY 'unit_id'";

//            return $query;

            $employees= DB::select(DB::raw($query));


            if (($request->viewType)=="Preview") {
                return view('report.employee.view_employee_list1',compact('employees','request'));
            }elseif(($request->viewType)=="Generate PDF"){
                // return view('report.pdf.employee_list_pdf_line_section',compact('employees','request'));
                $pagesize=$request->pagesize;
                $pageOrientation=$request->pageOrientation;
                if(($request->reportType)=="1"){
                    $pdf=PDF::loadView('report.pdf.employee_list_pdf_line_section',compact('employees','request'));
                }elseif(($request->reportType)=="2"){
                    $pdf=PDF::loadView('report.pdf.employee_list_pdf_line_section1',compact('employees','request'));

                }else{

                }

                $pdf->setPaper($pagesize, $pageOrientation);
                $name=time()."_employee_list.pdf";
                return $pdf->download($name);
            }else{
                Session::flash('error_message', "Invalid Request");
                return view('404');
            }

        }else{
            return 'error occoured.';
        }


    }


    public function employeeListByStatus(){
        return view('report.employee.employee_list_by_status');
    }
    public function viewEmployeeListByStatus(Request $request){
        $query=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";

        $query.=" AND employees.empAccStatus='".$request->accStatus."'  ORDER BY employees.employeeId ASC";

        $employees= DB::select(DB::raw($query));


        if (($request->viewType)=="Preview") {
            return view('report.employee.view_employee_list_status',compact('employees','request'));
        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.employee_list_pdf',compact('employees','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_employee_list.pdf";
            return $pdf->download($name);
        }else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }


    }


    public function employeeListByGender(){
        return view('report.employee.employee_list_by_gender');
    }

    public function viewEmployeeListByGender(Request $request){
        $query=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";


        if (($request->empGenderId)!=0) {
            $query.=" AND employees.empGenderId='".$request->empGenderId."'";
        }
        $query.=" ORDER BY employees.employeeId ASC";

        $employees= DB::select(DB::raw($query));


        if (($request->viewType)=="Preview") {
            return view('report.employee.view_employee_list_gender',compact('employees','request'));
        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.employee_list_pdf',compact('employees','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_employee_list.pdf";
            return $pdf->download($name);
        }else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }


    }


    public function employeeListByDepartment(){
        $departments=DB::table('departments')->get(['departmentName','id']);
        return view('report.employee.employee_list_by_department',compact('departments'));
    }

    public function viewEmployeeListByDepartment(Request $request){
        $query=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";


        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        $query.=" ORDER BY employees.employeeId ASC";
        $employees= DB::select(DB::raw($query));


        if (($request->viewType)=="Preview") {
            return view('report.employee.view_employee_list_department',compact('employees','request'));
        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.employee_list_pdf',compact('employees','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_employee_list.pdf";
            return $pdf->download($name);
        }else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }


    public function employeeListByDesignation(){
        $designations=DB::table('designations')->get(['designation','id']);
        return view('report.employee.employee_list_by_designation',compact('designations'));
    }

    public function viewEmployeeListByDesignation(Request $request){
        $query=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";


        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
        }

        $query.=" ORDER BY employees.employeeId ASC";

        $employees= DB::select(DB::raw($query));


        if (($request->viewType)=="Preview") {
            return view('report.employee.view_employee_list_designation',compact('employees','request'));
        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.employee_list_pdf',compact('employees','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_employee_list.pdf";
            return $pdf->download($name);
        }else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function employeeListByFloorLine(){
        $floors=DB::table('floors')->get(['floor','id']);
        return view('report.employee.employee_list_by_floor_line',compact('floors'));
    }

    public function viewEmployeeListByFloorLine(Request $request){
        $query=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";


        if (($request->floorId)!=0) {
            $query.=" AND employees.floor_id='".$request->floorId."'";
        }

        if (($request->line_id)!=0) {
            $query.=" AND employees.line_id='".$request->line_id."'";
        }


        $query.=" ORDER BY employees.employeeId ASC";

        $employees= DB::select(DB::raw($query));


        if (($request->viewType)=="Preview") {
            return view('report.employee.view_employee_list_by_floor_line',compact('employees','request'));
        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.employee_list_pdf',compact('employees','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_employee_list.pdf";
            return $pdf->download($name);
        }else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }


    public function employeeListBySection(){
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        return view('report.employee.employee_list_by_section',compact('sections'));
    }

    public function viewEmployeeListBySection(Request $request){
        $query=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";

        if (!empty($request->sectionName)) {
            $query.=" AND employees.empSection='".$request->sectionName."'";
        }

        $query.=" ORDER BY employees.employeeId ASC";

        $employees= DB::select(DB::raw($query));


        if (($request->viewType)=="Preview") {
            return view('report.employee.view_employee_list_by_section',compact('employees','request'));
        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.employee_list_pdf',compact('employees','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_employee_list.pdf";
            return $pdf->download($name);
        }else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    //Start Payroll Reports Area 
    public function payrollReportDashboard(){
        return view('report.payroll.index');
    }
    //End Payroll Reports Area 


    //Start Training Reports Area 
    public function trainingReportDashboard(){
        return view('report.training.index');
    }
    //End Training Reports Area 



    //Start Recruitment Reports Area 
    public function recruitmentReportDashboard(){
        return view('report.recruitment.index');
    }
    //End Recruitment Reports Area 


    // Start Attendance report area
    public function attendanceReportDashboard(){
        return view('report.attendance.index');
    }

    public function report_daily_attendance(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);

        return view('report.attendance.report_daily_attendance',compact('units','departments','floors','sections','designations'));

    }

    public function view_report_daily_attendance(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->report_daily_attendance();
        }

        $date=Carbon::parse($request->date)->toDateString();
        $ld=Carbon::parse($request->date)->toDateString();
        // $lateTime=AttendanceController::late_time();

        $query=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName, attendance_setup.max_entry_time AS max_entry_time, attendance_setup.shiftName AS shiftName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE employees.id>0 ";

        $queryLeave=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";

        $queryPresent=@"SELECT employees.*, attendance.id AS aid, attendance_setup.max_entry_time AS max_entry_time, attendance_setup.exit_time AS exit_time, attendance_setup.shiftName AS shiftName, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE employees.id>0 ";

        if (($request->unitId)!=0) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
            $queryLeave.=" AND employees.unit_id='".$request->unitId."'";
            $queryPresent.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (($request->floorId)!=0) {
            $query.=" AND employees.floor_id='".$request->floorId."'";
            $queryLeave.=" AND employees.floor_id='".$request->floorId."'";
            $queryPresent.=" AND employees.floor_id='".$request->floorId."'";
        }

        if (($request->line_id)!=0) {
            $query.=" AND employees.line_id='".$request->line_id."'";
            $queryLeave.=" AND employees.line_id='".$request->line_id."'";
            $queryPresent.=" AND employees.line_id='".$request->line_id."'";
        }

        if (!empty($request->sectionName)) {
            $query.=" AND employees.empSection='".$request->sectionName."'";
            $queryLeave.=" AND employees.empSection='".$request->sectionName."'";
            $queryPresent.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
            $queryLeave.=" AND employees.empDepartmentId='".$request->departmentId."'";
            $queryPresent.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
            $queryLeave.=" AND employees.empDesignationId='".$request->designationId."'";
            $queryPresent.=" AND employees.empDesignationId='".$request->designationId."'";
        }
        if (($request->empGenderId)!=0) {
            $query.=" AND employees.empGenderId='".$request->empGenderId."'";
            $queryLeave.=" AND employees.empGenderId='".$request->empGenderId."'";
            $queryPresent.=" AND employees.empGenderId='".$request->empGenderId."'";
        }

        $query.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryLeave.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryPresent.=" AND employees.empAccStatus='".$request->accStatus."'";
        $query.=" AND employees.id not in 
                          (SELECT * FROM (SELECT emp_id FROM attendance GROUP BY date,emp_id HAVING date='$date') AS subquery)";
        $query.=" AND employees.id not in 
                          (SELECT * FROM (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date and status='1') AS subquery)";
//        return $query;
        $queryPresent.=" AND attendance.date='$date'";
        $queryLeave.=" AND employees.id in
                            (SELECT * FROM (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date and status='1') AS subquery)";

//        return $queryLeave;

        $absent=DB::select($query);
        $present=DB::select($queryPresent);
        $leave=DB::select($queryLeave);
        $countAbsent=count($absent);
        $countPresent=count($present);
        $totalL=count($leave);

        foreach ($leave as $l){
            $l->leave='Leave';
        }

        $allData=array_merge($present, $absent);
        $allData=array_merge($allData, $leave);



        usort($allData, function($a, $b) {
            return $a->empFirstName <=> $b->empFirstName;
        });
        if (($request->viewType)=="Preview") {
            return view('report.attendance.view_report_daily_attendance',compact('allData','request','countPresent','countAbsent','totalL'));

        }
        elseif(($request->viewType)=="Generate PDF"){
            return view('report.pdf.daily_attendance_pdf',compact('allData','request','countAbsent','countPresent','totalL'));
        }

        elseif(($request->viewType)=="Generate Excel"){

            $excelName=time()."_Daily_Attendance_Report_Admin";
            Excel::create("$excelName", function($excel) use ($countAbsent, $countPresent, $totalL, $request, $allData) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Daily Attendance Report");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Daily Attendance Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($countAbsent, $totalL, $countPresent, $request, $allData) {
                    $sheet->loadView('report.excel.daily_attendance_excel')
                        ->with('allData',$allData)
                        ->with('request',$request)
                        ->with('countPresent',$countPresent)
                        ->with('totalL',$totalL)
                        ->with('countAbsent',$countAbsent);
                });

            })->download('xlsx');
        }



        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }


    public function public_report_daily_attendance(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        return view('report.attendance.public_report_daily_attendance',compact('units','departments','floors','sections','designations'));

    }

    public function public_view_report_daily_attendance(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->public_report_daily_attendance();
        }
        $date=Carbon::parse($request->date)->toDateString();
        $ld=Carbon::parse($request->date)->toDateString();
        $lateTime=AttendanceController::late_time();

        $query=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName, attendance_setup.max_entry_time AS max_entry_time, attendance_setup.shiftName AS shiftName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE employees.id>0 ";
        $queryLeave=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";
        $queryPresent=@"SELECT employees.*, attendance.id AS aid, attendance_setup.max_entry_time AS max_entry_time, attendance_setup.exit_time AS exit_time, attendance_setup.shiftName AS shiftName, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE employees.id>0 ";

        if (($request->unitId)!=0) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
            $queryLeave.=" AND employees.unit_id='".$request->unitId."'";
            $queryPresent.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (($request->floorId)!=0) {
            $query.=" AND employees.floor_id='".$request->floorId."'";
            $queryLeave.=" AND employees.floor_id='".$request->floorId."'";
            $queryPresent.=" AND employees.floor_id='".$request->floorId."'";
        }

        if (($request->line_id)!=0) {
            $query.=" AND employees.line_id='".$request->line_id."'";
            $queryLeave.=" AND employees.line_id='".$request->line_id."'";
            $queryPresent.=" AND employees.line_id='".$request->line_id."'";
        }

        if (!empty($request->sectionName)) {
            $query.=" AND employees.empSection='".$request->sectionName."'";
            $queryLeave.=" AND employees.empSection='".$request->sectionName."'";
            $queryPresent.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
            $queryLeave.=" AND employees.empDepartmentId='".$request->departmentId."'";
            $queryPresent.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
            $queryLeave.=" AND employees.empDesignationId='".$request->designationId."'";
            $queryPresent.=" AND employees.empDesignationId='".$request->designationId."'";
        }
        if (($request->empGenderId)!=0) {
            $query.=" AND employees.empGenderId='".$request->empGenderId."'";
            $queryLeave.=" AND employees.empGenderId='".$request->empGenderId."'";
            $queryPresent.=" AND employees.empGenderId='".$request->empGenderId."'";
        }


        $query.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryLeave.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryPresent.=" AND employees.empAccStatus='".$request->accStatus."'";
        $query.=" AND employees.id not in 
                          (SELECT * FROM (SELECT emp_id FROM attendance GROUP BY date, emp_id HAVING date='$date') AS subquery)";
        $query.=" AND employees.id not in 
                          (SELECT * FROM (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date and status='1') AS subquery)";
        $queryPresent.=" AND attendance.date='$date'";
        $queryLeave.=" AND employees.id in
                            (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date and status='1')";
        $absent=DB::select($query);
        $present=DB::select($queryPresent);
//        return $queryLeave;
        $leave=DB::select($queryLeave);

        $countAbsent=count($absent);
        $countPresent=count($present);
        $totalL=count($leave);
//        return $totalL;

        foreach ($leave as $l){
            $l->Status='Leave';
        }

        $allData=array_merge($present, $absent);
        $allData=array_merge($allData, $leave);

        $excelData=[];

        usort($allData, function($a, $b) {
            return $a->empFirstName <=> $b->empFirstName;
        });

//        return $allData;

        $excelName=time()."_Daily_Attendance_Report";


        if (($request->viewType)=="Preview") {
            return view('report.attendance.public_view_report_daily_attendance',compact('allData','request','lateTime','countPresent','countAbsent','totalL'));

        }elseif(($request->viewType)=="Generate PDF"){
            return view('print.daily_attendance_report_public',compact('allData','request','lateTime','countAbsent','countPresent','totalL'));
        }
        elseif(($request->viewType)=="Generate Excel"){
            Excel::create("$excelName", function($excel) use ($countAbsent, $lateTime, $countPresent, $totalL, $request, $allData,$excelData) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Daily Attendance Report");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Daily Attendance Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($countAbsent, $totalL, $countPresent, $lateTime, $request, $allData) {
                    $sheet->loadView('report.excel.public_daily_attendance_excel')
                        ->with('allData',$allData)
                        ->with('request',$request)
                        ->with('lateTime',$lateTime)
                        ->with('countPresent',$countPresent)
                        ->with('totalL',$totalL)
                        ->with('countAbsent',$countAbsent);
                });

            })->download('xlsx');
        }
        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }

    }



    // End Attendance report area

    //Report attendance view method
    public function attendance_report_view(){
        $employees=DB::table('employees')
            ->select(DB::raw("concat(empFirstName,' ', empLastName, ' (',employeeId,')' ) as name,id"))
            ->pluck('name','id')->all();
        $departments=DB::table('departments')->pluck('departmentName','id');
        return view('report.attendance.date_wise_report',compact('employees','departments'));
    }

    //Attendance Report show method
    public function attendance_report_show(Request $request)
    {
        $start = $request->start_date;
        $end = $request->end_date;
        $sdate = date_create($start);
        $edate = date_create($end);
        $start_date = date_format($sdate, "Y-m-d");
        $end_date = date_format($edate, "Y-m-d");
        if ($request->checkStatus == 'ae') {
            $data = DB::table('attendance')
                ->leftJoin('employees', function ($query) {
                    $query->on('attendance.emp_id', '=', 'employees.id');
                })
                ->join('designations', function ($query) {
                    $query->on('designations.id', '=', 'employees.empDesignationId');
                })
                ->select('attendance.*', 'employees.empFirstName', 'employees.id', 'employees.employeeId', 'employees.empLastName', 'designations.designation')
                ->selectRaw('attendance.emp_id')->whereBetween('attendance.date', [$start_date, $end_date])
                ->orderBy('employees.empFirstName', 'ASC')
                ->get();
            return response()->json($data);

        }

        else if($request->checkStatus=='bd'){
            $data = DB::table('attendance')
                ->leftJoin('employees', function ($query) {
                    $query->on('attendance.emp_id', '=', 'employees.id');
                })
                ->join('designations', function ($query) {
                    $query->on('designations.id', '=', 'employees.empDesignationId');
                })
                ->select('attendance.*', 'employees.empFirstName','employees.empDepartmentId', 'employees.id', 'employees.employeeId', 'employees.empLastName', 'designations.designation')
                ->selectRaw('attendance.emp_id')->whereBetween('attendance.date', [$start_date, $end_date])
                ->where('employees.empDepartmentId', $request->dept_id)
                ->orderBy('employees.empFirstName', 'ASC')
                ->get();
            return response()->json($data);

        }
    }

    public function attendanceCardReport(Request $request){
        $start = $request->start_date;
        $end = $request->end_date;
        $sdate = date_create($start);
        $edate = date_create($end);
        $start_date = date_format($sdate, "Y-m-d");
        $end_date = date_format($edate, "Y-m-d");
        $weekends = [];
        $weekend = DB::table('week_leave')->where(['status' => 1])->get();
        $festivalLeave = DB::table('tb_festival_leave')->get();
        $approvedLeave=DB::table('tb_leave_application')->where(['employee_id'=>$request->emp_id, 'status'=>1])->get();
        foreach ($weekend as $w) {
            $weekends[] = $w->day;
        }


        $dates = [];
        $datas = [];
        $abs=0;

        for ($i = Carbon::createFromFormat('Y-m-d', $start_date); $i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
            $key=0;
            $fes=0;
            $lev=0;
            $dates = $i->format('Y-m-d');
            $timestamp = strtotime($dates);
            $day = date('l', $timestamp);
            foreach ($festivalLeave as $fe){
                $fesDayStart=strtotime($fe->start_date);
                $fesDayEnd=strtotime($fe->end_date);
                if($timestamp >=$fesDayStart && $timestamp<=$fesDayEnd){
                    $fes=1;
                    break;
                }
            }
            foreach ($approvedLeave as $al){
                if($timestamp >=strtotime($al->leave_starting_date) && $timestamp<=strtotime($al->leave_ending_date))
                {
                    $lev=1;
                    break;
                }
            }
            for($j=0;$j<count($weekends);$j++)
            {
                if($day==$weekends[$j]){
                    $key=1;
                    break;
                }
            }
            if($key!=1) {

                if ($fes != 1) {

                    if ($lev != 1) {
                        $data = DB::select("SELECT attendance.*, employees.id as eid, employees.empFirstName, employees.empLastName,employees.empSection, employees.date_of_discontinuation, employees.empJoiningDate, employees.employeeId,designations.designation, departments.departmentName  from 
                        `attendance` inner join `employees` on `attendance`.`emp_id` = `employees`.`id` inner join `departments` on `employees`.`empDepartmentId` = `departments`.`id` inner join `designations` on `employees`.`empDesignationId` = `designations`.`id` where employees.id =" . $request->emp_id . " and date='$dates'");

                        if (count($data)) {
                            $datas[] = array_merge($data, [$dates, $abs]);
                        } else {
                            $data = DB::select("SELECT employees.id,employees.empFirstName, employees.empJoiningDate, employees.empLastName,
                                employees.employeeId, designations.designation FROM employees, designations
                                where employees.empDesignationId=designations.id HAVING employees.id =" . $request->emp_id);
                            $datas[] = array_merge($data, [$dates, ++$abs]);

                        }
                    } else {
                        $data = DB::select("SELECT employees.id,employees.empFirstName, employees.empJoiningDate, employees.empLastName,
                                employees.employeeId, designations.designation FROM employees, designations
                                where employees.empDesignationId=designations.id HAVING employees.id in (" . $request->emp_id . ")");
                        $datas[] = array_merge($data, [$dates, "On Leave"]);


                    }
                } else {
                    $data = DB::select("SELECT employees.id,employees.empFirstName, employees.empJoiningDate, employees.empLastName,
                                employees.employeeId, designations.designation FROM employees, designations
                                where employees.empDesignationId=designations.id HAVING employees.id in (" . $request->emp_id . ")");
                    $datas[] = array_merge($data, [$dates, "Festival Holiday"]);

                }
            }
            else{

                $data = DB::select("SELECT employees.id,employees.empFirstName,employees.empJoiningDate, employees.empLastName,
                                employees.employeeId, designations.designation FROM employees, designations
                                where employees.empDesignationId=designations.id HAVING employees.id in (" . $request->emp_id . ")");
                $datas[] = array_merge($data, [$dates, "weekend"]);


            }
        }
        return $datas;

    }

    function weekend_dates($d1,$d2){
        $date1=strtotime($d1);
        $date2=strtotime($d2);
        $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();
        $start_date=date('Y-m-d', $date1);
        $end_date=date('Y-m-d', $date2);
        $weekend_dates=array();
        for ($i = Carbon::createFromFormat('Y-m-d', $start_date);$i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
            $dates = $i->format('Y-m-d');
            $timestamp = strtotime($dates);
            $day = date('l', $timestamp);
            for($j=0;$j<count($weekends);$j++)
            {
                if($day==$weekends[$j]){
                    $weekend_dates[]=$dates;
                }
            }
        }
        return $weekend_dates;


    }

    public static function regular_day_count($d1,$d2){
        $start=Carbon::parse($d1)->toDateString();
        $end=Carbon::parse($d2)->toDateString();
        $c=DB::table('tbweekend_regular_day')
            ->whereBetween('weekend_date',[$start,$end])->count();
        return $c;
    }



    public static function weekendCalculator($d1,$d2){
        $date1=strtotime($d1);
        $date2=strtotime($d2);
        $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();
        $start_date=date('Y-m-d', $date1);
        $end_date=date('Y-m-d', $date2);
        $key=0;

        $festivalLeave = DB::table('tb_festival_leave')
            ->where(DB::raw('year(fest_starts)'),date('Y', $date1))
            ->orWhere(DB::raw('year(fest_ends)'),date('Y', $date1))
            ->get();

        $weekend_dates=array();
        for ($i = Carbon::createFromFormat('Y-m-d', $start_date);$i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
            $dates = $i->format('Y-m-d');
            $timestamp = strtotime($dates);
            $day = date('l', $timestamp);
            for($j=0;$j<count($weekends);$j++)
            {
                if($day==$weekends[$j]){
                    $key++;
                    $weekend_dates[]=$dates;
                }
            }
        }
        $h=0;
        foreach ($festivalLeave as $f){
//            return $f->fest_starts."     ".$f->fest_ends;
            foreach ($weekend_dates as $wd){
                if($wd>=$f->fest_starts && $wd<=$f->fest_ends){
                    $h++;
                };
            }
        }
        return $key-$h;

    }




    //weekend find function
    function weekdayCalculator($d1,$d2){
        $date1=strtotime($d1);
        $date2=strtotime($d2);
        $interval1=1+round(abs($date1-$date2)/86400);
        $festivalLeave = DB::table('tb_festival_leave')->get();
        $dcount=0;
        foreach($festivalLeave as $fleave){
            if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
                $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);
            }
            else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
                $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);
            }
            else{
                continue;
            }
        }
        $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();
        $start_date=date('Y-m-d', $date1);
        $end_date=date('Y-m-d', $date2);
        $key=0;
        for ($i = Carbon::createFromFormat('Y-m-d', $start_date);$i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
            $dates = $i->format('Y-m-d');
            $timestamp = strtotime($dates);
            $day = date('l', $timestamp);
            for($j=0;$j<count($weekends);$j++)
            {
                if($day==$weekends[$j]){
                    $key++;
                }
            }
        }
        $interval=(int)$interval1-((int)$dcount+$key);
        return $key;
    }



    //holiday find function
    public static function holiday($d1,$d2){
        $date1=strtotime($d1);
        $date2=strtotime($d2);
        $interval1=1+round(abs($date1-$date2)/86400);
        $festivalLeave = DB::table('tb_festival_leave')->get();
        $dcount=0;
        foreach($festivalLeave as $fleave){
            if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
                $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);
            }
            else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
                $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);
            }
            else{
                continue;
            }
        }
        return $dcount;
    }

    public function attendanceCardAll(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees=DB::table('employees')->get(['empFirstName','employeeId','id']);
        return view('report.attendance.job_card_all', compact('units','departments',
            'floors', 'sections', 'designations','employees'));
    }


    public function attendanceCardAllBuyer(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees=DB::table('employees')->get(['empFirstName','employeeId','id']);
        return view('report.attendance.job_card_all_buyer', compact('units','employees','departments','floors','sections','designations'));
    }

    public function job_card_all_data_buyer(Request $request){
        $query=DB::table('employees')->leftJoin('units','employees.unit_id','=','units.id')
            ->leftJoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftJoin('designations','employees.empDesignationId','=','designations.id');

        if($request->emp_id!=0){
            $query = $query->leftJoin('attendance_setup', 'employees.empShiftId', '=', 'attendance_setup.id');
            $query = $query->where('employees.id', '=', $request->emp_id);
        }
        else {
            if (($request->unitId) != 0) {
                $query = $query->where('employees.unit_id', '=', $request->unitId);
            }

            if (($request->floorId) != 0) {
                $query = $query->where('employees.floor_id', '=', $request->floorId);
            }

            if (!empty($request->sectionName)) {
                $query = $query->where('employees.empSection', '=', $request->sectionName);
            }

            if (($request->departmentId) != 0) {
                $query = $query->where('employees.empDepartmentId', '=', $request->departmentId);

            }

            if (($request->designationId) != 0) {
                $query = $query->where('employees.empDesignationId', '=', $request->designationId);

            }
            $query = $query->leftJoin('attendance_setup', 'employees.empShiftId', '=', 'attendance_setup.id');
        }

        $query=$query->where('employees.empJoiningDate','<', Carbon::parse($request->end_date)->toDateString());
    

        if($request->accStatus==0) {
            $query=$query->whereBetween('employees.date_of_discontinuation',[ Carbon::parse($request->start_date)->toDateString(), Carbon::parse($request->end_date)->toDateString()]);
        }
        else{
            $dt=Carbon::parse($request->end_date);
            $query=$query->where(function ($query) use ($dt){
                $query->where('date_of_discontinuation','=',null);
                $query->orWhere('date_of_discontinuation','>',$dt);
            });
        }

        $query=$query->select('attendance_setup.*','employees.id','employees.empFirstName','employees.empLastName',
            'employees.employeeId', 'employees.empShiftId','employees.empJoiningDate','employees.empDOB','employees.date_of_discontinuation',
            'employees.empCardNumber','departments.departmentName','designations.designation','attendance_setup.shiftName',
            'attendance_setup.entry_time','attendance_setup.exit_time','attendance_setup.max_entry_time', 'attendance_setup.max_entry_time',
            'employees.empSection','employees.empOTStatus');
        $month_check=date('Y-m-01',strtotime($request->start_date));
        $start = $request->start_date;
        $end = $request->end_date;
        $total_days=date('t',strtotime($start));
        $sdate = date_create($start);
        $edate = date_create($end);
        $request->start_date = date_format($sdate, "Y-m-d");
        $request->end_date = date_format($edate, "Y-m-d");

        $employee=$query->get();
        if($request->viewType=="Generate PDF"){
            return view('report.attendance.job_card_all_data_buyer_pdf',compact('employee','request','total_days','start','end','month_check'));
        }
        return view('report.attendance.job_card_all_data_buyer',compact('employee','request','total_days','start','end','month_check'));

    }


    public function job_card_all_data(Request $request){
        $query=DB::table('employees')->leftJoin('units','employees.unit_id','=','units.id')
        ->leftJoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftJoin('designations','employees.empDesignationId','=','designations.id');

    if($request->emp_id!=0){
        $query = $query->leftJoin('attendance_setup', 'employees.empShiftId', '=', 'attendance_setup.id');
        $query = $query->where('employees.id', '=', $request->emp_id);
    }
    else {
        if (($request->unitId) != 0) {
            $query = $query->where('employees.unit_id', '=', $request->unitId);
        }

        if (($request->floorId) != 0) {
            $query = $query->where('employees.floor_id', '=', $request->floorId);
        }

        if (!empty($request->sectionName)) {
            $query = $query->where('employees.empSection', '=', $request->sectionName);
        }

        if (($request->departmentId) != 0) {
            $query = $query->where('employees.empDepartmentId', '=', $request->departmentId);

        }

        if (($request->designationId) != 0) {
            $query = $query->where('employees.empDesignationId', '=', $request->designationId);

        }
        $query = $query->leftJoin('attendance_setup', 'employees.empShiftId', '=', 'attendance_setup.id');
    }

    $query=$query->where('employees.empJoiningDate','<', Carbon::parse($request->end_date)->toDateString());


    if($request->accStatus==0) {
        $query=$query->whereBetween('employees.date_of_discontinuation',[ Carbon::parse($request->start_date)->toDateString(), Carbon::parse($request->end_date)->toDateString()]);
    }
    else{
        $dt=Carbon::parse($request->end_date);
        $query=$query->where(function ($query) use ($dt){
            $query->where('date_of_discontinuation','=',null);
            $query->orWhere('date_of_discontinuation','>',$dt);
        });
    }

    $query=$query->select('attendance_setup.*','employees.id','employees.empFirstName','employees.empLastName',
        'employees.employeeId', 'employees.empShiftId','employees.empJoiningDate','employees.empDOB','employees.date_of_discontinuation',
        'employees.empCardNumber','departments.departmentName','designations.designation','attendance_setup.shiftName',
        'attendance_setup.entry_time','attendance_setup.exit_time','attendance_setup.max_entry_time', 'attendance_setup.max_entry_time',
        'employees.empSection','employees.empOTStatus');
                $start = $request->start_date;
                $end = $request->end_date;
                $total_days=date('t',strtotime($start));
                $total_holiday=$this->holiday($start,$end);
                $sdate = date_create($start);
                $edate = date_create($end);
                $request->start_date = date_format($sdate, "Y-m-d");
                $request->end_date = date_format($edate, "Y-m-d");
                $employee=$query->get();

                if($request->viewType=="Generate PDF"){
                    return view('report.attendance.job_card_all_data_pdf',compact('employee','request','total_days','total_holiday'));
                }
                return view('report.attendance.job_card_all_data',compact('employee','request','total_days','total_holiday'));
            }

    

    public function pdfAttendanceCard( Request $request){
        $emp_id=$request->emp_id;
        $s=date('Y-m-01',strtotime($request->start_date));
        $e=date('Y-m-t',strtotime($request->end_date));
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->start_date)){
            Session::flash('message','Incorrect date format.');
            return redirect('report/attendance/card');
        }
        elseif(!$helper->checkIsAValidDate($request->end_date)){
            Session::flash('message','Incorrect date format.');
            return redirect('report/attendance/card');
        }
        $month_check=date('Y-m-01',strtotime($request->start_date));
        $start = $request->start_date;
;
        $check_join=DB::table('employees')->where('id',$emp_id)->where('empJoiningDate' ,'>', Carbon::parse($request->start_date)->toDateString())->get();

        $end = $request->end_date;

        $leave_of_employee=DB::table('leave_of_employee')
        ->where('emp_id',$emp_id)
        ->where('month','=',$month_check)
        ->get();
        $leave_holiday=0;
        $dept_holiday_leave=0;
        foreach ($leave_of_employee as $loe){
            $leave_holiday+=$this->holiday($loe->start_date,$loe->end_date);
            $dept_holiday_leave+=DB::table('emp_wise_holiday')
            ->where('emp_id',$emp_id)
            ->whereBetween('holiday_date',[$loe->start_date,$loe->end_date])
            ->count();
        }

        $dept_total_holiday=DB::table('emp_wise_holiday')
        ->where('emp_id',$emp_id)
        ->whereBetween('holiday_date',[ date('Y-m-d',strtotime($start )),date('Y-m-d',strtotime($end))])
        ->count()-$dept_holiday_leave;

        $total_days=date('t',strtotime($start));
        $total_holiday=$this->holiday($start,$end)-$leave_holiday+$dept_total_holiday;
        $total_weekend=$this->weekdayCalculator($start,$end);

        if(count($check_join)>0){
            $dept_total_holiday=DB::table('emp_wise_holiday')
            ->where('emp_id',$emp_id)
            ->whereBetween('holiday_date',[ $check_join[0]->empJoiningDate,date('Y-m-d',strtotime($end)) ])
            ->count()-$dept_holiday_leave;
            $total_holiday=$this->holiday($check_join[0]->empJoiningDate,date('Y-m-d',strtotime($end)))-$leave_holiday+$dept_total_holiday;
        }
       
        $sdate = date_create($start);
        $edate = date_create($end);
        $start_date = date_format($sdate, "Y-m-d");
        $end_date = date_format($edate, "Y-m-d");

        if($start_date>$end_date){
            Session::flash('message','Start date must be less then end date.');
            return redirect('report/public/attendance/card');
        }

        $festivalLeave = DB::table('tb_festival_leave')->get();
        $approvedLeave = DB::table('tb_leave_application')
            ->leftJoin('tb_leave_type','tb_leave_application.leave_type_id','=','tb_leave_type.id')
            ->where(['employee_id' => $request->emp_id, 'status' => 1])
            ->select('tb_leave_application.*','tb_leave_type.leave_type')
            ->get();

        $dates = [];
        $datas = [];
        $abs=0;

        for ($i = Carbon::createFromFormat('Y-m-d', $start_date); $i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
            $key=0;
            $fes=0;
            $lev=0;
            $dates = $i->format('Y-m-d');
            $timestamp = strtotime($dates);

            $dept_holiday=DB::table('emp_wise_holiday')
            ->where('emp_id',$emp_id)
            ->get();
            
            $day = date('l', $timestamp);
            foreach ($festivalLeave as $fe){
                $fesDayStart=strtotime($fe->start_date);
                $fesDayEnd=strtotime($fe->end_date);
                if($timestamp >=$fesDayStart && $timestamp<=$fesDayEnd){
                    $fes=1;
                    $fesPurpose=$fe->purpose;
                    break;
                }
            }

            // department wise holiday
            foreach($dept_holiday as $d_holi){
                   $fesDayStart=strtotime($d_holi->holiday_date);
                    $fesDayEnd=strtotime($d_holi->holiday_end_date);
                    if($timestamp >=$fesDayStart && $timestamp<=$fesDayEnd){
                        $fes=1;
                        $fesPurpose=$d_holi->perpose;
                        break;
                    }
               }
            // department wise holiday

            foreach ($approvedLeave as $al){
                if($timestamp >=strtotime($al->leave_starting_date) && $timestamp<=strtotime($al->leave_ending_date))
                {
                    // return $al->leave_type_id;
                    $lev=1;
                    $leave_name=$al->leave_type;
                    break;
                }
            }
            if($key=1) {
                if ($lev != 1) {
                    if ($fes != 1) {
                        $data = DB::select("SELECT attendance.*, employees.id as eid, employees.empFirstName, employees.empLastName,employees.empSection, employees.date_of_discontinuation, employees.empJoiningDate, employees.employeeId,designations.designation, departments.departmentName  from 
                        `attendance` inner join `employees` on `attendance`.`emp_id` = `employees`.`id` inner join `departments` on `employees`.`empDepartmentId` = `departments`.`id` inner join `designations` on `employees`.`empDesignationId` = `designations`.`id` where employees.id =" . $request->emp_id . " and date='$dates'");

                        if (count($data)) {
                            $datas[] = array_merge($data, [$dates, $abs]);
                        } else {
                            $sql="SELECT employees.id,employees.empFirstName, employees.empJoiningDate, employees.empLastName, employees.empSection, employees.date_of_discontinuation,
                                employees.employeeId, designations.designation, departments.departmentName FROM employees, designations, departments
                                where employees.empDesignationId=designations.id AND employees.empDepartmentId=departments.id HAVING employees.id =$request->emp_id";
//                            return $sql;
                            $data = DB::select($sql);
                            $datas[] = array_merge($data, [$dates, ++$abs]);
                        }
                    } else {
                        $data = DB::select("SELECT employees.id,employees.empFirstName, employees.empJoiningDate, employees.empLastName,employees.empSection, employees.date_of_discontinuation,
                                employees.employeeId, designations.designation, departments.departmentName 
                                 FROM employees, designations, departments
                                where employees.empDesignationId=designations.id  
                                AND employees.empDepartmentId=departments.id 
                                HAVING employees.id in (" . $request->emp_id . ")");
                        $datas[] = array_merge($data, [$dates, "Holiday@$fesPurpose"]);

                    }
                } else {
                    $data = DB::select("SELECT employees.id,employees.empFirstName, employees.empJoiningDate, employees.empLastName,employees.empSection, employees.date_of_discontinuation,
                                employees.employeeId, designations.designation, departments.departmentName  
                                FROM employees, designations, departments
                                WHERE employees.empDesignationId=designations.id 
                                AND employees.empDepartmentId=departments.id  
                                HAVING employees.id in (" . $request->emp_id . ")");
                    $datas[] = array_merge($data, [$dates, "On Leave@$leave_name"]);
                }
            }
            else{
                $data = DB::select("SELECT employees.id,employees.empFirstName,employees.empJoiningDate, employees.empLastName,employees.empSection, employees.date_of_discontinuation,
                                employees.employeeId, designations.designation, departments.departmentName
                                 FROM employees, designations, departments
                                where employees.empDesignationId=designations.id
                                 AND employees.empDepartmentId=departments.id 
                                 HAVING employees.id in (" . $request->emp_id . ")");
                $datas[] = array_merge($data, [$dates, "weekend"]);
            }
        }

//        dd($datas);
        $ot_status=DB::table('employees')->where('id','=',$emp_id)->first()->empOTStatus;
//        return $ot_status;
        if ($request->viewType == "Download Excel") {
            if($ot_status==0){
                $excelName=time()."_job_card";
                Excel::create("$excelName", function($excel) use ($datas, $start_date, $end_date,$total_holiday,$total_days,$emp_id) {

                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("job card");


                    // Chain the setters
                    $excel->setCreator($name);

                    $excel->setDescription('Employee Job Card');

                    $excel->sheet('Sheet 1', function ($sheet) use ($datas, $end_date, $start_date,$emp_id,$total_days,$total_holiday) {
                        $sheet->loadView('report.excel.nonOT.attendance_card')
                            ->with('datas',$datas)
                            ->with('total_holiday',$total_holiday)
                            ->with('total_days',$total_days)
                            ->with('start_date',$start_date)
                            ->with('emp_id',$emp_id)
                            ->with('end_date',$end_date);
                    });

                })->download('xlsx');

            }
            $excelName=time()."_job_card";
            Excel::create("$excelName", function($excel) use ($datas, $start_date, $end_date,$total_holiday,$total_days,$emp_id) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("job card");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Employee Job Card');

                $excel->sheet('Sheet 1', function ($sheet) use ($datas, $end_date, $start_date,$emp_id,$total_days,$total_holiday) {
                    $sheet->loadView('report.excel.attendance_card')
                        ->with('datas',$datas)
                        ->with('total_holiday',$total_holiday)
                        ->with('total_days',$total_days)
                        ->with('start_date',$start_date)
                        ->with('emp_id',$emp_id)
                        ->with('end_date',$end_date);
                });

            })->download('xlsx');

        }

        else if ($request->viewType == "Download PDF English") {
                return view('report.pdf.attendance_card', compact('datas','emp_id', 'start_date', 'end_date','total_days','total_holiday','total_weekend'));
            // if($ot_status==0){
            //     $pdf = PDF::loadView('report.pdf.notOT.attendance_card', compact('datas','emp_id', 'start_date', 'end_date','total_days','total_holiday','total_weekend'));
            //     $pdf->setPaper('A4', 'portrait');
            //     $name = time() . "_attendance_card_english.pdf";
            //     return $pdf->download($name);
            // }
            // $pdf = PDF::loadView('report.pdf.attendance_card', compact('datas','emp_id', 'start_date', 'end_date','total_days','total_holiday','total_weekend'));
            // $pdf->setPaper('A4', 'portrait');
            // $name = time() . "_attendance_card_english.pdf";
            // return $pdf->download($name);
        }


        else if ($request->viewType == "Download PDF Bangla") {
            if($ot_status==0) {
                $pdf = mPDF::loadView('report.pdf.notOT.attendance_card_bangla', compact('datas','emp_id', 'start_date', 'end_date','total_days','total_holiday','total_weekend'));
                $name = time() . "_attendance_card_Bengali.pdf";
                return $pdf->download($name);
            }

            $pdf = mPDF::loadView('report.pdf.attendance_card_bangla', compact('datas','emp_id', 'start_date', 'end_date','total_days','total_holiday','total_weekend'));
//            $pdf->setPaper('A4', 'portrait');
            $name = time() . "_attendance_card_Bengali.pdf";
            return $pdf->download($name);
        }

    }




    public function pdfPublicAttendanceCard(Request $request)
    {
//        return $request->all();
        $emp_id=$request->emp_id;

        $helper = AppHelper::instance();
        if (!$helper->checkIsAValidDate($request->start_date)) {
            Session::flash('message', 'Incorrect date format.');
            return redirect('report/public/attendance/card');
        } elseif (!$helper->checkIsAValidDate($request->end_date)) {
            Session::flash('message', 'Incorrect date format.');
            return redirect('report/public/attendance/card');
        }
        $start = $request->start_date;
        $end = $request->end_date;
        $sdate = date_create($start);
        $edate = date_create($end);
        $start_date = date_format($sdate, "Y-m-d");
        $end_date = date_format($edate, "Y-m-d");
        if ($start_date > $end_date) {
            Session::flash('message', 'Start date must be less then end date.');
            return redirect('report/public/attendance/card');
        }
        $leave_name='';
        $weekends = [];
        $weekend = DB::table('week_leave')->where(['status' => 1])->get();
        $festivalLeave = DB::table('tb_festival_leave')->get();
        $approvedLeave = DB::table('tb_leave_application')
            ->leftJoin('tb_leave_type','tb_leave_application.leave_type_id','=','tb_leave_type.id')
            ->where(['employee_id' => $request->emp_id, 'status' => 1])
            ->select('tb_leave_application.*','tb_leave_type.leave_type')
            ->get();
//        return $approvedLeave;
        foreach ($weekend as $w) {
            $weekends[] = $w->day;
        }


        $dates = [];
        $datas = [];
        $abs = 0;

        for ($i = Carbon::createFromFormat('Y-m-d', $start_date); $i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
            $key = 0;
            $fes = 0;
            $lev = 0;
            $dates = $i->format('Y-m-d');
            $timestamp = strtotime($dates);

            $day = date('l', $timestamp);
            foreach ($festivalLeave as $fe) {
                $fesDayStart = strtotime($fe->start_date);
                $fesDayEnd = strtotime($fe->end_date);
                if ($timestamp >= $fesDayStart && $timestamp <= $fesDayEnd) {
                    $fes = 1;
                    break;
                }

            }
            foreach ($approvedLeave as $al) {
                if ($timestamp >= strtotime($al->leave_starting_date) && $timestamp <= strtotime($al->leave_ending_date)) {
                    // return $al->leave_type_id;
                    $lev = 1;
                    $leave_name=$al->leave_type;
                    break;
                }
            }
            for ($j = 0; $j < count($weekends); $j++) {
                if ($day == $weekends[$j]) {
                    $key = 1;
                    break;
                }
            }

            if ($key != 1) {

                if ($fes != 1) {

                    if ($lev != 1) {
                        $data = DB::select("SELECT attendance.*, employees.id as eid, employees.empFirstName, employees.empLastName,employees.empSection, employees.empJoiningDate, employees.employeeId,designations.designation, departments.departmentName  FROM attendance,
                        employees, designations, departments WHERE attendance.emp_id=employees.id and
                        employees.empDesignationId=designations.id AND employees.empDepartmentId=departments.id  HAVING employees.id =
                        " . $request->emp_id . " and date='$dates'");
                        if (count($data)) {
                            $datas[] = array_merge($data, [$dates, $abs]);
                        } else {
                            $data = DB::select("SELECT employees.id,employees.empFirstName, employees.empJoiningDate, employees.empLastName, employees.empSection,
                                employees.employeeId, designations.designation, departments.departmentName FROM employees, designations, departments
                                where employees.empDesignationId=designations.id AND employees.empDepartmentId=departments.id HAVING employees.id =" . $request->emp_id);
                            $datas[] = array_merge($data, [$dates, ++$abs]);

                        }
                    } else {
                        $data = DB::select("SELECT employees.id,employees.empFirstName, employees.empJoiningDate, employees.empLastName,employees.empSection,
                                employees.employeeId, designations.designation, departments.departmentName  FROM employees, designations, departments
                                WHERE employees.empDesignationId=designations.id AND employees.empDepartmentId=departments.id  HAVING employees.id in (" . $request->emp_id . ")");
                        $datas[] = array_merge($data, [$dates, "On Leave@$leave_name"]);


                    }
                } else {
                    $data = DB::select("SELECT employees.id,employees.empFirstName, employees.empJoiningDate, employees.empLastName,employees.empSection,
                                employees.employeeId, designations.designation, departments.departmentName  FROM employees, designations, departments
                                where employees.empDesignationId=designations.id  AND employees.empDepartmentId=departments.id HAVING employees.id in (" . $request->emp_id . ")");
                    $datas[] = array_merge($data, [$dates, "Festival Holiday"]);

                }
            } else {

                $data = DB::select("SELECT employees.id,employees.empFirstName,employees.empJoiningDate, employees.empLastName,employees.empSection,
                                employees.employeeId, designations.designation, departments.departmentName FROM employees, designations, departments
                                where employees.empDesignationId=designations.id AND employees.empDepartmentId=departments.id HAVING employees.id in (" . $request->emp_id . ")");
                $datas[] = array_merge($data, [$dates, "weekend"]);


            }
        }

//        $leave_check= explode('@',$datas[6][2]);
//        return $leave_check[0];

//        return $datas;

//        dd($datas);

        if ($request->viewType == "Download PDF Bangla") {
            $pdf = mPDF::loadView('report.pdf.attendance_card_public_bangla', compact('datas', 'start_date', 'end_date','emp_id'));
            //$pdf->setPaper('A4', 'portrait');
            $name = time() . "_attendance_card_bangla.pdf";
            return $pdf->download($name);
        }
        if ($request->viewType == "Download PDF English") {
            $pdf = PDF::loadView('report.pdf.attendance_card_public', compact('datas', 'start_date', 'end_date','emp_id'));
            $pdf->setPaper('A4', 'portrait');
            $name = time() . "_attendance_card_english.pdf";
            return $pdf->download($name);
        }

        else if ($request->viewType == "Download Excel") {
//            return $request->all();

//            return view('report.excel.attendance_card_public',compact('datas','start_date','end_date'));
//            return "AA";
            $excelName=time()."_job_card";
            Excel::create("$excelName", function($excel) use ($datas, $start_date, $end_date,$emp_id) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("job card");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Employee Job Card');

                $excel->sheet('Sheet 1', function ($sheet) use ($datas, $end_date, $start_date,$emp_id) {
//                    // Sets all borders
//                    $sheet->setAllBorders('thin');
//                    $sheet->setBorder('A1', 'thin');
//                    $sheet->setBorder('A1:F10', 'thin');'allData','request','lateTime','countPresent','countAbsent','Tlate','totalL'
//                    $sheet->setOrientation('landscape');
//                    $sheet->fromArray($data, NULL, 'A3',false,false);
                    $sheet->loadView('report.excel.attendance_card_public')
                        ->with('datas',$datas)
                        ->with('start_date',$start_date)
                        ->with('emp_id',$emp_id)
                        ->with('end_date',$end_date);
                });

            })->download('xlsx');

        }else if ($request->viewType == "Download Word"){
            // dd($datas);
            return view('report.attendance.attendance_card_word',compact('datas', 'start_date', 'end_date','emp_id'));
        }else{
            return redirect()->back();
        }
    }

    public function attendanceCard(){
        $employees=DB::table('employees')
        ->where('empAccStatus','=',1)
        ->where('is_three_shift','=',0)
        ->get(['empFirstName','employeeId','id']);
        return view('report.attendance.attendance_card',compact('employees'));
    }

    public function attendanceCardLefty(){
        $employees=DB::table('employees')
        ->where('empAccStatus','!=',1)
        ->where('is_three_shift','=',0)
        ->get(['empFirstName','employeeId','id']);
        return view('report.attendance.attendance_card_lefty',compact('employees'));

    }

    public function attendanceCardPublic(){
        $employees=DB::table('employees')->where('empAccStatus','=',1)->get(['empFirstName','employeeId','id']);
        return view('report.attendance.attendance_card_public',compact('employees'));

    }



    //Daily employee attendance method
    public function daily_employee_attendance(Request $request){
        echo "data hs comming";
    }

    public function daily_late_present(){

        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        return view('report.attendance.daily_late_present',compact('units','departments','floors','sections','designations'));

//        return view('report.attendance.daily_late_present');
    }

    public function daily_late_present_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->daily_late_present();
        }

        $queryPresent=@"SELECT employees.*, attendance.id AS aid, attendance_setup.max_entry_time AS max_entry_time, attendance_setup.shiftName AS shiftName, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE employees.id>0 ";

        if (($request->unitId)!=0) {
            $queryPresent.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (($request->floorId)!=0) {
            $queryPresent.=" AND employees.floor_id='".$request->floorId."'";
        }

        if (($request->line_id)!=0) {
            $queryPresent.=" AND employees.line_id='".$request->line_id."'";
        }

        if (!empty($request->sectionName)) {
            $queryPresent.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $queryPresent.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $queryPresent.=" AND employees.empDesignationId='".$request->designationId."'";
        }
        if (($request->empGenderId)!=0) {
            $queryPresent.=" AND employees.empGenderId='".$request->empGenderId."'";
        }
        $date=Carbon::parse($request->date)->toDateString();

        $queryPresent.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryPresent.=" AND attendance.date='$date' AND attendance.in_time>attendance_setup.max_entry_time ORDER BY employees.line_id ASC";
        // return $queryPresent;
        $present=DB::select($queryPresent);
        $countPresent=count($present);
        // return $present;

        if (($request->viewType)=="Preview") {
            return view('report.attendance.daily_late_present_data',compact('present','request','countPresent'));

        }elseif(($request->viewType)=="Generate PDF"){
//            $pagesize=$request->pagesize;
//            $pageOrientation=$request->pageOrientation;
//            $pdf=PDF::loadView('report.pdf.daily_late_pdf',compact('present','request','countPresent'));
//            $pdf->setPaper($pagesize, $pageOrientation);
//            $name=time()."_daily_late_attendance.pdf";
//            return view('report.pdf.daily_late_pdf',compact('present','request','lateTime','countPresent'));
//            return $pdf->download($name);
            return view('report.pdf.daily_late_pdf',compact('present','request','countPresent'));


        }
        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_Daily_Late_Present";
            Excel::create("$excelName", function($excel) use ($present, $countPresent, $request) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Daily Late Report");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Daily Late Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($present, $countPresent, $request) {
//                    // Sets all borders
//                    $sheet->setAllBorders('thin');
//                    $sheet->setBorder('A1', 'thin');
//                    $sheet->setBorder('A1:F10', 'thin');'allData','request','lateTime','countPresent','countAbsent','Tlate','totalL'
//                    $sheet->setOrientation('landscape');
//                    $sheet->fromArray($data, NULL, 'A3',false,false);
                    $sheet->loadView('report.excel.daily_late_excel')
                        ->with('present',$present)
                        ->with('request',$request)
                        ->with('countPresent',$countPresent);
                });

            })->download('xlsx');
        }
        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function daily_present_report(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        return view('report.attendance.daily_present_report',compact('units','departments','floors','sections','designations'));


        return view('report.attendance.daily_present_report');
    }

    public function daily_present_report_data(Request $request)
    {
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->daily_present_report();
        }
        // $lateTime=AttendanceController::late_time();
        $queryPresent=@"SELECT employees.*, attendance.id AS aid, attendance_setup.max_entry_time AS max_entry_time, attendance_setup.exit_time, attendance_setup.shiftName AS shiftName, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE employees.id>0 ";

        if (($request->unitId)!=0) {
            $queryPresent.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (($request->floorId)!=0) {
            $queryPresent.=" AND employees.floor_id='".$request->floorId."'";
        }

        if (($request->line_id)!=0) {
            $queryPresent.=" AND employees.line_id='".$request->line_id."'";
        }

        if (!empty($request->sectionName)) {
            $queryPresent.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $queryPresent.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $queryPresent.=" AND employees.empDesignationId='".$request->designationId."'";
        }
        if (($request->empGenderId)!=0) {
            $queryPresent.=" AND employees.empGenderId='".$request->empGenderId."'";
        }
        $date=Carbon::parse($request->date)->toDateString();

        $queryPresent.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryPresent.=" AND attendance.date='$date' ORDER BY employees.empFirstName";
//        return $queryPresent;
        $present=DB::select($queryPresent);
        $countPresent=count($present);

        if (($request->viewType)=="Preview") {
            return view('report.attendance.daily_present_report_data',compact('present','request','countPresent'));

        }elseif(($request->viewType)=="Generate PDF"){
//            $pagesize=$request->pagesize;
//            $pageOrientation=$request->pageOrientation;
//            $pdf=PDF::loadView('report.pdf.daily_present_pdf',compact('present','request','countPresent'));
//            $pdf->setPaper($pagesize, $pageOrientation);
//            $name=time()."_daily_present_report.pdf";
////            return view('report.pdf.daily_late_pdf',compact('present','request','lateTime','countPresent'));
//            return $pdf->download($name);
            return view('report.pdf.daily_present_pdf',compact('present','request','countPresent'));
        }

        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_Daily_Late_Present";
            Excel::create("$excelName", function($excel) use ($present, $countPresent, $request) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Daily Present Report");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Daily Present Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($present, $countPresent, $request) {
                    $sheet->loadView('report.excel.daily_present_excel')
                        ->with('present',$present)
                        ->with('request',$request)
                        ->with('countPresent',$countPresent);
                });

            })->download('xlsx');
        }


        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function daily_absent_report(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        return view('report.attendance.daily_absent_report',compact('units','departments','floors','sections','designations'));
    }
    public function daily_absent_report_data(Request $request){
//        return $request->all();
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->daily_absent_report();
        }


        $query=@"SELECT employees.*, MAX(attendance.date) as date, designations.designation,departments.id as d_id, departments.departmentName,tblines.line_no LineName, tblines.id as t_id, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id JOIN attendance ON employees.id=attendance.emp_id WHERE employees.id>0 ";

        if (($request->unitId)!=0) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (($request->floorId)!=0) {
            $query.=" AND employees.floor_id='".$request->floorId."'";
        }

        if (($request->line_id)!=0) {
            $query.=" AND employees.line_id='".$request->line_id."'";
        }

        if (!empty($request->sectionName)) {
            $section="";
            for($i=0;$i<\count($request->sectionName);$i++){
                $section.="'".$request->sectionName[$i]."', ";

            }
            $section= rtrim($section, ', ');
            $query.=" AND employees.empSection in (".$section.")";
        }

        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
        }
        if (($request->empGenderId)!=0) {
            $query.=" AND employees.empGenderId='".$request->empGenderId."'";
        }

        $date=Carbon::parse($request->date)->toDateString();

        $employee_id=DB::table('attendance')->where('date',$date)->select('emp_id')->get();
        $emp="0";
        foreach ($employee_id as $ei){
            $emp.=$ei->emp_id.", ";
        }
        $emp = rtrim($emp,", ");

        $query.=" AND employees.empJoiningDate <='$date'";
        $query.=" AND employees.empAccStatus='".$request->accStatus."'";
        $query.=" AND employees.id not in
                          ( $emp )";
        $query.=" AND employees.id not in 
                          (SELECT employee_id from tb_leave_application WHERE '$date' BETWEEN leave_starting_date and leave_ending_date and status='1')";

        if($request->viewType=="Preview Line Wise"){
            $lines=DB::table('tblines')->get(['id','line_no']);
            return view('report.attendance.daily_absent_data_line_wise',compact('query','request','lines'));

        }
        if($request->viewType=="Preview Department Wise"){
            $departments=DB::table('departments')->get(['id','departmentName']);
            return view('report.attendance.daily_absent_data_department_wise',compact('query','request','departments'));

        }

        if($request->viewType=="Excel Department Wise"){

            $departments=DB::table('departments')->get(['id','departmentName']);

            $excelName=time()."_Daily_Absent_Present_Line_Wise";
            Excel::create("$excelName", function($excel) use ($query, $departments, $request) {

                $name=Auth::user()->name;
                $excel->setTitle("Daily Absent Report Department Wise");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Daily Absent Report Department Wise');

                $excel->sheet('Sheet 1', function ($sheet) use ($query, $departments, $request) {
                    $sheet->loadView('report.excel.daily_absent_excel_department_wise')
                        ->with('query',$query)
                        ->with('request',$request)
                        ->with('departments',$departments);
                });

            })->download('xlsx');


            return view('report.attendance.daily_absent_data_department_wise',compact('query','request','departments'));

        }

        if(($request->viewType)=="PDF Department Wise"){
            $departments=DB::table('departments')->get(['id','departmentName']);
//            $pagesize=$request->pagesize;
//            $pageOrientation=$request->pageOrientation;
//            $pdf=PDF::loadView('report.pdf.daily_absent_department_wise_pdf',compact('query','request','departments'));
//            $pdf->setPaper($pagesize, $pageOrientation);
//            $name=time()."_daily_absent_department_wise.pdf";
//            return $pdf->download($name);
            return view('report.pdf.daily_absent_department_wise_pdf',compact('query','request','departments'));
        }

        if(($request->viewType)=="PDF Line Wise"){
            $lines=DB::table('tblines')->get(['id','line_no']);
//            $pagesize=$request->pagesize;
//            $pageOrientation=$request->pageOrientation;
//            $pdf=PDF::loadView('report.pdf.daily_absent_line_wise_pdf',compact('query','request','lines'));
//            $pdf->setPaper($pagesize, $pageOrientation);
//            $name=time()."_daily_absent_line_wise.pdf";
//            return $pdf->download($name);
            return view('report.pdf.daily_absent_line_wise_pdf',compact('query','request','lines'));
        }

        if($request->viewType=="Excel Line Wise"){
            $lines=DB::table('tblines')->get(['id','line_no']);

            $excelName=time()."_Daily_Absent_Present_Line_Wise";
            Excel::create("$excelName", function($excel) use ($query, $lines, $request) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Daily Absent Report Line Wise");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Daily Absent Report Line Wise');

                $excel->sheet('Sheet 1', function ($sheet) use ($query, $lines, $request) {
//                    // Sets all borders
//                    $sheet->setAllBorders('thin');
//                    $sheet->setBorder('A1', 'thin');
//                    $sheet->setBorder('A1:F10', 'thin');'allData','request','lateTime','countPresent','countAbsent','Tlate','totalL'
//                    $sheet->setOrientation('landscape');
//                    $sheet->fromArray($data, NULL, 'A3',false,false);
                    $sheet->loadView('report.excel.daily_absent_excel_line_wise')
                        ->with('query',$query)
                        ->with('request',$request)
                        ->with('lines',$lines);
                });

            })->download('xlsx');

        }

        $query.=" GROUP BY employees.id ORDER BY attendance.date DESC";
        $absent=DB::select($query);
        $countAbsent=count($absent);

        if(($request->viewType)=="Preview") {
            return view('report.attendance.daily_absent_report_data',compact('absent','request','countAbsent'));

        }

        elseif(($request->viewType)=="Generate PDF"){
//            $pagesize=$request->pagesize;
//            $pageOrientation=$request->pageOrientation;
//            $pdf=PDF::loadView('report.pdf.daily_absent_pdf',compact('absent','request','countAbsent'));
//            $pdf->setPaper($pagesize, $pageOrientation);
//            $name=time()."_daily_absent.pdf";
//            return $pdf->download($name);
            return view('report.pdf.daily_absent_pdf',compact('absent','request','countAbsent'));
        }

        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_Daily_Absent_Present";
            Excel::create("$excelName", function($excel) use ($absent, $countAbsent, $request) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Daily Absent Report");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Daily Absent Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($absent, $countAbsent, $request) {
//                    // Sets all borders
//                    $sheet->setAllBorders('thin');
//                    $sheet->setBorder('A1', 'thin');
//                    $sheet->setBorder('A1:F10', 'thin');'allData','request','lateTime','countPresent','countAbsent','Tlate','totalL'
//                    $sheet->setOrientation('landscape');
//                    $sheet->fromArray($data, NULL, 'A3',false,false);
                    $sheet->loadView('report.excel.daily_absent_excel')
                        ->with('absent',$absent)
                        ->with('request',$request)
                        ->with('countAbsent',$countAbsent);
                });

            })->download('xlsx');
        }


        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function date_wise_late(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees=DB::table('employees')->where('empAccStatus','=',1)->get(['empFirstName','employeeId','id']);
        return view('report.attendance.date_wise_late',compact('employees','units','departments','floors','sections','designations'));
    }

    public function date_wise_late_data(Request $request){
//        return $request->all();

        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_late();
        }
        elseif(!$helper->checkIsAValidDate($request->endDate)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_late();
        }


        $lateTime=AttendanceController::late_time();
        $date=Carbon::parse($request->date)->toDateString();
        $endDate=Carbon::parse($request->endDate)->toDateString();
        if($request->emp_id!=0){
            $queryPresent=@"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id='".$request->emp_id."'";
        }

        else {
            $queryPresent = @"SELECT employees.*, attendance_setup.max_entry_time, attendance_setup.exit_time, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";

            if (($request->unitId) != 0) {
                $queryPresent .= " AND employees.unit_id='" . $request->unitId . "'";
            }

            if (($request->floorId) != 0) {
                $queryPresent .= " AND employees.floor_id='" . $request->floorId . "'";
            }

            if (($request->line_id) != 0) {
                $queryPresent .= " AND employees.line_id='" . $request->line_id . "'";
            }

            if (!empty($request->sectionName)) {
                $queryPresent .= " AND employees.empSection='" . $request->sectionName . "'";
            }

            if (($request->departmentId) != 0) {
                $queryPresent .= " AND employees.empDepartmentId='" . $request->departmentId . "'";
            }

            if (($request->designationId) != 0) {
                $queryPresent .= " AND employees.empDesignationId='" . $request->designationId . "'";
            }
            if (($request->empGenderId) != 0) {
                $queryPresent .= " AND employees.empGenderId='" . $request->empGenderId . "'";
            }


            $queryPresent .= " AND employees.empAccStatus='" . $request->accStatus . "'";
        }


        $queryPresent.=" AND attendance.in_time>attendance_setup.max_entry_time AND attendance.date BETWEEN '$date' and '$endDate' ORDER BY employees.empFirstName";

        $present=DB::select($queryPresent);
        $countPresent=count($present);
//        return $present;

        if (($request->viewType)=="Preview") {
            return view('report.attendance.date_wise_late_data',compact('present','request', 'lateTime', 'countPresent'));

        }elseif(($request->viewType)=="Generate PDF"){
            if($countPresent>500){
                return view('report.pdf.date_wise_late_pdf',compact('present','request','lateTime','countPresent'));
            }
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.date_wise_late_pdf',compact('present','request','lateTime','countPresent'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_date_wise_late.pdf";
            return $pdf->download($name);
        }

        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_Date_Wise_Late";
            Excel::create("$excelName", function($excel) use ($present, $lateTime, $request, $countPresent) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Date Wise Late");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Date Wise Late Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($present, $lateTime, $request,$countPresent) {
//                    // Sets all borders
//                    $sheet->setAllBorders('thin');
//                    $sheet->setBorder('A1', 'thin');
//                    $sheet->setBorder('A1:F10', 'thin');'allData','request','lateTime','countPresent','countAbsent','Tlate','totalL'
//                    $sheet->setOrientation('landscape');
//                    $sheet->fromArray($data, NULL, 'A3',false,false);
                    $sheet->loadView('report.excel.date_wise_late_excel')
                        ->with('present',$present)
                        ->with('request',$request)
                        ->with('countPresent',$countPresent)
                        ->with('lateTime',$lateTime);
                });

            })->download('xlsx');
        }

        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }

    }
    public function date_wise_present(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees=DB::table('employees')->get(['empFirstName','empLastName','id']);
        return view('report.attendance.date_wise_present',compact('employees','units','departments','floors','sections','designations'));

    }

    public function date_wise_present_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_present();
        }
        elseif(!$helper->checkIsAValidDate($request->endDate)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_present();
        }
        $date=Carbon::parse($request->date)->toDateString();
        $endDate=Carbon::parse($request->endDate)->toDateString();
        if($request->emp_id!=0){
            $queryPresent=@"SELECT employees.*, attendance_setup.max_entry_time, attendance_setup.exit_time, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN attendance_setup ON employees.empShiftId = attendance_setup.id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id='".$request->emp_id."'";
        }
        else {
            $queryPresent = @"SELECT employees.*, attendance_setup.max_entry_time, attendance_setup.exit_time, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN attendance_setup ON employees.empShiftId = attendance_setup.id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";

            if (($request->unitId) != 0) {
                $queryPresent .= " AND employees.unit_id='" . $request->unitId . "'";
            }

            if (($request->floorId) != 0) {
                $queryPresent .= " AND employees.floor_id='" . $request->floorId . "'";
            }

            if (($request->line_id) != 0) {
                $queryPresent .= " AND employees.line_id='" . $request->line_id . "'";
            }

            if (!empty($request->sectionName)) {
                $queryPresent .= " AND employees.empSection='" . $request->sectionName . "'";
            }

            if (($request->departmentId) != 0) {
                $queryPresent .= " AND employees.empDepartmentId='" . $request->departmentId . "'";
            }

            if (($request->designationId) != 0) {
                $queryPresent .= " AND employees.empDesignationId='" . $request->designationId . "'";
            }
            if (($request->empGenderId) != 0) {
                $queryPresent .= " AND employees.empGenderId='" . $request->empGenderId . "'";
            }


            $queryPresent .= " AND employees.empAccStatus='" . $request->accStatus . "'";
        }

        $queryPresent.=" AND attendance.date BETWEEN '$date' and '$endDate' ORDER BY employees.empFirstName";
        $present=DB::select($queryPresent);
        $countPresent=count($present);

        if (($request->viewType)=="Preview") {
            return view('report.attendance.date_wise_present_data',compact('present','request', 'countPresent'));

        }elseif(($request->viewType)=="Generate PDF"){
            if($countPresent>500){
                return view('report.pdf.date_wise_present_pdf',compact('present','request','countPresent'));
            }
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.date_wise_present_pdf',compact('present','request','countPresent'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_admin_date_wise_present.pdf";
            return $pdf->download($name);
        }

        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."Admin_Date_Wise_Present";
            Excel::create("$excelName", function($excel) use ($present, $request, $countPresent) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Date Wise Present");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Date Wise Present Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($present, $request,$countPresent) {
                    $sheet->loadView('report.excel.date_wise_present_excel')
                        ->with('present',$present)
                        ->with('request',$request)
                        ->with('countPresent',$countPresent);
                });

            })->download('xlsx');
        }



        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function public_date_wise_present(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees=DB::table('employees')->where('empAccStatus','=',1)->get(['empFirstName','employeeId','id']);
        return view('report.attendance.public_date_wise_present',compact('employees','units','departments','floors','sections','designations'));

    }

    public function public_date_wise_present_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->public_date_wise_present();
        }
        elseif(!$helper->checkIsAValidDate($request->endDate)){
            Session::flash('message','Incorrect date format.');
            return $this->public_date_wise_present();
        }
        $date=Carbon::parse($request->date)->toDateString();
        $endDate=Carbon::parse($request->endDate)->toDateString();
        if($request->emp_id!=0){
            $queryPresent=@"SELECT employees.*, attendance_setup.max_entry_time, attendance_setup.exit_time, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id='".$request->emp_id."'";
        }
        else {
            $queryPresent = @"SELECT employees.*, attendance_setup.max_entry_time, attendance_setup.exit_time, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";

            if (($request->unitId) != 0) {
                $queryPresent .= " AND employees.unit_id='" . $request->unitId . "'";
            }

            if (($request->floorId) != 0) {
                $queryPresent .= " AND employees.floor_id='" . $request->floorId . "'";
            }

            if (($request->line_id) != 0) {
                $queryPresent .= " AND employees.line_id='" . $request->line_id . "'";
            }

            if (!empty($request->sectionName)) {
                $queryPresent .= " AND employees.empSection='" . $request->sectionName . "'";
            }

            if (($request->departmentId) != 0) {
                $queryPresent .= " AND employees.empDepartmentId='" . $request->departmentId . "'";
            }

            if (($request->designationId) != 0) {
                $queryPresent .= " AND employees.empDesignationId='" . $request->designationId . "'";
            }
            if (($request->empGenderId) != 0) {
                $queryPresent .= " AND employees.empGenderId='" . $request->empGenderId . "'";
            }


            $queryPresent .= " AND employees.empAccStatus='" . $request->accStatus . "'";
        }

        $queryPresent.=" AND attendance.date BETWEEN '$date' and '$endDate' ORDER BY employees.empFirstName";
        $present=DB::select($queryPresent);
        $countPresent=count($present);

        if (($request->viewType)=="Preview") {
            return view('report.attendance.public_date_wise_present_data',compact('present','request', 'countPresent'));

        }elseif(($request->viewType)=="Generate PDF"){
            if($countPresent >800 ){
                return view('report.pdf.public_date_wise_present_pdf',compact('present','request','countPresent'));
            }
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.public_date_wise_present_pdf',compact('present','request','countPresent'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_date_wise_present.pdf";
            return $pdf->download($name);
        }

        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."Date_Wise_Present";
            Excel::create("$excelName", function($excel) use ($present, $request, $countPresent) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Date Wise Present");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Date Wise Present Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($present, $request,$countPresent) {
                    $sheet->loadView('report.excel.public_date_wise_present_excel')
                        ->with('present',$present)
                        ->with('request',$request)
                        ->with('countPresent',$countPresent);
                });

            })->download('xlsx');
        }



        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function date_wise_absent(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees=DB::table('employees')->where('empAccStatus','=',1)->get(['empFirstName','employeeId','id']);
        return view('report.attendance.date_wise_absent',compact('employees','units','departments','floors','sections','designations'));
    }

    public function date_wise_absent_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_absent();
        }
        elseif(!$helper->checkIsAValidDate($request->endDate)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_absent();
        }
        $weekends = [];
        $weekend = DB::table('week_leave')->where(['status' => 1])->get();
        $festivalLeave = DB::table('tb_festival_leave')->get();
        $approvedLeave=DB::table('tb_leave_application')->where(['employee_id'=>$request->emp_id, 'status'=>1])->get();
//        return $approvedLeave;
        foreach ($weekend as $w) {
            $weekends[] = $w->day;
        }


        $date=Carbon::parse($request->date)->toDateString();
        $endDate=Carbon::parse($request->endDate)->toDateString();
        $absentData=[];

        if($request->emp_id!=0){
            $queryPresent=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees  LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id='".$request->emp_id."'";
        }
        else {
            $queryPresent = @"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";

            if (($request->unitId) != 0) {
                $queryPresent .= " AND employees.unit_id='" . $request->unitId . "'";
            }

            if (($request->floorId) != 0) {
                $queryPresent .= " AND employees.floor_id='" . $request->floorId . "'";
            }

            if (($request->line_id) != 0) {
                $queryPresent .= " AND employees.line_id='" . $request->line_id . "'";
            }

            if (!empty($request->sectionName)) {
                $queryPresent .= " AND employees.empSection='" . $request->sectionName . "'";
            }

            if (($request->departmentId) != 0) {
                $queryPresent .= " AND employees.empDepartmentId='" . $request->departmentId . "'";
            }

            if (($request->designationId) != 0) {
                $queryPresent .= " AND employees.empDesignationId='" . $request->designationId . "'";
            }
            if (($request->empGenderId) != 0) {
                $queryPresent .= " AND employees.empGenderId='" . $request->empGenderId . "'";
            }
            $queryPresent .= " AND employees.empAccStatus='" . $request->accStatus . "'";
        }

        $rightQuery=$queryPresent;

        for ($i = Carbon::createFromFormat('Y-m-d', $date); $i->lte(Carbon::createFromFormat('Y-m-d', $endDate)); $i->addDay(1)) {
//            $i=Carbon::parse($i)->toDateString();
            $key=0;
            $fes=0;
            $lev=0;
            $dates = $i->format('Y-m-d');
            $timestamp = strtotime($dates);

            $day = date('l', $timestamp);
            foreach ($festivalLeave as $fe){
                $fesDayStart=strtotime($fe->start_date);
                $fesDayEnd=strtotime($fe->end_date);
                if($timestamp >=$fesDayStart && $timestamp<=$fesDayEnd){
                    $fes=1;
                    break;
                }

            }
            foreach ($approvedLeave as $al){
                if($timestamp >=strtotime($al->leave_starting_date) && $timestamp<=strtotime($al->leave_ending_date))
                {
                    $lev=1;
                    break;
                }
            }
            for($j=0;$j<count($weekends);$j++)
            {
                if($day==$weekends[$j]){
                    $key=1;
                    break;
                }
            }

            if($fes !=1 && $lev !=1 && $key!=1) {
                $ld=Carbon::parse($i)->toDateString();
                $queryPresent .= " AND employees.id NOT IN (select emp_id from attendance WHERE date ='" . $ld . "')";
                $queryPresent .= " AND employees.id NOT IN 
                          (SELECT employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1')";
                $absent = DB::select($queryPresent);
                foreach ($absent as $a){
                    $a->date=$dates;
                }
                $absentData = array_merge($absent, $absentData);
                $queryPresent = $rightQuery;
            }

        }

        $countAbsent=count($absentData);

        if (($request->viewType)=="Preview") {
//            return
            return view('report.attendance.date_wise_absent_data',compact('absentData','request'));

        }elseif(($request->viewType)=="Generate PDF"){
//            return $countAbsent;
            if($countAbsent>500){
                return view('report.pdf.date_wise_absent_pdf',compact('absentData','request'));
            }
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.date_wise_absent_pdf',compact('absentData','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_date_wise_absent.pdf";
            return $pdf->download($name);
        }

        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_Date_Wise_Absent";
            Excel::create("$excelName", function($excel) use ($absentData, $request,$countAbsent) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Date Wise Absent");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Date Wise Absent Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($absentData, $request,$countAbsent) {
//                    // Sets all borders
//                    $sheet->setAllBorders('thin');
//                    $sheet->setBorder('A1', 'thin');
//                    $sheet->setBorder('A1:F10', 'thin');'allData','request','lateTime','countPresent','countAbsent','Tlate','totalL'
//                    $sheet->setOrientation('landscape');
//                    $sheet->fromArray($data, NULL, 'A3',false,false);
                    $sheet->loadView('report.excel.date_wise_absent_excel')
                        ->with('absentData',$absentData)
                        ->with('request',$request)
                        ->with('countAbsent',$countAbsent);
                });

            })->download('xlsx');
        }


        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function daily_overtime_report(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees=DB::table('employees')->get(['empFirstName','empLastName','id']);
        return view('report.attendance.daily_overtime_report',compact('employees','units','departments','floors','sections','designations'));

    }

    public function daily_overtime_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->daily_overtime_report();
        }
        $overtime_start=OvertimeController::overtime_start();
        $overtime_end=OvertimeController::overtime_end();
        $queryPresent=@"SELECT employees.*, attendance_setup.max_entry_time, attendance_setup.exit_time, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";

        if (($request->unitId)!=0) {
            $queryPresent.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (($request->floorId)!=0) {
            $queryPresent.=" AND employees.floor_id='".$request->floorId."'";
        }

        if (($request->line_id)!=0) {
            $queryPresent.=" AND employees.line_id='".$request->line_id."'";
        }

        if (!empty($request->sectionName)) {
            $queryPresent.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $queryPresent.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $queryPresent.=" AND employees.empDesignationId='".$request->designationId."'";
        }
        if (($request->empGenderId)!=0) {
            $queryPresent.=" AND employees.empGenderId='".$request->empGenderId."'";
        }
        $date=Carbon::parse($request->date)->toDateString();

        $queryPresent.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryPresent.=" AND attendance.date='$date' AND attendance.out_time>'$overtime_start' ORDER BY employees.empFirstName";
        $present=DB::select($queryPresent);
        $countPresent=count($present);

        if (($request->viewType)=="Preview") {
            return view('report.attendance.daily_overtime_data',compact('present','request','countPresent','overtime_end'));

        }elseif(($request->viewType)=="Generate PDF"){
//            $pagesize=$request->pagesize;
//            $pageOrientation=$request->pageOrientation;
//            $pdf=PDF::loadView('report.pdf.daily_overtime_pdf',compact('present','request','countPresent','overtime_end'));
//            $pdf->setPaper($pagesize, $pageOrientation);
//            $name=time()."_daily_overtime_report.pdf";
//            return $pdf->download($name);
            return view('report.pdf.daily_overtime_pdf',compact('present','request','countPresent','overtime_end'));
        }
        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_Daily_Overtime_Report";
            Excel::create("$excelName", function($excel) use ($present, $request,$countPresent,$overtime_end) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Daily Overtime Report");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Daily Overtime Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($present, $request,$countPresent,$overtime_end) {
//                    // Sets all borders
//                    $sheet->setAllBorders('thin');
//                    $sheet->setBorder('A1', 'thin');
//                    $sheet->setBorder('A1:F10', 'thin');'allData','request','lateTime','countPresent','countAbsent','Tlate','totalL'
//                    $sheet->setOrientation('landscape');
//                    $sheet->fromArray($data, NULL, 'A3',false,false);
                    $sheet->loadView('report.excel.daily_overtime_excel')
                        ->with('present',$present)
                        ->with('request',$request)
                        ->with('countPresent',$countPresent)
                        ->with('overtime_end',$overtime_end);
                });

            })->download('xlsx');
        }

        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function date_wise_overtime(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees=DB::table('employees')->where('empAccStatus','=',1)->get(['empFirstName','employeeId','id']);
        return view('report.attendance.date_wise_overtime',compact('employees','units','departments','floors','sections','designations'));

    }
    public function date_wise_overtime_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_overtime();
        }
        elseif(!$helper->checkIsAValidDate($request->endDate)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_overtime();
        }


        $lateTime=AttendanceController::late_time();
        $out_time=AttendanceController::out_time();
        $overtime_start=OvertimeController::overtime_start();
        $overtime_end=OvertimeController::overtime_end();
        if($request->emp_id!=0){
            $queryPresent=@"SELECT employees.*, attendance_setup.max_entry_time,attendance_setup.exit_time, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id='".$request->emp_id."'";
        }
        else {
            $queryPresent = @"SELECT employees.*, attendance_setup.max_entry_time,attendance_setup.exit_time, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";

            if (($request->unitId) != 0) {
                $queryPresent .= " AND employees.unit_id='" . $request->unitId . "'";
            }

            if (($request->floorId) != 0) {
                $queryPresent .= " AND employees.floor_id='" . $request->floorId . "'";
            }

            if (($request->line_id) != 0) {
                $queryPresent .= " AND employees.line_id='" . $request->line_id . "'";
            }

            if (!empty($request->sectionName)) {
                $queryPresent .= " AND employees.empSection='" . $request->sectionName . "'";
            }

            if (($request->departmentId) != 0) {
                $queryPresent .= " AND employees.empDepartmentId='" . $request->departmentId . "'";
            }

            if (($request->designationId) != 0) {
                $queryPresent .= " AND employees.empDesignationId='" . $request->designationId . "'";
            }
            if (($request->empGenderId) != 0) {
                $queryPresent .= " AND employees.empGenderId='" . $request->empGenderId . "'";
            }
        }
        $date=Carbon::parse($request->date)->toDateString();
        $endDate=Carbon::parse($request->endDate)->toDateString();

        $queryPresent.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryPresent.=" AND attendance.date BETWEEN '$date' and '$endDate' AND attendance.out_time>'$overtime_start' ORDER BY employees.empFirstName";
//        return $queryPresent;
        $present=DB::select($queryPresent);
        $countPresent=count($present);

        if (($request->viewType)=="Preview") {
            return view('report.attendance.date_wise_overtime_data',compact('present','request','lateTime','countPresent','out_time','overtime_end'));

        }elseif(($request->viewType)=="Generate PDF"){
            if($countPresent>500){
                return view('report.pdf.date_wise_overtime_pdf', compact('present','request','lateTime','countPresent','out_time','overtime_end'));
            }
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.date_wise_overtime_pdf',compact('present','request','lateTime','countPresent','out_time','overtime_end'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_date_wise_overtime.pdf";
            return $pdf->download($name);
        }

        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_date_wise_overtime";
            Excel::create("$excelName", function($excel) use ($present, $request,$countPresent,$lateTime, $out_time, $overtime_end) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Date Wise Overtime");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Date Wise Overtime Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($present, $lateTime, $request,$countPresent, $out_time, $overtime_end) {
                    $sheet->loadView('report.excel.date_wise_overtime_excel')
                        ->with('present',$present)
                        ->with('request',$request)
                        ->with('lateTime',$lateTime)
                        ->with('out_time',$out_time)
                        ->with('overtime_end',$overtime_end)
                        ->with('countPresent',$countPresent);
                });

            })->download('xlsx');
        }


        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }

    }

    public function admin_date_wise_overtime(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees=DB::table('employees')->get(['empFirstName','empLastName','id']);
        return view('report.attendance.admin_date_wise_overtime',compact('employees','units','departments','floors','sections','designations'));

    }

    public function admin_date_wise_overtime_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->admin_date_wise_overtime();
        }
        elseif(!$helper->checkIsAValidDate($request->endDate)){
            Session::flash('message','Incorrect date format.');
            return $this->admin_date_wise_overtime();
        }

        $lateTime=AttendanceController::late_time();
        $out_time=AttendanceController::out_time();
        $overtime_start=OvertimeController::overtime_start();
        $overtime_end=OvertimeController::overtime_end();
        if($request->emp_id!=0){
            $queryPresent=@"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,
attendance_setup.exit_time, attendance_setup.max_entry_time,
designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName 
FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations 
ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id 
LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors 
ON tblines.floor_id=floors.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE employees.id='".$request->emp_id."'";
        }
        else {
            $queryPresent = @"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date, attendance_setup.exit_time, attendance_setup.max_entry_time, designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE employees.id>0 ";

            if (($request->unitId) != 0) {
                $queryPresent .= " AND employees.unit_id='" . $request->unitId . "'";
            }

            if (($request->floorId) != 0) {
                $queryPresent .= " AND employees.floor_id='" . $request->floorId . "'";
            }

            if (($request->line_id) != 0) {
                $queryPresent .= " AND employees.line_id='" . $request->line_id . "'";
            }

            if (!empty($request->sectionName)) {
                $queryPresent .= " AND employees.empSection='" . $request->sectionName . "'";
            }

            if (($request->departmentId) != 0) {
                $queryPresent .= " AND employees.empDepartmentId='" . $request->departmentId . "'";
            }

            if (($request->designationId) != 0) {
                $queryPresent .= " AND employees.empDesignationId='" . $request->designationId . "'";
            }
            if (($request->empGenderId) != 0) {
                $queryPresent .= " AND employees.empGenderId='" . $request->empGenderId . "'";
            }
        }
        $date=Carbon::parse($request->date)->toDateString();
        $endDate=Carbon::parse($request->endDate)->toDateString();

        $queryPresent.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryPresent.=" AND employees.empOTStatus=1";
        $queryPresent.=" AND attendance.date BETWEEN '$date' and '$endDate' AND attendance.out_time>'$overtime_start' ORDER BY employees.empFirstName";
        $present=DB::select($queryPresent);
        $countPresent=count($present);

        if (($request->viewType)=="Preview") {
            return view('report.attendance.admin_date_wise_overtime_data',compact('present','request','lateTime','countPresent','out_time','overtime_end'));

        }
        elseif(($request->viewType)=="Generate PDF"){
            if($countPresent>500){
                return view('report.pdf.admin_date_wise_overtime_pdf',compact('present','request','lateTime','countPresent','out_time','overtime_end'));
            }
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.admin_date_wise_overtime_pdf',compact('present','request','lateTime','countPresent','out_time','overtime_end'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_admin_date_wise_overtime.pdf";
            return $pdf->download($name);
        }

        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_date_wise_overtime_admin";
            Excel::create("$excelName", function($excel) use ($present, $request,$countPresent,$lateTime, $out_time, $overtime_end) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Date Wise Overtime Private");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Date Wise Overtime Private');

                $excel->sheet('Sheet 1', function ($sheet) use ($present, $lateTime, $request,$countPresent, $out_time, $overtime_end) {
                    $sheet->loadView('report.excel.admin_date_wise_overtime_excel')
                        ->with('present',$present)
                        ->with('request',$request)
                        ->with('lateTime',$lateTime)
                        ->with('out_time',$out_time)
                        ->with('overtime_end',$overtime_end)
                        ->with('countPresent',$countPresent);
                });

            })->download('xlsx');
        }


        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function attendance_summary(){
        return view('report.attendance.attendance_summary');
    }

    public function attendance_summary_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->attendance_summary();
        }
        elseif(!$helper->checkIsAValidDate($request->endDate)){
            Session::flash('message','Incorrect date format.');
            return $this->attendance_summary();
        }
        $lateTime=AttendanceController::late_time();
        $weekends = [];
        $weekend = DB::table('week_leave')->where(['status' => 1])->get();
        $festivalLeave = DB::table('tb_festival_leave')->get();
        foreach ($weekend as $w) {
            $weekends[] = $w->day;
        }

        $totalAttendance=[];
        $date=Carbon::parse($request->date)->toDateString();
        $endDate=Carbon::parse($request->endDate)->toDateString();
        for ($i = Carbon::createFromFormat('Y-m-d', $date); $i->lte(Carbon::createFromFormat('Y-m-d', $endDate)); $i->addDay(1)) {
            $key=0;
            $dates = $i->format('Y-m-d');
            $timestamp = strtotime($dates);

            $day = date('l', $timestamp);
            foreach ($festivalLeave as $fe){
                $fesDayStart=strtotime($fe->start_date);
                $fesDayEnd=strtotime($fe->end_date);
                if($timestamp >=$fesDayStart && $timestamp<=$fesDayEnd){
                    $key=1;
                    break;
                }

            }
//            foreach ($approvedLeave as $al){
//                if($timestamp >=strtotime($al->leave_starting_date) && $timestamp<=strtotime($al->leave_ending_date))
//                {
//                    $key=2;
//                    break;
//                }
//            }
            for($j=0;$j<count($weekends);$j++)
            {
                if($day==$weekends[$j]){
                    $key=2;
                    break;
                }
            }

            $date=Carbon::parse($i)->toDateString();
            $d=Carbon::parse($i)->toDateString();
//            return $d;
            $attendance=DB::select("select COUNT(*) as attendance FROM attendance WHERE date='$date'");
            $late=DB::select("select COUNT(*) as late FROM attendance LEFT JOIN  employees ON  attendance.emp_id= employees.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE date='$date' and in_time>attendance_setup.max_entry_time");
            $query="SELECT COUNT(*) AS leaves FROM `tb_leave_application` WHERE '$d' BETWEEN leave_starting_date and leave_ending_date AND status=1";
            $leaves=DB::select($query);
//            return $leaves;
//            return $query;
            foreach ($attendance as $a)
            {
                $a->date=$date;
                foreach ($late as $l){
                    $a->late=$l->late;
                    $a->key=$key;
                }
                $a->leaves=$leaves[0]->leaves;
            }
            $totalAttendance=array_merge($attendance,$totalAttendance);
        }
        if (($request->viewType)=="Preview") {
            return view('report.attendance.attendance_summery_data', compact('totalAttendance', 'request'));
        }
        elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
//            return view('report.pdf.attendance_summery_pdf',compact('totalAttendance','request'));
            $pdf=PDF::loadView('report.pdf.attendance_summery_pdf',compact('totalAttendance','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_attendance.pdf";
            return $pdf->download($name);
        }

        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_attendance_summery";
            Excel::create("$excelName", function($excel) use ($totalAttendance, $request) {

                $name=Auth::user()->name;
                $excel->setTitle("Attendance Summery");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Attendance Summery');

                $excel->sheet('Sheet 1', function ($sheet) use ($totalAttendance, $request) {
                    $sheet->loadView('report.excel.attendance_summery_excel')
                        ->with('totalAttendance',$totalAttendance)
                        ->with('request',$request);
                });

            })->download('xlsx');
        }

        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function daily_attendance_exception(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees=DB::table('employees')->get(['empFirstName','empLastName','id']);
        return view('report.attendance.daily_attendance_exception',compact('employees','units','departments','floors','sections','designations'));
    }

    public function daily_attendance_exception_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->daily_attendance_exception();
        }


        $lateTime=AttendanceController::late_time();
        $queryPresent=@"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";

        $query=@"SELECT employees.*, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date, designations.designation,departments.id as d_id, departments.departmentName,tblines.line_no LineName, tblines.id as t_id, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";

        if (($request->unitId)!=0) {
            $queryPresent.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (($request->floorId)!=0) {
            $queryPresent.=" AND employees.floor_id='".$request->floorId."'";
        }

        if (($request->line_id)!=0) {
            $queryPresent.=" AND employees.line_id='".$request->line_id."'";
        }

        if (!empty($request->sectionName)) {
            $queryPresent.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $queryPresent.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $queryPresent.=" AND employees.empDesignationId='".$request->designationId."'";
        }
        if (($request->empGenderId)!=0) {
            $queryPresent.=" AND employees.empGenderId='".$request->empGenderId."'";
        }
        $date=Carbon::parse($request->date)->toDateString();

        $queryPresent.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryPresent.=" AND attendance.date='$date' AND attendance.in_time=attendance.out_time ORDER BY employees.empFirstName";
//        return $queryPresent;
        $present=DB::select($queryPresent);
        $countPresent=count($present);

        $query.=" AND employees.empAccStatus='".$request->accStatus."'";
        $query.=" AND attendance.date='$date' AND attendance.in_time=attendance.out_time";

        if($request->viewType=="Preview Department Wise"){
            $departments=DB::table('departments')->get(['id','departmentName']);
            return view('report.attendance.daily_attendance_exception_department_wise',compact('query','request','departments'));

        }


        if($request->viewType=="Excel Department Wise"){

            $departments=DB::table('departments')->get(['id','departmentName']);

            $excelName=time()."_Daily_Attendance_Exception_Department_Wise";
            Excel::create("$excelName", function($excel) use ($query, $departments, $request) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Daily Absent Report Department Wise");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Daily Attendance Exception Department Wise');

                $excel->sheet('Sheet 1', function ($sheet) use ($query, $departments, $request) {
//                    // Sets all borders
//                    $sheet->setAllBorders('thin');
//                    $sheet->setBorder('A1', 'thin');
//                    $sheet->setBorder('A1:F10', 'thin');'allData','request','lateTime','countPresent','countAbsent','Tlate','totalL'
//                    $sheet->setOrientation('landscape');
//                    $sheet->fromArray($data, NULL, 'A3',false,false);
                    $sheet->loadView('report.excel.exception_d_w')
                        ->with('query',$query)
                        ->with('request',$request)
                        ->with('departments',$departments);
                });

            })->download('xlsx');


            return view('report.attendance.daily_absent_data_department_wise',compact('query','request','departments'));

        }

        if(($request->viewType)=="PDF Department Wise"){
            $departments=DB::table('departments')->get(['id','departmentName']);
//            $pagesize=$request->pagesize;
//            $pageOrientation=$request->pageOrientation;
//            $pdf=PDF::loadView('report.pdf.daily_exception_department_wise_pdf',compact('query','request','departments'));
//            $pdf->setPaper($pagesize, $pageOrientation);
//            $name=time()."_daily_exception_department_wise.pdf";
//            return $pdf->download($name);
            return view('report.pdf.daily_exception_department_wise_pdf',compact('query','request','departments'));
        }



        if (($request->viewType)=="Preview") {
            return view('report.attendance.daily_attendance_exception_data',compact('present','request','lateTime','countPresent'));

        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.daily_attendance_exception_pdf',compact('present','request','countPresent','lateTime'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_daily_attendance_exception.pdf";
            return $pdf->download($name);
        }
        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_Daily_Attendance_Exception";
            Excel::create("$excelName", function($excel) use ($present, $request,$countPresent,$lateTime) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Daily Overtime Report");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Daily Overtime Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($present, $lateTime, $request,$countPresent) {
                    $sheet->loadView('report.excel.daily_attendance_exception_excel')
                        ->with('present',$present)
                        ->with('request',$request)
                        ->with('lateTime',$lateTime)
                        ->with('countPresent',$countPresent);
                });

            })->download('xlsx');
        }

        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }


    public function date_wise_inout_time_report(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees=DB::table('employees')->where('empAccStatus','=',1)->get(['empFirstName','employeeId','id']);
        return view('report.attendance.date_wise_inout_time_report',compact('employees','units','departments','floors','sections','designations'));
    }

    public function date_wise_inout_time_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_overtime();
        }
        elseif(!$helper->checkIsAValidDate($request->endDate)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_overtime();
        }

        $lateTime=AttendanceController::late_time();
        if($request->emp_id!=0){
            $queryPresent=@"SELECT employees.*, attendance_setup.max_entry_time,attendance_setup.exit_time, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id='".$request->emp_id."'";
        }
        else {
            $queryPresent = @"SELECT employees.*, attendance_setup.max_entry_time,attendance_setup.exit_time, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";

            if (($request->unitId) != 0) {
                $queryPresent .= " AND employees.unit_id='" . $request->unitId . "'";
            }

            if (($request->floorId) != 0) {
                $queryPresent .= " AND employees.floor_id='" . $request->floorId . "'";
            }

            if (($request->line_id) != 0) {
                $queryPresent .= " AND employees.line_id='" . $request->line_id . "'";
            }

            if (!empty($request->sectionName)) {
                $queryPresent .= " AND employees.empSection='" . $request->sectionName . "'";
            }

            if (($request->departmentId) != 0) {
                $queryPresent .= " AND employees.empDepartmentId='" . $request->departmentId . "'";
            }

            if (($request->designationId) != 0) {
                $queryPresent .= " AND employees.empDesignationId='" . $request->designationId . "'";
            }
            if (($request->empGenderId) != 0) {
                $queryPresent .= " AND employees.empGenderId='" . $request->empGenderId . "'";
            }
        }
        $date=Carbon::parse($request->date)->toDateString();
        $endDate=Carbon::parse($request->endDate)->toDateString();

        $queryPresent.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryPresent.=" AND attendance.date BETWEEN '$date' and '$endDate' ORDER BY employees.empFirstName";
        $present=DB::select($queryPresent);
        $countPresent=count($present);

        if (($request->viewType)=="Preview") {
            return view('report.attendance.date_wise_inout_time_data', compact('present','request','lateTime','countPresent'));

        }
        elseif(($request->viewType)=="Generate PDF"){
            if($countPresent>500){
                return view('report.pdf.date_wise_inout_time_pdf', compact('present','request','lateTime','countPresent'));
            }
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.date_wise_inout_time_pdf', compact('present','request','lateTime','countPresent'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_inout_time.pdf";
            return $pdf->download($name);
        }
        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_Date_Wise_inout_time_report";
            Excel::create("$excelName", function($excel) use ($present, $request,$countPresent,$lateTime) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("In & Out Time Report");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('In Out Time Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($present, $lateTime, $request,$countPresent) {
                    $sheet->loadView('report.excel.date_wise_inout_time_excel')
                        ->with('present',$present)
                        ->with('request',$request)
                        ->with('lateTime',$lateTime)
                        ->with('countPresent',$countPresent);
                });

            })->download('xlsx');
        }

        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }

    }



    public function date_wise_attendance_exception(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees=DB::table('employees')->where('empAccStatus','=',1)->get(['empFirstName','employeeId','id']);
        return view('report.attendance.date_wise_attendance_exception',compact('employees','units','departments','floors','sections','designations'));
    }

    public function date_wise_attendance_exception_data(Request $request){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_overtime();
        }
        elseif(!$helper->checkIsAValidDate($request->endDate)){
            Session::flash('message','Incorrect date format.');
            return $this->date_wise_overtime();
        }

        $lateTime=AttendanceController::late_time();
        if($request->emp_id!=0){
            $queryPresent=@"SELECT employees.*, attendance_setup.max_entry_time,attendance_setup.exit_time, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id='".$request->emp_id."'";
        }
        else {
            $queryPresent = @"SELECT employees.*, attendance_setup.max_entry_time,attendance_setup.exit_time, attendance.id AS aid, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 ";

            if (($request->unitId) != 0) {
                $queryPresent .= " AND employees.unit_id='" . $request->unitId . "'";
            }

            if (($request->floorId) != 0) {
                $queryPresent .= " AND employees.floor_id='" . $request->floorId . "'";
            }

            if (($request->line_id) != 0) {
                $queryPresent .= " AND employees.line_id='" . $request->line_id . "'";
            }

            if (!empty($request->sectionName)) {
                $queryPresent .= " AND employees.empSection='" . $request->sectionName . "'";
            }

            if (($request->departmentId) != 0) {
                $queryPresent .= " AND employees.empDepartmentId='" . $request->departmentId . "'";
            }

            if (($request->designationId) != 0) {
                $queryPresent .= " AND employees.empDesignationId='" . $request->designationId . "'";
            }
            if (($request->empGenderId) != 0) {
                $queryPresent .= " AND employees.empGenderId='" . $request->empGenderId . "'";
            }
        }
        $date=Carbon::parse($request->date)->toDateString();
        $endDate=Carbon::parse($request->endDate)->toDateString();

        $queryPresent.=" AND employees.empAccStatus='".$request->accStatus."'";
        $queryPresent.=" AND attendance.date BETWEEN '$date' and '$endDate' AND attendance.in_time=attendance.out_time ORDER BY employees.empFirstName";
        $present=DB::select($queryPresent);
        $countPresent=count($present);

        if (($request->viewType)=="Preview") {
            return view('report.attendance.date_wise_attendance_exception_data', compact('present','request','lateTime','countPresent'));

        }
        elseif(($request->viewType)=="Generate PDF"){
            if($countPresent>500){
                return view('report.pdf.date_wise_attendance_exception_pdf', compact('present','request','lateTime','countPresent'));
            }
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.date_wise_attendance_exception_pdf', compact('present','request','lateTime','countPresent'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."_date_wise_attendance_exception.pdf";
            return $pdf->download($name);
        }
        elseif(($request->viewType)=="Generate Excel"){
            $excelName=time()."_Date_Wise_Attendance_Exception";
            Excel::create("$excelName", function($excel) use ($present, $request,$countPresent,$lateTime) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Attendance Exception Report");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Attendacne Exception Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($present, $lateTime, $request,$countPresent) {
                    $sheet->loadView('report.excel.date_wise_attendance_exception_excel')
                        ->with('present',$present)
                        ->with('request',$request)
                        ->with('lateTime',$lateTime)
                        ->with('countPresent',$countPresent);
                });

            })->download('xlsx');
        }

        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }

    }

    public function total_late_list(Request $request){
        if(isset($request->type) && isset($request->id)){
            $date = Carbon::parse($request->date)->toDateString();
            if($request->type==1) {
                $present = DB::select("SELECT employees.*, attendance.id AS aid, attendance_setup.max_entry_time AS max_entry_time, attendance_setup.shiftName AS shiftName, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE employees.id>0 AND employees.empAccStatus='1' AND attendance.date='$date' AND attendance.in_time>attendance_setup.max_entry_time AND employees.empDepartmentId=$request->id ORDER BY employees.line_id ASC");
            }
            elseif ($request->type==2){
                $present = DB::select("SELECT employees.*, attendance.id AS aid, attendance_setup.max_entry_time AS max_entry_time, attendance_setup.shiftName AS shiftName, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE employees.id>0 AND employees.empAccStatus='1' AND attendance.date='$date' AND attendance.in_time>attendance_setup.max_entry_time AND employees.empSection='$request->id' ORDER BY employees.line_id ASC");
//                return "type 2";
            }
            elseif ($request->type==4){
                $present = DB::select("SELECT employees.*, attendance.id AS aid, attendance_setup.max_entry_time AS max_entry_time, attendance_setup.shiftName AS shiftName, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE employees.id>0 AND employees.empAccStatus='1' AND attendance.date='$date' AND attendance.in_time>attendance_setup.max_entry_time AND employees.empDesignationId='$request->id' ORDER BY employees.line_id ASC");
//                return $present;
            }
            elseif ($request->type==5){
                $present = DB::select("SELECT employees.*, attendance.id AS aid, attendance_setup.max_entry_time AS max_entry_time, attendance_setup.shiftName AS shiftName, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE employees.id>0 AND employees.empAccStatus='1' AND attendance.date='$date' AND attendance.in_time>attendance_setup.max_entry_time AND employees.line_id='$request->id' ORDER BY employees.line_id ASC");
//                return $present;
            }

            return view('report.attendance.total_late_list', compact('present', 'request'));

        }
        elseif ($request->date) {
            $date = Carbon::parse($request->date)->toDateString();
            $present = DB::select("SELECT employees.*, attendance.id AS aid, attendance_setup.max_entry_time AS max_entry_time, attendance_setup.shiftName AS shiftName, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE employees.id>0 AND employees.empAccStatus='1' AND attendance.date='$date' AND attendance.in_time>attendance_setup.max_entry_time ORDER BY employees.line_id ASC");
            return view('report.attendance.total_late_list', compact('present', 'request'));
        }

    }
    public function total_absent_list(Request $request){
        if(isset($request->type) && isset($request->id)){
            $date = Carbon::parse($request->date)->toDateString();
            if($request->type==1) {
                $sql = "SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 AND employees.empAccStatus='1' AND employees.empDepartmentId='$request->id' AND employees.id not in (SELECT emp_id FROM attendance WHERE date='$date' ORDER BY employees.empFirstName) AND employees.id not in (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date AND status='1')";
                $absent = DB::select($sql);
            }
            elseif ($request->type==2){
                $sql = "SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 AND employees.empAccStatus='1' AND employees.empSection='$request->id' AND employees.id not in (SELECT emp_id FROM attendance WHERE date='$date' ORDER BY employees.empFirstName) AND employees.id not in (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date AND status='1')";
                $absent = DB::select($sql);
            }
            elseif ($request->type==4){
                $sql = "SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 AND employees.empAccStatus='1' AND employees.empDesignationId='$request->id' AND employees.id not in (SELECT emp_id FROM attendance WHERE date='$date' ORDER BY employees.empFirstName) AND employees.id not in (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date AND status='1')";
                $absent = DB::select($sql);
            }
            elseif ($request->type==5){
                $sql = "SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 AND employees.empAccStatus='1' AND employees.line_id='$request->id' AND employees.id not in (SELECT emp_id FROM attendance WHERE date='$date' ORDER BY employees.empFirstName) AND employees.id not in (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date AND status='1')";
                $absent = DB::select($sql);
            }
            return view('report.attendance.total_absent_list', compact('absent', 'request'));


        }
        elseif ($request->date) {
            $date = Carbon::parse($request->date)->toDateString();
            $absent = DB::select("SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 AND employees.empAccStatus='1' AND employees.id not in (SELECT emp_id FROM attendance WHERE date='$date' ORDER BY employees.empFirstName) AND employees.id not in (SELECT employee_id from tb_leave_application WHERE '$date'>=leave_starting_date and '$date'<=leave_ending_date and status='1')");
            return view('report.attendance.total_absent_list', compact('absent', 'request'));
        }

    }

    public function total_present_list(Request $request){
        if(isset($request->id) &&isset($request->type)){
            $date = Carbon::parse($request->date)->toDateString();

            if($request->type==1){
                $sql="SELECT employees.*, attendance.id AS aid, attendance_setup.max_entry_time AS max_entry_time, attendance_setup.shiftName AS shiftName, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE employees.id>0 AND employees.empDepartmentId='$request->id' AND employees.empAccStatus='1' AND attendance.date='$date' ORDER BY employees.empFirstName";
                $present=DB::select($sql);
            }
            elseif ($request->type==2){
                $sql="SELECT employees.*, attendance.id AS aid, attendance_setup.max_entry_time AS max_entry_time, attendance_setup.shiftName AS shiftName, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE employees.id>0 AND employees.empSection='$request->id' AND employees.empAccStatus='1' AND attendance.date='$date' ORDER BY employees.empFirstName";
                $present=DB::select($sql);

            }
            elseif ($request->type==4){
                $sql="SELECT employees.*, attendance.id AS aid, attendance_setup.max_entry_time AS max_entry_time, attendance_setup.shiftName AS shiftName, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE employees.id>0 AND employees.empDesignationId='$request->id' AND employees.empAccStatus='1' AND attendance.date='$date' ORDER BY employees.empFirstName";
                $present=DB::select($sql);

            }
            elseif ($request->type==5){
                $sql="SELECT employees.*, attendance.id AS aid, attendance_setup.max_entry_time AS max_entry_time, attendance_setup.shiftName AS shiftName, attendance.in_time, attendance.out_time,attendance.date,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees JOIN attendance ON employees.id = attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE employees.id>0 AND employees.line_id='$request->id' AND employees.empAccStatus='1' AND attendance.date='$date' ORDER BY employees.empFirstName";
                $present=DB::select($sql);

            }
            return view('report.attendance.total_present_list',compact('present','request'));

        }
        return redirect()->back();

    }

    public function edit_present($id){
        $attendance=DB::table('attendance')
            ->join('employees','attendance.emp_id','=','employees.id')
            ->where('attendance.id','=',$id)
            ->select('employees.empFirstName','employees.empLastName',
                'employees.employeeId','attendance.in_time','attendance.out_time','attendance.id as aid',
                'attendance.date')
            ->first();
        return view('attendance.edit_present_attendance',compact('attendance'));
//        return $id;
    }

    public function update_present(Request $request){
        DB::table('attendance')->where('id','=',$request->id)
            ->update([
                'in_time'=>$request->stime,
                'out_time'=>$request->etime,
            ]);
        session()->flash('success','Attendance Updated Successfully');
        return redirect()->back();
    }

    public function edit_absent($id, $date){
        $employee=DB::table('employees')->where('id','=',$id)
            ->select('employees.empFirstName','employees.empLastName','employees.employeeId',
                'employees.id')
            ->first();

        return view('attendance.edit_absent',compact('employee','date'));

    }

    public function update_absent(Request $request){
        $this->validate($request,[
            'emp_id'=>'required',
            'date'=>'required',
            'stime'=>'required',
        ]);
        if($request->etime==null || $request->etime<$request->stime){
            $request->etime=$request->stime;
        }
        $check=DB::table('attendance')->where('emp_id','=',$request->emp_id)
            ->where('date','=',$request->date)
            ->count();
        if($check==0) {
            $id=DB::table('attendance')->insertGetId([
                'emp_id' => $request->emp_id,
                'date' => $request->date,
                'in_time' => $request->stime,
                'out_time' => $request->etime,
            ]);

            session()->flash('success', 'Attendance Data Recorded');
            return redirect(route('report.edit_present',$id));
        }
        else{
            session()->flash('error', 'Duplicate Data Found.');

        }
        return redirect()->back();

    }



    public function daily_attendance_summery(){
        return view('report.attendance.daily_attendance_summary');
    }

    public function summary_report_date(){
        return view('report.attendance.summary_report_date');

    }

    public function summary_report_date_data(Request $request){
//        return $request->all();
        $departments = DB::table('departments')->get(['id', 'departmentName']);
        if($request->viewType=="Generate PDF"){
            return view('report.pdf.summary_report_date_data', compact('departments', 'request'));


        }
        else{
            $departments = DB::table('departments')->get(['id', 'departmentName']);
            return view('report.attendance.summary_report_date_data', compact('departments', 'request'));
        }


    }

    public function daily_attendance_summary_data(Request $request){
//        return $request->all();
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date)){
            Session::flash('message','Incorrect date format.');
            return redirect(route('report.daily_attendance_summery'));
        }

        if(!empty($request->date)) {
            $date = Carbon::parse($request->date)->toDateString();
            $ld=Carbon::parse($request->date)->toDateString();
            $late_time=AttendanceController::late_time();
            $totalE=DB::table('employees')->where('empAccStatus','=','1')->count();
            $totalP=DB::table('employees')->join('attendance','employees.id','=','attendance.emp_id')->where('employees.empAccStatus','=','1')->where(['attendance.date'=>$date])->count();
            $totalL=DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1')");
            $totalL=$totalL[0]->leaveCount;
            $Tlate=DB::select("select COUNT(employees.id) as lateCount from `employees` inner join `attendance` on `employees`.`id` = `attendance`.`emp_id` left join `attendance_setup` on `employees`.`empShiftId` = `attendance_setup`.`id` where `employees`.`empAccStatus` = 1 and (`attendance`.`date` = '$date') AND attendance.in_time>attendance_setup.max_entry_time");
//            $Tlate=DB::table('employees')->join('attendance','employees.id','=','attendance.emp_id')->join('attendance_setup','employees.empShiftId','=','attendance_setup.id')->where('employees.empAccStatus','=','1')->where('attendance.date','=','2018-10-28')->where('attendance.in_time','>','attendance_setup.max_entry_time')->count();
            $Tlate=$Tlate[0]->lateCount;

//            return $totalL;

        }

        else{
            return redirect(route('report.daily_attendance_summery'));
        }

        if($request->viewType=='Preview') {

            if ($request->reportType == 1) {
                $data = [];
                $departments = DB::table('departments')->select('id', 'departmentName')->get();
                foreach ($departments as $id => $department) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('empDepartmentId', '=', $department->id)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empDepartmentId', '=', $department->id)->where(['attendance.date' => $date])->count();
//                    $late = DB::table('employeess')->join('attendance_setup','employees.empShiftId','=','attendance_setup.id')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empDepartmentId', '=', $department->id)->where(['attendance.date' => $date])->where('attendance.in_time', '>', 'attendance_setup.max_entry_time')->count();
//                    return $late;
                    $late=DB::select("select count(*) as aggregate from `employees` inner join `attendance_setup` on `employees`.`empShiftId` = `attendance_setup`.`id` inner join `attendance` on `employees`.`id` = `attendance`.`emp_id` where `employees`.`empAccStatus` = 1 and `empDepartmentId` = $department->id and (`attendance`.`date` = '$date') and `attendance`.`in_time` > attendance_setup.max_entry_time");
                    $late=$late[0]->aggregate;
                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and empDepartmentId=" . $department->id);
                    $leaveCount = $leaveCount[0]->leaveCount;
                    $data[$id]['designationId'] = $department->id;
                    $data[$id]['designationName'] = $department->departmentName;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }


                return view('report.attendance.daily_attendance_summary_data', compact('request', 'data', 'totalE', 'totalP', 'totalL', 'Tlate'));


            } else if ($request->reportType == 2) {
                $data = [];
                $sections = DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
//            return $sections;
                foreach ($sections as $id => $section) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('empSection', '=', $section->empSection)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empSection', '=', $section->empSection)->where(['attendance.date' => $date])->count();
                    $late=DB::select("select count(*) as aggregate from `employees` inner join `attendance_setup` on `employees`.`empShiftId` = `attendance_setup`.`id` inner join `attendance` on `employees`.`id` = `attendance`.`emp_id` where `employees`.`empAccStatus` = 1 and `empSection` = '$section->empSection' and `attendance`.`date` = '$date' and `attendance`.`in_time` > attendance_setup.max_entry_time");
                    $late=$late[0]->aggregate;
                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and empSection='" . $section->empSection . "'");
                    $leaveCount = $leaveCount[0]->leaveCount;
                    $data[$id]['designationId'] = $section->empSection;
                    $data[$id]['designationName'] = $section->empSection;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }

                return view('report.attendance.daily_attendance_summary_data', compact('request', 'data', 'totalE', 'totalP', 'totalL', 'Tlate'));

            } else if ($request->reportType == 3) {
                $data = [];
                $lines=DB::select("SELECT id, line_no, floor_id FROM tblines WHERE id in (SELECT line_id FROM employees WHERE empAccStatus=1)");

                foreach ($lines as $id => $line) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('line_id', '=', $line->id)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('line_id', '=', $line->id)->where(['attendance.date' => $date])->count();
                    $late = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('line_id', '=', $line->id)->where(['attendance.date' => $date])->where('attendance.in_time', '>', $late_time)->count();
                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and line_id=" . $line->id);
                    $leaveCount = $leaveCount[0]->leaveCount;
                    $data[$id]['designationId'] = $line->floor_id;
                    $data[$id]['designationName'] = $line->line_no;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }
                return view('report.attendance.daily_attendance_summary_data', compact('request', 'data', 'totalE', 'totalP', 'totalL', 'Tlate'));

            } else if ($request->reportType == 4) {
                $data = [];
                $designations = DB::table('designations')->select('id', 'designation')->get();
                foreach ($designations as $id => $designation) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('empDesignationId', '=', $designation->id)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empDesignationId', '=', $designation->id)->where(['attendance.date' => $date])->count();
//                    return $late;
                    $late=DB::select("select count(*) as aggregate from `employees` inner join `attendance` on `employees`.`id` = `attendance`.`emp_id` left join `attendance_setup` on `employees`.`empShiftId` = `attendance_setup`.`id` where `employees`.`empAccStatus` = 1 and `empDesignationId` = $designation->id and (`attendance`.`date` = '$date') and `attendance`.`in_time` > attendance_setup.max_entry_time");
                    $late=$late[0]->aggregate;

                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and empDesignationId=" . $designation->id);
                    $leaveCount = $leaveCount[0]->leaveCount;
//                return $leaveCount;
                    $data[$id]['designationId'] = $designation->id;
                    $data[$id]['designationName'] = $designation->designation;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }

                return view('report.attendance.daily_attendance_summary_data', compact('request', 'data', 'totalE', 'totalP', 'totalL', 'Tlate'));
            }

            else if ($request->reportType == 5) {
                $data = [];
                $lines = DB::table('tblines')->select('id', 'line_no')->get();
                foreach ($lines as $id => $designation) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('line_id', '=', $designation->id)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('line_id', '=', $designation->id)->where(['attendance.date' => $date])->count();
//                    return $late;
                    $late=DB::select("select count(*) as aggregate from `employees` inner join `attendance` on `employees`.`id` = `attendance`.`emp_id` left join `attendance_setup` on `employees`.`empShiftId` = `attendance_setup`.`id` where `employees`.`empAccStatus` = 1 and `line_id` = $designation->id and (`attendance`.`date` = '$date') and `attendance`.`in_time` > attendance_setup.max_entry_time");
                    $late=$late[0]->aggregate;

                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and line_id=" . $designation->id);
                    $leaveCount = $leaveCount[0]->leaveCount;
//                return $leaveCount;
                    $data[$id]['designationId'] = $designation->id;
                    $data[$id]['designationName'] = $designation->line_no;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }

                return view('report.attendance.daily_attendance_summary_data', compact('request', 'data', 'totalE', 'totalP', 'totalL', 'Tlate'));
            }
        }



        elseif(($request->viewType)=="Generate PDF"){
            if ($request->reportType == 1) {
                $data = [];
                $departments = DB::table('departments')->select('id', 'departmentName')->get();
                foreach ($departments as $id => $department) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('empDepartmentId', '=', $department->id)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empDepartmentId', '=', $department->id)->where(['attendance.date' => $date])->count();
                    $late=DB::select("select count(*) as aggregate from `employees` inner join `attendance_setup` on `employees`.`empShiftId` = `attendance_setup`.`id` inner join `attendance` on `employees`.`id` = `attendance`.`emp_id` where `employees`.`empAccStatus` = 1 and `empDepartmentId` = $department->id and (`attendance`.`date` = '$date') and `attendance`.`in_time` > attendance_setup.max_entry_time");
                    $late=$late[0]->aggregate;
                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and empDepartmentId=" . $department->id);
                    $leaveCount = $leaveCount[0]->leaveCount;
                    $data[$id]['designationId'] = $department->id;
                    $data[$id]['designationName'] = $department->departmentName;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }

            } else if ($request->reportType == 2) {
                $data = [];
                $sections = DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
//            return $sections;
                foreach ($sections as $id => $section) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('empSection', '=', $section->empSection)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empSection', '=', $section->empSection)->where(['attendance.date' => $date])->count();
                    $late=DB::select("select count(*) as aggregate from `employees` inner join `attendance_setup` on `employees`.`empShiftId` = `attendance_setup`.`id` inner join `attendance` on `employees`.`id` = `attendance`.`emp_id` where `employees`.`empAccStatus` = 1 and `empSection` = '$section->empSection' and `attendance`.`date` = '$date' and `attendance`.`in_time` > attendance_setup.max_entry_time");
                    $late=$late[0]->aggregate;
                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and empSection='" . $section->empSection . "'");
                    $leaveCount = $leaveCount[0]->leaveCount;
                    $data[$id]['designationId'] = $section->empSection;
                    $data[$id]['designationName'] = $section->empSection;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }

            } else if ($request->reportType == 3) {
                $data = [];
                $lines=DB::select("SELECT id, line_no, floor_id FROM tblines WHERE id in (SELECT line_id FROM employees WHERE empAccStatus=1)");
                foreach ($lines as $id => $line) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('line_id', '=', $line->id)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('line_id', '=', $line->id)->where(['attendance.date' => $date])->count();
                    $late = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('line_id', '=', $line->id)->where(['attendance.date' => $date])->where('attendance.in_time', '>', $late_time)->count();
                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and line_id=" . $line->id);
                    $leaveCount = $leaveCount[0]->leaveCount;
                    $data[$id]['designationId'] = $line->floor_id;
                    $data[$id]['designationName'] = $line->line_no;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }
                $floors=DB::select("SELECT floor_id FROM employees WHERE empAccStatus=1 AND floor_id in (SELECT id FROM floors) GROUP BY floor_id");

//                return view('report.pdf.daily_attendance_summary_floor_line_wise_pdf',compact('request','data','totalE','totalP','totalL','Tlate','floors'));
                $pagesize=$request->pagesize;
                $pageOrientation=$request->pageOrientation;

                $name=time()."_Attendance_Summary.pdf";
                return view('report.pdf.daily_attendance_summary_floor_line_wise_pdf',compact('request','data','totalE','totalP','totalL','Tlate','floors'));
                $pdf->setPaper($pagesize, $pageOrientation);
                return $pdf->download($name);


            } else if ($request->reportType == 4) {
                $data = [];
                $designations = DB::table('designations')->select('id', 'designation')->get();
                foreach ($designations as $id => $designation) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('empDesignationId', '=', $designation->id)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empDesignationId', '=', $designation->id)->where(['attendance.date' => $date])->count();
                    $late=DB::select("select count(*) as aggregate from `employees` inner join `attendance` on `employees`.`id` = `attendance`.`emp_id` left join `attendance_setup` on `employees`.`empShiftId` = `attendance_setup`.`id` where `employees`.`empAccStatus` = 1 and `empDesignationId` = $designation->id and (`attendance`.`date` = '$date') and `attendance`.`in_time` > attendance_setup.max_entry_time");
                    $late=$late[0]->aggregate;
                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and empDesignationId=" . $designation->id);
                    $leaveCount = $leaveCount[0]->leaveCount;
                    $data[$id]['designationId'] = $designation->id;
                    $data[$id]['designationName'] = $designation->designation;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }
            }

            else if ($request->reportType == 5) {
                $data = [];
                $designations = DB::table('tblines')->select('id', 'line_no')->get();
                foreach ($designations as $id => $designation) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('line_id', '=', $designation->id)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('line_id', '=', $designation->id)->where(['attendance.date' => $date])->count();
                    $late=DB::select("select count(*) as aggregate from `employees` inner join `attendance` on `employees`.`id` = `attendance`.`emp_id` left join `attendance_setup` on `employees`.`empShiftId` = `attendance_setup`.`id` where `employees`.`empAccStatus` = 1 and `line_id` = $designation->id and (`attendance`.`date` = '$date') and `attendance`.`in_time` > attendance_setup.max_entry_time");
                    $late=$late[0]->aggregate;
                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and line_id=" . $designation->id);
                    $leaveCount = $leaveCount[0]->leaveCount;
                    $data[$id]['designationId'] = $designation->id;
                    $data[$id]['designationName'] = $designation->line_no;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }
            }

            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;

            $name=time()."_Attendance_Summary.pdf";
            return view('report.pdf.daily_attendance_summary_pdf',compact('request','data','totalE','totalP','totalL','Tlate'));
            $pdf->setPaper($pagesize, $pageOrientation);
            return $pdf->download($name);

        }

        elseif(($request->viewType)=="Generate Excel"){
            $data = [];
            if ($request->reportType == 1) {
                $departments = DB::table('departments')->select('id', 'departmentName')->get();
                foreach ($departments as $id => $department) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('empDepartmentId', '=', $department->id)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empDepartmentId', '=', $department->id)->where(['attendance.date' => $date])->count();
                    $late=DB::select("select count(*) as aggregate from `employees` inner join `attendance_setup` on `employees`.`empShiftId` = `attendance_setup`.`id` inner join `attendance` on `employees`.`id` = `attendance`.`emp_id` where `employees`.`empAccStatus` = 1 and `empDepartmentId` = $department->id and (`attendance`.`date` = '$date') and `attendance`.`in_time` > attendance_setup.max_entry_time");
                    $late=$late[0]->aggregate;
                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and empDepartmentId=" . $department->id);
                    $leaveCount = $leaveCount[0]->leaveCount;
                    $data[$id]['designationId'] = $department->id;
                    $data[$id]['designationName'] = $department->departmentName;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }

            } else if ($request->reportType == 2) {
                $sections = DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
//            return $sections;
                foreach ($sections as $id => $section) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('empSection', '=', $section->empSection)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empSection', '=', $section->empSection)->where(['attendance.date' => $date])->count();
                    $late=DB::select("select count(*) as aggregate from `employees` inner join `attendance_setup` on `employees`.`empShiftId` = `attendance_setup`.`id` inner join `attendance` on `employees`.`id` = `attendance`.`emp_id` where `employees`.`empAccStatus` = 1 and `empSection` = '$section->empSection' and `attendance`.`date` = '$date' and `attendance`.`in_time` > attendance_setup.max_entry_time");
                    $late=$late[0]->aggregate;
                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and empSection='" . $section->empSection . "'");
                    $leaveCount = $leaveCount[0]->leaveCount;
                    $data[$id]['designationId'] = $section->empSection;
                    $data[$id]['designationName'] = $section->empSection;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }

            }
            else if ($request->reportType == 4) {
                $designations = DB::table('designations')->select('id', 'designation')->get();
                foreach ($designations as $id => $designation) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('empDesignationId', '=', $designation->id)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('empDesignationId', '=', $designation->id)->where(['attendance.date' => $date])->count();
                    $late=DB::select("select count(*) as aggregate from `employees` inner join `attendance` on `employees`.`id` = `attendance`.`emp_id` left join `attendance_setup` on `employees`.`empShiftId` = `attendance_setup`.`id` where `employees`.`empAccStatus` = 1 and `empDesignationId` = $designation->id and (`attendance`.`date` = '$date') and `attendance`.`in_time` > attendance_setup.max_entry_time");
                    $late=$late[0]->aggregate;
                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and empDesignationId=" . $designation->id);
                    $leaveCount = $leaveCount[0]->leaveCount;
                    $data[$id]['designationId'] = $designation->id;
                    $data[$id]['designationName'] = $designation->designation;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }
            }
            else if ($request->reportType == 5) {
                $designations = DB::table('tblines')->select('id', 'line_no')->get();
                foreach ($designations as $id => $designation) {
                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('line_id', '=', $designation->id)->count();
                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('line_id', '=', $designation->id)->where(['attendance.date' => $date])->count();
                    $late=DB::select("select count(*) as aggregate from `employees` inner join `attendance` on `employees`.`id` = `attendance`.`emp_id` left join `attendance_setup` on `employees`.`empShiftId` = `attendance_setup`.`id` where `employees`.`empAccStatus` = 1 and `line_id` = $designation->id and (`attendance`.`date` = '$date') and `attendance`.`in_time` > attendance_setup.max_entry_time");
                    $late=$late[0]->aggregate;
                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and line_id=" . $designation->id);
                    $leaveCount = $leaveCount[0]->leaveCount;
                    $data[$id]['designationId'] = $designation->id;
                    $data[$id]['designationName'] = $designation->line_no;
                    $data[$id]['total'] = $total;
                    $data[$id]['present'] = $present;
                    $data[$id]['late'] = $late;
                    $data[$id]['leave'] = $leaveCount;
                }
            }



            $excelName=time()."_Daily_Attendance_Summery";
            Excel::create("$excelName", function($excel) use ($request,$data,$totalL,$totalE,$totalP,$Tlate) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Daily Attendance Summary");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Daily Attendance Summary');


                $excel->sheet('Sheet 1', function ($sheet) use ($request,$data,$Tlate,$totalP,$totalL,$totalE) {
                    $sheet->loadView('report.excel.daily_attendance_summary_excel')
                        ->with('data',$data)
                        ->with('request',$request)
                        ->with('Tlate',$Tlate)
                        ->with('totalP',$totalP)
                        ->with('totalL',$totalL)
                        ->with('totalE',$totalE);
                });

            })->download('xlsx');
        }
    }



    //leave report view method
    public function leave_report(){
        return view('report.leave.index');
    }
    public function training_wise_employee_report(){
        $training_name=DB::table('tb_training')->pluck('training_name','id')->all();
        return view('report.training.trainingwiseEmployeereport',compact('training_name'));
    }
    public function dateWiseLeaveReport(){
        //dd('hello');
        return view('report.leave.dateWiseLeaveReport');
    }
    //Leave Report show
    public function showDateWiseLeaveReport(Request $request){
        $start=$request->leave_start;
        $end=$request->leave_end;
        $sdate=date_create($start);
        $edate=date_create($end);
        $start_date=date_format($sdate,"Y-m-d");
        $end_date=date_format($edate,"Y-m-d");
        //return($end_date);
        $tb_leave_type=DB::table('tb_leave_type')->get();
        $data=DB::table('tb_employee_leave');
        $tb_leave_type=DB::table('tb_leave_type')->get();
        $data=DB::table('tb_leave_application')
            ->leftJoin('employees', function ($query) {
                $query->on('tb_leave_application.employee_id', '=', 'employees.id');
            })
            ->leftJoin('tb_leave_type', function ($query) {
                $query->on('tb_leave_application.leave_type_id', '=', 'tb_leave_type.id');
            })
            ->join('designations', function ($query){
                $query->on('designations.id', '=', 'employees.empDesignationId');
            })
            ->select('tb_leave_application.*',
                DB::raw('group_concat(tb_leave_type.total_days)as leave_amount_days'),
                'employees.empFirstName','employees.employeeId','employees.empLastName',
                'designations.designation','tb_leave_type.leave_type',
                'tb_leave_application.leave_starting_date','tb_leave_application.leave_ending_date',
                DB::raw('group_concat(tb_leave_type.leave_type,DATEDIFF(tb_leave_application.leave_starting_date,
            tb_leave_application.leave_ending_date)) as names'),
                DB::raw('group_concat(tb_leave_application.leave_starting_date) as leave_sdays'),
                DB::raw('group_concat(tb_leave_application.leave_ending_date) as leave_edays'),
                DB::raw(' group_concat(DATEDIFF(tb_leave_application.leave_starting_date,tb_leave_application.leave_ending_date))as distance_value'),
                DB::raw('group_concat(tb_leave_type.leave_type,abs(tb_leave_type.total_days)  -  
            abs(DATEDIFF(tb_leave_application.leave_starting_date,tb_leave_application.leave_ending_date))) 
            as leave_available'))
            ->selectRaw('tb_leave_application.created_at')->whereBetween('tb_leave_application.created_at',[$start_date,$end_date])
            ->selectRaw('tb_leave_application.status as sta')->WHERE('tb_leave_application.status','=',1)
            ->groupBy('tb_leave_application.employee_id')
            ->orderBy('tb_leave_application.created_at','ASC')
            ->orderBy('tb_leave_application.leave_type_id','ASC')
            ->get();
        //dd($data);
        $current_date=date("m/d/Y");
        //dd($request->tests);
        if (($request->tests)=="leave_report_generate") {
            return view('report.leave.showDateWiseLeaveReport',compact('data','tb_leave_type'));
        }elseif(($request->tests)==null){
            $pdf=PDF::loadView('report.pdf.Employee_datewise_report',compact('data','tb_leave_type','request'));
            $pdf->setPaper('A4', 'Landscape');
            $name=$current_date."Employee_datewise_report.pdf";
            return $pdf->download($name);
        }else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }

    }

    public function onLeaveReport(){
        $current_date=date("Y-m-d");
        $onLeaveEmployee=DB::SELECT("SELECT leave_of_employee.start_date,tb_leave_type.leave_type,leave_of_employee.end_date,employees.empFirstName,employees.empLastName,employees.employeeId,designations.designation
        FROM leave_of_employee
        LEFT JOIN employees ON leave_of_employee.emp_id=employees.id
        LEFT JOIN tb_leave_type ON leave_of_employee.leave_type_id=tb_leave_type.id
        LEFT JOIN designations ON employees.empDesignationId=designations.id
        LEFT JOIN tb_leave_application ON leave_of_employee.emp_id=tb_leave_application.leave_type_id 
        WHERE leave_of_employee.start_date between '$current_date' and '$current_date' OR leave_of_employee.end_date between '$current_date' and '$current_date'
        GROUP BY leave_of_employee.emp_id 
        ORDER BY employees.employeeId DESC");      
        return view('report.leave.onLeaveReport',compact('onLeaveEmployee'));
    }

    public function onLeaveListPDF(){
        $current_date=date("Y-m-d");
        $onLeaveEmployee=DB::SELECT("SELECT leave_of_employee.start_date,tb_leave_type.leave_type,leave_of_employee.end_date,employees.empFirstName,employees.empLastName,employees.employeeId,designations.designation
        FROM leave_of_employee
        LEFT JOIN employees ON leave_of_employee.emp_id=employees.id
        LEFT JOIN tb_leave_type ON leave_of_employee.leave_type_id=tb_leave_type.id
        LEFT JOIN designations ON employees.empDesignationId=designations.id
        LEFT JOIN tb_leave_application ON leave_of_employee.emp_id=tb_leave_application.leave_type_id 
        WHERE leave_of_employee.start_date between '$current_date' and '$current_date' OR leave_of_employee.end_date between '$current_date' and '$current_date'
        GROUP BY leave_of_employee.emp_id 
        ORDER BY employees.employeeId DESC");

        $pdf=PDF::loadView('report.pdf.onLeaveEmployeePDF',compact('onLeaveEmployee'));
        $pdf->setPaper('A4', 'Potrait');
        $name=$current_date."onLeaveEmployee.pdf";
        return $pdf->download($name);
        //dd($onLeaveEmployee);

    }
    //Employee all types bonus report
    public function employee_month_wise_bonus_Report(){
        return view('report.payroll.bonus.bonus_report');
    }

    //increment bonus view page
    public function incrementBonus(){
        return view('report.payroll.bonus.empincrementreport');
    }

    //attendance bonus view page
    public function attendanceBonus(){
        return view('report.payroll.bonus.empattendancereport');
    }

    //festival bonus view page
    public function festivalBonus(){
        $department =DB::table('departments')->select('id','departmentName')->get();
        $section    =DB::table('employees')->select('empSection')->whereNotNull('empSection')->groupBy('empSection')->get();
        $title      =DB::table('festival_bonus')->select('bonus_title')->groupBy('bonus_title')->get();
        $payment_mode=DB::table('employees')->select('payment_mode')->groupBy('payment_mode')->get();
        return view('report.payroll.bonus.empfestivalreport',compact('department','section','title','payment_mode'));
    }

    //increment bonus report month wise
    public function incrementEmployeeBonusReport(Request $request){
        $month=$request->year_bonus_month;
        $data=DB::SELECT("select employees_bonus.emp_id,employees.empFirstName,employees.empLastName,users.name,users.email,users.is_permission,
              employees_bonus.emp_gross,employees_bonus.emp_bonus,employees_bonus.emp_amount,employees_bonus.emp_total_percent,employees_bonus.emp_total_amount,employees_bonus.created_at
              FROM employees_bonus
              LEFT JOIN employees ON employees_bonus.emp_id = employees.id
              LEFT JOIN users ON employees_bonus.bonus_given_id = users.id 
              WHERE date='$month'
             ");
        return response()->json($data);
    }
    //attendance bonus report month wise
    public function attendanceEmployeeBonusReport(Request $request){
        $month=$request->year_bonus_month;
        $data=DB::SELECT("select attendance_bonus.bonus_id,employees.empFirstName,employees.empLastName,users.name,users.email,users.is_permission,attendance_bonus.emp_gross,attendance_bonus.bonus_amount,attendance_bonus.bonus_percent,attendance_bonus.emp_total_percent,attendance_bonus.emp_total_amount,attendance_bonus.created_at
              FROM attendance_bonus
              LEFT JOIN employees ON attendance_bonus.bonus_id = employees.id
              LEFT JOIN users ON attendance_bonus.bonus_given_id = users.id 
              WHERE month='$month'
             ");
        return response()->json($data);
    }
    //festival bonus report month wise
    public function festivalEmloyeeBonusReport(Request $request){
        $month=$request->year_bonus_month;
        $data=DB::SELECT("select festival_bonus.emp_id,designations.designation,departments.departmentName,employees.empFirstName,employees.empLastName,users.name,users.is_permission,
              festival_bonus.emp_gross,festival_bonus.bonus_title,festival_bonus.emp_bonus,festival_bonus.emp_amount,festival_bonus.emp_total_percent,festival_bonus.emp_total_amount,festival_bonus.created_at
              FROM festival_bonus
              LEFT JOIN employees ON festival_bonus.emp_id = employees.id
              LEFT JOIN departments ON employees.empDepartmentId = departments.id
              LEFT JOIN designations ON employees.empDesignationId = designations.id
              LEFT JOIN users ON festival_bonus.bonus_given_id = users.id 
              WHERE month='$month'");
        return response()->json($data);
    }

    // pdf report increment report
    public function incrementreportpdf(Request $request){
        $months=$request->year_bonus_month;
        $type=$request->increment_work_group;
        $month=date('F-Y',strtotime($request->year_bonus_month));
        $data=DB::table('employees_bonus')
            ->leftjoin('employees','employees_bonus.emp_id','=','employees.id')
            ->leftjoin('emp_inc_check','employees_bonus.emp_id','=','emp_inc_check.emp_check_id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('designations','employees.empDesignationId','=','designations.id')
            ->select('employees.employeeId','employees.empFirstName','employees.work_group','departments.departmentName','designations.designation','emp_inc_check.increment_month','employees_bonus.emp_gross','employees_bonus.emp_total_percent','employees_bonus.emp_bonus')
            ->where('date',$months)
            ->where('employees.work_group',$type)
            ->paginate(50);
        return view('report.payroll.increment_bonus_report_show',compact('data','month'));


    }

    // pdf report attendance bonus report
    public function attendancereportpdf(Request $request){
        $workgroup=$request->att_bonus_work_group;
        $month=$request->year_bonus_month;
        $data=DB::table('tb_salary_history')
            ->join('employees','tb_salary_history.emp_id','=','employees.id')
            ->join('attendance_bonus','tb_salary_history.emp_id','attendance_bonus.bonus_id')
            ->select('employees.empFirstName','employees.work_group','employees.employeeId','attendance_bonus.bonus_amount','attendance_bonus.emp_total_amount','tb_salary_history.absent','tb_salary_history.total')
            ->where('tb_salary_history.dates',$month)
            ->where('employees.work_group',$workgroup)
            ->groupBy('attendance_bonus.bonus_id')
            ->paginate(200);
        return view('report.payroll.bonus.attendance_bonus_show',compact('data','month'));
    }

    //festival bonus report pdf
    public function festivalreportpdf(Request $request){
        $month=$request->year_bonus_month;
        $month_name=DB::SELECT("SELECT created_at FROM `festival_bonus` WHERE month='$month' LIMIT 1");
        $data=DB::SELECT("select festival_bonus.emp_id,employees.empFirstName,employees.empLastName,users.name,users.email,users.is_permission,
              festival_bonus.emp_gross,festival_bonus.bonus_title,festival_bonus.emp_bonus,festival_bonus.emp_amount,festival_bonus.emp_total_percent,festival_bonus.emp_total_amount,festival_bonus.created_at
              FROM festival_bonus
              LEFT JOIN employees ON festival_bonus.emp_id = employees.id
              LEFT JOIN users ON festival_bonus.bonus_given_id = users.id 
              WHERE month='$month'");
        $pdf = PDF::loadView('report.pdf.festivalbonuspdf',compact('data','month_name'));
        $pdf->setPaper('A4','portrait');
        $name = time()."attendance.pdf";
        return $pdf->download($name);
    }


    public function ongoing_training_report_show(){
        $current_date=date("Y-m-d");
        //dd($current_date);

        $training=DB::table('tb_employee_training')
            ->join('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
            ->join('employees', 'employees.id','=','tb_employee_training.employee_id')
            ->join('designations', 'designations.id','=','employees.empDesignationId')
            ->select('tb_training.training_name',
                DB::raw("(GROUP_CONCAT(employees.empFirstName,' ', employees.empLastName,
            '(',employees.employeeId,')'  SEPARATOR ' , ')) as `employee_name`"),
                'tb_training.id as tid','tb_employee_training.training_starting_date',
                'tb_employee_training.training_ending_date','employees.employeeId',
                'designations.designation')
            ->where('tb_employee_training.training_starting_date','<=',$current_date)
            ->where('tb_employee_training.training_ending_date','>=',$current_date)
            ->groupBy('tb_training.id')
            ->groupBy('tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
            ->get();

        //dd($training);
        return view('report.training.onGoing',compact('training'));
    }

    public function completed_training_report_show(){
        $current_date=date("Y-m-d");
        //dd($current_date);

        $training=DB::table('tb_employee_training')
            ->join('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
            ->join('employees', 'employees.id','=','tb_employee_training.employee_id')
            ->join('designations', 'designations.id','=','employees.empDesignationId')
            ->select('tb_training.training_name',
                DB::raw("(GROUP_CONCAT(employees.empFirstName,' ', employees.empLastName,
            '(',employees.employeeId,')'  SEPARATOR ' , ')) as `employee_name`"),
                'tb_training.id as tid','tb_employee_training.training_starting_date',
                'tb_employee_training.training_ending_date','employees.employeeId',
                'designations.designation')
            ->where('tb_employee_training.training_starting_date','<',$current_date)
            ->where('tb_employee_training.training_ending_date','<',$current_date)
            ->groupBy('tb_training.id')
            ->groupBy('tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
            ->get();
        //dd($training);
        return view('report.training.completed',compact('training'));
    }

    public function completedTrainingPDF(){
        $current_date=date("Y-m-d");
        //dd($current_date);

        $training=DB::table('tb_employee_training')
            ->join('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
            ->join('employees', 'employees.id','=','tb_employee_training.employee_id')
            ->join('designations', 'designations.id','=','employees.empDesignationId')
            ->select('tb_training.training_name',
                DB::raw("(GROUP_CONCAT(employees.empFirstName,' ', employees.empLastName,
            '(',employees.employeeId,')'  SEPARATOR ' , ')) as `employee_name`"),
                'tb_training.id as tid','tb_employee_training.training_starting_date',
                'tb_employee_training.training_ending_date','employees.employeeId',
                'designations.designation')
            ->where('tb_employee_training.training_starting_date','<',$current_date)
            ->where('tb_employee_training.training_ending_date','<',$current_date)
            ->groupBy('tb_training.id')
            ->groupBy('tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
            ->get();
        //dd($training);

        $pdf=PDF::loadView('report.pdf.completedTrainingPDF',compact('training'));
        $pdf->setPaper('A4', 'portrait');
        $name=$current_date."completedTrainingPDF.pdf";
        return $pdf->download($name);

    }


    public function employee_training_history_report(){


        $training=DB::table('employees')
            ->leftjoin('tb_employee_training', 'employees.id','=','tb_employee_training.employee_id')
            ->leftjoin('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
            ->leftjoin('designations', 'designations.id','=','employees.empDesignationId')
            ->select('employees.employeeId','employees.empFirstName','employees.empLastName',
                DB::raw("(GROUP_CONCAT(tb_training.training_name  SEPARATOR ' , ')) as `training_name`"),
                'designations.designation','tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
            ->where('tb_employee_training.training_ending_date',"<",Carbon::now())
            ->groupBy('employees.id')
            ->get();

        $training2=DB::table('employees')
            ->leftjoin('tb_employee_training', 'employees.id','=','tb_employee_training.employee_id')
            ->leftjoin('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
            ->leftjoin('designations', 'designations.id','=','employees.empDesignationId')
            ->select('employees.employeeId','employees.empFirstName','employees.empLastName',
                'designations.designation','tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
            ->whereNotIn('employees.id',function($query) {
                $query->select('employee_id')->from('tb_employee_training')
                    ->where('tb_employee_training.training_ending_date',"<",Carbon::now());
            })
            ->groupBy('employees.id')
            ->get();
        //dd($training,$training2);
        return view('report.training.employeeTrainingHistoryReport',compact('training','training2'));
    }
    public function employeeTrainingPDFReport(){
        $training=DB::table('employees')
            ->leftjoin('tb_employee_training', 'employees.id','=','tb_employee_training.employee_id')
            ->leftjoin('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
            ->leftjoin('designations', 'designations.id','=','employees.empDesignationId')
            ->select('employees.employeeId','employees.empFirstName','employees.empLastName',
                DB::raw("(GROUP_CONCAT(tb_training.training_name  SEPARATOR ' , ')) as `training_name`"),
                'designations.designation','tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
            ->where('tb_employee_training.training_ending_date',"<",Carbon::now())
            ->groupBy('employees.id')
            ->get();

        $training2=DB::table('employees')
            ->leftjoin('tb_employee_training', 'employees.id','=','tb_employee_training.employee_id')
            ->leftjoin('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
            ->leftjoin('designations', 'designations.id','=','employees.empDesignationId')
            ->select('employees.employeeId','employees.empFirstName','employees.empLastName',
                'designations.designation','tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
            ->whereNotIn('employees.id',function($query) {
                $query->select('employee_id')->from('tb_employee_training')
                    ->where('tb_employee_training.training_ending_date',"<",Carbon::now());
            })
            ->groupBy('employees.id')
            ->get();
        $current_date=date("d-m-Y");
        $pdf=PDF::loadView('report.pdf.employeeTrainingPDFReport',compact('training','training2'));
        $pdf->setPaper('A4', 'portrait');
        $name=$current_date."_enployee_training.pdf";
        return $pdf->download($name);

    }

    public function training_report_show(Request $request){
        // return $request->all();
        $start=date("Y-m-d",strtotime($request->training_start));
        $end=date("Y-m-d",strtotime($request->training_end));
        $training_id=$request->training_id;
        //dd($start,$end,$training_id);
        if($training_id==0){
            $trainingwiseEmployee=DB::table('tb_employee_training')
                ->join('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
                ->join('employees', 'employees.id','=','tb_employee_training.employee_id')
                ->join('designations', 'designations.id','=','employees.empDesignationId')
                ->select('tb_training.training_name', DB::raw("(GROUP_CONCAT(employees.empFirstName,' ', employees.empLastName,'(',employees.employeeId,')'  SEPARATOR ' , '))
             as `employee_name`"),'tb_training.id as tid','tb_employee_training.training_starting_date','tb_employee_training.training_ending_date','employees.employeeId','designations.designation')
                ->whereBetween('tb_employee_training.training_starting_date',[$start, $end])

                ->groupBy('tb_training.id')
                ->groupBy('tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
                ->get();
        }
        else{
            $trainingwiseEmployee=DB::table('tb_employee_training')
                ->join('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
                ->join('employees', 'employees.id','=','tb_employee_training.employee_id')
                ->join('designations', 'designations.id','=','employees.empDesignationId')
                ->select('tb_training.training_name', DB::raw("(GROUP_CONCAT(employees.empFirstName,' ', employees.empLastName,'(',employees.employeeId,')'  SEPARATOR ' , '))
             as `employee_name`"),'tb_training.id as tid','tb_employee_training.training_starting_date','tb_employee_training.training_ending_date','employees.employeeId','designations.designation')
                ->whereBetween('tb_employee_training.training_starting_date',[$start, $end])
                ->where(['tb_training.id'=>$training_id])
                ->groupBy('tb_training.id')
                ->groupBy('tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
                ->get();
        }
        return view('report.training.ajaxtable1',compact('trainingwiseEmployee'));
    }

    public function trainingWiseEmployeePDF(Request $request){
        // return $request->all();
        $start=date("Y-m-d",strtotime($request->training_start));
        $end=date("Y-m-d",strtotime($request->training_end));
        $training_id=$request->training_id;
        //dd($start,$end,$training_id);
        if($training_id==0){
            $trainingwiseEmployee=DB::table('tb_employee_training')
                ->join('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
                ->join('employees', 'employees.id','=','tb_employee_training.employee_id')
                ->join('designations', 'designations.id','=','employees.empDesignationId')
                ->select('tb_training.training_name', DB::raw("(GROUP_CONCAT(employees.empFirstName,' ', employees.empLastName,'(',employees.employeeId,')'  SEPARATOR ' , '))
             as `employee_name`"),'tb_training.id as tid','tb_employee_training.training_starting_date','tb_employee_training.training_ending_date','employees.employeeId','designations.designation')
                ->whereBetween('tb_employee_training.training_starting_date',[$start, $end])

                ->groupBy('tb_training.id')
                ->groupBy('tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
                ->get();
        }
        else{
            $trainingwiseEmployee=DB::table('tb_employee_training')
                ->join('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
                ->join('employees', 'employees.id','=','tb_employee_training.employee_id')
                ->join('designations', 'designations.id','=','employees.empDesignationId')
                ->select('tb_training.training_name', DB::raw("(GROUP_CONCAT(employees.empFirstName,' ', employees.empLastName,'(',employees.employeeId,')'  SEPARATOR ' , '))
             as `employee_name`"),'tb_training.id as tid','tb_employee_training.training_starting_date','tb_employee_training.training_ending_date','employees.employeeId','designations.designation')
                ->whereBetween('tb_employee_training.training_starting_date',[$start, $end])
                ->where(['tb_training.id'=>$training_id])
                ->groupBy('tb_training.id')
                ->groupBy('tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
                ->get();
        }

        $current_date=date("d-m-Y");
        $pdf=PDF::loadView('report.pdf.trainingwiseEmployeePDF',compact('trainingwiseEmployee','request'));
        $pdf->setPaper('A4', 'portrait');
        $name=$current_date."_trainingwiseEmployee.pdf";
        return $pdf->download($name);
    }



    public function pdfOngoingTrainingCard(){
        $current_date=date("Y-m-d");
        //dd($current_date);

        $training=DB::table('tb_employee_training')
            ->join('tb_training', 'tb_training.id','=','tb_employee_training.training_id')
            ->join('employees', 'employees.id','=','tb_employee_training.employee_id')
            ->join('designations', 'designations.id','=','employees.empDesignationId')
            ->select('tb_training.training_name',
                DB::raw("(GROUP_CONCAT(employees.empFirstName,' ', employees.empLastName,
            '(',employees.employeeId,')'  SEPARATOR ' , ')) as `employee_name`"),
                'tb_training.id as tid','tb_employee_training.training_starting_date',
                'tb_employee_training.training_ending_date','employees.employeeId',
                'designations.designation')
            ->where('tb_employee_training.training_starting_date','<=',$current_date)
            ->where('tb_employee_training.training_ending_date','>=',$current_date)
            ->groupBy('tb_training.id')
            ->groupBy('tb_employee_training.training_starting_date','tb_employee_training.training_ending_date')
            ->get();
        //dd($training);
        $pdf=PDF::loadView('PDF.ongoingTrainingCard',compact('training'));
        $pdf->setPaper('A4', 'portrait');
        $name=time()."_ongoing_training_card.pdf";
        return $pdf->download($name);

    }
    public function vacancyListFirst(){
        return view('report.recruitment.vacancyList');
    }

    public function vacancyList(Request $request){
        $vacancy_start=date("Y-m-d",strtotime($request->vacancy_start));
        $vacancy_end=date("Y-m-d",strtotime($request->vacancy_end));
        //dd($vacancy_end);
        $vacancyList=DB::table('vacancies')
            ->where('VacAnnounceStartingDate','>',$vacancy_start)
            ->where('vacAnnounceEndingDate','<',$vacancy_end)
            ->get();
        //dd($vacancyList);


        $current_date=date("m/d/Y");
        //dd($request->tests);
        if (($request->viewType)=="Preview") {
            return view('report.recruitment.vacancyListReport',compact('vacancyList'));
        }
        elseif(($request->viewType)=="pdfenglish"){
            $pdf=PDF::loadView('report.pdf.vacancyListPDF',compact('vacancyList','request'));
            $pdf->setPaper('A4', 'Potrait');
            $name=$current_date."Vacancy_list.pdf";
            return $pdf->download($name);

        }

        elseif(($request->viewType)=="pdfbangla"){
            $pdf=mPDF::loadView('report.pdf.vacancyListPDFBangla',compact('vacancyList','request'));
            //$pdf->setPaper('A4', 'Potrait');
            $name=$current_date."Vacancy_list.pdf";
            return $pdf->download($name);

        }

        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }
    public function applicantList(){
        $applicantList=DB::table('vacancies')
            ->select('vacTitle','id as vID')
            ->get();
        //dd($applicantList);
        return view('report.recruitment.applicantList',compact('applicantList'));
    }
    public function applicantListShow(Request $request){
        $query=@"SELECT vacancy_application.*,vacancies.* 
        FROM vacancy_application JOIN vacancies ON 
        vacancy_application.vacancy_id=vacancies.id  ";

        if (($request->vacId)!=0) {
            $query.=" AND vacancies.id='".$request->vacId."'";
        }

        if (($request->priority)!=0) {
            $query.=" AND vacancy_application.priorityStatus='".$request->priority."'";
        }

        if (($request->elgInterview)!=0) {
            $query.=" AND vacancy_application.interviewStatus='".$request->elgInterview."'";
        }

        if (!empty($request->jobStatus)) {
            $query.=" AND vacancy_application.jobStatus='".$request->jobStatus."'";
        }

        if (($request->vacStatus)!=3) {
            $query.=" AND vacancies.vacStatus='".$request->vacStatus."'";
        }


        $query.=" ORDER BY vacancy_application.submittedDate ASC";
        // $query.=" GROUP BY 'unit_id'";

        $applicantListShow= DB::select(DB::raw($query));


        if (($request->viewType)=="Preview") {
            return view('report.recruitment.viewApplicantList',compact('applicantListShow','request'));
        }
        elseif(($request->viewType)=="pdfenglish"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.ApplicantListPDF',compact('applicantListShow','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."ApplicantList.pdf";
            return $pdf->download($name);
        }
        elseif(($request->viewType)=="pdfbangla"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=mPDF::loadView('report.pdf.ApplicantListPDFBangla',compact('applicantListShow','request'));
            //$pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."ApplicantList.pdf";
            return $pdf->download($name);
        }

        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function acceptedList(){
        $acceptedList=DB::table('vacancy_application')
            ->join('vacancies', 'vacancies.id','=','vacancy_application.vacancy_id')
            ->select('name','phone','vacTitle','interviewDate','submittedDate','priorityStatus','vacStatus')
            ->where(['jobStatus'=>'Confirm'])
            ->get();
        //dd($applicantList);
        return view('report.recruitment.accepted',compact('acceptedList'));
    }

    public function acceptedListPDF(){

        $acceptedList=DB::table('vacancy_application')
            ->join('vacancies', 'vacancies.id','=','vacancy_application.vacancy_id')
            ->select('name','phone','vacTitle','interviewDate','submittedDate','priorityStatus','vacStatus')
            ->where(['jobStatus'=>'Confirm'])
            ->get();
        //dd($applicantList);
        $current_date=date("m/d/Y");
        $pdf=PDF::loadView('report.pdf.ApplicantAcceptedListPDF',compact('acceptedList'));
        $pdf->setPaper('A4','Potrait');
        $name=$current_date."ApplicantAcceptedList.pdf";
        return $pdf->download($name);
    }

    public function acceptedListPDFBangla(){

        $acceptedList=DB::table('vacancy_application')
            ->join('vacancies', 'vacancies.id','=','vacancy_application.vacancy_id')
            ->select('name','phone','vacTitle','interviewDate','submittedDate','priorityStatus','vacStatus')
            ->where(['jobStatus'=>'Confirm'])
            ->get();
        //dd($applicantList);
        $current_date=date("m/d/Y");
        $pdf=mPDF::loadView('report.pdf.ApplicantAcceptedListPDFBangla',compact('acceptedList'));
        //$pdf->setPaper('A4','Potrait');
        $name=$current_date."ApplicantAcceptedList.pdf";
        return $pdf->download($name);
    }

    public function dailyRecruitmentList(){
        $current_date=date("Y-m-d");
        $dailyRecruitment=DB::table('vacancy_application')
            ->join('vacancies', 'vacancies.id','=','vacancy_application.vacancy_id')
            ->select('name','phone','vacTitle','interviewDate','submittedDate','priorityStatus','vacStatus')
            ->where(['submittedDate'=>$current_date])
            ->get();
        //dd($dailyRecruitment);
        return view('report.recruitment.dailyRecruitmentList',compact('dailyRecruitment'));
    }

    public function dailyRecruitmentListPDF(){
        $current_date=date("Y-m-d");
        $dailyRecruitment=DB::table('vacancy_application')
            ->join('vacancies', 'vacancies.id','=','vacancy_application.vacancy_id')
            ->select('name','phone','vacTitle','interviewDate','submittedDate','priorityStatus','vacStatus')
            ->where(['submittedDate'=>$current_date])
            ->get();
        //dd($dailyRecruitment);
        $current_date=date("d/m/Y");
        $pdf=PDF::loadView('report.pdf.dailyRecruitmentListPDF',compact('dailyRecruitment'));
        $pdf->setPaper('A4','Potrait');
        $name=$current_date."dailyRecruitment.pdf";
        return $pdf->download($name);

    }

    public function earnLeave(){
        $units=DB::table('units')->distinct()->groupBy('name')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        return view('report.leave.earnLeaveReport',compact('units','departments','floors','sections','designations'));

        //return view('report.leave.earnLeaveReport',compact('designation'));
    }

    public function maternityLeave(){
        $units=DB::table('units')->distinct()->groupBy('name')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        return view('report.leave.maternityLeaveReport',compact('units','departments','floors','sections','designations'));

        //return view('report.leave.earnLeaveReport',compact('designation'));
    }

    public function viewMaternityReport(Request $request)
    {
        $query=@"SELECT tb_maternity_allowance.*, employees.empFirstName,employees.empLastName,employees.employeeId,
        employees.empSection,employees.id,employees.empJoiningDate,designations.designation,
        departments.departmentName,floors.floor floorName,units.name unitName
        FROM tb_maternity_allowance
        LEFT JOIN employees ON tb_maternity_allowance.employee_id=employees.id  
        LEFT JOIN designations ON employees.empDesignationId=designations.id 
        LEFT JOIN departments ON employees.empDepartmentId=departments.id 
        LEFT JOIN units ON employees.unit_id=units.id 
        LEFT JOIN floors ON employees.floor_id=floors.id
        WHERE tb_maternity_allowance.employee_id>0";

        if (($request->method) != '01') {
            $query.=" AND tb_maternity_allowance.other3=".$request->method."";
            //dd($query);
        }
        if (($request->unitId)!=0) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (($request->floorId)!=0) {
            $query.=" AND employees.floor_id='".$request->floorId."'";
        }

        if (!empty($request->sectionName)) {
            $query.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
        }

        //$query.="ORDER BY employees.id ASC";
        // $quer.=" GROUP BY 'unit_id'";

        $maternityLeave= DB::select(DB::raw($query));

        //dd($maternityLeave);

        if (($request->viewType)=="Preview") {
            return view('report.leave.view_maternityLeave_report',compact('maternityLeave','request'));
        }
        elseif(($request->viewType)=="pdfbangla"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=mPDF::loadView('report.pdf.maternityLeave_pdf',compact('maternityLeave','request'));
            //$pdf->setPaper($pagesize, $pageOrientation);
            $name=date("d-M-Y")."_maternityLeave.pdf";
            return $pdf->download($name);
        }
        elseif(($request->viewType)=="pdfenglish"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.maternityLeave_pdfenglish',compact('maternityLeave','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=date("d-M-Y")."_maternityLeave.pdf";
            return $pdf->download($name);
        }
        elseif(($request->viewType)=="Generate Excel"){
            // $maternityLeaveArray = [];
            // $maternityLeaveArray[] = ['id', 'Name','Designation','Department','Leave Period',
            // 'Previous 3 Month Salary','Previous 3 Month Working Days','Total Payable amount',
            // 'Per Installment','First Installment Date','Second Installment date','Status','Approved By'];

            // foreach ($maternityLeave as $maternityLeave) {
            //     $maternityLeaveArray[] = $maternityLeave;
            // }
            // Excel::create('users', function ($excel) use ($maternityLeaveArray) {

            //     // Build the spreadsheet, passing in the users array
            //     $excel->sheet('sheet1', function ($sheet) use ($maternityLeaveArray) {
            //         $sheet->fromArray($maternityLeaveArray);
            //     });

            // })->download('xlsx');
            Excel::create('Report2016', function($excel) use ($maternityLeave) {

                // Set the title
                $excel->setTitle('My awesome report 2016');

                // Chain the setters
                $excel->setCreator('Me')->setCompany('Our Code World');

                $excel->setDescription('A demonstration to change the file properties');


                $data = $maternityLeave;

                $excel->sheet('Sheet 1', function ($sheet) use ($data) {
                    $sheet->setOrientation('landscape');
                    $sheet->fromArray($data, NULL, 'A3');
                });

            })->download('xlsx');
        }
        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }

    public function viewEarnReport(Request $request)
    {
        $query=@"SELECT employees.empFirstName,employees.empLastName,employees.employeeId,employees.empSection,
        employees.empJoiningDate,tb_leave_type.total_days,tb_salary_history.month,
        tb_salary_history.net_amount,tb_employee_leave.leave_taken,tb_employee_leave.leave_available,
        designations.designation,departments.departmentName, floors.floor floorName, units.name unitName 
        FROM employees 
        LEFT JOIN designations ON employees.empDesignationId=designations.id 
        LEFT JOIN tb_leave_application ON employees.id=tb_leave_application.employee_id
        LEFT JOIN tb_salary_history ON employees.id=tb_salary_history.emp_id
        LEFT JOIN tb_employee_leave ON employees.id=tb_employee_leave.employee_id
        LEFT JOIN tb_leave_type ON tb_leave_type.id=tb_employee_leave.leave_type_id
        LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units 
        ON employees.unit_id=units.id LEFT JOIN floors ON employees.floor_id=floors.id 
        WHERE employees.id>0 AND tb_leave_application.leave_type_id=4 AND tb_leave_application.status=1
        AND tb_employee_leave.leave_type_id=4 AND tb_employee_leave.year=YEAR(CURRENT_DATE) AND YEAR(CURDATE())-YEAR(empJoiningDate)>=1 
        AND MONTH(tb_salary_history.month)=MONTH(CURRENT_DATE - INTERVAL 1 MONTH)" ;

        if (($request->unitId)!=0) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (($request->floorId)!=0) {
            $query.=" AND employees.floor_id='".$request->floorId."'";
        }

        if (!empty($request->sectionName)) {
            $query.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
        }

        if (($request->empGenderId)!=0) {
            $query.=" AND employees.empGenderId='".$request->empGenderId."'";
        }

        $query.="GROUP BY employees.id ORDER BY employees.employeeId ASC";
        // $quer.=" GROUP BY 'unit_id'";

        $employee_benefit= DB::select(DB::raw($query));

        $query2=@"SELECT employees.empFirstName,employees.empLastName,employees.employeeId,employees.empSection,
                employees.empJoiningDate,tb_salary_history.month,
                tb_salary_history.net_amount,designations.designation,departments.departmentName, 
                floors.floor floorName, units.name unitName 
                FROM employees 
                LEFT JOIN designations ON employees.empDesignationId=designations.id 
                LEFT JOIN tb_salary_history ON employees.id=tb_salary_history.emp_id
                LEFT JOIN departments ON employees.empDepartmentId=departments.id 
                LEFT JOIN units ON employees.unit_id=units.id 
                LEFT JOIN floors ON employees.floor_id=floors.id 
                WHERE employees.id>0 
                AND employees.id NOT IN (SELECT employee_id
                                    FROM `tb_leave_application`
                                    WHERE tb_leave_application.leave_type_id=4 
                                    and tb_leave_application.status=1)
                AND YEAR(CURDATE())-YEAR(empJoiningDate)>=1 
                AND MONTH(tb_salary_history.month)=MONTH(CURRENT_DATE - INTERVAL 1 MONTH)" ;

        if (($request->unitId)!=0) {
            $query2.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (($request->floorId)!=0) {
            $query2.=" AND employees.floor_id='".$request->floorId."'";
        }

        if (!empty($request->sectionName)) {
            $query2.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $query2.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query2.=" AND employees.empDesignationId='".$request->designationId."'";
        }

        if (($request->empGenderId)!=0) {
            $query2.=" AND employees.empGenderId='".$request->empGenderId."'";
        }

        $query2.="ORDER BY employees.employeeId ASC";
        // $quer.=" GROUP BY 'unit_id'";

        $employee_without_leave_benefit= DB::select(DB::raw($query2));

        $earnLeave=DB::table("tb_leave_type")
            ->where(['id'=>4])
            ->get();

        //dd($employee_benefit);
        if (($request->viewType)=="Preview") {
            return view('report.leave.view_earnLeave_report',compact('employee_benefit','earnLeave','employee_without_leave_benefit','request'));
        }
        elseif(($request->viewType)=="pdfenglish"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.earnLeave_pdf',compact('employee_benefit','earnLeave','employee_without_leave_benefit','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."earnLeave.pdf";
            return $pdf->download($name);
        }
        elseif(($request->viewType)=="pdfbangla"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=mPDF::loadView('report.pdf.earnLeave_bangla_pdf',compact('employee_benefit','earnLeave','employee_without_leave_benefit','request'));
            //$pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."earnLeave.pdf";
            return $pdf->download($name);
        }

        else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }

    }

    public function availableLeaveReport(){
        $units=DB::table('units')->distinct()->groupBy('name')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        $employees= DB::table('employees')
            ->select(DB::raw("CONCAT(empFirstName,' ',empLastName,' , ID : ',employeeId)AS full_name, id"))
            ->get(['full_name','id']);
        return view('report.leave.employeeAvailableLeaveReport',compact('employees','units','departments','floors','sections','designations'));
    }

    public function viewEmployeeAvailableLeave(Request $request){
        $query=@"SELECT employees.empFirstName,employees.empLastName,employees.employeeId,employees.empSection,
        employees.empJoiningDate,
        GROUP_CONCAT(DISTINCT tb_leave_type.leave_type SEPARATOR ', ') as leave_name,
        GROUP_CONCAT(DISTINCT tb_leave_type.total_days SEPARATOR ', ') as total_days,
        GROUP_CONCAT(DISTINCT tb_employee_leave.leave_type_id SEPARATOR ', ') as leave_id,
        GROUP_CONCAT(tb_employee_leave.leave_taken SEPARATOR ', ') as leave_taken,
        GROUP_CONCAT(tb_employee_leave.leave_available SEPARATOR ', ') as leave_available,
        designations.designation,departments.departmentName, floors.floor floorName, units.name unitName 
        FROM employees 
        LEFT JOIN designations ON employees.empDesignationId=designations.id
        LEFT JOIN tb_employee_leave ON employees.id=tb_employee_leave.employee_id
        LEFT JOIN tb_leave_type ON tb_leave_type.id=tb_employee_leave.leave_type_id
        LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units 
        ON employees.unit_id=units.id LEFT JOIN floors ON employees.floor_id=floors.id 
        WHERE employees.id>0 AND tb_employee_leave.year=YEAR(CURDATE())";

        if (($request->empId)!=0) {
            $query.=" AND employees.unit_id='".$request->empId."'";
        }

        if (($request->unitId)!=0) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (($request->floorId)!=0) {
            $query.=" AND employees.floor_id='".$request->floorId."'";
        }

        if (!empty($request->sectionName)) {
            $query.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
        }

        if (($request->empGenderId)!=0) {
            $query.=" AND employees.empGenderId='".$request->empGenderId."'";
        }

        $query.="GROUP BY tb_employee_leave.employee_id ORDER BY employees.employeeId ASC";
        // $quer.=" GROUP BY 'unit_id'";

        $leaveAvailable= DB::select(DB::raw($query));
        dd($leaveAvailable);

        $query2=@"SELECT employees.empFirstName,employees.empLastName,employees.employeeId,employees.empSection,
                employees.empJoiningDate,tb_salary_history.month,
                tb_salary_history.net_amount,designations.designation,departments.departmentName, 
                floors.floor floorName, units.name unitName 
                FROM employees 
                LEFT JOIN designations ON employees.empDesignationId=designations.id 
                LEFT JOIN tb_salary_history ON employees.id=tb_salary_history.emp_id
                LEFT JOIN departments ON employees.empDepartmentId=departments.id 
                LEFT JOIN units ON employees.unit_id=units.id 
                LEFT JOIN floors ON employees.floor_id=floors.id 
                WHERE employees.id>0 
                AND employees.id NOT IN (SELECT employee_id
                                    FROM `tb_leave_application`
                                    WHERE tb_leave_application.leave_type_id=4 
                                    and tb_leave_application.status=1)
                AND YEAR(CURDATE())-YEAR(empJoiningDate)>=1 
                AND MONTH(tb_salary_history.month)=MONTH(CURRENT_DATE - INTERVAL 1 MONTH)" ;

        if (($request->unitId)!=0) {
            $query2.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (($request->floorId)!=0) {
            $query2.=" AND employees.floor_id='".$request->floorId."'";
        }

        if (!empty($request->sectionName)) {
            $query2.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $query2.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query2.=" AND employees.empDesignationId='".$request->designationId."'";
        }

        if (($request->empGenderId)!=0) {
            $query2.=" AND employees.empGenderId='".$request->empGenderId."'";
        }

        $query2.="ORDER BY employees.employeeId ASC";
        // $quer.=" GROUP BY 'unit_id'";

        $employee_without_leave_benefit= DB::select(DB::raw($query2));

        $earnLeave=DB::table("tb_leave_type")
            ->where(['id'=>4])
            ->get();

        //dd($employee_benefit);
        if (($request->viewType)=="Preview") {
            return view('report.leave.view_earnLeave_report',compact('employee_benefit','earnLeave','employee_without_leave_benefit','request'));
        }elseif(($request->viewType)=="Generate PDF"){
            $pagesize=$request->pagesize;
            $pageOrientation=$request->pageOrientation;
            $pdf=PDF::loadView('report.pdf.earnLeave_pdf',compact('employee_benefit','earnLeave','employee_without_leave_benefit','request'));
            $pdf->setPaper($pagesize, $pageOrientation);
            $name=time()."earnLeave.pdf";
            return $pdf->download($name);
        }else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }
    public function personWiseMaternityLeaveBenefit($id){

        //dd($id);

        // $query=@"SELECT tb_maternity_allowance.*, employees.*,designations.designation,
        // departments.departmentName,floors.floor floorName,units.name unitName,nominees.*
        // FROM tb_maternity_allowance
        // LEFT JOIN employees ON tb_maternity_allowance.employee_id=employees.id  
        // LEFT JOIN nominees ON nominees.emp_id=employees.id  
        // LEFT JOIN designations ON employees.empDesignationId=designations.id 
        // LEFT JOIN departments ON employees.empDepartmentId=departments.id 
        // LEFT JOIN units ON employees.unit_id=units.id 
        // LEFT JOIN floors ON employees.floor_id=floors.id
        // WHERE tb_maternity_allowance.employee_id>0 AND employees.id=$id";

        $employee=DB::table('tb_maternity_allowance')
            ->leftjoin('employees','tb_maternity_allowance.employee_id','=','employees.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('nominees', 'nominees.emp_id','=','employees.id')
            ->leftjoin('units', 'employees.unit_id','=','units.id')
            ->select('employees.*','nominees.*','units.name as unitName','tb_maternity_allowance.*','designations.designation','departments.departmentName')
            ->where('employees.id','=',$id)
            ->first();

        $current_month=Carbon::now()->format('m-Y');
        $dayCount=cal_days_in_month(CAL_GREGORIAN,Carbon::now()->subMonth()->format('m'),Carbon::now()->subMonth()->format('Y'));
        $present_salary=DB::table('payroll_salary')->where('emp_id','=',$id)->where('current_month','=',$current_month)->select('total_employee_salary','basic_salary')->first();

        //dd($employee);

        $pdf=mPDF::loadView('report.pdf.personWiseMaternityLeaveBenefitPDF',compact('employee','present_salary'));
        //$pdf->setPaper($pagesize, $pageOrientation);
        $name=date("d-M-Y")."_maternityLeave.pdf";
        return $pdf->download($name);
    }

    public function notification_list(){
        $notifi=DB::table('notifications')
            ->leftjoin('users','users.id','=','notifications.notification_by')
            ->select('notifications.*','users.name as userName')
            ->orderBy('id','DESC')
            ->limit(1000)
            ->get();
        $t=DB::table('notifications')->whereNull('read_at')->update([
            'read_at'=>Carbon::now()->toDateTimeString(),
        ]);

        return view('report.notifications',compact('notifi'));
    }

    public function newEmployeeListReport(){
        $units=DB::table('units')->distinct()->groupBy('name')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        return view('report.employee.new_employee_list_report',compact('units','departments','floors','sections','designations'));
    }

    public function viewNewEmployeeList(Request $request){
        $startDate=date("Y-m-d", strtotime($request->startDate));
        $endDate=date("Y-m-d", strtotime($request->endDate));
        $query2=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName, payroll_salary.total_employee_salary, attendance_setup.shiftName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN  payroll_salary ON employees.id=payroll_salary.emp_id LEFT JOIN attendance_setup ON attendance_setup.id=employees.empAttBonusId LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.id>0 AND employees.empJoiningDate BETWEEN '".$startDate."' AND '".$endDate."' ";

        if (($request->unitId)!=0) {
            $query2.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (($request->floorId)!=0) {
            $query2.=" AND employees.floor_id='".$request->floorId."'";
        }

        if (($request->line_id)!=0) {
            $query2.=" AND employees.line_id='".$request->line_id."'";
        }

        if (!empty($request->sectionName)) {
            $query2.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $query2.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query2.=" AND employees.empDesignationId='".$request->designationId."'";
        }

        if (($request->empGenderId)!=0) {
            $query2.=" AND employees.empGenderId='".$request->empGenderId."'";
        }
        $query2.=" AND employees.empAccStatus=1";

        $query2.=" ORDER BY employees.empSection ASC";
//        $query2.=" ORDER BY employees.empJoiningDate ASC";

        // return $query2; 
        $employees= DB::select(DB::raw($query2));
        // dd($employees);

        if (($request->viewType)=="Preview") {
            return view('print.preview_new_employee_list',compact('employees','request'));
//            return view('report.employee.view_new_employee_list',compact('employees','request'));
        }elseif(($request->viewType)=="print"){
            return view('print.print_new_employee_list',compact('employees','request'));
        }elseif(($request->viewType)=="GenerateExcel"){
            $excelName=time()."_employee_list";
            Excel::create("$excelName", function($excel) use ($employees, $request) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("New Employee List");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('New Employee List');

                $excel->sheet('Sheet 1', function ($sheet) use ($employees, $request) {
                    $sheet->loadView('report.excel.new_employee_list_excel')
                        ->with('employees',$employees)
                        ->with('request',$request);
                });

            })->download('xlsx');

        }else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }


    public function employee_attendance_summary(){
        $units=DB::table('units')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        return view('report.attendance.employee_attendance_summary',compact('units','departments','floors','sections','designations'));

    }

    public function employee_attendance_summary_data(Request $request){
        DB::table('total_employee_leave')->truncate();
        DB::table('tb_total_present')->truncate();
        DB::table('total_late_employee')->truncate();
        DB::table('total_weekend_present')->truncate();
        DB::table('regular_weekend_presence')->truncate();
        $regular_days_setup_check=DB::table('tbweekend_regular_day')->where('weekend_month',date('Y-m-01',strtotime($request->end_date)))->count();
        $regular_fridays_date=DB::table('tbweekend_regular_day')->where('weekend_month',date('Y-m-01',strtotime($request->end_date)))->get(['weekend_date']);
        $regular_weekend_dates=array();
        foreach ($regular_fridays_date as $rfd){
            $regular_weekend_dates[]=$rfd->weekend_date;
        }
//        return $regular_weekend_dates;

        $query=DB::table('employees')->leftJoin('units','employees.unit_id','=','units.id')
            ->leftJoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftJoin('designations','employees.empDesignationId','=','designations.id')
            ->leftJoin('floors','employees.floor_id','=','floors.id');

        if (($request->unitId)!=0) {
            $query=$query->where('employees.unit_id','=', $request->unitId);
//            $query.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (($request->floorId)!=0) {
            $query=$query->where('employees.floor_id','=', $request->floorId);
        }

        if (!empty($request->sectionName)) {
            $query=$query->where('employees.empSection','=', $request->sectionName);
        }

        if (($request->departmentId)!=0) {
            $query=$query->where('employees.empDepartmentId','=', $request->departmentId);

        }

        if (($request->accStatus)==0) {
            $query=$query->where('employees.empAccStatus','=', 0)
                ->where('employees.date_of_discontinuation','>', Carbon::parse($request->start_date)->toDateString());

        }
        else{
            $query=$query->where('employees.empAccStatus','=', 1)
                ->where('employees.empJoiningDate','<', Carbon::parse($request->end_date)->toDateString());


        }


        $query=$query->leftJoin('attendance_setup','employees.empShiftId','=','attendance_setup.id');

        $start=Carbon::parse($request->start_date)->toDateTimeString();
        $end=Carbon::parse($request->end_date)->toDateTimeString();

        $month=date('Y-m-d',strtotime($start));
        $holidays=$this->holiday($start,$end);
        $weekend=$this->weekendCalculator($start,$end)-$regular_days_setup_check;


        $regular_weekend_presence=DB::table('employees')
            ->join('attendance','employees.id', '=', 'attendance.emp_id')
            ->whereIn('attendance.date',$regular_weekend_dates)
            ->select('employees.id as emp_id',DB::raw("count(attendance.emp_id) as present"))
            ->groupBy('employees.id')
            ->get();


        foreach ($regular_weekend_presence as $presents){
            $insert[]=[
                'emp_id'=>$presents->emp_id,
                'regular_present'=>$presents->present,
            ];
        }


        if(!empty($insert)){
            DB::table('regular_weekend_presence')->insert($insert);
            unset($insert);
        }

        $present=DB::table('employees')->join('attendance','employees.id','=','attendance.emp_id')
            ->whereBetween('attendance.date',[$start,$end])
            ->where('empAccStatus','=',$request->accStatus)
            ->select('employees.id as emp_id',DB::raw("count(attendance.emp_id) as present"))
            ->groupBy('employees.id')
            ->get();

        $total_days = Carbon::parse($request->start_date)->diffInDays(Carbon::parse($request->end_date))+1;

        foreach ($present as $presents){
            $insert[]=[
                'emp_id'=>$presents->emp_id,
                'total_present'=>$presents->present,
                'month'=>$month,
            ];
        }


        if(!empty($insert)){
            DB::table('tb_total_present')->insert($insert);
            unset($insert);
        }
        $leave=DB::table('leave_of_employee')->where('month','=',$month)
            ->select("leave_of_employee.emp_id as employee_id",DB::raw("SUM(leave_of_employee.total) as total_leave"))
            ->groupBy('leave_of_employee.emp_id')
            ->get();
        foreach ($leave as $l){
            $insert[]=[
                'emp_idss'=>$l->employee_id,
                'total_leaves_taken'=>$l->total_leave,
                'month'=>$month,
            ];
        }
        if(!empty($insert)){
            DB::table('total_employee_leave')->insert($insert);
            unset($insert);
        }

        $late=DB::table('employees')
            ->join('attendance_setup','employees.empShiftId','=','attendance_setup.id')
            ->join('attendance','employees.id','=','attendance.emp_id')
            ->whereBetween('attendance.date',[$start,$end])
            ->where('attendance.in_time','>',DB::raw('attendance_setup.max_entry_time'))
            ->select('employees.id as emp_id',DB::raw('count(employees.id) as late'))
            ->groupBy('employees.id')
            ->get();
        foreach ($late as $la){
            $insert[]=[
                'emp_id'=>$la->emp_id,
                'late'=>$la->late,
            ];
        }
        if(!empty($insert)){
            DB::table('total_late_employee')->insert($insert);
            unset($insert);
        }

        $weekend_present=DB::table('employees')
            ->join('attendance','employees.id','=','attendance.emp_id')
            ->whereIn('attendance.date',$this->weekend_dates($start,$end))
            ->select(DB::raw('count(employees.id) as weekend_present'),'employees.id as emp_id')
            ->groupBy('employees.id')
            ->get();
        foreach ($weekend_present as $wp){
            $insert[]=[
                'emp_id'=>$wp->emp_id,
                'weekend_present'=>$wp->weekend_present,
            ];
        }
//        return $insert;
        if(!empty($insert)){
            DB::table('total_weekend_present')->insert($insert);
            unset($insert);
        }
//        return dd($weekend_present);


//        return $insert;

        $query=$query->leftJoin('total_employee_leave','employees.id','=','total_employee_leave.emp_idss')
            ->leftJoin('tb_total_present','employees.id','=','tb_total_present.emp_id')
            ->leftJoin('total_late_employee','employees.id','=','total_late_employee.emp_id')
            ->leftJoin('total_weekend_present','employees.id','=','total_weekend_present.emp_id')
            ->leftJoin('regular_weekend_presence','employees.id','=','regular_weekend_presence.emp_id')
//            ->where('employees.empAccStatus','=', $request->accStatus)
            ->select('employees.id','employees.empFirstName','employees.empLastName',
                'employees.employeeId', 'employees.empShiftId','employees.empJoiningDate','employees.empDOB',
                'employees.empCardNumber','employees.date_of_discontinuation','departments.departmentName','designations.designation','attendance_setup.shiftName',
                'attendance_setup.entry_time','attendance_setup.exit_time','attendance_setup.max_entry_time',
                'employees.empSection',
                DB::raw('IFNULL (total_employee_leave.total_leaves_taken,0) as total_leaves_taken'),
                DB::raw('IFNULL (tb_total_present.total_present,0) as total_present'),
                DB::raw('IFNULL (total_late_employee.late,0) as late'),
                DB::raw('IFNULL (regular_weekend_presence.regular_present,0) as regular_present'),
                DB::raw('IFNULL (total_weekend_present.weekend_present,0) as weekend_present'))->orderBy('employees.employeeId');

        $employees=$query->get();


        DB::table('total_employee_leave')->truncate();
        DB::table('tb_total_present')->truncate();
        DB::table('total_late_employee')->truncate();
        DB::table('total_weekend_present')->truncate();
        DB::table('regular_weekend_presence')->truncate();

//        return $employees;


        if($request->viewType=="Generate PDF"){
            return view('report.pdf.employee_attendance_summary_pdf',compact('employees','weekend','holidays','total_days','request','regular_days_setup_check'));
        }
        elseif($request->viewType=="Generate Excel"){
            $excelName=time()."_employee_attendance_summary";
            Excel::create("$excelName", function($excel) use ($employees, $request, $weekend, $holidays, $total_days, $regular_days_setup_check) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Employee Attendance Summary");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Employee List');

                $excel->sheet('Sheet 1', function ($sheet) use ($employees, $request, $weekend,$holidays,$total_days,$regular_days_setup_check) {
                    $sheet->loadView('report.excel.employee_attendance_summary_excel')
                        ->with('employees',$employees)
                        ->with('request',$request)
                        ->with('weekend',$weekend)
                        ->with('holidays',$holidays)
                        ->with('holidays',$regular_days_setup_check)
                        ->with('total_days',$total_days);
                });

            })->download('xlsx');
        }

        return view('report.attendance.employee_attendance_summary_data',compact('employees','weekend','holidays','total_days','request','regular_days_setup_check'));

    }

    public function search_attendance_summary(Request $request){
        return $request->all();
    }




    public function inactiveEmployeeListReport(){
        $units=DB::table('units')->distinct()->groupBy('name')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        return view('report.employee.inactive_employee_list_report',compact('units','departments','floors','sections','designations'));
    }

    public function viewInactiveEmployeeList(Request $request){
        $startDate=date("Y-m-d", strtotime($request->startDate));
        $endDate=date("Y-m-d", strtotime($request->endDate));
        $query2=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName, attendance_setup.shiftName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id LEFT JOIN attendance_setup ON attendance_setup.id=employees.empAttBonusId WHERE employees.empAccStatus='0' AND employees.date_of_discontinuation BETWEEN '".$startDate."' AND '".$endDate."' ";

        if (($request->unitId)!=0) {
            $query2.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (($request->floorId)!=0) {
            $query2.=" AND employees.floor_id='".$request->floorId."'";
        }

        if (($request->line_id)!=0) {
            $query2.=" AND employees.line_id='".$request->line_id."'";
        }

        if (!empty($request->sectionName)) {
            $query2.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $query2.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query2.=" AND employees.empDesignationId='".$request->designationId."'";
        }

        if (($request->empGenderId)!=0) {
            $query2.=" AND employees.empGenderId='".$request->empGenderId."'";
        }

        $query2.=" ORDER BY employees.empJoiningDate ASC";

        // return $query2; 
        $employees= DB::select(DB::raw($query2));
        // dd($employees);

        if (($request->viewType)=="Preview") {
            return view('report.employee.view_inactive_employee_list',compact('employees','request'));
        }elseif(($request->viewType)=="print"){
            return view('print.print_inactive_employee_list',compact('employees','request'));
        }elseif(($request->viewType)=="GenerateExcel"){
            $excelName=time()."_employee_list";
            Excel::create("$excelName", function($excel) use ($employees, $request) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Inactive Employee List");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Inactive Employee List');

                $excel->sheet('Sheet 1', function ($sheet) use ($employees, $request) {
                    $sheet->loadView('report.excel.inactive_employee_list_excel')
                        ->with('employees',$employees)
                        ->with('request',$request);
                });

            })->download('xlsx');

        }else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }
    public function resign_lefty_employee_list(){
        $units=DB::table('units')->distinct()->groupBy('name')->get(['name','id']);
        $departments=DB::table('departments')->get(['departmentName','id']);
        $floors=DB::table('floors')->get(['floor','id']);
        $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
        $designations=DB::table('designations')->get(['designation','id']);
        return view('report.employee.resign_lefty_employee_list',compact('units','departments','floors','sections','designations'));
    }

    public function resign_lefty_employee_list_data(Request $request){
//        return $request->all();
        $startDate=date("Y-m-d", strtotime($request->startDate));
        $endDate=date("Y-m-d", strtotime($request->endDate));
        $query2=@"SELECT employees.*,designations.designation,departments.departmentName,tblines.line_no LineName, floors.floor floorName, units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id LEFT JOIN tblines ON employees.line_id=tblines.id LEFT JOIN floors ON tblines.floor_id=floors.id WHERE employees.empAccStatus='0' AND employees.date_of_discontinuation BETWEEN '".$startDate."' AND '".$endDate."' ";

        if (($request->unitId)!=0) {
            $query2.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (($request->floorId)!=0) {
            $query2.=" AND employees.floor_id='".$request->floorId."'";
        }

        if (($request->line_id)!=0) {
            $query2.=" AND employees.line_id='".$request->line_id."'";
        }

        if (!empty($request->sectionName)) {
            $query2.=" AND employees.empSection='".$request->sectionName."'";
        }

        if (($request->departmentId)!=0) {
            $query2.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query2.=" AND employees.empDesignationId='".$request->designationId."'";
        }

        if (($request->empGenderId)!=0) {
            $query2.=" AND employees.empGenderId='".$request->empGenderId."'";
        }

        if (($request->disconType)!=3) {
            $query2.=" AND employees.discon_type='".$request->disconType."'";
        }

        $query2.=" ORDER BY employees.employeeId ASC";

//         return $query2;
        $employees= DB::select(DB::raw($query2));
        // dd($employees);

        if (($request->viewType)=="Preview") {
            return view('report.employee.resign_lefty_employee_list_data',compact('employees','request'));
        }elseif(($request->viewType)=="Generate PDF"){
            return view('print.resign_lefty_employee_list_data',compact('employees','request'));
        }elseif(($request->viewType)=="GenerateExcel"){
            $excelName=time()."_employee_list";
            Excel::create("$excelName", function($excel) use ($employees, $request) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Resign/Lefty Employee List");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Resign/Lefty Employee List');

                $excel->sheet('Sheet 1', function ($sheet) use ($employees, $request) {
                    $sheet->loadView('report.excel.resign_lefty_employee_list_data')
                        ->with('employees',$employees)
                        ->with('request',$request);
                });

            })->download('xlsx');

        }else{
            Session::flash('error_message', "Invalid Request");
            return view('404');
        }
    }
}
