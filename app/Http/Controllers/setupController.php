<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Excel;


class setupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    private function upDepartments(){
        DB::select("INSERT INTO `departments` (`id`, `departmentName`, `departmentDescription`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
            (2, 'General Support', 'General Support', 'Admin', 'Admin', '2018-06-26 05:06:41', '2018-06-26 05:06:41'),
            (4, 'Housekeeping', 'Housekeeping', 'Admin', 'Admin', '2018-06-26 05:07:11', '2018-06-26 05:07:11'),
            (5, 'Security', 'Security', 'Admin', 'Admin', '2018-06-26 05:07:31', '2018-06-26 05:07:31'),
            (8, 'Human Resource', 'Human Resource', 'Admin', 'Admin', '2018-06-26 05:09:52', '2018-06-26 05:09:52'),
            (10, 'Compliance', 'Compliance', 'Admin', 'Admin', '2018-06-26 05:10:39', '2018-06-26 05:10:39'),
            (23, 'Electrical Engineering', 'Electrical Engineering', 'Admin', 'Admin', '2018-06-26 05:11:19', '2018-06-26 05:11:19'),
            (25, 'Utility', 'Utility', 'Admin', 'Admin', '2018-06-26 05:11:57', '2018-06-26 05:11:57'),
            (28, 'Merchandising', 'Merchandising', 'Admin', 'Admin', '2018-06-26 05:12:24', '2018-06-26 05:12:24'),
            (30, 'Network & Infrastucture', 'Network & Infrastucture', 'Admin', 'Admin', '2018-06-26 05:12:48', '2018-06-26 05:12:48'),
            (31, 'Finishing', 'Finishing', 'Admin', 'Admin', '2018-06-26 05:08:36', '2018-06-26 05:08:36'),
            (33, 'Cutting', 'Cutting', 'Admin', 'Admin', '2018-06-26 05:13:32', '2018-06-26 05:13:32'),
            (34, 'Sewing', 'Sewing', 'Admin', 'Admin', '2018-06-26 05:14:00', '2018-06-26 05:14:00'),
            (35, 'Planning', 'Planning', 'Admin', 'Admin', '2018-06-26 05:15:12', '2018-06-26 05:15:12'),
            (38, 'Quality Assurance', 'Quality Assurance', 'Admin', 'Admin', '2018-06-26 05:15:49', '2018-06-26 05:15:49'),
            (40, 'Process Audit', 'Process Audit', 'Admin', 'Admin', '2018-06-26 05:16:18', '2018-06-26 05:16:18'),
            (42, 'Inspection', 'Inspection', 'Admin', 'Admin', '2018-06-26 05:16:36', '2018-06-26 05:16:36'),
            (44, 'General', 'General', 'Admin', 'Admin', '2018-06-26 05:17:10', '2018-06-26 05:17:10'),
            (46, 'Sample', 'Sample', 'Admin', 'Admin', '2018-06-26 05:17:33', '2018-06-26 05:17:33'),
            (47, 'CAD', 'CAD', NULL, NULL, NULL, NULL),
            (56, 'Accounts & Finance', 'Accounts & Finance', 'Admin', 'Admin', '2018-06-26 05:05:16', '2018-06-26 05:05:16'),
            (63, 'Marketing & Merchanding', 'Marketing & Merchanding', NULL, NULL, NULL, NULL),
            (64, 'Production', 'Production', NULL, NULL, NULL, NULL);");
    }

    private function upDesignations(){
        DB::select("INSERT INTO `designations` (`id`, `designation`, `description`, `created_at`, `updated_at`) VALUES
            (2, 'Executive Accounts', NULL, '2018-06-25 22:43:28', '2018-06-25 22:43:28'),
            (3, 'Purchase Officer', NULL, '2018-06-25 22:43:47', '2018-06-25 22:43:47'),
            (4, 'Driver', NULL, '2018-06-25 22:44:36', '2018-06-25 22:44:36'),
            (5, 'Cleaner', NULL, '2018-06-25 22:45:14', '2018-06-25 22:45:14'),
            (6, 'Security Guard', NULL, '2018-06-25 22:46:03', '2018-06-25 22:46:03'),
            (7, 'Security Supervisor', NULL, '2018-06-25 22:46:44', '2018-06-25 22:46:44'),
            (8, 'Loader', NULL, '2018-06-25 22:47:11', '2018-06-25 22:47:11'),
            (9, 'Jr. Personnel Officer', NULL, '2018-06-25 22:47:32', '2018-06-25 22:47:32'),
            (10, 'Jr. Officer Hr', NULL, '2018-06-25 22:47:53', '2018-06-25 22:47:53'),
            (11, 'Peon', NULL, '2018-06-25 22:48:30', '2018-06-25 22:48:30'),
            (12, 'H.supervisor', NULL, '2018-06-25 22:49:14', '2018-06-25 22:49:14'),
            (13, 'Jr. Welfare Officer', NULL, '2018-06-25 22:49:33', '2018-06-25 22:49:33'),
            (14, 'Time Executive', NULL, '2018-06-25 22:50:04', '2018-06-25 22:50:04'),
            (15, 'Baby Sitter', NULL, '2018-06-25 22:50:33', '2018-06-25 22:50:33'),
            (16, 'Cook', NULL, '2018-06-25 22:51:16', '2018-06-25 22:51:16'),
            (17, 'Checker', NULL, '2018-06-25 22:51:37', '2018-06-25 22:51:37'),
            (18, 'Scissor Man', NULL, '2018-06-25 22:52:28', '2018-06-25 22:52:28'),
            (19, 'Caretaker', NULL, '2018-06-25 22:52:49', '2018-06-25 22:52:49'),
            (20, 'Sweeper', NULL, '2018-06-25 22:53:11', '2018-06-25 22:53:11'),
            (21, 'Assistant Manager ( H R & Compliance)', NULL, '2018-06-25 22:53:38', '2018-06-25 22:53:38'),
            (22, 'Deputy Manager Compliance', NULL, '2018-06-25 22:54:05', '2018-06-25 22:54:05'),
            (23, 'Cutting Supervisor', NULL, '2018-06-25 22:55:17', '2018-06-25 22:55:17'),
            (24, 'Cutting Section Incharge', NULL, '2018-06-25 22:55:42', '2018-06-25 22:55:42'),
            (25, 'Cutting Asst.', NULL, '2018-06-25 22:56:17', '2018-06-25 22:56:17'),
            (26, 'Jr.cutter Man', NULL, '2018-06-25 22:56:44', '2018-06-25 22:56:44'),
            (27, 'Sr. Cutter Man', NULL, '2018-06-25 22:57:35', '2018-06-25 22:57:35'),
            (28, 'Cutter Man', NULL, '2018-06-25 22:58:13', '2018-06-25 22:58:13'),
            (29, 'Sr. Marker Man', NULL, '2018-06-25 22:58:34', '2018-06-25 22:58:34'),
            (30, 'Marker Man', NULL, '2018-06-25 22:59:15', '2018-06-25 22:59:15'),
            (31, 'Band Knife Op', NULL, '2018-06-25 22:59:42', '2018-06-25 22:59:42'),
            (32, 'Asst.op', NULL, '2018-06-25 23:00:13', '2018-06-25 23:00:13'),
            (33, 'Emb Supervisor', NULL, '2018-06-25 23:00:39', '2018-06-25 23:00:39'),
            (34, 'Sr. Operator', NULL, '2018-06-25 23:01:00', '2018-06-25 23:01:00'),
            (35, 'Sr. Quality Inspector', NULL, '2018-06-25 23:01:33', '2018-06-25 23:01:33'),
            (36, 'In-charge(emb.sec)', NULL, '2018-06-25 23:01:56', '2018-06-25 23:01:56'),
            (37, 'Designer', NULL, '2018-06-25 23:02:25', '2018-06-25 23:02:25'),
            (38, 'Sr. Supervisor', NULL, '2018-06-25 23:02:53', '2018-06-25 23:02:53'),
            (39, 'Finishing Supervisor', NULL, '2018-06-25 23:03:17', '2018-06-25 23:03:17'),
            (40, 'Finishing Iron Man', NULL, '2018-06-25 23:03:35', '2018-06-25 23:03:35'),
            (41, 'Agm (hr, Admin & Compliance)', 'Agm (hr, Admin & Compliance)', '2018-06-26 05:49:36', '2018-06-26 05:49:36'),
            (42, 'Agm (production.)', NULL, '2018-06-26 05:51:56', '2018-06-26 05:51:56'),
            (43, 'Asst. Electrical Executive', 'Asst. Electrical Executive', '2018-06-26 21:30:01', '2018-06-26 21:30:01'),
            (44, 'Asst. General Manager(f&a)', 'Asst. General Manager(f&a)', '2018-06-26 21:30:32', '2018-06-26 21:30:32'),
            (45, 'F.folding Man', 'F.folding Man', '2018-06-26 21:31:14', '2018-06-26 21:31:14'),
            (46, 'Asst. Manager ( I E )', 'Asst. Manager ( I E )', '2018-06-26 21:31:43', '2018-06-26 21:31:43'),
            (47, 'Asst. Manager (finishing Section)', 'Asst. Manager (finishing Section)', '2018-06-26 21:32:10', '2018-06-26 21:32:10'),
            (48, 'Fin.asst', 'Fin.asst', '2018-06-26 21:32:42', '2018-06-26 21:32:42'),
            (49, 'Asst. Manager (merchandiser)', 'Asst. Manager (merchandiser)', '2018-06-26 21:33:12', '2018-06-26 21:33:12'),
            (50, 'Asst. Manager(planning)', NULL, '2018-06-26 21:34:47', '2018-06-26 21:34:47'),
            (51, 'Asst. Manager(quality Assurance)', 'Asst. Manager(quality Assurance)', '2018-06-26 21:36:00', '2018-06-26 21:36:00'),
            (52, 'Asst. Merchandiser', 'Asst. Merchandiser', '2018-06-26 21:36:29', '2018-06-26 21:36:29'),
            (53, 'Jr. Operator', 'Jr. Operator', '2018-06-26 21:38:19', '2018-06-26 21:38:19'),
            (54, 'Operator', 'Operator', '2018-06-26 21:39:02', '2018-06-26 21:39:02'),
            (55, 'Quality Inspector', 'Quality Inspector', '2018-06-26 21:39:45', '2018-06-26 21:39:45'),
            (56, 'Work Study Officer', 'Work Study Officer', '2018-06-27 00:16:51', '2018-06-27 00:16:51'),
            (57, 'Welfare Officer', 'Welfare Officer', '2018-06-27 00:17:24', '2018-06-27 00:17:24'),
            (58, 'Wash Technician', 'Wash Technician', '2018-06-27 00:17:45', '2018-06-27 00:17:45'),
            (59, 'Sewing Supervisor', 'Sewing Supervisor', '2018-06-27 00:22:11', '2018-06-27 00:22:11'),
            (60, 'Wash Supervisor', 'Wash Supervisor', '2018-06-27 00:22:50', '2018-06-27 00:22:50'),
            (61, 'Wash Asst', 'Wash Asst', '2018-06-27 00:23:13', '2018-06-27 00:23:13'),
            (62, 'Training Center In-charge', 'Training Center In-charge', '2018-06-27 00:23:58', '2018-06-27 00:23:58'),
            (63, 'Trainer', 'Trainer', '2018-06-27 00:24:25', '2018-06-27 00:24:25'),
            (64, 'Trainee Merchandiser', 'Trainee Merchandiser', '2018-06-27 00:26:39', '2018-06-27 00:26:39'),
            (65, 'Technical Assistant', 'Technical Assistant', '2018-06-27 00:27:45', '2018-06-27 00:27:45'),
            (66, 'Store Officer', 'Store Officer', '2018-06-27 00:28:08', '2018-06-27 00:28:08'),
            (67, 'Store In-charge', 'Store In-charge', '2018-06-27 00:28:47', '2018-06-27 00:28:47'),
            (68, 'Store Assistant', 'Store Assistant', '2018-06-27 00:29:07', '2018-06-27 00:29:07'),
            (69, 'Line Iron Man', 'Line Iron Man', '2018-06-27 00:29:38', '2018-06-27 00:29:38'),
            (70, 'Sample Quality Inspector', NULL, '2018-06-27 00:31:15', '2018-06-27 00:31:15'),
            (71, 'Packing Man', 'Packing Man', '2018-06-27 00:31:39', '2018-06-27 00:31:39'),
            (72, 'Line Quality Controller', 'Line Quality Controller', '2018-06-27 00:32:04', '2018-06-27 00:32:04'),
            (73, 'Jr. Line Quality Controller', NULL, '2018-06-27 00:32:45', '2018-06-27 00:32:45'),
            (74, 'Sr. Technician', 'Sr. Technician', '2018-06-27 00:33:46', '2018-06-27 00:33:46'),
            (75, 'Sr. Merchandiser', 'Sr. Merchandiser', '2018-06-27 00:34:07', '2018-06-27 00:34:07'),
            (76, 'Spot Man', 'Spot Man', '2018-06-27 00:34:57', '2018-06-27 00:34:57'),
            (77, 'Sample Man', 'Sample Man', '2018-06-27 00:36:00', '2018-06-27 00:36:00')");
    }

    public function up()
    {
        $path = "/file/123456.xlsx";
        $data = Excel::load($path, function ($reader) {
        })->get();
//        $i=0;
//        return $data;
        if (!empty($data) && $data->count()) {
            foreach ($data as $value) {
               // dd ($value);
//                $i++;
//                return count($value);
                foreach ($value as $key => $v) {
//                    return $v;
                    if($v->emp_id==null){
                        continue;
                    }

                    if(!empty($value["$key"]->spous_name)){
                       $spous_name='Spous'; 
                    }else{
                        $spous_name="";
                    }

                    $insert = [
                        'employeeId' => $value["$key"]->emp_id,
                        'empCardNumber' => sprintf('%010s',$value["$key"]->proxy_id),
                        'empFirstName' => $value["$key"]->name,
                        'empDesignationId' => round($value["$key"]->designation),
                        'empDepartmentId' => $value["$key"]->department,
                        'empJoiningDate' => $value["$key"]->joining_date,
                        'empDOB' => $value["$key"]->date_of_birth,
                        'empFatherName' => $value["$key"]->father_name,
                        'empMotherName' => $value["$key"]->mother_name,
                        'empEcontactName' => $value["$key"]->spous_name,
                        'emergency_contact_relation' => $spous_name,
                        'empBloodGroup' => $value["$key"]->blood_group,
                        'empGenderId' => $value["$key"]->gender,
                        'empReligion' => $value["$key"]->religion,
                        'empMaritalStatusId' => $value["$key"]->marital_status,
                        'empPhone' => $value["$key"]->mobile_no,
                        'empNid' => $value["$key"]->nid,
                        'empNationalityId' => $value["$key"]->nationality,
                        'empPhoto' => 'default.jpg',
                        'unit_id' => '1',
                        'line_id' => $value["$key"]->line_no,
                        'floor_id' => '1',
                        'empSection' => $value["$key"]->section,
                        'empParAddress' => $value["$key"]->permanent_address,
                        'empCurrentAddress' => $value["$key"]->current_address,
                        'salary_type' => 'Monthly',
                        'bank_account' => $value["$key"]->bank_account_no,
                        'empRole' => "6",
                        'empAccStatus' => "1",
                    ];

                     $insertData=DB::table('employees')->insertGetId($insert);

                     if($v->salary!=NULL){
                        $gs=$v->salary;
                        $basic=($gs-1100)/1.4;
                        $house = (($basic/100)*40);
                       DB::table('payroll_salary')->insert([
                        'emp_id' => $insertData,
                        'grade_id' => '1',
                        'basic_salary' => $basic,
                        'house_rant' => $house,
                        'medical' => '250.00',
                        'transport' => '200.00',
                        'food' => '650.00',
                        'other' => '0.00',
                        'total_employee_salary' => $v->salary,
                        'current_month' => '10-2018',

                       ]);
                     }
                }

            }

            // return ($insert);

            if(!empty($insert)){
                 // $insertData=DB::table('employees')->insert($insert);
                if ($insertData){
                   // $this->upDesignations();
                    //$this->upDepartments();

                    return "OK";
                }
                else{
                    return "NOT OK";
                }
            }

        }
    }
    public function process_deg()
    {
                $dd=DB::table('departments1')
                ->select('departmentName')->orderBy('departmentName', 'ASC')->distinct()->get();
                $i=1;
                foreach($dd as $d){
                    $insert[] = [
                        'id' => $i++,
                        'line_no' => $d->departmentName,
                        'floor_id' => 1,
                    ];
                }

            // return ($insert);
            $insertData=DB::table('tblines')->insert($insert);

    }

    public function do()
    {
                $dd=DB::table('departments1')
                ->select('departmentName')->orderBy('departmentName', 'ASC')->distinct()->get();
                $i=1;
                foreach($dd as $d){
                    $insert[] = [
                        'id' => $i++,
                        'line_no' => $d->departmentName,
                        'floor_id' => 1,
                    ];
                }

            // return ($insert);
            $insertData=DB::table('tblines')->insert($insert);

    }

}
