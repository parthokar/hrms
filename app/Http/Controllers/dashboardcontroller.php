<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;

class dashboardcontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function user_profile(){
        $id=Auth::id();
        $employee=DB::table('employees')
            ->join('departments','employees.empDepartmentId','=','departments.id')
            ->join('marital_statuses','employees.empMaritalStatusId','=','marital_statuses.id')
            ->leftJoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftJoin('nationalities','employees.empNationalityId','=','nationalities.id')
            ->leftJoin('units','employees.unit_id','=','units.id')
            ->leftJoin('tblines','employees.line_id','=','tblines.id')
            ->leftJoin('floors','employees.floor_id','=','floors.id')
            ->select('employees.*','designations.designation','departments.departmentName',
                'marital_statuses.name as ms_name','nationalities.name as nationalitiesName',
                'units.name as unit_name', 'tblines.line_no','floors.floor','floors.id as ffid')
            ->where(['employees.id'=>$id])
            ->first();

        if(empty($employee)){
            return abort(404);
        }

        return view('employee.my_profile', compact('emp_id','skillTest','employee','attachments'));

    }

    public function dashboard_view(){

        return view('dashboard');
    }

    public static function getAttendanceValueByDate($date){

        $attendance=DB::table('attendance')->where('date','=',$date)->count();
        return $attendance;
    }

    public static function getMonthWiseLeaveApplication(){

        $getMonthWiseLeaveApplication=DB::table('tb_leave_application')
        ->select("id" ,DB::raw("(sum(actual_days)) as actual_days"),DB::raw("DATE_FORMAT(created_at, '%b-%Y') as monthYear"))
        ->orderBy('created_at','DESC')
        ->groupBy(DB::raw("MONTH(created_at)"))
        ->take(4)
        ->get();
        return $getMonthWiseLeaveApplication;
    }

    public static function getMonthWiseLeaveApplicationForOneYear(){

        $getMonthWiseLeaveApplication=DB::table('tb_leave_application')
        ->select("id" ,DB::raw("(sum(actual_days)) as actual_days"),DB::raw("DATE_FORMAT(created_at, '%b-%Y') as monthYear"))
        ->orderBy('created_at','DESC')
        ->groupBy(DB::raw("MONTH(created_at)"))
        ->take(12)
        ->get();
        return $getMonthWiseLeaveApplication;
    }

}
