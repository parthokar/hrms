<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Redirect;
use Session;
use Carbon\Carbon;

class SalaryController extends Controller
{
    //salary view method
    public function index(){
   //employee salary view
        $grade_name=DB::table('payroll_grade')->get();
        $data = DB::table('payroll_salary')
            ->leftjoin('employees', 'employees.id', '=', 'payroll_salary.emp_id')
            ->leftJoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftJoin('payroll_grade', 'designations.gradeId','=','payroll_grade.id')
            ->select('payroll_salary.*', 'employees.empFirstName','employees.employeeId','designations.designation','payroll_grade.grade_name')
            ->where('employees.empAccStatus','=',1)
            ->orderBy('employees.id', 'asc')
            ->paginate(50);
            return view('payroll.salary_view',compact('data','grade_name'));
        }


       //employee search
       public function employeeSearch(Request $request){
        $grade_name=DB::table('payroll_grade')->get();
        $data = DB::table('payroll_salary')
            ->leftjoin('employees', 'employees.id', '=', 'payroll_salary.emp_id')
            ->leftJoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftJoin('payroll_grade', 'designations.gradeId','=','payroll_grade.id')
            ->select('payroll_salary.*', 'employees.empFirstName','employees.employeeId','designations.designation','payroll_grade.grade_name')
            ->where('employees.employeeId','LIKE',"%".$request->employee."%")
            ->orwhere('employees.empFirstName','LIKE',"%".$request->employee."%")
            ->where('employees.empAccStatus','=',1)
            ->get();
            return view('payroll.search.employee',compact('data','grade_name'));
       }

    //salary store method
    public function store (Request $request){
        $check=DB::table('payroll_salary')->where('emp_id',$request->emp_select)->count();
        if($check==0) {
            $current_time = Carbon::now()->toDateTimeString();
            $total = $request->basic + $request->houserant + $request->medical + $request->transport + $request->foods + $request->others;
            $data = DB::table('payroll_salary')->insert([
                'emp_id' => $request->emp_select,
                'grade_id' => $request->grade_select,
                'basic_salary' => $request->basic,
                'house_rant' => $request->houserant,
                'medical' => $request->medical,
                'transport' => $request->transport,
                'food' => $request->foods,
                'other' => $request->others,
                'total_employee_salary' => $total,
                'current_month' => $request->current_month,
                'created_at' => $current_time,
                'updated_at' => $current_time,
            ]);
            if ($data) {
                Session::flash('message', 'Salary Store Successful!');
                return redirect('/employee/' . $request->emp_select);
            }
        }
        else{
            Session::flash('message', 'Salary information already added!');
            return redirect('/employee/' . $request->emp_select);

        }
    }

    //id wise grade select and json response
    public function gradeWisesalaryUpdate($id){
        $grade_wise_salary=DB::table('payroll_grade')->where('id',$id)->get();
        return response()->json($grade_wise_salary);
    }

    //payroll_salary data edit with modal open
    public function employeeSalaryEditmodal($id){
        $data=DB::SELECT("SELECT payroll_salary.id as default_id,payroll_salary.emp_id,payroll_salary.grade_id,payroll_salary.basic_salary,payroll_salary.house_rant,payroll_salary.medical,payroll_salary.transport,payroll_salary.food,payroll_salary.other as default_others,payroll_salary.total_employee_salary,payroll_grade.id as grades_id,payroll_grade.grade_name,employees.employeeId,employees.empFirstName,employees.empLastName
        FROM payroll_salary
        LEFT JOIN payroll_grade ON payroll_salary.grade_id = payroll_grade.id
        LEFT JOIN employees ON payroll_salary.emp_id = employees.id
        WHERE payroll_salary.id=$id");
        return response()->json($data);
    }

    //salary update method
    public function update(Request $request ){
        $current_time = Carbon::now()->toDateTimeString();
        $total=$request->basic+$request->house_rant+$request->medical+$request->transport+$request->foods+$request->others;
        $id=$request->salary_hidden_id;
        
        $data=DB::table('payroll_salary')->where('id',$id)->update([
           'basic_salary' =>$request->basic,
           'emp_id' =>$request->emp_ac_id,
           'grade_id' =>$request->emp_grade_id,
           'house_rant' =>$request->house_rant,
           'medical' =>$request->medical,
           'transport' =>$request->transport,
           'food' =>$request->foods,
           'other' =>$request->others,
           'total_employee_salary' =>$total,
           'created_at' =>$current_time,
           'updated_at' =>$current_time,
        ]);
        if($data){
            Session::flash('message', 'Salary Update Successful!');
            return redirect()->route('salary.View');
        }
    }

    //advance salary pay view
    public function advance_salary_pay(){
        $data=DB::table('employees')->where('empAccStatus','=',1)->get();
        return view('advanced_salary',compact('data'));
    }

    //advance Salary Store
    public function advance_salary_pay_store(Request $request){
       $check=DB::table('tb_advance_salary')->where('month',$request->salary_sheet_month)->where('emp_id',$request->emp_id)->count();
       if($check>0){
          DB::table('tb_advance_salary')->where('month',$request->salary_sheet_month)->where('emp_id',$request->emp_id)->delete();
          $data=DB::table('tb_advance_salary')->insert([
            'emp_id' =>$request->emp_id,
            'advance_amount' =>$request->advance_amount,
            'month' =>$request->salary_sheet_month,
          ]);
       }else{
        $data=DB::table('tb_advance_salary')->insert([
            'emp_id' =>$request->emp_id,
            'advance_amount' =>$request->advance_amount,
            'month' =>$request->salary_sheet_month,
          ]);
       }
       Session::flash('message', 'Advance Salary Paid Successfully!');
       return redirect('/advance/salary/');
        
    }

    //leave summary report view
    public function leave_summary(){
        return view('leave_month_summary');
    }

    //leave summary report show
    public function leaveSummary(Request $request){
       $month=$request->salary_sheet_month;
       $data=DB::SELECT("SELECT departments.departmentName,designations.designation,employees.empFirstName,employees.employeeId,employees.empLastName,GROUP_CONCAT(tb_leave_type.leave_type,':', cast(leave_of_employee.total as char) SEPARATOR ' ; ' ) as leave_type,  GROUP_CONCAT(DATE_FORMAT(leave_of_employee.start_date,'%d-%m-%Y'),' > ', cast(DATE_FORMAT(leave_of_employee.end_date,'%d-%m-%Y') as char) SEPARATOR ' ; ' ) as leave_date,SUM(leave_of_employee.total) as total_leave
       FROM leave_of_employee
       LEFT JOIN employees ON leave_of_employee.emp_id=employees.id
       LEFT JOIN tb_leave_type ON leave_of_employee.leave_type_id=tb_leave_type.id
       LEFT JOIN departments ON employees.empDepartmentId=departments.id
       LEFT JOIN designations ON employees.empDesignationId=designations.id
       WHERE leave_of_employee.month='$request->salary_sheet_month' 
       GROUP BY leave_of_employee.emp_id
       ORDER BY employees.employeeId DESC");
       return view('leave_month_summary_show',compact('data','month'));
    }

    //leave summary report view date wise
    public function leave_summary_date_wise(){
        return view('leave_month_summary_datewise');
    }

    //leave summary report date wise show
    public function leaveSummaryDatewiseShow(Request $request){
        $s=date('Y-m-d',strtotime($request->start));
        $e=date('Y-m-d',strtotime($request->end));
        $data=DB::SELECT("SELECT departments.departmentName,designations.designation,employees.empFirstName,employees.employeeId,employees.empLastName,GROUP_CONCAT(tb_leave_type.leave_type,':', cast(leave_of_employee.total as char) SEPARATOR ' ; ' ) as leave_type,  GROUP_CONCAT(DATE_FORMAT(leave_of_employee.start_date,'%d-%m-%Y'),' > ', cast(DATE_FORMAT(leave_of_employee.end_date,'%d-%m-%Y') as char) SEPARATOR ' ; ' ) as leave_date,SUM(leave_of_employee.total) as total_leave
        FROM leave_of_employee
        LEFT JOIN employees ON leave_of_employee.emp_id=employees.id
        LEFT JOIN tb_leave_type ON leave_of_employee.leave_type_id=tb_leave_type.id
        LEFT JOIN departments ON employees.empDepartmentId=departments.id
        LEFT JOIN designations ON employees.empDesignationId=designations.id
        LEFT JOIN tb_leave_application ON leave_of_employee.emp_id=tb_leave_application.leave_type_id 
        WHERE leave_of_employee.start_date between '$s' and '$e' OR leave_of_employee.end_date between '$s' and '$e'
        GROUP BY leave_of_employee.emp_id 
        ORDER BY employees.employeeId DESC");
        return view('leave_month_summary_show_datewise',compact('data','s','e'));
    }
    

}
