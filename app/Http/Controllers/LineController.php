<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class LineController extends Controller
{
    function __construct()
    {
        $this->middleware(['middleware'=>'check-permission:admin|hr|hr-admin']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $floors=DB::table('floors')->pluck('floor','id')->all();
        $lines=DB::table('tblines')
            ->leftJoin('floors','tblines.floor_id','=','floors.id')
            ->select('tblines.*','floors.floor')
            ->get();
        return view('settings.line.index',compact('lines','floors'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'line_no'=>'required',
            'floor_id'=>'required',
//            'lineDescription'=>'required',

        ]);
        DB::table('tblines')->insert([
            'line_no'=>$request->line_no,
            'lineDescription'=>$request->lineDescription,
            'floor_id'=>$request->floor_id,
            'created_at'=>Carbon::now()->toDateTimeString(),
            'updated_at'=>Carbon::now()->toDateTimeString(),
        ]);
        Session::flash('message','Line inserted');
        return redirect('settings/line');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $line=DB::table('tblines')
            ->leftJoin('floors','tblines.floor_id','=','floors.id')
            ->where(['tblines.id'=>$id])
            ->first();
        return view('settings.line.showForm',compact('line'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $floors=DB::table('floors')->pluck('floor','id')->all();
        $line=DB::table('tblines')->where(['id'=>$id])->first();
        return view('settings.line.editForm',compact('line','floors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'line_no'=>'required',
            'floor_id'=>'required',
        ]);
        DB::table('tblines')->where(['id'=>$id])->update([
            'line_no'=>$request->line_no,
            'floor_id'=>$request->floor_id,
            'lineDescription'=>$request->lineDescription,
            'updated_at'=>Carbon::now()->toDateTimeString(),
        ]);
        Session::flash('edit','Line Information Updated');
        return redirect('settings/line');
    }

    public function showDelete($id){
        $line=DB::table('tblines')->where(['id'=>$id])->first();
        return view('settings.line.deleteForm',compact('line'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('tblines')->where(['id'=>$id])->delete();
        Session::flash('delete','Line information deleted');
        return redirect('settings/line');
    }
}
