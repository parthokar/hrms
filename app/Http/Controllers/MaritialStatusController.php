<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
//use DB;

class MaritialStatusController extends Controller
{

    public function __construct()
    {
        $this->middleware(['middleware'=>'check-permission:admin|hr|hr-admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $country = \DB::table('marital_statuses')
            ->leftJoin('users', 'marital_statuses.created_by', '=', 'users.id')
            ->select('marital_statuses.id as id','marital_statuses.name as n_name','users.name as u_name','users.id as u_id')
            ->get();

        $mses=\DB::table('marital_statuses')->get();
        return view('settings.maritial_status.index',compact('country'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' =>'required',
        ]);
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $ms=\DB::table('marital_statuses')->insert(['name'=>$request->name,
            'created_by'=>$user->id,
            'modified_by'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now
    ]);
        if($ms) {
            Session::flash('message', 'Marital Status Inserted Successfully');
            return redirect("settings/maritialstatus");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required',
        ]);
        $now=Carbon::now()->toDateTimeString();
        $update=DB::table('marital_statuses')->where('id',$id)->update([
            'name'=>$request->name,
            'updated_at'=>$now,
            'modified_by'=>Auth::user()->id,
        ]);
        Session::flash('edit','Info updated');
        return redirect("settings/maritialstatus");


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check=DB::table('employees')->where(['empMaritalStatusId'=>$id])->get();
        if(count($check)){
            Session::flash('edit',"Can't delete. There are already some employees bearing this Marital Status." );
        }
        else {
            DB::table('marital_statuses')->where('id', $id)->delete();
            Session::flash('delete',"Marital Status Deleted");
        }

        return redirect("settings/maritialstatus");
    }
}
