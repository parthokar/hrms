<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class DepartmentController extends Controller
{
    public function __construct()
    {
        $this->middleware([
            'middleware'=>'check-permission:admin|hr|hr-admin'
        ]);
    }

    public function index()
    {
        $departments=DB::table('departments')->get();
//        $count=DB::table('departments')->get();
        return view("settings.department.index",compact('departments'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'description'=>'required',
        ]);
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('departments')->insert([
            'departmentName'=>$request->name,
            'departBanglaName'=>$request->departBanglaName,
            'departmentDescription'=>$request->description,
            'created_by'=>$user->name,
            'modified_by'=>$user->name,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);
        if($str)
        {
            Session::flash('message','Department information has been successfully inserted.');
        }
        return redirect('settings/department');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required',
        ]);
        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();
        $dept=DB::table('departments')->where(['id'=>$id])->update([
            'departmentName'=>$request->name,
            'departBanglaName'=>$request->departBanglaName,
            'departmentDescription'=>$request->description,
            'modified_by'=>$user->name,
            'updated_at'=>$now,
        ]);

        if($dept){
            Session::flash('edit', 'Department information has been successfully updated.');
        }
        return redirect('settings/department');
    }


    public function destroy($id)
    {
        $check=DB::table('employees')->where(['empDepartmentId'=>$id])->get();
        if(count($check)){
            Session::flash('delete',"Can't delete. There are already some employees in this Department." );
        }
        else {
            $dept=DB::table('departments')->where(['id'=>$id])->delete();
            Session::flash('message','Department information has been successfully deleted.');
        }
        return redirect('settings/department');
    }
}
