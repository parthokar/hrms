<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicle_info=DB::table('tbvehicle_info')->get();
        return view('vehicle.index',compact('vehicle_info'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[
            'vehicleName'=>'required',
        ]);
        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $des=DB::table('tbvehicle_info')->insert([
            'vehicleName'=>$request->vehicleName,
            'registrationNumber'=>$request->registrationNumber,
            'insuranceNumber'=>$request->insuranceNumber,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);
        if($des){
            Session::flash('message','Vehicle inserted Successfully');
            return  redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'vehicleName'=>'required',
        ]);
        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $des=DB::table('tbvehicle_info')->where('id','=',$id)->update([
            'vehicleName'=>$request->vehicleName,
            'registrationNumber'=>$request->registrationNumber,
            'insuranceNumber'=>$request->insuranceNumber,
            'createdBy'=>$user->id,
            'updated_at'=>$now,

        ]);
        
        if($des){
            Session::flash('message','Vehicle Updated Successfully');
            return  redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $check=DB::table('tbvehicle_expenses')->where(['vehicleId'=>$id])->get();
        if(count($check)){
            Session::flash('edit',"Can't delete. There are already some data bearing this vehicle." );
        }
        else {
            DB::table('tbvehicle_info')->where(['id'=>$id])->delete();
            Session::flash('message', "Vehicle Successfully Deleted");
        }
        return  redirect()->back();
    }

    public function excatlist()
    {
        $expenses_category=DB::table('tbvexcategory')->get();
        return view('vehicle.excategory',compact('expenses_category'));
    }

    public function excatstore(Request $request)
    {
        $this->validate($request,[
            'vexName'=>'required',
        ]);
        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $des=DB::table('tbvexcategory')->insert([
            'vexName'=>$request->vexName,
            'vexDescription'=>$request->vexDescription,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);
        if($des){
            Session::flash('message','Vehicle expense category successfully inserted.');
            return  redirect()->back();
        }
    }

    public function excatupdate(Request $request)
    {
        $this->validate($request,[
            'vexName'=>'required',
        ]);
        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $des=DB::table('tbvexcategory')->where(['id'=>$request->id])->update([
            'vexName'=>$request->vexName,
            'vexDescription'=>$request->vexDescription,
            'createdBy'=>$user->id,
            'updated_at'=>$now,

        ]);
        if($des){
            Session::flash('message','Vehicle expense category successfully updated.');
            return  redirect()->back();
        }
    }

    public function excatdelete(Request $request)
    {
       $check=DB::table('tbvehicle_expenses')->where(['vexCategoryId'=>$request->id])->get();
        if(count($check)){
            Session::flash('edit',"Can't delete. There are already some expenses bearing this vehicle category." );
        }
        else {
            DB::table('tbvexcategory')->where(['id'=>$request->id])->delete();
            Session::flash('message', "Expense Category Successfully Deleted");
        }
        return  redirect()->back();
    }

    public function expenses()
    {
        
        $vfuel_expense=DB::table('tbvfuel_expense')
        ->leftJoin('tbvehicle_info','tbvehicle_info.id','=','tbvfuel_expense.vehicleId')
        ->get();

        $vehicle_expenses=DB::table('tbvehicle_expenses')
        ->leftJoin('tbvehicle_info','tbvehicle_info.id','=','tbvehicle_expenses.vehicleId')
        ->leftJoin('tbvexcategory','tbvexcategory.id','=','tbvehicle_expenses.vexCategoryId')
        ->select('tbvehicle_expenses.*','tbvehicle_info.vehicleName as vehicleName','tbvexcategory.vexName as vexName')
        ->get();

        $vehicle_info=DB::table('tbvehicle_info')->get();
        $vexcategory=DB::table('tbvexcategory')->get();

        return view('vehicle.expenses',compact('vfuel_expense','vehicle_expenses','vehicle_info','vexcategory'));
    }

    public function newfuelexpense(Request $request){
        $this->validate($request,[
            'vehicleId'=>'required',
            'fuelType'=>'required',
            'amount'=>'required',
            'vexDate'=>'required',
        ]);

        if($file=$request->file('newfuelAttachment')){
            if($request->file('newfuelAttachment')->getClientSize()>5000000)
            {
                Session::flash('fileSize','Could Not Upload. File Size Limit Exceeded.');
                return redirect()->back();
            }
            $name=time()."_"."fe_".$file->getClientOriginalName();
            $file->move('vehicle_expenses',$name);
        }
        else{
            $name=null;
        }

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();
        $str = DB::table('tbvfuel_expense')->insert([
            'vehicleId'=>$request->vehicleId,
            'fuelType'=>$request->fuelType,
            'vexReferences'=>$request->vexReferences,
            'vexDate'=>Carbon::parse($request->vexDate)->format('Y-m-d'),
            'quantity'=>$request->quantity,
            'amount'=>$request->amount,
            'vexDescription'=>$request->vexDescription,
            'vexAttachment'=>$name,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);

       if($str)
        {
            Session::flash('message','Fuel Expense Information Successfully Added. ');
        }else{
            Session::flash('failedMessage','Fuel Expense Information Adding Failed.');
        }
        return redirect()->back();
    }

    public function newotherexpense (Request $request){
        $this->validate($request,[
            'vehicleId'=>'required',
            'vexCategoryId'=>'required',
            'amount'=>'required',
        ]);

        if($file=$request->file('newOtherAttachment')){
            if($request->file('newOtherAttachment')->getClientSize()>5000000)
            {
                Session::flash('fileSize','Could Not Upload. File Size Limit Exceeded.');
                return redirect()->back();

            }
            $name=time()."_"."oe_".$file->getClientOriginalName();
            $file->move('vehicle_expenses',$name);
        }
        else{
            $name=null;
        }

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();
        $str = DB::table('tbvehicle_expenses')->insert([
            'vehicleId'=>$request->vehicleId,
            'vexCategoryId'=>$request->vexCategoryId,
            'amount'=>$request->amount,
            'vexDate'=>Carbon::parse($request->vexDate)->format('Y-m-d'),
            'vexReferences'=>$request->vexReferences,
            'vexDescription'=>$request->vexDescription,
            'vexAttachment'=>$name,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);

       if($str)
        {
            Session::flash('message','Expense Information Successfully Added. ');
        }else{
            Session::flash('failedMessage','Expense Information Adding Failed.');
        }
        return redirect()->back();
    }

    public function booking_list()
    {
        $vehicle_info=DB::table('tbvehicle_info')->get();
        $date=date('Y-m-d');
        $booking_list=DB::table('tbvehicle_booking')
        ->leftJoin('tbvehicle_info','tbvehicle_info.id','=','tbvehicle_booking.vehicleId')
        ->select('tbvehicle_booking.*','tbvehicle_info.vehicleName')
        ->whereDate('vbDate', '>=', $date)
        ->get();
        return view('vehicle.booking_list',compact('booking_list','vehicle_info'));
    }

    public function update_booking(Request $request)
    {
        $this->validate($request,[
            'vehicleId'=>'required',
            'bookedBy'=>'required',
            'driverName'=>'required',
            'vbDate'=>'required',
        ]);

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $str = DB::table('tbvehicle_booking')->where(['id'=>$request->id])->update([
            'vehicleId'=>$request->vehicleId,
            'bookedBy'=>$request->bookedBy,
            'driverName'=>$request->driverName,
            'vbDate'=>Carbon::parse($request->vbDate)->format('Y-m-d'),
            'vbStartTime'=>$request->vbStartTime,
            'vbEndTime'=>$request->vbEndTime,
            'purpose'=>$request->purpose,
            'createdBy'=>$user->id,
            'updated_at'=>$now,
        ]);

       if($str)
        {
            Session::flash('message','Vehicle Booking Information Successfully Updated ');
        }else{
            Session::flash('failedMessage','Vehicle Booking Information Updation Failed.');
        }
        return redirect()->back();
    }

    public function booking_cancel(Request $request)
    {

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $str = DB::table('tbvehicle_booking')->where(['id'=>$request->id])->update([
            'status'=>1,
            'createdBy'=>$user->id,
            'updated_at'=>$now,
        ]);

       if($str)
        {
            Session::flash('message','Vehicle Booking Information Successfully Cancelled. ');
        }else{
            Session::flash('failedMessage','Vehicle Booking Information Updation Failed.');
        }
        return redirect()->back();
    }

    public function store_booking(Request $request)
    {
        $this->validate($request,[
            'vehicleId'=>'required',
            'bookedBy'=>'required',
            'driverName'=>'required',
            'vbDate'=>'required',
        ]);

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();
        $str = DB::table('tbvehicle_booking')->where([['vehicleId','=',$request->vehicleId],['status','!=',1],['vbDate','=',Carbon::parse($request->vbDate)->format('Y-m-d')]])->get();
        $save=0;

        foreach ($str as $key) {
            $time1=strtotime($key->vbStartTime); 
            $time2=strtotime($key->vbEndTime); 
            $currenttime=strtotime(date($request->vbStartTime)); 
            if($time1 - $currenttime <= 0 && $currenttime - $time2 <= 0 ) { 
                $save++;
            }
        }

            if($save==0){
                $str = DB::table('tbvehicle_booking')->insert([
                    'vehicleId'=>$request->vehicleId,
                    'bookedBy'=>$request->bookedBy,
                    'driverName'=>$request->driverName,
                    'vbDate'=>Carbon::parse($request->vbDate)->format('Y-m-d'),
                    'vbStartTime'=>$request->vbStartTime,
                    'vbEndTime'=>$request->vbEndTime,
                    'purpose'=>$request->purpose,
                    'createdBy'=>$user->id,
                    'created_at'=>$now,
                    'updated_at'=>$now,
                ]);
                Session::flash('message','Vehicle Booking Information Successfully Added. ');
            }else{
                Session::flash('delete','This Vehicle already booked for your selected time. Choose another vehicle or differnt time.');
            }
        return redirect()->back();
    }


    public function report_view()
    {
        return view('vehicle.report_view');
    }


    public function vehicle_wise_report_view()
    {
        $vehicle_info=DB::table('tbvehicle_info')->get();
        return view('vehicle.vehicle_wise_report_view',compact('vehicle_info'));
    }

    public function vehicle_wise_report_data(Request $request)
    {
        $start_date=Carbon::parse($request->start_date)->format('Y-m-d');
        $end_date=Carbon::parse($request->end_date)->format('Y-m-d');
       if(($request->vehicleId)==0){
            $vfuel_expense=DB::table('tbvfuel_expense')
            ->select('tbvfuel_expense.*','tbvehicle_info.vehicleName as vehicleName','tbvehicle_info.registrationNumber as registrationNumber')
            ->leftJoin('tbvehicle_info','tbvehicle_info.id','=','tbvfuel_expense.vehicleId')
            ->whereBetween('tbvfuel_expense.vexDate', [$start_date, $end_date])
            ->get();
            $vehicle_expenses=DB::table('tbvehicle_expenses')
            ->leftJoin('tbvehicle_info','tbvehicle_info.id','=','tbvehicle_expenses.vehicleId')
            ->leftJoin('tbvexcategory','tbvexcategory.id','=','tbvehicle_expenses.vexCategoryId')
            ->select('tbvehicle_expenses.*','tbvehicle_info.vehicleName as vehicleName','tbvehicle_info.registrationNumber as registrationNumber','tbvexcategory.vexName as vexName')
            ->whereBetween('tbvehicle_expenses.vexDate', [$start_date, $end_date])
            ->get();
       }else{
            $vfuel_expense=DB::table('tbvfuel_expense')
            ->leftJoin('tbvehicle_info','tbvehicle_info.id','=','tbvfuel_expense.vehicleId')
            ->select('tbvfuel_expense.*','tbvehicle_info.vehicleName as vehicleName','tbvehicle_info.registrationNumber as registrationNumber')
            ->whereBetween('tbvfuel_expense.vexDate', [$start_date, $end_date])
            ->where('tbvfuel_expense.vehicleId','=',$request->vehicleId)
            ->get();

            $vehicle_expenses=DB::table('tbvehicle_expenses')
            ->leftJoin('tbvehicle_info','tbvehicle_info.id','=','tbvehicle_expenses.vehicleId')
            ->leftJoin('tbvexcategory','tbvexcategory.id','=','tbvehicle_expenses.vexCategoryId')
            ->select('tbvehicle_expenses.*','tbvehicle_info.vehicleName as vehicleName','tbvehicle_info.registrationNumber as registrationNumber','tbvexcategory.vexName as vexName')
            ->whereBetween('tbvehicle_expenses.vexDate', [$start_date, $end_date])
            ->where('tbvehicle_expenses.vehicleId','=',$request->vehicleId)
            ->get();
       }
        return view('vehicle.vehicle_expense_report_data',compact('vfuel_expense','vehicle_expenses','request'));
    }

    public function vehicle_booking_report_view()
    {
        $vehicle_info=DB::table('tbvehicle_info')->get();
        return view('vehicle.vehicle_booking_report_view',compact('vehicle_info'));
    }

    public function vehicle_booking_report_data(Request $request)
    {
        $start_date=Carbon::parse($request->start_date)->format('Y-m-d');
        $end_date=Carbon::parse($request->end_date)->format('Y-m-d');
       if(($request->vehicleId)==0){
            $booking_list=DB::table('tbvehicle_booking')
            ->leftJoin('tbvehicle_info','tbvehicle_info.id','=','tbvehicle_booking.vehicleId')
            ->select('tbvehicle_booking.*','tbvehicle_info.vehicleName','tbvehicle_info.registrationNumber as registrationNumber')
            ->whereBetween('tbvehicle_booking.vbDate', [$start_date, $end_date])
            ->get();
       }else{
            $booking_list=DB::table('tbvehicle_booking')
            ->leftJoin('tbvehicle_info','tbvehicle_info.id','=','tbvehicle_booking.vehicleId')
            ->select('tbvehicle_booking.*','tbvehicle_info.vehicleName','tbvehicle_info.registrationNumber as registrationNumber')
            ->whereBetween('tbvehicle_booking.vbDate', [$start_date, $end_date])
            ->where('tbvehicle_booking.vehicleId','=',$request->vehicleId)
            ->get();

       }
        return view('vehicle.vehicle_booking_report_data',compact('booking_list','request'));
    }



}
