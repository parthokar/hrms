<?php

namespace App\Http\Controllers;

use DB;
use Session;
use Redirect;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WeekendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function index()
    {
        $weekends=DB::table('tbweekend_regular_day')
        ->leftJoin('users','users.id','=','tbweekend_regular_day.createdBy')
        ->select('tbweekend_regular_day.*', 'users.name as created_by')
        ->orderBy('tbweekend_regular_day.weekend_date')
        ->get();
        return view('settings.weekend.index',compact('weekends'));
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'weekend_date'=>'required',
        ]);

        $now=Carbon::now()->toDateTimeString();
        $weekend_date=date('Y-m-d', strtotime($request->weekend_date));
        $weekend_month=date('Y-m-01', strtotime($request->weekend_date));

        $data=DB::table('tbweekend_regular_day')->insert([
            'weekend_date'=>$weekend_date,
            'weekend_month'=>$weekend_month,
            'createdBy'=>Auth::user()->id,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        
        if($data){
            Session::flash('message', 'Weekend information has been successfully added.');
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id= base64_decode($id);
        $weekends=DB::table('tbweekend_regular_day')->where('id', '=', $id)->delete();
         Session::flash('message', 'Weekend information has been successfully destroyed.');
            return redirect()->back();
    }
}
