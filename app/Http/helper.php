<?php
function checkPermission($permissions){
    $userAccess = getMyPermission(auth()->user()->is_permission);
    foreach ($permissions as $key => $value) {
        if($value == $userAccess){
            return true;
        }
    }
    return false;
}

function getMyPermission($id)
{
    switch ($id) {
        case 1:
            return 'admin';
            break;
        case 2:
            return 'hr';
            break;
        case 3:
            return 'employee';
            break;
        case 4:
            return 'executive';
            break;
        case 5:
            return 'accountant';
            break;
        case 7:
            return 'hr-admin';
            break;
    }
}

