<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class mailSender extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$request,$companyInformation,$lastNotify,$secondLastNotify)
    {
        $this->user = $user;
        $this->request = $request;
        $this->companyInformation = $companyInformation;
        $this->lastNotify = $lastNotify;
        $this->secondLastNotify = $secondLastNotify;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.continued_absent_list')->with(['user' => $this->user,'request' => $this->request,'lastNotify'=>$this->lastNotify,'secondLastNotify'=>$this->secondLastNotify])->subject($this->companyInformation->company_name);
    }
}
