@extends('layouts.guest')
@section('content')
    <div class="container" style="padding-top: 50px;">
        <div class="row animated fadeIn">
            <div class="col-sm-6 col-md-4 col-md-offset-4">
            @if(isset($access))
               {!! Form::open(['method'=>'PATCH', 'action'=>['HrmsActivationController@update', 1]]) !!}
            	<table class="table table-bordered">
            		<tr>
            			<td><input style="text-align: center;" type="number" name="numberOfDays" placeholder="Number of Days" class="form-control" required autofocus ></td>
            		</tr>
            		<tr>
            			<td style="text-align: center;" >
            				<button type="submit" class="btn btn-success" onclick="return confirm('Are you sure?')"> &raquo; Update </button>
            				<button type="reset" class="btn btn-warning"> Reset </button>
            			</td>
            		</tr>
            	</table>
               {!! Form::close() !!}
               @else
                    <p id="alert_message" class="alert alert-danger">Invalid Request.</p>
               @endif
            </div>
        </div>
    </div>
    <script>
	    setTimeout(function() {
	        $('#alert_message').fadeOut('fast');
	    }, 5000);
	</script>
@endsection