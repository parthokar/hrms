@php
    use App\Http\Controllers\dashboardcontroller;
@endphp
@extends('layouts.master')
@section('title', 'Manual Attendance')
@section('content')
    <style>
        .input-form-gap{
            display: block !important;
            padding: 10px;
        }
        .form-control{
            padding: 3px;
        }
    </style>
    <div class="page-content ">
        <div class="row panel"  style="border:1px solid #999">

            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Edit Attendance Date Wise</b></h2>
                    <hr>
                </div>
            </div>

            <div class="col-xlg-7 col-md-10 col-md-offset-1 col-xlg-offset-2">
                @if(Session::has('error'))
                    <p id="alert_message" class="alert alert-danger">{{Session::get('error')}}</p>
                @endif


                <div class="form-group">
                    <div class="form-group">
                        <div class="input-form-gap"></div>
                        <label class="col-md-3">Name<span class="clon">:</span></label>
                        <div class="col-md-9">
                            <label><b>{{$employee->empFirstName. " ".$employee->empLastName}}</b></label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-form-gap"></div>
                        <label class="col-md-3">Employee Id<span class="clon">:</span></label>
                        <div class="col-md-9">
                            <label><b>{{$employee->employeeId}}</b></label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-form-gap"></div>
                        <label for="emp_id" class="col-md-3">Designation<span class="clon">:</span></label>
                        <div class="col-md-9">
                            <label><b>{{$employee->designation}}</b></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-form-gap"></div>
                        <label for="emp_id" class="col-md-3">Department<span class="clon">:</span></label>
                        <div class="col-md-9">
                            <label><b>{{$employee->departmentName}}</b></label>
                        </div>
                    </div>

                </div>
                <hr>

            </div>

            {!! Form::open(['method'=>'POST','action'=>'AttendanceController@edit_store_date_store']) !!}
            {!! Form::hidden('emp_id',$employee->id) !!}
            <div class="col-xs-12">
                <hr>
                <div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Day</th>
                            <th>In Time</th>
                            <th>Out Time</th>
                        </tr>
                        </thead>
                        <tbody>
                        @for($i=$start_date;$i<=$end_date;$i=\Carbon\Carbon::parse($i)->addDay())
                            <?php
                            $attendance_data=\Illuminate\Support\Facades\DB::table('attendance')
                                ->where('emp_id','=',$employee->id)
                                ->where('date','=',\Carbon\Carbon::parse($i)->toDateTimeString())
                                ->first();

                            if(isset($attendance_data->in_time)) {

                            ?>

                            {!! Form::hidden('ids[]',$attendance_data->id) !!}

                            {!! Form::hidden('dates[]',$attendance_data->date) !!}

                            <tr>
                                <td>{{\Carbon\Carbon::parse($i)->format('j M Y')}}

                                </td>
                                <td>{{\Carbon\Carbon::parse($i)->format('l')}}</td>
                                <td>
                                    {!! Form::time('in_time[]',$attendance_data->in_time,['class'=>'form-control']) !!}
                                </td>
                                <td>
                                    {!! Form::time('out_time[]',$attendance_data->out_time,['class'=>'form-control']) !!}
                                </td>
                            </tr>
                            <?php } ?>
                        @endfor

                        </tbody>
                    </table>
                </div>
                <div class="form-group">
                    <div class="col-md-9 col-md-offset-3">
                        <hr>
                        <button style="float: right;" type="submit" class="btn btn-success"><i class="fa fa-list"></i> &nbsp;Update</button>
                        {{--<button type="submit" value="Generate PDF" name="viewType" class="btn btn-primary"><i class="fa fa-download"></i> &nbsp;Download as PDF</button>--}}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#process-btn').click(function (event) {
                var emp_id=$('#emp_id').val();
                if(emp_id.length===0){
                    alert('Select a Employee First');
                    event.preventDefault();
                }
                else {

                }

            });

        });

    </script>

    @include('include.copyright')
@endsection