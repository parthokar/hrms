@php
    use App\Http\Controllers\dashboardcontroller;
@endphp
@extends('layouts.master')
@section('title', 'Manual Attendance')
@section('content')
    <style>
        .input-form-gap{
            display: block !important;
            padding: 10px;
        }
        .form-control{
            padding: 3px;
        }
    </style>
    <div class="page-content ">
        <div class="row panel">
            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Manual Attendance Date Wise</b></h2>
                </div>
            </div>
            <div class="col-xlg-7 col-md-10 col-md-offset-1 col-xlg-offset-2">
                @if(Session::has('error'))
                    <p id="alert_message" class="alert alert-danger">{{Session::get('error')}}</p>
                @endif
        </div>
  
    </div>
    <div class="col-md-12">
        <div class="panel-content pagination2 table-responsive">
            <table class="table table-hover table-dynamic dataTable no-footer">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Date</th>
                    <th>Day</th>
                    <th>In Time</th>
                    <th>Out Time</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $employee)     
                @for($i=$start;$i<=$end;$i++)
                <form method="post" action="{{url('att/setup/dept/section/date/wise/store')}}">
                    @csrf 
                @php
                $attendance_data=DB::table('attendance')
                                    ->where('emp_id','=',$employee->id)
                                    ->where('date','=',\Carbon\Carbon::parse($i)->toDateTimeString())
                                    ->first(); 
                @endphp
                <tr>
                <td>{{$employee->employeeId}}</td>
                    <td>{{$employee->empFirstName}}</</td>
                    <td>{{\Carbon\Carbon::parse($i)->format('j M Y')}}</td>
                    <td>{{\Carbon\Carbon::parse($i)->format('l')}}</td>
                <td>
                    @if(isset($attendance_data->in_time))
                    <input class="form-control" type="time" id="td" name="in_time" value="{{$attendance_data->in_time}}">
                    @else 
                    <input class="form-control" id="td" type="time" name="in_time" value="">
                    @endif
                </td>
                    <td>
                        @if(isset($attendance_data->out_time))
                        <input class="form-control" type="time" name="out_time[]" value="{{$attendance_data->out_time}}">
                        @else 
                        <input class="form-control" type="time" name="out_time[]" value="">
                        @endif
                    </td>
                </tr>
                <button type="submit"></button>
               </form>
                @endfor
                @endforeach
                </tbody>
            </table>
        </div>

        {{-- </form> --}}
    </div>
    @include('include.copyright')
@endsection
