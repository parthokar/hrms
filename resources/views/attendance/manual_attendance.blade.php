@php
    use App\Http\Controllers\dashboardcontroller;
@endphp
@extends('layouts.master')
@section('title', 'Manual Attendance')
@section('content')
    <div class="page-content page-content-hover-1">
        <div class="row panel myAnimation"  style="border:1px solid #dcdcdc;">

            <div class="col-xlg-12 col-lg-12  col-sm-12 animated bounceInLeft">
                <div class="text-center" >
                    <h2><b>Manual Attendance</b></h2>
                    <hr>
                </div>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
                <a target="_BLANK" href="{{route('attendance.manual.create')}}">
                    <div class="panel">
                        <div class="panel-content emp_sel" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-search f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Manual Attendance (Daily)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
                <a target="_BLANK" href="{{ route('attendance.manual_store_date') }}">
                    <div class="panel">
                        <div class="panel-content emp_elist" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-users f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b> Manual Attendance (Date Wise)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
                <a target="_BLANK" href="{{route('attendance.manual.edit')}}">
                    <div class="panel">
                        <div class="panel-content att_jobcardp" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-exclamation-circle f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Edit Attendance</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
                <a target="_BLANK" href="{{route('attendance.manual.edit_date_wise')}}">
                    <div class="panel">
                        <div class="panel-content att_jobcardp" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-exclamation-circle f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Edit Attendance (Date Wise)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
                <a target="_BLANK" href="{{route('attendance.manual.delete')}}">
                    <div class="panel">
                        <div class="panel-content emp_emlbas" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-ban f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Delete Attendance Record</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div>

    @include('include.copyright')
@endsection