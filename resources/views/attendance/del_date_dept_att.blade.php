@extends('layouts.master')
@section('content')
    <div class="page-content">
        <div class="panel-content">
            <div class="col-md-12">
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong>Delete </strong>Attendance Record</h2>
                    </div>
                    <h4 class="text-center">{{ $data->count()}} Data Found</h4> 
                    <div class="panel-body bg-white">
                            {!! Form::open(['method'=>'post', 'url'=>['/attendance/delete/date/wise/dept/confirm']]) !!}
                            @foreach($data as $item)
                               <input type="hidden" name="id[]" value="{{$item->id}}">
                            @endforeach

                            @if($data->count()>0)
                                <div class="text-center  m-t-20">
                                    <button  type="submit" class="btn btn-embossed btn-danger"><i class="fa fa-right-arrow"></i>Delete All</button>
                                </div>
                            @endif
                            {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        </div>
    @include('include.copyright')
@endsection