@php
use App\Http\Controllers\dashboardcontroller;
@endphp
@extends('layouts.master')
@section('content')

 <div class="page-content page-thin">
   <div class="row">
      <div class="col-md-10">
            @if(Session::has('error_message'))
                <h2 id="alert_message" class="alert alert-danger"><b>{{Session::get('error_message')}}</b></h2>
            @endif
      </div>
  </div>

  <div class="row">
      <div class="col-md-12">
      	<img src="" alt="">
          <h2 id="alert_message" class="alert bg-red"><b> Whoops, looks like something went wrong.</b></h2>
      </div>
  </div>

 </div>

@include('include.copyright')
@endsection
