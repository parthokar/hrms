@extends('layouts.master')
@section('title', 'Create New Notice')
@section('content')
	<div class="page-content">
		<div class="row">

            <div class="col-md-12">
                @if(Session::has('message'))
                    <p id="alert_message" class="alert alert-success">{{Session::get('message')}}</p>
                @endif
                @if(Session::has('failedMessage'))
                        <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
                @endif
            </div>
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="panel panel-default no-bd">
					<div class="panel-header bg-dark">
						<h2 class="panel-title"><strong>Create</strong> New Notice</h2>
					</div>
					<div class="panel-body bg-white">
						<div class="">
                        {!! Form::open(['method'=>'POST','action'=>['noticeController@store']]) !!}
						
                            <div class="required form-group">
                                <label class="control-label">Notice Starting Date</label>
                                <div class="append-icon">
                                    <input type="text" name="startDate" class="date-picker form-control" required placeholder="Click here to select date" >
                                </div>
                            </div>

                            <div class="required form-group">
                                <label class="control-label">Notice Ending Date</label>
                                <div class="append-icon">
                                    <input type="text" name="endDate" class="date-picker form-control" required placeholder="Click here to select date" >
                                </div>
                            </div>

                            <div class="required form-group">
                                <label class="control-label">Notice</label>
                                <div class="append-icon">
                                    <textarea rows="8" name="noticeDescription" class="form-control" placeholder="Minimum 100 Character..." required ></textarea>
                                </div>
                            </div>
                            
                            <div class="required form-group">
                                <label class="control-label">Notice Status</label>
                                <div class="append-icon">
                                    <select name="status" class="form-control">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>

                            <div class="text-center modal-footer">
                                <button type="submit" class="btn btn-success">Add Notice</button>
                            </div>

                        {!! Form::close() !!}
						
						</div>

                       

					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
<script>
    setTimeout(function() {
        $('#alert_message').fadeOut('fast');
    }, 5000);
</script>
@endsection
