
<!DOCTYPE html>
<html>
<head>
    <title>Lefty Salary Sheet</title>
    <style>
        @page        {
            size: landscape;
        }
        @media print{@page {size: legal landscape}}
        .verticalTableHeader {
            text-align:center;
            white-space:nowrap;
            g-origin:50% 50%;
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            -o-transform: rotate(90deg);
            transform: rotate(90deg);

        }
        .verticalTableHeader p {
            margin:0 -100% ;
            display:inline-block;
        }
        .verticalTableHeader p:before{
            content:'';
            width:0;
            padding-top:110%;/* takes width as reference, + 10% for faking some extra padding */
            display:inline-block;
            vertical-align:middle;
        }
        .company{
            text-align: center;
        }
        .company p{
            padding: 0;
            margin: 0;
            font-size: 17px;
        }
        .main_div{
            width:100%;
            margin: 0px;
        }
        .salary_table{
            width: 100%;
            margin-top:8px;
        }
        .authority{
            width:100%;
            padding-top: 47px;
        }
        .prepared_by{
            width: 25%;
            float: left;
        }
        .audited_by{
            width: 25%;
            float: left;
        }
        .recomended_by{
            width: 25%;
            float: left;
        }
        .approved_by{
            width: 25%;
            float: left;
        }
        .table_1{
            border-collapse: collapse;
            width: 100%;
            font-size:12px;
        }

        .table_1 tr th,td{
            border: 1px solid black;
            text-align: center;
        }

        .table_lefttd{
            /*padding-left:5px;*/
            text-align: center !important;
        }
        .table2{
            width: 100%;
            font-size:12px;
        }

        .table2 tr>td{
            border: none;
            text-align: left;
        }
        .main_foter{
            border: 1px solid #000000;
            margin-top: -68px;
            /*padding-left: 290px;*/
            font-size: 14px;
            height: 24px;
            line-height: 24px;
        }
        .main_foter span{
            margin-right:6px;
        }

        tfoot{
            width: 100%;
        }


        tfoot tr td{

            width: 100%;
            clear: both;
            border:none;
            padding-top: 0px!important;
            color:#fff;
        }
        .authority span{
            border-top: 1px solid #fff;
            padding-top: 3px;
            padding-left: 15px;
            padding-right: 15px;
        }

        tfoot tr td .main_foter{
            text-align: left;
            border:none;
        }
        tfoot tr td .main_foter span{
            margin-right: 10px;
        }
        .company_heading_1 td{
            border: none;
            line-height: .7;
        }

        .company_heading_2 td{
            border:none;

        }
        tfoot td{
            border:none;
        }
        td.signature_td{
            /*width:70px;
            height:81px;*/
        }
        .dayofmonth{
            display: inline-block;
            padding-right: 40px;
        }
        .company_heading_1 td span{
            display: inline-block;
            margin-bottom: 5px;
        }
        .company_heading_1 h3{
            margin-bottom:2px;
            margin-top: 5px;
        }
        .company_heading_1 p {
            margin-top: 0px;
        }
        .main_foter span:nth-child(1){

            padding-right: 17%;
        }
        .main_foter span:nth-child(2){
            font-size:12px;
        }
        .main_foter span:nth-child(3){
            font-size:12px;
        }
        .main_foter span:nth-child(4){
            font-size:12px;
        }
        .main_foter span:nth-child(5){
            font-size:12px;
        }
        .main_foter span:nth-child(6){
            font-size:12px;
        }
        .main_foter span:nth-child(7){
            font-size:12px;
        }
        .main_foter span:nth-child(8){
            font-size:12px;
            margin-left:6%;
        }
        .main_foter span:nth-child(9){
            font-size:12px;
            margin-left: 9%;
        }
        .main_foter span:nth-child(10){
            font-size:12px;
        }
        .ex_footer{
            opacity:0;
        }


        #signature_11{
            padding-left: 35px;
            padding-right: 35px;
            
        }
        .signature_td{
                width: 130px!important;
                height: 70px!important;
        }

        @media    print {
            .btn_hidden{
                display: none;
            }
            .main_div{page-break-after: always;}
            table { page-break-inside:auto; }
            tr    { page-break-inside:avoid; page-break-after:auto;}
            thead {display: table-header-group;}
            tbody { page-break-after:always;
                display: table-row-group;}
            tfoot {
                display: table-footer-group;
            }
            /*td.signature_td{
                width:70px;
                height:101px;
            }*/


            .ex_footer{
                opacity:1;
                position: fixed;
                bottom:0px;
                left:0;
                width:100%;
                /* padding-top:20px; */
                /* height:100px; */

            }
            .ex_footer .authority{
                padding-bottom: 0px;
                padding-top:20px;
                text-align:center;


            }
            .ex_footer .authority span{
                font-size:14px;
            }

            tfoot tr td .authority span{
                color:#fff;
                opacity:0;
            }
            tfoot tr td .authority span{
                border-top: 1px solid #fff;
                color:#fff;
                opacity:0;
            }

            .ex_footer tr td .authority span{
                color:#000;
            }
            .ex_footer .authority span{
                border-top: 1px solid #000;
                color:#000;
            }
            .signature_td{
                width: 130px!important;
                height: 68px!important;
            }

            .pageNumber1:after {
                counter-increment: page;
                content:"Page number: " counter(page);
                left: 0;
                top: 100%;
                white-space: nowrap;
                z-index: 20;
                -moz-border-radius: 5px;
                -moz-box-shadow: 0px 0px 4px #222;
                background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
                background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
            }

        }
    </style>





</head>
<body>
        {{-- <button style="margin-top: 17px;" class="btn btn-info" id="pdf_btn" type="button" onclick="printDiv('print_area')"><i class="fa fa-print"></i> Print</button> --}}
<div id="print_area">
        <div class="main_div">

            <div class="salary_table">
                <table class="table_1">
                    <thead>
                    <tr class="table_heading">
                    <tr style="text-align: center;" class="company_heading_1">
                        <td colspan="30"> 
                            <p>LEFTY EMPLOYEE SALARY SHEET 3-SHIFT</p>
                         <h2>Fin Bangla Apparels Ltd.</h2>
                         <p>Pay Sheet for the month of  @if(!empty($monthname->month))
                                <b>{{date('F-Y',strtotime($monthname->month))}} (3-Shift)</b>
                            @else
                                No Data
                            @endif</p>
                         <p>Days in Month: @if(!empty($monthname->month))
                                {{date('t',strtotime($monthname->month))}}
                            @endif</p>
                     </td>
                    </tr>
                    <tr>
                        <th rowspan="2">SN</th>
                        <th rowspan="2">EmpID</th>
                        <th rowspan="2" width="14%" >Name</th>
                        <th rowspan="2" class="table_lefttd">Section</th>
                        <th rowspan="2"  width="7%" class="table_lefttd">Department</th>
                        <th rowspan="2" class="table_lefttd">Designation</th>
                        <th rowspan="2">Grade</th>
                        <th rowspan="2">Basic</th>
                        <th rowspan="2">Gross</th>
                        <th rowspan="2">Leave </th>
                        <th rowspan="2" width="3%">Work days</th>
                        <th rowspan="2">Abs Days</th>
                        <th rowspan="2">Weekend</th>
                        <th rowspan="2" >Holiday</th>
                        <th rowspan="2">Payable days</th>
                        <th style="text-align: center" colspan="3" >Deduction</th>
                        <th rowspan="2">Gross Pay</th>
                        <th style="text-align: center" colspan="3">Overtime</th>
                        <th rowspan="2">Att Bonus</th>
                        <th rowspan="3">Special Allow</th>
                        <th rowspan="2">Net Wages</th>
                        <th id="signature_id" rowspan="2">  ____Signature____  </th>
                    </tr>
                    </tr>
                    <tr>
                        <th>Adv</th>
                        <th>Abs</th>
                        <th>Stm</th>
                        <th>Hrs</th>
                        <th>Rate</th>
                        <th>Amt</th>
                      
                    </tr>
                    </thead>
                    <tbody>
                        @php $order=0; $total_basic=0; $total_gross=0; $total_gross_pay=0; $total_ot_hours=0; $t_amounts=0; $atts_bonus=0; $special_allow=0; $net_wages=0; @endphp
                        @foreach($data as $salary)
                        @php $order++; $total_basic+=$salary->basic_salary; $total_gross+=$salary->gross; $total_gross_pay+=$salary->gross_pay; $total_ot_hours+=$salary->overtime; $t_amounts+=$salary->overtime_amount;$atts_bonus+=$salary->attendance_bonus;$special_allow+=$salary->increment_bonus_percent;  $net_wages+=$salary->net_amount; @endphp
                        <tr>
                        <td>{{$order}}</td>
                            <td>{{$salary->s_emp_id}}</td>
                            <td class="table_lefttd" WIDTH="8%">{{$salary->e_first_name}} <br>Join: {{date('d-m-Y',strtotime($salary->empJoiningDate))}}</td>
                            <td class="table_lefttd" style="text-align:center">{{$salary->empSection}}</td>
                            <td class="table_lefttd" style="text-align:center">{{$salary->departmentName}}</td>
                            <td class="table_lefttd" style="text-align:center">{{$salary->designation}}</td>
                            <td>{{$salary->grade_name}}</td>
                            <td>{{round($salary->basic_salary)}}</td>
                            <td>{{round($salary->gross)}}</td>
                            <td>
                             @if($salary->total=='')
                                 0
                               @else
                                {{$salary->total}}
                             @endif
                           </td>
                            <td>   
                                @if($salary->present=='')
                                    0
                                    @else
                                    {{$salary->present}}
                                @endif
                            </td>
                            <td>
                                {{$salary->absent}}
                            </td>
                            
                            <td>{{$salary->weekend}}</td>
                            <td> @if($salary->holiday=='')
                                    0
                                    @else
                                    {{$salary->holiday}}
                                @endif
                            </td>
                            <td>@if($salary->leave==1) @php $payable=$salary->present+$salary->weekend+$salary->holiday; @endphp @else @php $payable=$salary->present+$salary->weekend+$salary->holiday+$salary->total; @endphp @endif {{$payable}}</td>
                            <td>
                                @if($salary->advanced_deduction=='')
                                    0
                                @else
                                    {{$salary->advanced_deduction}}
                                @endif
                            </td>
                            <td>
                               @if($salary->absent_deduction_amount=='')
                                    0
                                @else
                                    {{round($salary->absent_deduction_amount)}}
                                @endif
                            </td>
                            <td>
                                @if($salary->net_amount<=1000) 0
                                @else
                                    @if($salary->payment_mode=='Bank' || $salary->payment_mode=='bKash')
                                    0
                                @else
                                    10
                                @endif
                                @endif
                            </td>

                            <td>@if($salary->gross_pay=='')
                                    0
                                @else
                                    {{round($salary->gross_pay)}}
                                @endif
                            </td>
                            <td>
                                @if($salary->overtime=='')
                                    0
                                @else
                                    {{$salary->overtime}}
                                @endif
                            </td>
                            <td>
                                @if($salary->overtime_rate=='')
                                    0
                                @else
                                    {{round($salary->overtime_rate,2)}}
                                @endif
                            </td>
                            <td>
                                @if($salary->overtime_amount=='')
                                    0
                                @else
                                    {{round($salary->overtime_amount)}}
                                @endif
                            </td>
                            <td>{{$salary->attendance_bonus}}</td>
                            <td>  @if($salary->increment_bonus_percent=='')
                                    0
                                @else
                                    {{$salary->increment_bonus_percent}}
                                @endif
                            </td>
                            <td>
                                @php
                                    $total=$salary->net_amount
                                @endphp
                                {{round($total)}}
                            </td>
                            <td class="signature_td"></td>
                        </tr>
                        @endforeach
                                                               
                        <tr style="padding: 3px 3px;">
                            <td style="border-bottom: 1px solid #555;text-align: left;" colspan="1">Net</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1"></td>
                            <td style="border-bottom: 1px solid #555;" colspan="1"></td>
                            <td style="border-bottom: 1px solid #555;" colspan="1"></td>
                            <td style="border-bottom: 1px solid #555;" colspan="1"></td>
                            <td style="border-bottom: 1px solid #555;" colspan="1"></td>
                            <td style="border-bottom: 1px solid #555;" colspan="1"></td>
                        <td style="border-bottom: 1px solid #555;" colspan="1">{{round($total_basic)}}</td>
                        <td style="border-bottom: 1px solid #555;" colspan="1">{{round($total_gross)}}</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1"></td>
                            <td  style="border-bottom: 1px solid #555;" colspan="1"></td>
                            <td  style="border-bottom: 1px solid #555;" colspan="1"></td>
                            <td  style="border-bottom: 1px solid #555;" colspan="1"></td>
                            <td  style="border-bottom: 1px solid #555;" colspan="1"></td>
                            <td style="border-bottom: 1px solid #555;" colspan="1"></td>
                            <td style="border-bottom: 1px solid #555;" colspan="1"></td>
                            <td style="border-bottom: 1px solid #555;" colspan="1"></td>
                            <td style="border-bottom: 1px solid #555;" colspan="1"></td>
                        <td style="border-bottom: 1px solid #555;" colspan="1">{{round($total_gross_pay)}}</td>
                        <td style="border-bottom: 1px solid #555;" colspan="1">{{$total_ot_hours}}</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1"></td>
                        <td style="border-bottom: 1px solid #555;" colspan="1">{{round($t_amounts)}}</td>
                        <td colspan="1">{{$atts_bonus}}</td>
                        <td style="border-bottom: 1px solid #555;" colspan="1">{{$special_allow}}</td>
                        <td style="border-bottom: 1px solid #555;">{{round($net_wages)}}</td>
                            <td style="border-bottom: 1px solid #555;"></td>
                        </tr>
                    <tr></tr>
                
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="31" style="padding-top:20px;border:none;border-top:4px solid #ffffff;" >
                            <div class="authority">
                                <div class="prepared_by">
                                    <span>Prepared By</span>
                                </div>
                                <div class="audited_by">
                                    <span>Audited By</span>
                                </div>
                                <div class="recomended_by">
                                    <span>Recommended By</span>
                                </div>
                                <div class="approved_by">
                                    <span>Director</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

<div class="ex_footer">
    <tr>
        <td colspan="31" style="padding-top:20px;border:none;border-top:4px solid #ffffff;" >

            <div class="authority">
                <div class="prepared_by">
                    <span>Prepared By</span>
                </div>
                <div class="audited_by">
                    <span>Audited By</span>
                </div>
                <div class="recomended_by">
                    <span>Recommended By</span>
                </div>
                <div class="approved_by">
                    <span>Director</span>
                </div>
            </div>
        </td>

    </tr>

</div>

<script>
    // window.print();
    // window.close();
        // function printDiv(divName) {
        //     var printContents = document.getElementById(divName).innerHTML;
        //     var originalContents = document.body.innerHTML;
        //     document.body.innerHTML = printContents;
        //     window.print();
        //     document.body.innerHTML = originalContents;
        // }
    </script>
</body>
</html>
