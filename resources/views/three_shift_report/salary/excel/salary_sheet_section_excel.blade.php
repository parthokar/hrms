<!DOCTYPE html>
<html>
<head>
    <title>Salary Sheet</title>

</head>
<body>
<div id="print_area">
    @php $order=0; $total_gross_pay=0;$total_ot=0; $total_ot_amount=0; $total_attendance_bonus=0; $total_net_amount=0; $total_advance_salary=0; @endphp
    @foreach($department as $datas)
        @php
            $dept_id=$datas->section_id;
             $month=$months;
             $yearss=$ye;
             $monthss=$mo;
         $count=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('payroll_grade','tb_salary_history_shift.grade_id','=','payroll_grade.id')
         ->leftjoin('designations','tb_salary_history_shift.designation_id','=','designations.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('employees.employeeId as em_idss','employees.work_group','tb_salary_history_shift.emp_id','tb_salary_history_shift.present','tb_salary_history_shift.weekend','tb_salary_history_shift.holiday','tb_salary_history_shift.total_payable_days','tb_salary_history_shift.basic_salary','tb_salary_history_shift.house_rant','tb_salary_history_shift.medical','tb_salary_history_shift.transport','tb_salary_history_shift.food','tb_salary_history_shift.gross','tb_salary_history_shift.leave','tb_salary_history_shift.total','tb_salary_history_shift.working_day','tb_salary_history_shift.absent','tb_salary_history_shift.absent_deduction_amount','tb_salary_history_shift.advanced_deduction','tb_salary_history_shift.gross_pay','tb_salary_history_shift.overtime','tb_salary_history_shift.overtime_rate','tb_salary_history_shift.overtime_amount','tb_salary_history_shift.attendance_bonus','tb_salary_history_shift.increment_bonus_percent','tb_salary_history_shift.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','tb_salary_history_shift.emp_id as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.employeeId','tb_salary_history_shift.total_late')
         ->where('tb_salary_history_shift.dates',$month)
         ->where(function ($query) use ($dt){
            $query->where('date_of_discontinuation','=',null);
            $query->orWhere('date_of_discontinuation','>',$dt);
         })
         ->where('employees.empJoiningDate','<=',$dt)
         ->where('tb_salary_history_shift.section_id','=',$dept_id)
         ->where('tb_salary_history_shift.status','=',0)
         ->orderBy('employees.employeeId','ASC')
         ->count();

         if($count>0){
         $employees=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('payroll_grade','tb_salary_history_shift.grade_id','=','payroll_grade.id')
         ->leftjoin('designations','tb_salary_history_shift.designation_id','=','designations.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('shift_start','shift_end','employees.employeeId as em_idss','employees.payment_mode','employees.work_group','tb_salary_history_shift.emp_id','tb_salary_history_shift.present','tb_salary_history_shift.weekend','tb_salary_history_shift.holiday','tb_salary_history_shift.total_payable_days','tb_salary_history_shift.basic_salary','tb_salary_history_shift.house_rant','tb_salary_history_shift.medical','tb_salary_history_shift.transport','tb_salary_history_shift.food','tb_salary_history_shift.gross','tb_salary_history_shift.leave','tb_salary_history_shift.total','tb_salary_history_shift.working_day','tb_salary_history_shift.absent','tb_salary_history_shift.absent_deduction_amount','tb_salary_history_shift.advanced_deduction','tb_salary_history_shift.gross_pay','tb_salary_history_shift.overtime','tb_salary_history_shift.overtime_rate','tb_salary_history_shift.overtime_amount','tb_salary_history_shift.attendance_bonus','tb_salary_history_shift.increment_bonus_percent','tb_salary_history_shift.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','tb_salary_history_shift.emp_id as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.employeeId','tb_salary_history_shift.total_late','tb_salary_history_shift.advance_salary')
         ->where('tb_salary_history_shift.dates',$month)
         ->where(function ($query) use ($dt){
            $query->where('date_of_discontinuation','=',null);
            $query->orWhere('date_of_discontinuation','>',$dt);
         })
         ->where('employees.empJoiningDate','<=',$dt)
         ->where('tb_salary_history_shift.section_id','=',$dept_id)
         ->where('tb_salary_history_shift.status','=',0)
         ->orderBy('employees.employeeId','ASC')
         ->get();


         $emp_count_with_salary=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select(DB::raw('count(tb_salary_history_shift.emp_id) as emp_count'),DB::raw('SUM(tb_salary_history_shift.basic_salary) as total_basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as total_house'),DB::raw('SUM(tb_salary_history_shift.medical) as total_medical'),DB::raw('SUM(tb_salary_history_shift.transport) as total_transport'),DB::raw('SUM(tb_salary_history_shift.food) as total_food'),DB::raw('SUM(tb_salary_history_shift.gross) as total_gross'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as total_deduction'), DB::raw('SUM(tb_salary_history_shift.net_amount) as total_net') )
         ->where('tb_salary_history_shift.dates',$month)
         ->where(function ($query) use ($dt){
            $query->where('date_of_discontinuation','=',null);
            $query->orWhere('date_of_discontinuation','>',$dt);
         })
         ->where('employees.empJoiningDate','<=',$dt)
         ->where('tb_salary_history_shift.section_id','=',$dept_id)
         ->orderBy('employees.employeeId','ASC')
         ->get();
         }else{
             echo "No Data Found";
             exit();
         }
        @endphp
        <div class="main_div">
            {{--<div class="company">--}}
            {{--<h3>{{$companyInformation->company_name}}</h3>--}}
            {{--<p>Pay Sheet for the month of</p>--}}
            {{--<p>--}}
            {{--@if($monthname)--}}
            {{--{{date('F-Y',strtotime($monthname->month))}}<br>--}}
            {{--<span style="text-align: center">Payment Date: <b>{{date('d-m-Y',strtotime($payment_date))}}</b></span>--}}
            {{--@else--}}
            {{--Sorry No Data Found--}}
            {{--@endif--}}
            {{--</p>--}}
            {{--</div>--}}

            <div class="salary_table">
                <table class="table_1">
                    <thead>


                    <tr class="table_heading">

                    <tr style="text-align: center;" class="company_heading_1">

                        <td colspan="31" style="text-align: center;">  <h3>{{$companyInformation->company_name}}</h3></td>
                    </tr>
                    <tr  style="text-align: center;" class="company_heading_1">

                        <td colspan="3"  style="text-align: left;">

                            <span>Department:</span>
                            <span><b>{{$datas->departmentName}}</b></span>
                            {{-- <span>Section/Line: <b>@if($datas->line_no=='') {{$datas->section_id}} @else {{$datas->line_no}} @endif</b></span><br /> --}}
                            
                        </td>
                        <td colspan="3">
                            <p>Section/Line: <b>@if($datas->line_no=='') {{$datas->section_id}} @else {{$datas->line_no}} @endif</b></p>
                        </td>
                        <td colspan="17" >
                            <p>Pay Sheet for the month of : <span> @if($monthname)
                                        {{date('F-Y',strtotime($monthname->month))}} (3-Shift)<br>

                                    @else
                                        Sorry No Data Found
                                    @endif </span></p>

                        </td>

                        <td colspan="11" style="text-align: right;">
                            <span class="pageNumber"></span><br>
                            <span class="dayofmonth">Days in Month:
                                            <b>
                                            @if($monthname)
                                                    {{date('t',strtotime($monthname->month))}}
                                                @else
                                                @endif
                                        </b>
                                        </span><br />
                            <span style="margin-right:60px">Payment Date:</span>
                                
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>


                    <tr>



                        <th>SN</th>
                        <th>EmpID</th>
                        <th>Name</th>
                        <th   class="table_lefttd">Designation</th>
                        <th class="table_lefttd">Grade</th>
                        <th>Basic</th>
                        <th>House</th>
                        <th>Medical</th>
                        <th>Food</th>
                        <th >Transport</th>
                        <th>Gross</th>
                        <th>
                            CL
                        </th>
                        <th>
                            SL
                        </th>
                        <th>
                            EL
                        </th>
                        <th>
                            ML
                        </th>
                        <th class="verticalTableHeader">Work<br />Days</th>
                        <th>Abs Days</th>
                        <th>Late</th>
                        <th class="verticalTableHeader">Week<br />end</th>
                        <th class="verticalTableHeader">Holiday</th>
                        <th>Total Payable<br /> days</th>
                        <th>Adv</th>
                        <th> Abs</th>
                        <th> Stm</th>
                        <th>Gross Pay</th>
                        <th>Hrs</th>
                        <th>Rate</th>
                        <th>Amt</th>
                        <th>Att Bonus</th>
                        <th >Special Allow</th>
                        <th >Advance Salary</th>
                        <th>Net wages</th>
                        <th style=""  id="signature_11">Signature</th>
                    </tr>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($employees as $salary)
                        @php
                            $order++;
                            $test=$salary->emp_id;

                                  $casual=DB::SELECT("SELECT tb_salary_history_shift.emp_id as main_id,emp_shift_leave.leave_id,SUM(emp_shift_leave.total) as total
                                  FROM tb_salary_history_shift
                                  LEFT JOIN emp_shift_leave ON emp_shift_leave.emp_id=tb_salary_history_shift.emp_id
                                  WHERE emp_shift_leave.leave_date BETWEEN '$salary->shift_start' AND '$salary->shift_end' AND tb_salary_history_shift.emp_id='$test' AND emp_shift_leave.leave_id=5 GROUP BY emp_shift_leave.leave_id,emp_shift_leave.emp_id");

                                  $sick=DB::SELECT("SELECT tb_salary_history_shift.emp_id as main_id,emp_shift_leave.leave_id,SUM(emp_shift_leave.total) as total
                                  FROM tb_salary_history_shift
                                  LEFT JOIN emp_shift_leave ON emp_shift_leave.emp_id=tb_salary_history_shift.emp_id
                                  WHERE emp_shift_leave.leave_date BETWEEN '$salary->shift_start' AND '$salary->shift_end' AND tb_salary_history_shift.emp_id='$test' AND emp_shift_leave.leave_id=6 GROUP BY emp_shift_leave.leave_id,emp_shift_leave.emp_id");


                                  $earn=DB::SELECT("SELECT tb_salary_history_shift.emp_id as main_id,emp_shift_leave.leave_id,SUM(emp_shift_leave.total) as total
                                  FROM tb_salary_history_shift
                                  LEFT JOIN emp_shift_leave ON emp_shift_leave.emp_id=tb_salary_history_shift.emp_id
                                  WHERE emp_shift_leave.leave_date BETWEEN '$salary->shift_start' AND '$salary->shift_end' AND tb_salary_history_shift.emp_id='$test' AND emp_shift_leave.leave_id=4 GROUP BY emp_shift_leave.leave_id,emp_shift_leave.emp_id");


                                  $maternity=DB::SELECT("SELECT tb_salary_history_shift.emp_id as main_id,emp_shift_leave.leave_id,SUM(emp_shift_leave.total) as total
                                  FROM tb_salary_history_shift
                                  LEFT JOIN emp_shift_leave ON emp_shift_leave.emp_id=tb_salary_history_shift.emp_id
                                  WHERE emp_shift_leave.leave_date BETWEEN '$salary->shift_start' AND '$salary->shift_end' AND tb_salary_history_shift.emp_id='$test' AND emp_shift_leave.leave_id=1 GROUP BY emp_shift_leave.leave_id,emp_shift_leave.emp_id");



                                  if($salary->leave==1){
                                    $total_pay=$salary->present+$salary->weekend+$salary->holiday;
                                  }else{
                                    $total_pay=$salary->present+$salary->weekend+$salary->holiday+$salary->total;
                                  }
                        @endphp
                        <tr>
                            <td style="text-align:left;">{{$order}}</td>
                            <td style="text-align:left;">{{$salary->em_idss}}</td>
                            <td style="text-align:left;">{{$salary->e_first_name}} <br>Join: {{date('d-m-Y',strtotime($salary->empJoiningDate))}}</td>
                            <td style="text-align:left;">{{$salary->designation}}</td>
                            <td style="text-align:center";>{{$salary->grade_name}}</td>
                            <td style="text-align:left;">{{round($salary->basic_salary,0)}}</td>
                            <td style="text-align:left;">{{round($salary->house_rant,0)}}</td>
                            <td style="text-align:left;">{{round($salary->medical,0)}}</td>
                            <td style="text-align:left;">{{round($salary->food,0)}}</td>
                            <td style="text-align:left;">{{round($salary->transport,0)}}</td>
                            <td style="text-align:left;">{{round($salary->gross,0)}}</td>


                                <td style="text-align:left;">
                                    @if($casual)
                                    @foreach($casual as $leave_type)
                                        {{$leave_type->total}}
                                    @endforeach
                                    @else
                                    0
                                    @endif
                                </td>

                                <td style="text-align:left;">
                                    @if($sick)
                                    @foreach($sick as $leave_type)
                                        {{$leave_type->total}}
                                   @endforeach
                                   @else
                                   0
                                   @endif
                                </td>

                                <td style="text-align:left;">
                                   @if($earn)
                                    @foreach($earn as $leave_type)
                                        {{$leave_type->total}}
                                    @endforeach
                                    @else
                                    0
                                    @endif
                                </td>

                                <td style="text-align:left;">
                                  @if($maternity)
                                    @foreach($maternity as $leave_type)
                                        {{$leave_type->total}}
                                    @endforeach
                                    @else
                                    0
                                    @endif
                                </td>


                            <td style="text-align:left;">
                                @if($salary->present=='')
                                    0
                                @else
                                    {{$salary->present}}
                                @endif
                            </td>
                            <td style="text-align:left;">
                                @if($salary->absent=='')
                                    0
                                @else
                                    {{$salary->absent}}
                                @endif
                            </td>

                            <td style="text-align:left;">
                                @if($salary->total_late=='')
                                    0
                                @else
                                    {{$salary->total_late}}
                                @endif
                            </td>

                            <td style="text-align:left;">{{$salary->weekend}}</td>
                            <td style="text-align:left;">
                                @if($salary->holiday=='')
                                    0
                                @else
                                    {{$salary->holiday}}
                                @endif
                            </td>
                            <td style="text-align:left;">{{$total_pay}}</td>
                            <td style="text-align:left;">
                                @if($salary->advanced_deduction=='')
                                    0
                                @else
                                    {{round($salary->advanced_deduction,0)}}
                                @endif
                            </td>
                            <td style="text-align:left;">
                                @if($salary->absent_deduction_amount=='')
                                    0
                                @else
                                    {{round($salary->absent_deduction_amount,0)}}
                                @endif
                            </td>
                            <td style="text-align: left">
                               @if($salary->payment_mode=='Bank' || $salary->payment_mode=='bKash')
                                    0
                                @else
                                    10
                                @endif
                            </td>
                            <td style="text-align:left;">
                                @if($salary->gross_pay=='')
                                    0
                                @else
                                    {{round($salary->gross_pay)}}
                                @endif
                                @php $total_gross_pay+=$salary->gross_pay; @endphp
                            </td>
                            <td style="text-align:left;">
                                @if($salary->overtime=='')
                                    0
                                @else
                                    {{$salary->overtime}}
                                @endif
                                @php $total_ot+=$salary->overtime; @endphp
                            </td>
                            <td style="text-align:left;">
                                @if($salary->work_group=='Staff')
                                @else
                                    {{round($salary->overtime_rate,2)}}
                                @endif
                            </td>
                            <td style="text-align:left;">
                                @if($salary->overtime_amount=='')
                                    0
                                @else
                                    {{round($salary->overtime_amount)}}
                                @endif
                                @php $total_ot_amount+=round($salary->overtime_amount); @endphp
                            </td>
                            <td style="text-align:left;">
                                   {{round($salary->attendance_bonus,0)}}
                                    @php $total_attendance_bonus+=$salary->attendance_bonus; @endphp
                            </td>
                            <td style="text-align:left;">
                                @if($salary->increment_bonus_percent=='')
                                    0
                                @else
                                    {{round($salary->increment_bonus_percent,0)}}
                                @endif
                            </td>
                            <td style="text-align:left;">@if($salary->advance_salary=='') 0 @else {{ $salary->advance_salary}} @endif @php $total_advance_salary+=$salary->advance_salary; @endphp</td>
                            <td style="text-align:left;">
                                 @php $total=$salary->net_amount @endphp
                                    {{round($total)}}
                                 @php $total_net_amount+=$total; @endphp

                            </td>
                            <td class="signature_td" style="text-align:left;"></td>
                        </tr>
                    @endforeach
                    @foreach($emp_count_with_salary as $total_counts)
                    <tr style="padding: 3px 3px;">
                        <td style="border-bottom: 1px solid #555;text-align: left;" colspan="5">Dept/Section:{{$total_counts->emp_count}}</td>
                        <td style="border-bottom: 1px solid #555;" colspan="1" >{{$total_counts->total_basic}}</td>
                        <td style="border-bottom: 1px solid #555;" colspan="1">{{$total_counts->total_house}}</td>
                        <td style="border-bottom: 1px solid #555;" colspan="1">{{$total_counts->total_medical}}</td>
                        <td style="border-bottom: 1px solid #555;" colspan="1">{{$total_counts->total_food}}</td>
                        <td style="border-bottom: 1px solid #555;" colspan="1">{{$total_counts->total_transport}}</td>
                        <td style="border-bottom: 1px solid #555;" colspan="1">{{$total_counts->total_gross}}</td>
                        <td  style="border-bottom: 1px solid #555;" colspan="10"></td>
                        <td style="border-bottom: 1px solid #555;" colspan="3">{{$total_counts->total_deduction}}</td>
                        <td style="border-bottom: 1px solid #555;" colspan="1">{{round($total_gross_pay)}}</td>
                        <td style="border-bottom: 1px solid #555;" colspan="1">{{$total_ot}}</td>
                        <td style="border-bottom: 1px solid #555;" colspan="1"></td>
                        <td style="border-bottom: 1px solid #555;" colspan="1">{{$total_ot_amount}}</td>
                        <td style="border-bottom: 1px solid #555;" colspan="1">{{$total_attendance_bonus}}</td>
                        <td style="border-bottom: 1px solid #555;" colspan="1">{{$total_counts->total_increment}}</td>
                    <td colspan="1">{{$total_advance_salary}}</td>
                    <td style="border-bottom: 1px solid #555;" colspan="1">{{round($total_net_amount)}}</td>
                        <td style="border-bottom: 1px solid #555;"></td>
                    </tr>
                    <tr></tr>
                @endforeach
                    </tbody>
                    <tfoot>
                            <tr>
                                    <td colspan="2"></td>
                                    <td colspan="1"><p>Prepared By</p></td>
                                    <td colspan="3"><p>Audited By</p></td>
                                    <td colspan="3"><p>Recommended By</p></td>
                                    <td colspan="4"><p>Director</p></td>
        
                                </tr>

                    </tfoot>
                </table>

            </div>
        </div>
    @endforeach
</div>

{{-- <div class="ex_footer">
    <tr>
        <td colspan="31" style="padding-top:20px;border:none;border-top:4px solid #ffffff;" >

            <div class="authority">
                <div class="prepared_by">
                    <span>Prepared By</span>
                </div>
                <div class="audited_by">
                    <span>Audited By</span>
                </div>
                <div class="recomended_by">
                    <span>Recommended By</span>
                </div>
                <div class="approved_by">
                    <span>Director</span>
                </div>
            </div>
        </td>

    </tr>

</div> --}}



<script>
    // window.print();
    // window.close();
</script>
</body>
</html>