@extends('layouts.master')
@section('title', 'Vehicle Report')
@section('content')
    <div class="page-content page-content-hover-1">
        <div class="row panel myAnimation_1"  style="border:1px solid #dcdcdc;">

            <div class="col-xlg-12 col-lg-12  col-sm-12 animated bounceInRight">
                <div class="text-center" >
                    <h2><b>Vehicle Report</b></h2>
                    <hr class="animated bounceInLeft">
                </div>
            </div>

            <div class="col-xlg-3 col-xlg-offset-3 col-lg-3 col-sm-3 animated bounceInLeft">
              <a target="_BLANK" href="{{route('vehicle.vehicle_wise_report_view')}}">
              <div class="panel">
                <div class="panel-content emp_sel" style="">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-money f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Vehicle Expense report</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>


            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInRight">
              <a target="_BLANK" href="{{route('vehicle.vehicle_booking_report_view')}}">
              <div class="panel">
                <div class="panel-content emp_emlbas" style="">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-car f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Vehicle Booking report</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

        <div class="col-md-12">
          <hr>
        </div>
        </div>
    </div>
  
    @include('include.copyright')
@endsection