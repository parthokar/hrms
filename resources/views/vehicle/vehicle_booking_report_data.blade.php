@extends('layouts.master')
@section('title', 'Booked Vehicle List')
@section('content')
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('delete'))

            <p id="alert_message" class="alert alert-danger">{{Session::get('delete')}}</p>

        @endif
        @if(Session::has('edit'))

            <p id="alert_message" class="alert alert-info">{{Session::get('edit')}}</p>

        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-lg-12">
                                <h3><i class="fa fa-car"></i> <strong>Booked Vehicle </strong> List from <b>{{\Carbon\Carbon::parse($request->start_date)->format('d-M-Y')}}</b> to <b>{{\Carbon\Carbon::parse($request->end_date)->format('d-M-Y')}}</b></h3>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-bordered table-dynamic">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Vehicle Name</th>
                                <th>Booked By</th>
                                <th>Driver</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Status</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=0; @endphp
                            @foreach($booking_list as $vi)
                                <tr>
                                    <td>{{++$i}}</td>
                                    <td>{{$vi->vehicleName}}</td>
                                    <td>{{$vi->bookedBy}}</td>
                                    <td>{{$vi->driverName}}</td>
                                    <td>{{$vi->vbDate}}</td>
                                    <td>{{date("h:i a", strtotime($vi->vbStartTime))}}-{{date("h:i a", strtotime($vi->vbEndTime))}}</td>
                                    <td><?php if($vi->status==0){ echo "<span style='color:green;'>Active</span>"; }
                                    if($vi->status==1){ echo "<span style='color:red;'>Cancelled</span>"; } ?></td>
                                    <td style="text-align: center;">
                                        <a data-toggle="modal" title="Purpose" data-target="#view{{$vi->id}}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> Purpose</a>
                                       
                                    </td>
                                </tr>


                                <div class="modal fade" id="view{{$vi->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Purpose of Booking</strong></h5>
                                            </div>
                                            <div class="modal-body">
                                                <h5><b>{{$vi->purpose}}</b></h5>
                                                <hr>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut();
        }, 10000);
    </script>


    @include('include.copyright')
@endsection