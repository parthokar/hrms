@extends('layouts.master')
@section('title', 'Vehicle Expense Category List')
@section('content')
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('delete'))

            <p id="alert_message" class="alert alert-danger">{{Session::get('delete')}}</p>

        @endif
        @if(Session::has('edit'))

            <p id="alert_message" class="alert alert-info">{{Session::get('edit')}}</p>

        @endif
        <div class="row">
            
            <div class="col-lg-6 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-plus"></i> <strong>New vehicle expense category</strong></h3>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        {!! Form::open(['method'=>'POST', 'route'=>'vehicle.excatstore']) !!}
                        <div class="form-group">
                          <label for="vexName"> Category Name</label>
                          <input type="text" name="vexName" class="form-control" required="" placeholder="Category Name" >
                        </div>
                        <div class="form-group">
                          <label for="vexDescription"> Category Description</label>
                          <textarea name="vexDescription" class="form-control" placeholder="Category Description" ></textarea>
                        </div>
                        <div class="form-group">
                            <hr>
                            <button type="Submit" class="btn btn-primary"><i class="fa fa-save"></i> Save Category</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>           
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-lg-6">
                                <h3><i class="fa fa-car"></i> <strong>Vehicle Expense Category List </strong></h3>
                            </div>
                            <div class="col-lg-6" style="text-align: right;">
                               
                            </div>
                        </div>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-bordered table-dynamic">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Category Name</th>
                                <th>Description</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=0; @endphp
                            @foreach($expenses_category as $vi)
                                <tr>
                                    <td>{{++$i}}</td>
                                    <td>{{$vi->vexName}}</td>
                                    <td>{{$vi->vexDescription}}</td>
                                    <td>
                                        <a data-toggle="modal" title="Edit" data-target="#edit{{$vi->id}}" class="btn btn-default btn-sm"><i class="fa fa-edit"></i></a>
                                        <a data-toggle="modal" title="Delete Data" data-target="#delete{{$vi->id}}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>

                                <!-- Edit Modal -->
                                <div class="modal fade" id="edit{{$vi->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Update Info</strong></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                    {!! Form::open(['method'=>'POST', 'route'=>'vehicle.excatupdate']) !!}
                                                    <div class="form-group">
                                                      <label for="vexName"> Category Name</label>
                                                      <input type="text" value="{{$vi->vexName}}" name="vexName" class="form-control" required="" placeholder="Category Name" >
                                                      <input type="hidden" name="id" value="{{$vi->id}}">
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="vexDescription"> Category Description</label>
                                                      <textarea name="vexDescription" class="form-control" placeholder="Category Description" >{{$vi->vexDescription}}</textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <hr>
                                                        <button type="Submit" class="btn btn-primary">Update Category</button>
                                                    </div>
                                                    {!! Form::close() !!}
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                {{--@endif--}}

                                <!--End of Edit Model-->





                                <!-- Delete Modal -->
                                <div class="modal fade" id="delete{{$vi->id}}" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Confirm Delete</strong></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Do you want to delete <strong>{{$vi->vexName}} category?</strong></p>
                                                <hr>
                                                 {!! Form::open(['method'=>'POST', 'route'=>'vehicle.excatdelete']) !!}
                                                     <input type="hidden" name="id" value="{{$vi->id}}">
                                                    <button type="submit" class="btn btn-danger">Confirm Delete</button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                {!! Form::close() !!}
                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <!--Delete Modal Ended-->

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>


    @include('include.copyright')
@endsection