<!-- BEGIN TOPBAR -->
<div class="topbar">
    <div class="header-left">
        <div class="topnav">
            <a class="menutoggle" href="#" data-toggle="sidebar-collapsed"><span class="menu__handle"><span>Menu</span></span></a>
            <ul class="nav nav-icons">
                <li><a href="#" class="toggle-sidebar-top"><span class="icon-user-following"></span></a></li>
            </ul>

        </div>
    </div>
    <div class="header-right">
        <ul class="header-menu nav navbar-nav">
               <li class="dropdown" style="margin:12px;">
                <span class="dash_time" style="font-size:16px;font-weight:bold;margin:15px;">Time: <script src="<?php echo URL::to('hrm_script/js/time.js') ?>" type="text/javascript"></script></span>
              </li>         
            <!-- BEGIN NOTIFICATION DROPDOWN -->
            <li class="dropdown" id="notifications-header">
                <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <i class="icon-bell"></i>
                    <span class="badge badge-danger badge-header">
                      @php
                          $total=$pending_leave+$meeting_date+count($notifications)
                      @endphp
                      @if(checkPermission(['admin']) or checkPermission(['hr']) or checkPermission(['executive']))
                            {{$total}}
                             @else
                             0
                      @endif
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="dropdown-header clearfix">
                        <p class="pull-left">Notifications</p>
                    </li>
                    <li>
                        <ul class="dropdown-menu-list withScroll" data-height="220">

                            @if(checkPermission(['admin']) or checkPermission(['hr']) or checkPermission(['executive']))
                            @foreach($all_pending_leave_with_name as $all_leave)
                                <li>                                 
                                    <a href="{{url('/employee/pending/leave/details/'.$all_leave->employee_id)}}">
                                        <i class="fa fa-bell p-r-10 f-18 c-orange"></i>
                                       {{$all_leave->emp_name}} has pending leave
                                        <span class="dropdown-time">{{ \Carbon\Carbon::parse($all_leave->created_at)->diffForHumans() }}</span>
                                    </a>
                                </li>
                            @endforeach
                            @foreach($metting_all as $all_metting)
                                    <li>
                                        <a href="{{route('meeting.index')}}">
                                            <i class="fa fa-bell p-r-10 f-18 c-orange"></i>
                                            {{$all_metting->msub}}   Meeting today
                                            <span class="dropdown-time">{{ \Carbon\Carbon::parse($all_metting->created_at)->diffForHumans() }}</span>
                                        </a>
                                    </li>
                            @endforeach

                            @foreach($notifications as $notification)
                                <li>
                                    <a class="notif" data-custom-value="{{$notification->id}}" href="{{route('employee.show',$notification->notification_to)}}">
                                        <i class="fa fa-bell p-r-10 f-18 c-orange"></i>
                                        {{$notification->message}}
                                        <span class="dropdown-time">{{ \Carbon\Carbon::parse($notification->created_at)->diffForHumans() }}</span>
                                    </a>
                                </li>
                            @endforeach
                                @else
                                Not permission
                            @endif
                        </ul>
                    </li>
                    <li class="dropdown-footer clearfix">
                        <a href="{{route('notifications.index')}}" class="btn-link pull-left">All notifications</a>
                        <a href="#" class="pull-right">
                            <i class="icon-settings"></i>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- END NOTIFICATION DROPDOWN -->
            <!-- BEGIN USER DROPDOWN -->
            <li class="dropdown" id="user-header">
                <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <img src="{{url('hrm_script/images/avatars/user1.png')}}" alt="user image">
                    <span class="username">{{ Auth::user()->name }}</span>
                </a>
                <ul class="dropdown-menu">
                    {{--<li>--}}
                        {{--<a href="{{route('user.profile')}}"><i class="icon-user"></i><span>My Profile</span></a>--}}
                    {{--</li>--}}
                   <!--  <li>
                        <a href="#"><i class="icon-settings"></i><span>Account Settings</span></a>
                    </li> -->
                    <li>
                        <a href="{{url('/logout')}}"><i class="icon-logout"></i><span>Logout</span></a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- END TOPBAR -->
@php
use App\Http\Controllers\AccessController;
@endphp
<script>
    $('.notif').click(function (event) {
        var not_id=$(this).data('custom-value');
//        alert(not_id);
//        event.preventDefault();
        var url="{{route('notification.read',1)}}"+"?not_id="+not_id;
//        alert(url);
        $.ajax({
           url:url,
            method:'get',
            success:function (response) {
               console.log(response);

            }
        });
//        event.preventDefault();
    })
</script>