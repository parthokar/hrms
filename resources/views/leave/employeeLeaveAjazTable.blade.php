

        <div class="panel-content pagination2 table-responsive">
                <div class="panel-controls">
                        <h3><i class="fa fa-table"></i> <strong>Employee leave details</strong></h3>
                    </div>
            <table class="table table-info table-bordered">
                <thead>
                <tr>
                    <th>Leave Name</th>
                    <th>Employee ID</th>
                    <th>Leave Name</th>
                    <th>Total Days</th>
                    <th>Available Days</th>
                    <th>Leave Taken</th>
                </tr>
                </thead>
                <tbody>
                {{--{{$leave_data->leave_type}}--}}
                @foreach($leave_data as $key=>$ld)
                    @if($key!=0 || $employee->empGenderId!=1)
                        <?php
                        if($ld->leave_type=="Earn Leave"){;
                            $ld->total_days=floor($days/18)+$previous_earn_leave;
                        }
                        else{
                            if(isset($employee->empJoiningDate)){
                                if(\Carbon\Carbon::parse($employee->empJoiningDate)->format('Y')==\Carbon\Carbon::now()->format('Y'))
                                {
                                    $mr=13-\Carbon\Carbon::parse($employee->empJoiningDate)->format('m');
                                    $ld->total_days=floor(($ld->total_days/12)*$mr);
                                }
                            }
                        }

                        ?>
                        <tr>
                            <td>{{$employee->empFirstName." ".$employee->empLastName}}</td>
                            <td>{{$employee->employeeId}}</td>
                            <td>{{$ld->leave_type}}</td>
                            <td>{{$ld->total_days}}</td>
                            @if(isset($ld->leave_taken))
                                <?php
                                $ld->leave_available= $ld->total_days-$ld->leave_taken-$paid_leave
                                ?>
                            @else
                                <?php
                                $ld->leave_available=$ld->total_days-$paid_leave;
                                ?>
                            @endif
                            <td>
                                {{$ld->leave_available}}

                            </td>
                            <td>
                                @if(isset($ld->leave_taken))
                                    {{$ld->leave_taken}}

                                @else
                                    {{$ld->total_days-$ld->leave_available}}
                                @endif
                            </td>

                        </tr>
                    @endif

                @endforeach
                </tbody>
            </table>
        </div>



