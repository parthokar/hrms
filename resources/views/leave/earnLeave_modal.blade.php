
{!! Form::open(['method'=>'POST','action'=>'leaveController@earn_leave_payment']) !!}
{!! Form::hidden('emp_id',base64_encode($emp_id)) !!}

<div class="form-group">
    {!! Form::label('available_leave','Available Leave:') !!}
    {!! Form::number('available_leave',"$leave_available",['class'=>'form-control','readonly','required type'=>'number', 'placeholder'=>'Enter Available Leave']) !!}

</div>

<div class="form-group">

    {!! Form::label('leave_to_cash','Leave To Cash:') !!}
    {!! Form::number('leave_to_cash',null,['class'=>'form-control', 'required'=>'', 'placeholder'=>'Enter Other Details Here']) !!}

</div>

<div class="form-group">
    {!! Form::label('payment_amount','Payment Amount:') !!}
    {!! Form::text('payment_amount',null,['class'=>'form-control', 'required'=>'', 'placeholder'=>'Enter Other Details Here']) !!}
</div>

<div class="modal-footer">
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="Submit" class="btn btn-primary">Make Payment</button>
    </div>
</div>

{!! Form::close() !!}

<script>
    $(document).ready(function () {
        $('#leave_to_cash').keyup(function (event) {
            var val= $(this).val();
            {{--alert("{{$leave_available}}");--}}
            if(val>{{$leave_available}}){
                alert("Leave to cash can not be greater than available leave");
                $('#leave_to_cash').val(0);
                $('#payment_amount').val(0);
            }
            else{
                $('#payment_amount').val(val*"{{$sal_per_day}}");

            }
            
        });

    });
</script>