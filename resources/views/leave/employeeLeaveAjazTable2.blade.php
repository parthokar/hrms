
        
        <div class="panel-content pagination2 table-responsive">
                <div class="panel-controls">
                        <h3><i class="fa fa-table"></i> <strong>Employee Leave details</strong></h3>
                    </div>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Employee ID</th>
                    <th>Designation</th>
                    <th>Leave type</th>
                    <th>Total Days</th>
                    <th>Available Days</th>
                </tr>
                </thead>
                <tbody>
                @foreach($leaveData as $b)
                    <?php
                    if($b->leave_type=="Earn Leave"){;
                        $b->total_days=floor($days/18)+$previous_earn_leave;
                    }
                    else{
                        if(isset($employeeData[0]->empJoiningDate)){
                            if(\Carbon\Carbon::parse($employeeData[0]->empJoiningDate)->format('Y')==\Carbon\Carbon::now()->format('Y'))
                            {
                                $mr=13-\Carbon\Carbon::parse($employeeData[0]->empJoiningDate)->format('m');
                                $b->total_days=floor(($b->total_days/12)*$mr);
                            }
                        }
                    }

                    ?>
                    <tr>
                        <td>{{$employeeData[0]->empFirstName}} {{$employeeData[0]->empLastName}}</td>
                        <td>{{$employeeData[0]->employeeId}}</td>
                        <td>{{$employeeData[0]->designation}}</td>
                {{--@endforeach--}}
                {{--@foreach($leaveData as $b)--}}
                        <td>{{$b->leave_type}}</td>
                        <td>{{$b->total_days}}</td>
                        <td>{{$b->total_days-$paid_leave}}</td>
                    
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>



