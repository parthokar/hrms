<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Earn Leave Report</title>
    <style>
        #employeeDetails{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 50%;
            text-align: center;
            border:1px;
            font-size: 12px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            border: 1px solid #ddd;
            text-align: center !important;

        }
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: left;

        }

        #customers th {
            text-align: left;
            padding: 5px;
            background:#eee;

        }

        table td {
            padding: 2px;
            margin: 0;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
          font-size: 18px !important;
          
        }
    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h1 class="reportHeaderCompany">{{$companyInformation->company_name}}</h1>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader">{{$companyInformation->company_email}}</p>
        <p class="reportHeader">{{$companyInformation->company_phone}}</p>
    </div>
    <center>
            <div class="col-md-6">
                <h3 > <strong>Earn Leave Report</strong></h3>
                    
            </div>

    @if(!empty($employee_benefit)||!empty($employee_without_leave_benefit))
    <table id='customers' style="margin-top:10px;font-size:10px;" border="1px">
        <thead>
            <tr>
                <th>id</th>
                <th>Name</th>
               
                
                @if(!empty($request->coldesignation))
                <th>Designation</th>
                @endif
                <th>Total Earn Leave</th>
                <th>Enjoyed</th>
                <th>Available</th>
                <th>Previous Month Salary</th>
                <th>Per Day Salary</th>
                <th>Payable Earn Leave Benefit</th>
                @if(!empty($request->coldepartment))
                <th>Department</th>
                @endif
                @if(!empty($request->coljoiningdate))
                <th>Joining Date</th>
                @endif
                @if(!empty($request->colunit))
                <th>Unit</th>
                @endif
                @if(!empty($request->colfloor))
                <th>Floor</th>
                @endif
               
                @if(!empty($request->colsection))
                <th>Section</th>
                @endif
            
            </tr>
            </thead>
            <tbody>
            @foreach($employee_benefit as $employee)
            @php
            $previousMonthDay=date("t", mktime(0,0,0, date("n") - 1));
            $payPerDay=($employee->net_amount)/$previousMonthDay;
            $totalBenefit=($employee->leave_available)*$payPerDay;
           
            @endphp
            <tr>
                <td>{{$employee->employeeId}}</td>
                <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>
                @if(!empty($request->coldesignation))
                <td>{{$employee->designation}}</td>
                @endif
                <td>{{$employee->total_days}}</td>
                <td>{{$employee->leave_taken}}</td>
                <td>{{$employee->leave_available}}</td>
                <td>{{number_format(($employee->net_amount))}} BDT</td>
                <td>{{number_format(($payPerDay),2)}} BDT</td>
                <td>{{number_format(($totalBenefit),2)}} BDT</td>
               
                @if(!empty($request->coldepartment))
                <td>{{$employee->departmentName}}</td>
                @endif
                @if(!empty($request->coljoiningdate))
                <td>{{date("d-M-Y",strtotime($employee->empJoiningDate))}}</td>
                @endif
            
                @if(!empty($request->colunit))
                    <td>{{$employee->unitName}}</td>
                @endif
                @if(!empty($request->colfloor))
                    <td>{{$employee->floorName}}</td>
                @endif
               
                @if(!empty($request->colsection))
                <td>{{$employee->empSection}}</td>
                @endif
               
            </tr>
            @endforeach
            @foreach($employee_without_leave_benefit as $leave)
                        @php
                        $previousMonthDay=date("t", mktime(0,0,0, date("n") - 1));
                        $payPerDay=($leave->net_amount)/$previousMonthDay;
                        @endphp
                        <tr>
                           
                            <td>{{$leave->employeeId}}</td>
                            <td>{{$leave->empFirstName}} {{$leave->empLastName}}</td>
                            @if(!empty($request->coldesignation))
                            <td>{{$leave->designation}}</td>
                            @endif
                            @foreach($earnLeave as $earn)
                            @php 
                            $totalBenefit=($earn->total_days)*$payPerDay;
                            @endphp
                            <td>{{$earn->total_days}}</td>
                            <td>0</td>
                            <td>{{$earn->total_days}}</td>
                            <td>{{number_format(($leave->net_amount))}} BDT</td>
                            <td>{{number_format(($payPerDay),2)}} BDT</td>
                            <td>{{number_format(($totalBenefit),2)}} BDT</td>
                        
                            @endforeach
                            @if(!empty($request->coldepartment))
                            <td>{{$leave->departmentName}}</td>
                            @endif
                            @if(!empty($request->coljoiningdate))
                            <td>{{date("d-M-Y",strtotime($leave->empJoiningDate))}}</td>
                            @endif
                        
                            @if(!empty($request->colunit))
                                <td>{{$leave->unitName}}</td>
                            @endif
                            @if(!empty($request->colfloor))
                                <td>{{$leave->floorName}}</td>
                            @endif
                           
                            @if(!empty($request->colsection))
                            <td>{{$leave->empSection}}</td>
                            @endif
                        </tr>
                           
                    @endforeach
          </tbody>
    </table>
    @else
        <hr>
        <h4 style="color:red;"><center> No Matched data found.</center></h4>
    @endif

</div>

</body>
</html>




