<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Daily Attendance Exception</title>
    <style>
        #employeeDetails{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 50%;
            text-align: center;
            border:1px;
            font-size: 12px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            border: 1px solid #ddd;
            text-align: center !important;

        }
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: left;

        }

        #customers th {
            text-align: left;
            padding: 5px;
            background:#eee;

        }

        table td {
            padding: 2px;
            margin: 0;
        }
        table td p{
            margin: 0px;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
            font-size: 18px !important;

        }
    </style>
</head>
<body>

<div>
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader">{{$companyInformation->company_email}}</p>
        <p class="reportHeader">{{$companyInformation->company_phone}}</p>
        <p class="reportDateRange">Attendance exception report for <b>{{\Carbon\Carbon::parse($request->date)->format('d M Y (l)')}} </b></p>

    </div>
        @if(!empty($present))
            <table id='customers' style="margin-top:10px;font-size:10px;" border="1px">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>ID</th>
                    <th>Name</th>
                    @if(!empty($request->coldesignation))
                        <th>Designation</th>
                    @endif
                    @if(!empty($request->coldepartment))
                        <th>Department</th>
                    @endif
                    @if(!empty($request->coljoiningdate))
                        <th>Joining Date</th>
                    @endif
                    @if(!empty($request->colgender))
                        <th>Gender</th>
                    @endif
                    @if(!empty($request->colunit))
                        <th>Unit</th>
                    @endif
                    @if(!empty($request->colfloor))
                        <th>Floor</th>
                    @endif
                    @if(!empty($request->colline))
                        <th>Line</th>
                    @endif
                    @if(!empty($request->colsection))
                        <th>Section</th>
                    @endif
                    @if(!empty($request->colstatus))
                        <th>Status</th>
                    @endif
                    <th>In Time</th>
                    @if(!empty($request->collateTime))
                        <th>Late Time</th>
                    @endif
                    <th>Out Time</th>
                </tr>
                </thead>
                <tbody>
                @php $i=0 @endphp
                @foreach($present as $employee)
                    <tr>
                        <td>{{sprintf('%02d', ++$i)}}</td>
                        <td>{{$employee->employeeId}}</td>
                        <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>
                        @if(!empty($request->coldesignation))
                            <td>{{$employee->designation}}</td>
                        @endif
                        @if(!empty($request->coldepartment))
                            <td>{{$employee->departmentName}}</td>
                        @endif
                        @if(!empty($request->coljoiningdate))
                            <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('Y-m-d')}}</td>
                        @endif
                        @if(!empty($request->colgender))
                            <td>
                                @if($employee->empGenderId==1)
                                    Male
                                @endif
                                @if($employee->empGenderId==2)
                                    Female
                                @endif
                                @if($employee->empGenderId==3)
                                    Other
                                @endif
                            </td>
                        @endif
                        @if(!empty($request->colunit))
                            <td>{{$employee->unitName}}</td>
                        @endif
                        @if(!empty($request->colfloor))
                            <td>{{$employee->floorName}}</td>
                        @endif
                        @if(!empty($request->colline))
                            <td>{{$employee->LineName}}</td>
                        @endif
                        @if(!empty($request->colsection))
                            <td>{{$employee->empSection}}</td>
                        @endif
                        @if(!empty($request->colstatus))
                            <td>
                                @if($employee->empAccStatus==1)
                                    Active
                                @elseif($employee->empAccStatus==0)
                                    Inactive
                                @endif
                            </td>
                        @endif
                        <td>
                            {{$employee->in_time}}
                        </td>
                        @if(!empty($request->collateTime))
                            <td>
                                <span>{{ date('H:i:s', strtotime($employee->in_time) - strtotime($lateTime)) }}</span>
                            </td>
                        @endif
                        <td>
                            <span style="color: #FF0000">Not Given</span>
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>

            <div style="float:left;padding-top: 15px;font-size: 12px;">

                <span><b>Total Employee :</b> {{ ($countPresent) }}</span><br>
                {{--<span><b>Total Absent :</b> {{ $countAbsent }}</span><br>--}}
                {{--<span><b>Total Late Employee :</b> {{ $countPresent }}</span><br>--}}
            </div>

        @else
            <hr>
            <h4 style="color:red;"><center> No record found.</center></h4>
    @endif

</div>

</body>
</html>




