<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Employees on leave Report</title>
    <style>
        #employeeDetails{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 50%;
            text-align: center;
            border:1px;
            font-size: 12px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            border: 1px solid #ddd;
            text-align: center !important;

        }
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: left;

        }

        #customers th {
            text-align: left;
            padding: 5px;
            background:#eee;
            font-size: 9px;

        }

        table td {
            padding: 2px;
            margin: 0;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
          font-size: 18px !important;
          
        }
    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h1 class="reportHeaderCompany">{{$companyInformation->company_name}}</h1>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader">{{$companyInformation->company_email}}</p>
        <p class="reportHeader">{{$companyInformation->company_phone}}</p>
    </div>
    <center>
            <div class="col-md-6">
                <h4><strong>Employees on Leave Report</strong></h4> 
                <h5><strong>Date: {{date("d M Y")}}</strong></h5> 

            </div>

    @if(count($onLeaveEmployee)!=0)
    <table id='customers' style="margin-top:10px;font-size:10px;" border="1px">
            <thead>
                    <tr>
                        <th>Order</th>
                        <th>Employee</th>
                        <th>Employee ID</th>
                        <th>Designation</th>
                        <th>Leave Type</th>
                        <th>Leave Duration</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $order=0; @endphp
                    @foreach($onLeaveEmployee as $item)
                    @php 
                    
                    $leaveStart=strtotime($item->start_date);
                    $leaveEnd=strtotime($item->end_date);
                    $order++; 
                    @endphp
                    <tr>
                    <td>{{$order}}</td>
                    <td>{{$item->empFirstName}} {{$item->empLastName}}</td>
                    <td>{{$item->employeeId}}</td>
                    <td>{{$item->designation}}</td>
                    <td>{{$item->leave_type}}</td>
                    <td><strong>{{date("d F", $leaveStart)}}</strong>  to  <strong>{{date("d F Y", $leaveEnd)}}</strong></td>

                    </tr>
                @endforeach
                    </tbody>
                </table>
    @else
        <hr>
        <h4 style="color:red;"><center> No Matched data found.</center></h4>
    @endif

</div>

</body>
</html>




