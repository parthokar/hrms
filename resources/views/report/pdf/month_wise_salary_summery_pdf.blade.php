<!DOCTYPE html>
<html>
<head>
    <title>Month wise salarsummery</title>
    <style>
        .table>thead>tr>td.active, .table>tbody>tr>td.active, .table>tfoot>tr>td.active, .table>thead>tr>th.active, .table>tbody>tr>th.active, .table>tfoot>tr>th.active, .table>thead>tr.active>td, .table>tbody>tr.active>td, .table>tfoot>tr.active>td, .table>thead>tr.active>th, .table>tbody>tr.active>th, .table>tfoot>tr.active>th {
            background-color: #f5f5f5;
            font-size: 12px;
            text-align: center;
        }
        .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
            padding: 1px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }
        .paysheet{
            text-align: center;
        }
    </style>
</head>
<body>
<h4 style="text-align: center;font-weight: bold;">{{$companyInformation->company_name}}</h4>
<div class="paysheet">Salary summery for the month of</div>
<p style="text-align: center">
    @foreach($paymonth as $monthname)
        {{date('F Y',strtotime($monthname->month))}}<br>
        Days in Month: {{date('t',strtotime($monthname->month))}}
    @endforeach
</p>
<p style="text-align: center">
    Payment Date:
    <?php
    if(isset($_POST['salary_pay_date'])){
        echo $_POST['salary_pay_date'];
    }
    ?>
</p>
<table class="table" style="border-collapse: collapse;width:100%;" border="1px">
    <thead>
    <tr>
        <th style="font-size: 8px;">SN</th>
        <th style="font-size: 8px;">Department</th>
        <th style="font-size: 8px;">EmpID</th>
        <th style="font-size: 8px;">Name</th>
        <th style="font-size: 8px;">Designation</th>
        <th style="font-size: 8px;">Grade</th>
        <th style="font-size: 8px;">Basic</th>
        <th style="font-size: 8px;">House</th>
        <th style="font-size: 8px;">Medical</th>
        <th style="font-size: 8px;">Food</th>
        <th style="font-size: 8px;">Transport</th>
        <th style="font-size: 8px;">Gross</th>
        <th style="font-size: 8px;">Leave</th>
        <th style="font-size: 8px;">WorkDays</th>
        <th style="font-size: 8px;">Present</th>
        <th style="font-size: 8px;">Abs days</th>
        <th style="font-size: 8px;">Weekend</th>
        <th style="font-size: 8px;">Holiday</th>
        <th style="font-size: 8px;">Deduction</th>
        <th style="font-size: 8px;">Gross Pay</th>
        <th colspan="3"  style="text-align: center !important;font-size: 9px;">
            <span>Overtime</span>
        </th>
        <th style="font-size: 8px;">Att Bonus</th>
        <th style="font-size: 8px;">Special allow</th>
        <th style="font-size: 8px;">Net Wages</th>
        <th style="font-size: 8px;">Signature</th>
    </tr>
    </thead>
    <tbody>
    @php
        $order=0;
    @endphp
    @foreach($data as $salary_report)
        @php
            $order++;
        @endphp
        <tr class="active">
            <td style="padding: 1px;">{{$order}}</td>
            <td>{{$salary_report->departmentName}}</td>
            <td>{{$salary_report->employeeId}}</td>
            <td>
                {{$salary_report->empFirstName}} {{$salary_report->empLastName}}<br>
                Join: {{$salary_report->empJoiningDate}}
            </td>
            <td>{{$salary_report->designation}}</td>
            <td>{{$salary_report->grade_name}}</td>
            <td>{{$salary_report->basic_salary}}</td>
            <td>{{$salary_report->house_rant}}</td>
            <td>{{$salary_report->medical}}</td>
            <td>{{$salary_report->food}}</td>
            <td>{{$salary_report->transport}}</td>
            <td>{{$salary_report->gross}}</td>
            <td>
                @if($salary_report->leave=='')
                    0
                @else
                    {{$salary_report->leave}}
                @endif
            </td>
            <td>{{$salary_report->working_day}}</td>
            <td>
                @if($salary_report->present=='')
                    0
                @else
                    {{$salary_report->present}}
                @endif
            </td>
            <td>{{$salary_report->absent}}</td>
            <td>{{$salary_report->weekend}}</td>
            <td>{{$salary_report->holiday}}</td>
            <td>
                <span style="border-bottom: 1px solid #000000;">Absent:</span>
                {{$salary_report->absent_deduction_amount}}
            </td>
            <td>{{$salary_report->gross_pay}}</td>
            <td>
                <span style="border-bottom: 1px solid #000000;">Hrs:</span>
                @if($salary_report->overtime=='')
                    0/hr
                @else
                    {{$salary_report->overtime}}/hr
                @endif
            </td>
            <td>
                <span style="border-bottom: 1px solid #000000;"> Rat:</span>
                {{$salary_report->overtime_rate}}
            </td>
            <td>
                <span style="border-bottom: 1px solid #000000;">Amt:</span>
                {{$salary_report->overtime_amount}}
            </td>
            <td>
                @if($salary_report->attendance_bonus=='')
                    0
                @else
                    {{$salary_report->attendance_bonus}}
                @endif
            </td>
            <td>
                @if($salary_report->one_year_bonus=='')
                    0
                @else
                    {{$salary_report->one_year_bonus}}
                @endif
            </td>
            <td>{{$salary_report->net_amount}}</td>
            <td></td>
        </tr>
    @endforeach
    </tbody>
</table>

<div style="font-size: 12px;">
    <div class="department-employee" style="border: 1px solid #111;">
        Total Employee:
        @foreach ($employees_count_department_wise as $total_employee)
            {{ $total_employee->departmentName }}
            :
            {{ $total_employee->count }}
        @endforeach
    </div>
    <div class="department-basic" style="border: 1px solid #111;">
        Basic:
        @foreach($departmentwise_total as $total_amount)
            {{$total_amount->departmentName}} : {{$total_amount->total_basic}}
        @endforeach
    </div>
    <div class="department-house" style="border: 1px solid #111;">
        House:
        @foreach($departmentwise_total as $total_amount)
            {{$total_amount->departmentName}} : {{$total_amount->total_house}}
        @endforeach
    </div>

    <div class="department-medical" style="border: 1px solid #111;">
        Medical:
        @foreach($departmentwise_total as $total_amount)
            {{$total_amount->departmentName}} : {{$total_amount->total_medical}}
        @endforeach
    </div>

    <div class="department-food" style="border: 1px solid #111;">
        Food:
        @foreach($departmentwise_total as $total_amount)
            {{$total_amount->departmentName}} : {{$total_amount->total_food}}
        @endforeach
    </div>

    <div class="department-transport" style="border: 1px solid #111;">
        Transport:
        @foreach($departmentwise_total as $total_amount)
            {{$total_amount->departmentName}} : {{$total_amount->total_trasport}}
        @endforeach
    </div>

    <div class="department-gross" style="border: 1px solid #111;">
        Gross:
        @foreach($departmentwise_total as $total_amount)
            {{$total_amount->departmentName}} : {{$total_amount->total_gross}}
        @endforeach
    </div>

    <div class="department-gross-pay" style="border: 1px solid #111;">
        Gross Pay:
        @foreach($departmentwise_total as $total_amount)
            {{$total_amount->departmentName}} : {{$total_amount->total_gross_pay}}
        @endforeach
    </div>
    <div class="department-overtime" style="border: 1px solid #111;">
        Overtime:
        @foreach($departmentwise_total as $total_amount)
            {{$total_amount->departmentName}} : {{$total_amount->total_overtime_hour}}/hr
        @endforeach
    </div>

    <div class="department-overtime-amount" style="border: 1px solid #111;">
        Overtime Amount:
        @foreach($departmentwise_total as $total_amount)
            {{$total_amount->departmentName}} : {{$total_amount->total_overtime_amount}}
        @endforeach
    </div>

    <div class="department-attendance-bonus" style="border: 1px solid #111;">
        Attendance Bonus:
        @foreach($departmentwise_total as $total_amount)
            {{$total_amount->departmentName}}{{$total_amount->total_attendance_bous}}
        @endforeach
    </div>

    <div class="department-special-bonus" style="border: 1px solid #111;">
        Special Bonus:
        @foreach($departmentwise_total as $total_amount)
            {{$total_amount->departmentName}} : {{$total_amount->total_one_Year_bonus}}
        @endforeach
    </div>

    <div class="department-special-bonus" style="border: 1px solid #111;">
        Net Wages:
        @foreach($departmentwise_total as $total_amount)
            {{$total_amount->departmentName}} : {{$total_amount->total_net_amount}}
        @endforeach
    </div>
</div>
<br>
<span style="text-align:left;margin-top: 20px;border-top:1px solid #000000;">Prepared By</span>
<span style="text-align:left;margin-top: 20px;margin-left: 20px;border-top:1px solid #000000;">Audited By</span>
<span style="text-align:left;margin-top: 20px;margin-left: 20px;border-top:1px solid #000000;">Recommended By</span>
<span style="text-align:left;margin-top: 20px;margin-left: 20px;border-top:1px solid #000000;">Approved By</span>
</body>
</html>