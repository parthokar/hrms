<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
                        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
                    'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>আবেদনকারীদের তালিকা</title>
    <style>
        @page { sheet-size: A4; }
        body{
            font-family: 'bangla', sans-serif;
            font-size: 12px;

        }
        p{  
            line-height: 1px;
        }

        #employeeDetails{
            font-family: "kalpurush", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 90%;
            font-size: 11px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            /*border: 1px solid #ddd;*/
            font-size: 12px;

        }
        #customers {
           
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: center !important;

        }

        #customers th {
            font-size: 15px;
            text-align: center;
            padding: 5px;

        }
        #customers td {
            font-size: 13px;

        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
          font-size: 18px !important;
          
        }

        #cardFooter{
            border-collapse: collapse;
            font-size:11px;
            width:70%;
            margin:0px auto;
            margin-top:15px;
            /*float:right; */

        }
        .reportDateRange{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif  !important;
            font-size: 12px !important; 
            font-weight: bold;
        }
        #cardFooter td {
            text-align: left !important;
        }

    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader">{{$companyInformation->company_email}}</p>
        <p class="reportHeader">{{$companyInformation->company_phone}}</p>
    </div>
    
    <div style="text-align: center">
        <h2><strong>আবেদনকারীদের তালিকা</strong></h2> 
        </div>


        @if(!empty($applicantListShow))
    <table id='customers' style="margin-top:10px;font-size:10px;" border="1px">
        <thead>
            <tr>
                <th>ক্রমিক নং</th>
                <th>নাম</th>
                <th>যোগাযোগ</th>
                <th>পদের নাম</th>
                @if(!empty($request->vacStatusCheck))
                <th>পদের বিবৃতি</th>
                @endif
                @if(!empty($request->applicationDateCheck))
                <th>আবেদনের তারিখ</th>
                @endif
                @if(!empty($request->priorityCheck))
                <th>অগ্রাধিকার</th>
                @endif
                @if(!empty($request->eligibilityCheck))
                <th>ইন্টারভিউ এর জন্য</th>
                @endif
                @if(!empty($request->interviewDateCheck))
                <th>ইন্টারভিউ এর তারিখ</th>
                @endif
                @if(!empty($request->jobStatusCheck))
                <th>চাকুরী বিবৃতি</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @php $i=0 @endphp
            @foreach($applicantListShow as $employee)
            <tr>
                <td>{{en2bnNumber(sprintf('%02d', ++$i))}}</td>
                <td>{{$employee->name}}</td>
                <td>{{$employee->phone}}</td>
                <td>{{$employee->vacTitle}}</td>
                @if(!empty($request->vacStatusCheck))
                <td>
                    @if($employee->vacStatus==0)
                        সক্রিয় নয়
                    @endif
                    @if($employee->vacStatus==1)
                        সক্রিয়
                    @endif
                </td>
                @endif
                @if(!empty($request->applicationDateCheck))
                <td>{{en2bnNumber(date("d M Y",strtotime($employee->submittedDate)))}}</td>
                @endif
                @if(!empty($request->priorityCheck))
                <td>
                    @if($employee->priorityStatus=="Low")
                        নিম্ন
                    @elseif($employee->priorityStatus=="High")
                        উচ্চ
                    @elseif($employee->priorityStatus=="Regular")
                        সাধারণ
                    @elseif($employee->priorityStatus==null)
                        সাধারণ
                    @endif
                </td>
                @endif
                @if(!empty($request->eligibilityCheck))
                <td>

                    @if($employee->interviewStatus=="Accepted")
                        নির্বাচিত হয়েছেন
                    @elseif($employee->interviewStatus=="Rejected")
                        নির্বাচিত হননি
                    @elseif($employee->interviewStatus==null)
                        অপেক্ষাধীন রয়েছে
                    @endif
                
                </td>
                @endif
                
                @if(!empty($request->interviewDateCheck))
                    <td>
                        @if($employee->interviewDate)
                        {{en2bnNumber(date("d M Y",strtotime($employee->interviewDate)))}}
                        @elseif($employee->interviewDate==null)
                            তথ্য নেই
                        @endif  
                        
                    </td>
                @endif
                @if(!empty($request->jobStatusCheck))
                    <td>
                        @if($employee->jobStatus=="Confirm")
                            নিশ্চিত হয়েছে
                        @elseif($employee->jobStatus=="Waiting")
                            অপেক্ষাধীন রয়েছে
                        @elseif($employee->jobStatus=="Reject")
                            খারিজ হয়েছে
                        @elseif($employee->jobStatus==null)
                            অপেক্ষাধীন রয়েছে
                        @endif
                    
                    </td>
                @endif
                
            </tr>
            @endforeach
          </tbody> 
    </table>
    @else
        <hr>
        <h4 style="color:red;"><center>কোন তথ্য পাওয়া যায়নি</center></h4>
    @endif
    


</div>


</body>
</html>




