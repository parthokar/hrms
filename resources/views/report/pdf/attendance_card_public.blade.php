<?php

        function roundTime($time){
            $hour=date('G',strtotime($time));
            $minute=date('i',strtotime($time));

            if($minute<25){
                $minute=0;
            }
            elseif ($minute>=25 &&$minute<55){
                $minute=.5;
            }
            else{
                $minute=0;
                $hour++;
            }
            $kind=number_format((float)($hour+$minute),1,'.','');
            return $kind;

        }

        function late_time($pretime,$time){
            $phour=date('G',strtotime($pretime));
            $pminute=date('i',strtotime($pretime));
            $hour=date('G',strtotime($time));
            $minute=date('i',strtotime($time));
            $thour=$phour+$hour;
            $tminute=$pminute+$minute;
            $h=floor($tminute/60);
            $tminute=sprintf("%02d",$tminute%60);
            $thour+=$h;
            $thour=sprintf("%02d",$thour);
            return "$thour:$tminute";

        }

        function end_time($time){
            $hour=date('G',strtotime($time));
            $minute=date('i',strtotime($time));
            $minute=sprintf("%02d",$minute+rand(0,20));
            return "$hour:$minute";

        }

$absent=0;
$working_days=0;
$present=0;
$late=0;
$onleave=0;
$startTime=\App\Http\Controllers\OvertimeController::overtime_start();
$endTime=\App\Http\Controllers\OvertimeController::overtime_end();
$ot=0;
$late_time="00:00";

$attendance_settings=\Illuminate\Support\Facades\DB::table('attendance_setup')->leftJoin('employees','attendance_setup.id','=','employees.empShiftId')->where('employees.id','=',$emp_id)->first();


?>
<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Attendance Card</title>
    <style>
        #employeeDetails{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 90%;
            font-size: 11px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            /*border: 1px solid #ddd;*/
            font-size: 12px;

        }
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: center !important;

        }

        #customers th {
            text-align: left;
            padding: 5px;

        }

        table td {
            padding: 2px;
            margin: 0;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
            font-size: 18px !important;

        }

        #cardFooter{
            border-collapse: collapse;
            font-size:11px;
            width:70%;
            margin:0px auto;
            margin-top:15px;
            /*float:right; */

        }
        .reportDateRange{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif  !important;
            font-size: 12px !important;
            font-weight: bold;
        }
        #cardFooter td {
            text-align: left !important;
        }

    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportDateRange">Job Card Report Month From <b>{{\Carbon\Carbon::parse($start_date)->format('d-M-Y')}}</b> to <b>{{\Carbon\Carbon::parse($end_date)->format('d-M-Y')}} </b></p>
    </div>
    @if(isset($datas[0][0]))
    <table id="employeeDetails">
        <tr>
            <td style="text-align:left" width="35%"><b>Employee ID :</b> {{$datas[0][0]->employeeId}}</td>
            <td style="text-align:left" width="35%"><b>Name : </b> {{$datas[0][0]->empFirstName." ".$datas[0][0]->empLastName }}</td>
            <td style="text-align:left" width="30%"><b>Joining Date :</b> {{\Carbon\Carbon::parse($datas[0][0]->empJoiningDate)->format('d-M-Y')}}</td>
        </tr>
        <tr>
            <td style="text-align:left" width="35%"><b>Designation :</b> {{$datas[0][0]->designation}}</td>
            <td style="text-align:left" width="35%"><b>Department : </b> {{$datas[0][0]->departmentName}}</td>
            <td style="text-align:left" width="30%"><b> Section : </b> {{$datas[0][0]->empSection}}</td>
        </tr>

    </table>

    <table id='customers' style="margin-top:15px;font-size:11px;" border="1px">
        <thead>
        <tr>
            <th>Date</th>
            <th>Day</th>
            <th>In Time </th>
            <th>Status</th>
            <th>Late Time</th>
            <th>Out Time</th>
            <th>OT (Hour)</th>
        </tr>
        </thead>
        <tbody>
        @foreach($datas as $d)
            <?php
            $working_days++;
            $leave_check= explode('@',$d[2]);
            ?>
            <tr>
                <td>{{\Carbon\Carbon::parse($d[1])->format('d-M-Y')}}</td>
                <td>{{\Carbon\Carbon::parse($d[1])->format('l')}}</td>
                <td>
                    @if(isset($d[0]->in_time))
                        <?php
                        $present++;
                        ?>

                        {{ date('G:i', strtotime($d[0]->in_time)) }}

                    @endif
                </td>
                <td>
                    @if(isset($d[0]->in_time))
                        @if($d[0]->in_time>$attendance_settings->max_entry_time)
                            <?php $late++; ?>
                            <span>Late</span>
                        @else
                            <span>Present</span>
                        @endif
                    @else
                        @if($d[2]=='weekend' || $d[2]=='Festival Holiday' || $leave_check[0]=='On Leave')
                            @if($leave_check[0]!='On Leave')
                                <?php $working_days--; ?>
                            @else
                                <?php $onleave++; ?>
                            @endif
                            @if($leave_check[0]=='On Leave')
                                @if(isset($leave_check[1]))
                                        <span>{{$leave_check[1]}}</span>

                                @endif
                            @else
                                <span>{{$d[2]}}</span>

                            @endif


                        @else
                            <span style="color:#FF0000">Absent</span>

                        @endif
                         @endif
                </td>
                <td>
                    @if(isset($d[0]->in_time))
                        @if($d[0]->in_time>$attendance_settings->max_entry_time)
                            <?php
                                $late_time=late_time($late_time,date('G:i', strtotime($d[0]->in_time) - strtotime($attendance_settings->max_entry_time)));

                            ?>

                            {{ date('G:i', strtotime($d[0]->in_time) - strtotime($attendance_settings->max_entry_time)) }}
                        @else
                            00:00
                        @endif

                    @endif
                </td>
                <td>
                    @if(isset($d[0]->out_time))
                        @if($d[0]->in_time==$d[0]->out_time)
                            <span style="color: #FF0000;">Not Given</span>
                        @elseif($d[0]->out_time>$endTime)
                            {{end_time($endTime)}}

                        @else

                            {{ date('G:i', strtotime($d[0]->out_time)) }}

                        @endif

                    @endif
                </td>
                <td>
                    @if(isset($d[0]->out_time) && $d[0]->in_time!=$d[0]->out_time)
                        @if($d[0]->out_time>$startTime && $d[0]->out_time<=$endTime)
                        <?php
                            $ot+=roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time)))
                        ?>
                            {{ roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time))) }}

                        @elseif($d[0]->out_time>$endTime)
                        <?php
                            $ot+=roundTime(date('G:i', strtotime($endTime) - strtotime($attendance_settings->exit_time)))
                        ?>
                            {{ roundTime(date('G:i', strtotime($endTime) - strtotime($attendance_settings->exit_time))) }}

                        @else
                            0.0
                        @endif

                    @endif
                </td>
            </tr>
        @endforeach
        <tr style="font-weight: bold">
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>{{$late_time}}</td>
            <td></td>
            <td>{{$ot}} hour</td>
        </tr>
        </tbody>
    </table>

</div>


<div>
    <center>
        <table id="cardFooter">
            <tr>
                <td width="50%"><b>Total Working Days: </b>{{ $working_days }} Days</td>
            </tr>
            <tr>
                <td  width="50%"><b>Total Present Days: </b> {{ $present }} Days</td>
            </tr>
            <tr>
                <td class="padTop10"><b>Total Leave :</b> {{ ($onleave) }} Days</td>
            </tr>
            <tr>
                <td class="padTop10"><b>Total Absent :</b> {{ ($working_days-($present+$onleave)) }} Days</td>
            </tr>

        </table>
    </center>
</div>

@else
    <center><h4 style="color: #FF0000">No data found for the given date</h4></center>

@endif

</body>
</html>




