<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Employee List</title>
    <style>
        #employeeDetails{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 50%;
            text-align: center;
            border:1px;
            font-size: 12px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            border: 1px solid #ddd;
            text-align: center !important;

        }
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: left;

        }

        #customers th {
            text-align: left;
            padding: 5px;
            background:#eee;

        }

        table td {
            padding: 2px;
            margin: 0;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
          font-size: 18px !important;
          
        }
    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader">{{$companyInformation->company_email}}</p>
        <p class="reportHeader">{{$companyInformation->company_phone}}</p>
    </div>
    <center>

    @if(!empty($training)||!empty($training2))
        <h3>Employee Training History</h3>
    <table id='customers' style="margin-top:10px;font-size:10px;" border="1px">
        <thead>
            <thead>
                <tr>
                    <th>Order</th>
                    <th>Employee ID</th>
                    <th>Name</th>
                    <th>Designation</th>
                    
                    <th>Completed Trainings</th>
                    
                </tr>
                </thead>
                <tbody>
                
                @php $order=0; @endphp
                @foreach($training as $data)
        
                @php $order++; @endphp
                <tr>
                <td>{{$order}}</td>
                <td>{{$data->employeeId}}</td>
                <td>{{$data->empFirstName}} {{$data->empLastName}}</td>
                <td>{{$data->designation}}</td>
                
                <td>{{$data->training_name}}</td>
                </tr>
            @endforeach
            @foreach($training2 as $data)
        
                @php $order++; @endphp
                <tr>
                <td>{{$order}}</td>
                <td>{{$data->employeeId}}</td>
                <td>{{$data->empFirstName}} {{$data->empLastName}}</td>
                <td>{{$data->designation}}</td>
                
                <td>No Training Taken</td>

                </tr>

                </tbody>
                @endforeach
            </table>
    @else
        <hr>
        <h4 style="color:red;"><center> No Matched data found.</center></h4>
    @endif

</div>

</body>
</html>




