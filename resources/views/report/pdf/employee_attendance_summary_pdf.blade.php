<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Employee Attendance Summary</title>
    <style>
        #employeeDetails{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 50%;
            text-align: center;
            border:1px;
            font-size: 12px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            border: 1px solid #ddd;
            text-align: center !important;

        }
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: left;

        }

        #customers th {
            text-align: left;
            padding: 5px;
            background:#eee;

        }

        table td {
            padding: 2px;
            margin: 0;
        }
        table td p{
            margin: 0px;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
            font-size: 18px !important;

        }
    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader">{{$companyInformation->company_email}}</p>
        <p class="reportHeader">{{$companyInformation->company_phone}}</p>
        <p class="reportDateRange">Employee Attendance Summary from <b>{{\Carbon\Carbon::parse($request->start_date)->format('d M Y')}} </b> to <b>{{\Carbon\Carbon::parse($request->end_date)->format('d M Y')}} </b></p>

    </div>



    <center>

        @if(!empty($employees))
            <table id='customers' style="margin-top:10px;font-size:10px;" border="1px">
                <thead>
                <tr>
                    <th align="center">Serial No.</th>
                    <th>Employee Id</th>
                    <th>Name</th>
                    <th>Department</th>
                    <th>Designation</th>
                    <th>Present</th>
                    {{--                                    <th>#</th>--}}
                    <th>Absent</th>
                    <th>Leave</th>
                    <th>Late</th>

                </tr>
                </thead>
                <tbody>
                @foreach($employees as  $key=>$employee)
                    <?php
                    $percent=0;
                    if($request->accStatus==1){
                        if($employee->empJoiningDate>\Carbon\Carbon::parse($request->start_date)){
                            $days = \Carbon\Carbon::parse($employee->empJoiningDate)->diffInDays(\Carbon\Carbon::parse($request->end_date))+1;
                            $w=App\Http\Controllers\ReportController::weekendCalculator($employee->empJoiningDate,$request->end_date)-App\Http\Controllers\ReportController::regular_day_count($employee->empJoiningDate,$request->end_date);
                            $h=App\Http\Controllers\ReportController::holiday($employee->empJoiningDate,$request->end_date);
                        }
                        else{
                            $days=$total_days;
                            $w=$weekend;
                            $h=$holidays;

                        }
                    }
                    else{
                        if($employee->date_of_discontinuation<\Carbon\Carbon::parse($request->end_date)){
                            $days = \Carbon\Carbon::parse($request->start_date)->diffInDays(\Carbon\Carbon::parse($employee->date_of_discontinuation))+1;
                            $w=App\Http\Controllers\ReportController::weekendCalculator($request->start_date,$employee->date_of_discontinuation)-App\Http\Controllers\ReportController::regular_day_count($request->start_date,$employee->date_of_discontinuation);
                            $h=App\Http\Controllers\ReportController::holiday($request->start_date,$employee->date_of_discontinuation);
                        }
                        else{
                            $days=$total_days;
                            $w=$weekend;
                            $h=$holidays;

                        }

                    }
                    ?>
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$employee->employeeId}}</td>
                        <td>{{$employee->empFirstName ." ".$employee->empLastName}}</td>
                        <td>{{$employee->departmentName}}</td>
                        <td>{{$employee->designation}}</td>
                        <td>
                            <?php
                            $percent=number_format((($employee->total_present)/$days)*100,2)
                            ?>

                            {{$employee->total_present." ($percent%) "}}</td>
                        <?php
                        $x=$employee->total_present-($employee->weekend_present-$employee->regular_present);
                        $y=$w;
                        $absent=max(($days-($x+$y+$h+$employee->total_leaves_taken)),0);
                        $percent=number_format((($absent)/$days)*100,2);
                        ?>
                        {{--                                    <td>{{max($total_days-(($employee->total_present-($employee->weekend_present-$employee->regular_present))+($weekend-$regular_days_setup_check)+$holidays+$employee->total_leaves_taken),0)}}</td>--}}
                        <td>{{$absent." ($percent%) "}}</td>

                        <td>{{$employee->total_leaves_taken}}</td>
                        <td>
                            <?php
                            if($employee->total_present!=0){
                                $percent=number_format((($employee->late)/$employee->total_present)*100,2);
                            }
                            else{
                                $percent=0;
                            }
                            ?>
                            {{$employee->late." ($percent%)"}}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>

        @else
            <hr>
            <h4 style="color:red;"><center> No Matched data found.</center></h4>
    @endif

</div>

</body>
</html>

<script type="text/javascript">

    window.print();
</script>




