<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
                        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
                    'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>অর্জিত ছুটির রিপোর্ট</title>
    <style>
        @page { sheet-size: A4-L; }
        body{
            font-family: 'bangla', sans-serif;
            font-size: 12px;

        }
        p{  
            line-height: 1px;
        }

        #employeeDetails{
            font-family: "kalpurush", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 90%;
            font-size: 11px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            /*border: 1px solid #ddd;*/
            font-size: 12px;

        }
        #customers {
           
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {        
            border: 1px solid #ddd;
            text-align: center !important;

        }

        #customers th {
            font-size: 14px;
            text-align: center;
            padding: 5px;

        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
          font-size: 18px !important;
          
        }

        #cardFooter{
            border-collapse: collapse;
            font-size:11px;
            width:70%;
            margin:0px auto;
            margin-top:15px;
            /*float:right; */

        }
        .reportDateRange{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif  !important;
            font-size: 12px !important; 
            font-weight: bold;
        }
        #cardFooter td {
            text-align: left !important;
        }

    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader">{{$companyInformation->company_email}}</p>
        <p class="reportHeader">{{$companyInformation->company_phone}}</p>
    </div>
    <div style="text-align: center">
        <h2><strong>অর্জিত ছুটির রিপোর্ট</strong></h2>  
    </div>
   
</div>

<div>
        @if(!empty($employee_benefit)||!empty($employee_without_leave_benefit))
        <table id='customers' style="margin-top:10px;font-size:10px;" border="1px">
            <thead>
                <tr>
                    <th>আইডি</th>
                    <th>নাম</th>
                   
                    
                    @if(!empty($request->coldesignation))
                    <th>পদবী</th>
                    @endif
                    <th>মোট অর্জিত ছুটি</th>
                    <th>উপভোগ করেছেন</th>
                    <th>বাকি আছে</th>
                    <th>বিগত মাসের বেতন</th>
                    <th>দৈনিক বেতন</th>
                    <th>প্রাপ্য টাকা</th>
                    @if(!empty($request->coldepartment))
                    <th>বিভাগ</th>
                    @endif
                    @if(!empty($request->coljoiningdate))
                    <th>যোগদানের তারিখ</th>
                    @endif
                    @if(!empty($request->colunit))
                    <th>ইউনিট</th>
                    @endif
                    @if(!empty($request->colfloor))
                    <th>ফ্লোর</th>
                    @endif
                   
                    @if(!empty($request->colsection))
                    <th>সেকশন</th>
                    @endif
                
                </tr>
                </thead>
                <tbody>
                @foreach($employee_benefit as $employee)
                @php
                $previousMonthDay=date("t", mktime(0,0,0, date("n") - 1));
                $payPerDay=($employee->net_amount)/$previousMonthDay;
                $totalBenefit=($employee->leave_available)*$payPerDay;
               
                @endphp
                <tr>
                    <td>{{$employee->employeeId}}</td>
                    <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>
                    @if(!empty($request->coldesignation))
                    <td>{{$employee->designation}}</td>
                    @endif
                    <td>{{en2bnNumber($employee->total_days)}} দিন</td>
                    <td>{{en2bnNumber($employee->leave_taken)}} দিন</td>
                    <td>{{en2bnNumber($employee->leave_available)}} দিন</td>
                    <td>{{en2bnNumber(number_format(($employee->net_amount)))}} টাকা</td>
                    <td>{{en2bnNumber(number_format(($payPerDay),2))}} টাকা</td>
                    <td>{{en2bnNumber(number_format(($totalBenefit),2))}} টাকা</td>
                   
                    @if(!empty($request->coldepartment))
                    <td>{{$employee->departmentName}}</td>
                    @endif
                    @if(!empty($request->coljoiningdate))
                    <td>{{en2bnNumber(date("d M Y",strtotime($employee->empJoiningDate)))}}</td>
                    @endif
                
                    @if(!empty($request->colunit))
                        <td>{{$employee->unitName}}</td>
                    @endif
                    @if(!empty($request->colfloor))
                        <td>{{$employee->floorName}}</td>
                    @endif
                   
                    @if(!empty($request->colsection))
                    <td>{{$employee->empSection}}</td>
                    @endif
                   
                </tr>
                @endforeach
                @foreach($employee_without_leave_benefit as $leave)
                            @php
                            $previousMonthDay=date("t", mktime(0,0,0, date("n") - 1));
                            $payPerDay=($leave->net_amount)/$previousMonthDay;
                            @endphp
                            <tr>
                               
                                <td>{{$leave->employeeId}}</td>
                                <td>{{$leave->empFirstName}} {{$leave->empLastName}}</td>
                                @if(!empty($request->coldesignation))
                                <td>{{$leave->designation}}</td>
                                @endif
                                @foreach($earnLeave as $earn)
                                @php 
                                $totalBenefit=($earn->total_days)*$payPerDay;
                                @endphp
                                <td>{{en2bnNumber($earn->total_days)}} দিন</td>
                                <td>০ দিন</td>
                                <td>{{en2bnNumber($earn->total_days)}} দিন</td>
                                <td>{{en2bnNumber(number_format(($leave->net_amount)))}} টাকা</td>
                                <td>{{en2bnNumber(number_format(($payPerDay),2))}} টাকা</td>
                                <td>{{en2bnNumber(number_format(($totalBenefit),2))}} টাকা</td>
                            
                                @endforeach
                                @if(!empty($request->coldepartment))
                                <td>{{$leave->departmentName}}</td>
                                @endif
                                @if(!empty($request->coljoiningdate))
                                <td>{{en2bnNumber(date("d M Y",strtotime($leave->empJoiningDate)))}}</td>
                                @endif
                            
                                @if(!empty($request->colunit))
                                    <td>{{$leave->unitName}}</td>
                                @endif
                                @if(!empty($request->colfloor))
                                    <td>{{$leave->floorName}}</td>
                                @endif
                               
                                @if(!empty($request->colsection))
                                <td>{{$leave->empSection}}</td>
                                @endif
                            </tr>
                               
                        @endforeach
              </tbody>
        </table>
        @else
            <hr>
            <h4 style="color:red;"><center>কোন তথ্য পাওয়া যায়নি</center></h4>
        @endif



</div>


</body>
</html>




