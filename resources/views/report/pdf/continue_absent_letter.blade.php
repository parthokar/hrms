<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>Lefty Letter</title>
    <style>
        body{
            font-family: 'bangla', sans-serif;
            font-size: 15px;

        }

        p{  
            line-height: 5px;
        }

        .mainBody p{  
            line-height: 22px;
        }

        #employeeDetails{
            font-family: "kalpurush", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 90%;
            font-size: 11px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            /*border: 1px solid #ddd;*/
            font-size: 12px;

        }
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: center !important;

        }

        #customers th {
            text-align: left;
            padding: 5px;

        }
        .company_name p{
            font-size: 24px !important;
        }
        table td {
            padding: 2px;
            margin: 0;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
          font-size: 18px !important;
          
        }

        #cardFooter{
            border-collapse: collapse;
            font-size:11px;
            width:70%;
            margin:0px auto;
            margin-top:15px;
            /*float:right; */

        }
        .reportDateRange{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif  !important;
            font-size: 12px !important; 
            font-weight: bold;
        }
        #cardFooter td {
            text-align: left !important;
        }

        .info tr>td{
            padding: 2px 2px;
        }

    </style>
</head>
<body>
    <table width="100%">
        <tr>
            <td style="text-align: center; font-size: 30px; font-weight: bold;">ফিন-বাংলা এ্যাপারেলস লিমিটেড</td>
        </tr> 
        <tr>
            <td style="text-align: center; margin-bottom: 25px;">৭২, ডেগেরচালা রোড, ছয়দানা, জাতীয় বিশ্ববিদ্যালয়, গাজীপুর।</td>
        </tr>
    </table>
    <br><br>
    <table width="100%" class="info">
        <tr>
            <td colspan="4"> 
                <?php 
                    if(($request->days)>=10&&($request->days)<20){ 
                 ?>
                Ref:
                <br>
                    (নমুনা চিঠি-১) বাংলাদেশ শ্রম আইন-২০০৬ এর ২৭ (৩ক) ধারা মতে অননুমোদিত অনুপস্থিত শ্রমিকের চূড়ান্ত নিষ্পত্তি প্রক্রিয়ায় ইস্যু করা ১ম পত্র। 
                <?php }elseif (($request->days)>=20&&($request->days)<28) { ?>
                Ref:
                <br>
                    (নমুনা চিঠি-২) বাংলাদেশ শ্রম আইন-২০০৬ এর ২৭ (৩ক) ধারা মতে অননুমোদিত অনুপস্থিত শ্রমিকের চূড়ান্ত নিষ্পত্তি প্রক্রিয়ায় ইস্যু করা ২য় পত্র। 
                <?php }elseif (($request->days)>=28) { ?>
                    Ref:
                <br>
                    (নমুনা চিঠি-৩) বাংলাদেশ শ্রম আইন-২০০৬ এর ২৭ (৩ক) ধারা মতে অননুমোদিত অনুপস্থিত শ্রমিকের চূড়ান্ত নিষ্পত্তি প্রক্রিয়ায় ইস্যু করা ৩য় পত্র। 
                <?php }else{

                    echo "Error Occured";
                } ?>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: right;">
               (রেজিস্টার ডাক প্রেরিত) 
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <br>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                কারখানার নাম:  <span style="font-size: 18px; font-weight: bold; ">{{$companyInformation->bComName}}</span>
            </td>
            <td colspan="2" style="text-align: right;">
                তারিখ: ...............................................
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <br>
            </td>
        </tr>
        <tr>
            <td>
                নাম:  <?php if(!empty($user->empBnFullName)){echo "<span style='font-size: 14px; font-weight: bold; '>".$user->empBnFullName."</span>";}else{ echo "<span style='font-size: 12px; font-weight: bold; '>".$user->empFirstName ." ".$user->empLastName."</span>"; } ?></span>
            </td>
            <td>
                পদবী:  <?php if(!empty($user->designationBangla)){echo  "<span style='font-size: 14px; font-weight: bold; '>".$user->designationBangla."</span>";}else{ echo "<span style='font-size: 12px; font-weight: bold; '>".$user->designation."</span>"; } ?>
            </td>

            <td>
                কার্ড নং: <?php if(!empty($user->employeeBnId)){echo  "<span style='font-size: 14px; font-weight: bold; '>".$user->employeeBnId."</span>";}else{ echo "<span style='font-size: 12px; font-weight: bold; '>".$user->employeeId."</span>";; } ?>
            </td>

            <td>
                সেকশন: <?php if(!empty($user->empSection)){echo "<span style='font-size: 12px; font-weight: bold; '>".$user->empSection."</span>";}else{ echo ""; } ?>
            </td>
        </tr>
        <tr>
            <td colspan="4"></td>
        </tr>
        <tr style="margin-top: 15px;">
            <td colspan="2">
                স্থায়ী ঠিকানা:  
                <br>
                নামঃ  <?php if(!empty($user->empBnFullName)){echo "<span style='font-size: 14px; font-weight: bold; '>".$user->empBnFullName."</span>";}else{ echo "<span style='font-size: 12px; font-weight: bold; '>".$user->empFirstName ." ".$user->empLastName."</span>"; } ?></span>
                <br>
                পিতার নামঃ  <?php if(!empty($user->empBnFatherName)){echo "<span style='font-size: 14px; font-weight: bold; '>".$user->empBnFatherName."</span>";}else{ echo "<span style='font-size: 12px; font-weight: bold; '>".$user->empFatherName."</span>"; ; } ?></span>
                <br>
                ঠিকানাঃ <?php if(!empty($user->empBnParAddress)){echo  "<span style='font-size: 14px; font-weight: bold; '>".$user->empBnParAddress."</span>";}else{ echo "<span style='font-size: 12px; font-weight: bold; '>".$user->empParAddress."</span>"; } ?>
            </td>
            <td colspan="2">
                 বর্তমান ঠিকানা:
                <br>
                নামঃ  <?php if(!empty($user->empBnFullName)){echo "<span style='font-size: 14px; font-weight: bold; '>".$user->empBnFullName."</span>";}else{ echo "<span style='font-size: 12px; font-weight: bold; '>".$user->empFirstName ." ".$user->empLastName."</span>"; } ?></span>
                <br>
                পিতার নামঃ  <?php if(!empty($user->empBnFatherName)){echo "<span style='font-size: 14px; font-weight: bold; '>".$user->empBnFatherName."</span>";}else{ echo "<span style='font-size: 12px; font-weight: bold; '>".$user->empFatherName."</span>"; ; } ?></span>
                <br>
                ঠিকানাঃ <?php if(!empty($user->empBnCurrentAddress)){echo  "<span style='font-size: 14px; font-weight: bold; '>".$user->empBnCurrentAddress."</span>";}else{ echo "<span style='font-size: 12px; font-weight: bold; '>".$user->empCurrentAddress."</span>"; } ?>
            </td>
        </tr>
    </table>

    <div class="head2">
        <div class="parmanentAdd">
            
        </div>
    </div>
    <div class="subject">
        <br>
        <p>বিষয়ঃ অননুমোদিত অনুপস্থিতির কারন ব্যাখ্যা সহ অনতিবিলম্বে কাজে যোগদান বা অনুপস্থিতির কারন ব্যাখ্যা প্রসঙ্গে ।</p>
    </div>
    <div class="mainBody">
            <p>জনাব/জনাবা, </p>

        <?php 
            if(($request->days)>=10&&($request->days)<18){ ?>

            <p>আপনি গত <?php echo en2bnNumber(date("d-m-Y", strtotime($request->lastPresent))); ?>  ইং তারিখ হতে অদ্যাবধি বিনা নোটিশে বা কর্তৃপক্ষের বিনা অনুমতিতে কারখানায় অনুপস্থিত রয়েছেন । আপনার এ ধরনের অনুপস্থিতির কারনে কারখানার উৎপাদন প্রক্রিয়া ব্যহত হচ্ছে বিধায় বাংলাদেশ শ্রম আইন, ২০০৬ এর ২৭(৩ক) ধারামতে অনতিবিলম্বে  আপনাকে অনুপস্থিতির কারন ব্যাখ্যা সহ কাজে যোগদান অথবা আপনার এ ধরনের বিনা নোটিশে বা উর্দ্ধতন কর্তৃপক্ষের বিনা অনুমতিতে ১০ দিনের অধিক সময় কারখানায় অনুপস্থিত থাকার বিষয়ে ব্যাখ্যা প্রদানের নির্দেশ প্রদান করা হল । 
            <br />
            আপনি অদ্য হতে  ১০ দিনের মধ্যে কাজে যোগদানে বা আপনার এ ধরনের অননুমোদিত অনুপস্থিতির বিষয়ে ব্যাখ্যা প্রদানে ব্যর্থ হলে কর্তৃপক্ষ আপনার বিরুদ্ধে পরবর্তী আইনানুগ ব্যবস্থা গ্রহন করবে ।

            </p>
            <?php }elseif (($request->days)>=18&&($request->days)<28) { ?>

            <p>আপনি গত <?php echo en2bnNumber(date("d-m-Y", strtotime($request->lastPresent))); ?> ইং তারিখ হতে অদ্যাবধি কারখানায় অননুমোদিত ভাবে অনুপস্থিত রয়েছেন এবং উক্ত অনুপস্থিতির বিষয়ে কর্তৃপক্ষকে কোন প্রকার নোটিশ প্রদান বা অবহিত করেননি ।  এ প্রেক্ষিতে আপনাকে বাংলাদেশ শ্রম আইন, ২০০৬ এর ২৭(৩ক) ধারামতে অনতিবিলম্বে অনুপস্থিতির কারন ব্যাখ্যা সহ কাজে যোগদান বা উক্ত অনুপস্থিতির কারন ব্যাখ্যা করনের নির্দেশ প্রদান করে ১০ দিনের সময় দিয়ে গত  <?php if(isset($lastNotify)){echo en2bnNumber(date("d-m-Y", strtotime($lastNotify->ndate)));}else{ echo "............................................."; } ?> ইং তারিখে একটি চিঠি ইস্যু করা হয় ।  কিন্ত প্রদত্ত অতিক্রম করা সত্বেও আপনি অদ্যাবধি কাজে যোগদানে বা অনুপস্থিতির কারন ব্যাখ্যা প্রদানে ব্যর্থ হন ।  তবুও আপনার আত্নপক্ষ সমর্থনে, ন্যয় বিচারের স্বার্থে এবং আইনগত অধিকার সংরক্ষনে বাংলাদেশ শ্রম আইন, ২০০৬ এর ২৭(৩ক) ধারামতে অনতিবিলম্বে  আপনাকে অনুপস্থিতির কারন ব্যাখ্যা সহ কাজে যোগদান অথবা আপনার এ ধরনের বিনা নোটিশে বা উর্দ্ধতন কর্তৃপক্ষের বিনা অনুমতিতে ২০ দিনের অধিক সময় কারখানায় অনুপস্থিত থাকার বিষয়ে ব্যাখ্যা প্রদানের জন্য পূনরায় ০৭ দিন সময় প্রদান করা হল । 
            <br />
            আপনি অদ্য হতে ০৭ দিনের মধ্যে কাজে যোগদানে বা উক্ত অননুমোদিত অনুপস্থিতির বিষয়ে ব্যাখ্যা প্রদানে ব্যর্থ হলে বাংলাদেশ শ্রম আইন, ২০০৬ এর ২৭(৩ক) ধারামতে, আপনি স্বেচ্চায় চাকুরী হতে অব্যাহতি গ্রহন করেছেন এবং কোম্পানীর সাথে আপনার যাবতীয় সম্পর্ক ছিন্ন হয়েছে বলে গন্য হবে ।</p>


            <?php }elseif (($request->days)>=28) { ?>
                <p>আপনি গত  <?php echo en2bnNumber(date("d-m-Y", strtotime($request->lastPresent))); ?>  ইং তারিখ হতে অদ্যাবধি কারখানায় অননুমোদিত ভাবে অনুপস্থিত রয়েছেন এবং উক্ত অনুপস্থিতির বিষয়ে কর্তৃপক্ষকে কোন প্রকার নোটিশ প্রদান বা অবহিত করেননি ।  এ প্রেক্ষিতে আপনাকে বাংলাদেশ শ্রম আইন, ২০০৬ এর ২৭(৩ক) ধারামতে অনতিবিলম্বে অনুপস্থিতির কারন ব্যাখ্যা সহ কাজে যোগদান বা উক্ত অনুপস্থিতির কারন ব্যাখ্যা করনের নির্দেশ প্রদান করে ১০ দিনের সময় দিয়ে গত <?php if(isset($lastNotify)){echo en2bnNumber(date("d-m-Y", strtotime($secondLastNotify->ndate)));}else{ echo "(........)"; } ?> ইং তারিখে একটি চিঠি ইস্যু করা হয় ।  কিন্তু প্রদত্ত সময় অতিক্রম করা সত্বেও আপনি কাজে যোগদান বা উক্ত অনুপস্থিতির কারন ব্যাখ্যা প্রদানে ব্যর্থ হওয়ায় পূনরায় ০৭ দিনের সময় দিয়ে গত <?php if(isset($lastNotify)){echo en2bnNumber(date("d-m-Y", strtotime($lastNotify->ndate)));}else{ echo "(........)"; } ?>  ইং তারিখে আরো একটি চিঠি ইস্যু করা হয়। <br /> 
                দ্বিতীয়বারও প্রদত্ত সময়ের মধ্যে আপনি কাজে যোগদান বা উক্ত অনুপস্থিতির কারন ব্যাখ্যা প্রদানে ব্যর্থ হন। যেহেতু আপনাকে বাংলাদেশ শ্রম আইন, ২০০৬ এর ২৭(৩ক) ধারামতে অনতিবিলম্বে অনুপস্থিতির কারন ব্যাখ্যা সহ কাজে যোগদান বা উক্ত অনুপস্থিতির কারন ব্যাখ্যা প্রদানের জন্য আপনার আইনগত অধিকার সংরক্ষন করে দুইবার সময় প্রদান করা সত্বেও আপনি যথা সময়ে কাজে যোগদান বা উক্ত অনুপস্থিতির কারন ব্যাখ্যা প্রদানে ব্যর্থ হয়েছেন, সেহেতু আপনি বাংলাদেশ শ্রম আইন, ২০০৬ এর ২৭(৩ক) ধারামতে উপরোল্লেখিত অনুপস্থিতির দিন হতে চাকুরী থেকে অব্যাহতি গ্রহন করেছেন বলে গন্য হল। আপনার কোন পাওনাদি থাকলে কারখানা চলাকালীন সময় কোম্পানীর হিসাব বিভাগ থেকে নিয়ে যাওয়ার জন্য বলা হল। উল্লেখ থাকে যে, নোটিশ মেয়াদের মজুরীর বিপরীতে আপনার পাওনাদি সমন্বয় (ঝবঃ ড়ভভ) করা হবে । </p>
            <?php }else{

                echo "Error Occured";
                } ?>

            <br /><br />

            ধন্যবাদান্তে, 
            <br /><br />
            <br />মানব সম্পদ ও প্রশাসন বিভাগ
            <br />
            ফিন-বাংলা এ্যাপারেলস লিমিটেড

            <br /><br />
            অনুলিপি : ব্যক্তিগত নথি


</body>
</html>




