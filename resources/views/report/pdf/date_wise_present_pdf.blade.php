@php
    $out_time=\App\Http\Controllers\AttendanceController::out_time();
@endphp

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Daily Attendance Report</title>
    <style>
        #employeeDetails{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 50%;
            text-align: center;
            border:1px;
            font-size: 12px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            border: 1px solid #ddd;
            text-align: center !important;

        }
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: left;

        }

        #customers th {
            text-align: left;
            padding: 5px;
            background:#eee;

        }

        table td {
            padding: 2px;
            margin: 0;
        }
        table td p{
            margin: 0px;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
            font-size: 18px !important;

        }
    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader">{{$companyInformation->company_email}}</p>
        <p class="reportHeader">{{$companyInformation->company_phone}}</p>
        <p class="reportDateRange">Present report from <b>{{\Carbon\Carbon::parse($request->date)->format('d M Y')}} </b> to <b>{{\Carbon\Carbon::parse($request->endDate)->format('d M Y')}} </b>(Private)</p>

    </div>



    <center>

        @if(!empty($present))
            <table id='customers' style="margin-top:10px;font-size:10px;" border="1px">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Name</th>
                    <th>id</th>
                    @if(!empty($request->coldesignation))
                        <th>Designation</th>
                    @endif
                    @if(!empty($request->coldepartment))
                        <th>Department</th>
                    @endif
                    @if(!empty($request->coljoiningdate))
                        <th>Joining Date</th>
                    @endif
                    @if(!empty($request->colgender))
                        <th>Gender</th>
                    @endif
                    @if(!empty($request->colunit))
                        <th>Unit</th>
                    @endif
                    @if(!empty($request->colfloor))
                        <th>Floor</th>
                    @endif
                    @if(!empty($request->colline))
                        <th>Line</th>
                    @endif
                    @if(!empty($request->colsection))
                        <th>Section</th>
                    @endif
                    @if(!empty($request->colstatus))
                        <th>Status</th>
                    @endif
                    <th>Date</th>
                    <th>In Time</th>
                    @if(!empty($request->collateTime))
                        <th>Late Time</th>
                    @endif
                    <th>Out Time</th>
                    @if(!empty($request->colOverTime))
                        <th>O.T.</th>
                    @endif
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @php $i=0 @endphp
                @foreach($present as $employee)
                    <tr>
                        <td>{{sprintf('%02d', ++$i)}}</td>
                        <td>{{$employee->employeeId}}</td>
                        <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>
                        @if(!empty($request->coldesignation))
                            <td>{{$employee->designation}}</td>
                        @endif
                        @if(!empty($request->coldepartment))
                            <td>{{$employee->departmentName}}</td>
                        @endif
                        @if(!empty($request->coljoiningdate))
                            <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('Y-m-d')}}</td>
                        @endif
                        @if(!empty($request->colgender))
                            <td>
                                @if($employee->empGenderId==1)
                                    Male
                                @endif
                                @if($employee->empGenderId==2)
                                    Female
                                @endif
                                @if($employee->empGenderId==3)
                                    Other
                                @endif
                            </td>
                        @endif
                        @if(!empty($request->colunit))
                            <td>{{$employee->unitName}}</td>
                        @endif
                        @if(!empty($request->colfloor))
                            <td>{{$employee->floorName}}</td>
                        @endif
                        @if(!empty($request->colline))
                            <td>{{$employee->LineName}}</td>
                        @endif
                        @if(!empty($request->colsection))
                            <td>{{$employee->empSection}}</td>
                        @endif
                        @if(!empty($request->colstatus))
                            <td>
                                @if($employee->empAccStatus==1)
                                    Active
                                @elseif($employee->empAccStatus==0)
                                    Inactive
                                @endif
                            </td>
                        @endif
                        <td>
                            {{\Carbon\Carbon::parse($employee->date)->format('d M Y')}}
                        </td>
                        <td>
                            {{$employee->in_time}}
                        </td>
                        @if(!empty($request->collateTime))
                            @if($employee->in_time>$employee->max_entry_time)
                                <td>
                                    <span class="red-text">{{ gmdate('H:i:s', strtotime($employee->in_time) - strtotime($employee->max_entry_time)) }}</span>
                                </td>
                            @else
                                <td></td>
                            @endif
                        @endif
                        <td>
                            @if($employee->in_time==$employee->out_time)
                                <span style="color: #FF0000">Not Given</span>

                            @else
                                {{$employee->out_time}}
                            @endif
                        </td>
                        @if(!empty($request->colOverTime))
                            <td>
                                @if(isset($employee->out_time))
                                    @if($employee->out_time>\App\Http\Controllers\OvertimeController::overtime_start())
                                        {{ date('H:i:s', strtotime($employee->out_time) - strtotime($employee->exit_time)) }}
                                    @endif
                                @endif
                            </td>
                        @endif
                        <td>
                            @if($employee->in_time>$employee->exit_time)
                                <span style=" color: #FF0000">Late</span>
                            @else
                                <span>Present</span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        @else
            <hr>
            <h4 style="color:red;"><center> No Present Record Found.</center></h4>
    @endif

</div>

</body>
</html>

<script type="text/javascript">
    window.print();
</script>




