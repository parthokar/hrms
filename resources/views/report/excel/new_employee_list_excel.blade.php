<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Employee List</title>
</head>
<body>

<div class="container">
    <table>
        <thead>
        </thead>
        <tbody>
        <tr><td></td><td colspan="4" align="center"><h2><b>{{$companyInformation->company_name}}</b></h2></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_address1}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_email}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_phone}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"> <b>Employee List</b></td></tr>

        </tbody>
    </table>
<table>
      <thead>
      <tr>
          <th>ID</th>
          <th>Name</th>
          @if(!empty($request->coldepartment))
          <th>Department</th>
          @endif
          @if(!empty($request->coldesignation))
          <th>Designation</th>
          @endif
          @if(!empty($request->colgender))
          <th>Gender</th>
          @endif
          <th>Joining Date</th>
          @if(!empty($request->colunit))
          <th>Unit</th>
          @endif
          @if(!empty($request->colfloor))
          <th>Floor</th>
          @endif
          @if(!empty($request->colline))
          <th>Line</th>
          @endif
          @if(!empty($request->colsection))
          <th>Section</th>
          @endif
          @if(!empty($request->colstatus))
          <th>Status</th>
          @endif
          @if(!empty($request->colnid))
          <th>National ID</th>
          @endif
          @if(!empty($request->colCurrentAddress))
          <th>Current Address</th>
          @endif
          @if(!empty($request->colPermanentAddress))
          <th>Permanent Address </th>
          @endif
          @if(!empty($request->colReligion))
          <th>Religion</th>
          @endif
          @if(!empty($request->colFatherName))
          <th>Father's Name</th>
          @endif
          @if(!empty($request->colMotherName))
          <th>Mother's Name</th>
          @endif
          @if(!empty($request->colBiometricID))
          <th>Biometric ID</th>
          @endif
          @if(!empty($request->colPaymentMode))
          <th>Payment Mode</th>
          @endif
          @if(!empty($request->colWorkingGroup))
          <th>Working Group</th>
          @endif
          @if(!empty($request->colBloodGroup))
          <th>Blood Group</th>
          @endif
          @if(!empty($request->colCard))
          <th>Proxy ID </th>
          @endif
          @if(!empty($request->colShift))
          <th>Work Shift </th>
          @endif
          @if(!empty($request->coldob))
          <th>DOB</th>
          @endif
          <th>Salary</th>
      </tr>
      </thead>
      <tbody>
      @foreach($employees as $employee)
      <tr>
          <td>{{$employee->employeeId}}</td>
          <td>{{$employee->empFirstName}}{{$employee->empLastName}}</td>
          @if(!empty($request->coldepartment))
          <td>{{$employee->departmentName}}</td>
          @endif
          @if(!empty($request->coldesignation))
          <td>{{$employee->designation}}</td>
          @endif
          @if(!empty($request->colgender))
          <td>
              @if($employee->empGenderId==1)
                  Male
              @endif
              @if($employee->empGenderId==2)
                  Female
              @endif
              @if($employee->empGenderId==3)
                  Other
              @endif
          </td>
          @endif
          <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('Y-m-d')}}</td>
          @if(!empty($request->colunit))
          <td>{{$employee->unitName}}</td>
          @endif
          @if(!empty($request->colfloor))
          <td>{{$employee->floorName}}</td>
          @endif
          @if(!empty($request->colline))
          <td>{{$employee->LineName}}</td>
          @endif
          @if(!empty($request->colsection))
          <td>{{$employee->empSection}}</td>
          @endif
          @if(!empty($request->colstatus))
          <td>
              @if($employee->empAccStatus==1)
                  Active
              @elseif($employee->empAccStatus==0)
                  Inactive
              @endif
          </td>
          @endif
          @if(!empty($request->colnid))
          <td>{{$employee->empNid}}</td>
          @endif
          @if(!empty($request->colCurrentAddress))
          <td>{{$employee->empCurrentAddress}}</td>
          @endif
          @if(!empty($request->colPermanentAddress))
          <td>{{$employee->empParAddress}}</td>
          @endif
          @if(!empty($request->colReligion))
          <td>{{$employee->empReligion}}</td>
          @endif
          @if(!empty($request->colFatherName))
          <td>{{$employee->empFatherName}}</td>
          @endif
          @if(!empty($request->colMotherName))
          <td>{{$employee->empMotherName}}</td>
          @endif
          @if(!empty($request->colBiometricID))
          <td>{{$employee->empGlobalId}}</td>
          @endif
          @if(!empty($request->colPaymentMode))
          <td>{{$employee->payment_mode}}</td>
          @endif
          @if(!empty($request->colWorkingGroup))
          <td>{{$employee->work_group}}</td>
          @endif
          @if(!empty($request->colBloodGroup))
          <td>{{$employee->empBloodGroup}}</td>
          @endif
          @if(!empty($request->colCard))
          <td>{{$employee->empCardNumber}}</td>
          @endif
          @if(!empty($request->colShift))
          <td>{{$employee->shiftName}}</td>
          @endif
          @if(!empty($request->coldob))
          <td>{{\Carbon\Carbon::parse($employee->empDOB)->format('d-m-Y')}}</td>
          @endif
          <td>{{$employee->total_employee_salary}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>

        

</div>

</body>
</html>




