<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Resigned Employees Salary Sheet</title>
</head>
<body>
<table>
    <thead>
    </thead>
    <tbody>
    <tr><td></td><td colspan="14" align="center"><h4 style="font-weight: bold;text-transform: uppercase;font-size: 12px;" class="text-center">Resigned Employee Salary Sheet</h4></td></tr>
    <tr><td></td><td colspan="14" align="center"><h2><b>{{$companyInformation->company_name}}</b></h2></td></tr>
    <tr><td></td><td colspan="14" align="center"><b>{{$companyInformation->company_address1}}</b></td></tr>
    <tr><td></td><td colspan="14" align="center"><b>{{$companyInformation->company_email}}</b></td></tr>
    <tr><td></td><td colspan="14" align="center"><b>{{$companyInformation->company_phone}}</b></td></tr>
    <tr><td></td><td colspan="14" align="center"><b>@if($monthname){{date('F-Y',strtotime($monthname->month))}} @else NO Data Found @endif</b></td></tr>
    </tbody>
</table>
<table>
    <tr>
        <th>EMPID</th>
        <th>NAME</th>
        <th>JOIN DATE</th>
        <th>DEPARTMENT</th>
        <th>DESIGNATION</th>
        <th>GRADE</th>
        <th>BASIC</th>
        <th>GROSS</th>
        <th>WORK DAYS</th>
        <th>ABS DAYS</th>
        <th style="text-align: center" colspan="2">DEDUCTION</th>
        <th>LEAVE DAYS</th>
        <th>GROSS PAY</th>
        <th style="text-align: center" colspan="3">OVERTIME</th>
        <th>ATTENDANCE BONUS</th>
        <th>SPECIAL ALLOW</th>
        <th>NET WAGES</th>
        <th>SIGNATURE</th>
    </tr>
    @foreach($data as $emp)
        <tr>
            <td>{{$emp->employeeId}}</td>
            <td>{{$emp->e_first_name}}</td>
            <td>{{date('F-Y',strtotime($emp->empJoiningDate))}}</td>
            <td>{{$emp->departmentName}}</td>
            <td>{{$emp->designation}}</td>
            <td>{{$emp->grade_name}}</td>
            <td>{{$emp->basic_salary}}</td>
            <td>{{$emp->gross}}</td>
            <td>{{$emp->working_day}}</td>
            <td>{{$emp->absent}}</td>
            <td>
                @if($emp->absent_deduction_amount=='')
                    Absent: 0
                @else
                    Absent: {{$emp->absent_deduction_amount}}
                @endif
            </td>
            <td>
                @if($emp->advanced_deduction=='')
                    Advanced: 0
                @else
                    Advanced:{{$emp->advanced_deduction}}
                @endif
            </td>
            <td>
                @if($emp->total=='')
                    0
                @else
                    {{$emp->total==''}}
                @endif
            </td>
            <td>{{$emp->gross_pay}}</td>
            <td>
                @if($emp->overtime=='')
                    Hour: 0
                @else
                    Hour: {{$emp->overtime}}
                @endif
            </td>
            <td>Rate: {{$emp->overtime_rate}}</td>
            <td>
                @if($emp->overtime_amount=='')
                    Amount:0
                @else
                    Amount: {{$emp->overtime_amount}}
                @endif
            </td>
            <td>
                @if($emp->absent <3 && $emp->total=='')
                    {{$emp->attendance_bonus}}
                @else
                    0
                @endif
            </td>
            <td>
                @if($emp->increment_bonus_amount=='')
                    0
                @else
                    {{$emp->increment_bonus_amount}}
                @endif
            </td>
            <td>
                @if($emp->absent <3 && $emp->total=='')
                    @php $total=$emp->net_amount; @endphp
                @else
                    @php
                        $net=$emp->net_amount;
                        $bonus=$emp->attendance_bonus;
                        $total=$net-$bonus;
                    @endphp
                    {{$total}}
                @endif
            </td>
            <td></td>
        </tr>
    @endforeach
</table>
</body>
</html>