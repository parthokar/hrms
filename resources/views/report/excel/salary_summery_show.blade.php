<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Resigned Employees Salary Sheet</title>
</head>
<body>
<table>
    <thead>
    </thead>
    <tbody>
    <tr><td></td><td colspan="14" align="center"><h2><b>{{$companyInformation->company_name}}</b></h2></td></tr>
    <tr><td></td><td colspan="14" align="center"><b>{{$companyInformation->company_address1}}</b></td></tr>
    <tr><td></td><td colspan="14" align="center"><b>{{$companyInformation->company_email}}</b></td></tr>
    <tr><td></td><td colspan="14" align="center"><b>{{$companyInformation->company_phone}}</b></td></tr>
    {{--<tr><td></td><td colspan="14" align="center"><b>@if($monthname){{date('F-Y',strtotime($monthname->month))}} @else NO Data Found @endif</b></td></tr>--}}
    </tbody>
</table>
<table>
    <thead>
    <tr>
        <th>Month</th>
        <th>Basic</th>
        <th>House</th>
        <th>Medical</th>
        <th>Transport</th>
        <th>Food</th>
        <th>Gross</th>
        <th style="text-align: center;" colspan="2">Overtime</th>
        <th>Gross Pay</th>
        <th style="text-align: center;" colspan="3">Deduction</th>
        <th style="text-align: center;" colspan="4">Bonus</th>
        <th>Net</th>
    </tr>
    </thead>
    <tbody>
    @if($data)
        @foreach($data as $item)
            <tr>
                <td>{{date('F-Y',strtotime($item->month))}}</td>
                <td>{{$item->basic}}</td>
                <td>{{$item->house}}</td>
                <td>{{$item->medicals}}</td>
                <td>{{$item->transports}}</td>
                <td>{{$item->foods}}</td>
                <td>{{$item->gorss}}</td>
                <td>
                    @if($item->overtimes=='')
                        Hour: 0/Hr
                    @else
                        Hour: {{$item->overtimes}}
                    @endif
                </td>
                <td>
                    @if($item->ot_amounts=='')
                        Amount: 0
                    @else
                        Amount: {{$item->ot_amounts}}
                    @endif
                </td>
                <td>{{$item->gross_pay}} </td>
                <td>
                    @if($item->nor_deduction=='')
                        Normal:0
                    @else
                        Normal:{{$item->nor_deduction}}
                    @endif
                </td>
                <td>
                    @if($item->advanced_deduction=='')
                        Advanced:0
                    @else
                        Advanced:{{$item->advanced_deduction}}
                    @endif
                </td>
                <td>
                    @if($item->a_deduction_amount=='')
                        Absent:0
                    @else
                        Absent: {{$item->a_deduction_amount}}
                    @endif
                </td>

                <td>
                    @foreach($atts_bonuss as $d_att_bonus)
                        @php
                            $attendance_bonusss=$item->att_bonus;
                            $deduc_att_bonus=$d_att_bonus->bonus;
                            $total_att_bonus=$attendance_bonusss-$deduc_att_bonus;
                        @endphp
                        Attendance: {{$total_att_bonus}}
                </td>

                <td>
                    @if($item->festival_bonus=='')
                        Festival: 0
                    @else
                        Festival: {{$item->festival_bonus}}
                    @endif
                </td>
                <td>
                    @if($item->increment_bonus=='')
                        Increment: 0
                    @else
                        Increment:  {{$item->increment_bonus}}
                    @endif
                </td>
                <td>
                    @if($item->p_bonus=='')
                        Production: 0
                    @else
                        Production: {{$item->p_bonus}}
                    @endif
                </td>
                <td>
                    @php
                        $deduc_att_bonus=$d_att_bonus->bonus;
                        $net=$item->net_total;
                        $net_total_amount=$net-$deduc_att_bonus;
                    @endphp
                    Net: {{$net_total_amount}}
                </td>
            </tr>
        @endforeach
        @endforeach
    @else
        <span style="text-align: center; font-size: 12px;color:red;">Sorry no data found</span>
    @endif
    </tbody>
</table>
</body>
</html>