@php
    $out_time=\App\Http\Controllers\AttendanceController::out_time();
    $countLate=0;
@endphp

        <!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Daily Attendance (Private)</title>
</head>
<body>

<div>
    <table>
        <tbody>
        <tr><td></td><td colspan="4" align="center"><h2><b>{{$companyInformation->company_name}}</b></h2></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_address1}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_email}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_phone}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center">Attendance report for <b>{{\Carbon\Carbon::parse($request->date)->format('d M Y (l)')}} </b></td></tr>

        </tbody>
    </table>


        @if(!empty($allData))
            <table>
                <thead>
                <tr>
                    <th align="center">ID</th>
                    <th>Name</th>
                    @if(!empty($request->coldesignation))
                        <th>Designation</th>
                    @endif
                    @if(!empty($request->coldepartment))
                        <th>Department</th>
                    @endif
                    @if(!empty($request->coljoiningdate))
                        <th>Joining Date</th>
                    @endif
                    @if(!empty($request->colgender))
                        <th>Gender</th>
                    @endif
                    @if(!empty($request->colunit))
                        <th>Unit</th>
                    @endif
                    @if(!empty($request->colfloor))
                        <th>Floor</th>
                    @endif
                    @if(!empty($request->colline))
                        <th>Line</th>
                    @endif
                    @if(!empty($request->colsection))
                        <th>Section</th>
                    @endif
                    @if(!empty($request->colstatus))
                        <th>Status</th>
                    @endif
                    @if(!empty($request->colshift))
                        <th>Work Shift</th>
                    @endif
                    <th>In Time</th>
                    @if(!empty($request->collateTime))
                        <th>Late Time</th>
                    @endif
                    <th>Out Time</th>
                    @if(!empty($request->colOverTime))
                        <th>O.T.</th>
                    @endif
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @foreach($allData as $employee)
                    <tr>
                        <td align="center">{{$employee->employeeId}}</td>
                        <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>
                        @if(!empty($request->coldesignation))
                            <td>{{$employee->designation}}</td>
                        @endif
                        @if(!empty($request->coldepartment))
                            <td>{{$employee->departmentName}}</td>
                        @endif
                        @if(!empty($request->coljoiningdate))
                            <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('Y-m-d')}}</td>
                        @endif
                        @if(!empty($request->colgender))
                            <td>
                                @if($employee->empGenderId==1)
                                    Male
                                @endif
                                @if($employee->empGenderId==2)
                                    Female
                                @endif
                                @if($employee->empGenderId==3)
                                    Other
                                @endif
                            </td>
                        @endif
                        @if(!empty($request->colunit))
                            <td>{{$employee->unitName}}</td>
                        @endif
                        @if(!empty($request->colfloor))
                            <td>{{$employee->floorName}}</td>
                        @endif
                        @if(!empty($request->colline))
                            <td>{{$employee->LineName}}</td>
                        @endif
                        @if(!empty($request->colsection))
                            <td>{{$employee->empSection}}</td>
                        @endif
                        @if(!empty($request->colstatus))
                            <td>
                                @if($employee->empAccStatus==1)
                                    Active
                                @elseif($employee->empAccStatus==0)
                                    Inactive
                                @endif
                            </td>
                        @endif
                        @if(!empty($request->colshift))
                            <td>{{$employee->shiftName}}</td>
                        @endif
                        <td>
                            @if(isset($employee->in_time))
                                {{$employee->in_time}}
                            @endif
                        </td>

                        @if(!empty($request->collateTime))
                            <td>
                                @if(isset($employee->in_time))
                                    @if($employee->in_time>$employee->max_entry_time)
                                        {{ date('H:i:s', strtotime($employee->in_time) - strtotime($employee->max_entry_time)) }}
                                    @endif
                                @endif
                            </td>
                        @endif
                        <td>
                            @if(isset($employee->out_time))
                                @if($employee->in_time==$employee->out_time)
                                    <p style="color: #FF0000">Not Given</p>

                                @else
                                    {{$employee->out_time}}
                                @endif
                            @endif
                        </td>
                        @if(!empty($request->colOverTime))
                            <td>
                                @if(isset($employee->out_time))
                                    @if($employee->out_time>$employee->exit_time)
                                        {{ date('H:i:s', strtotime($employee->out_time) - strtotime($employee->exit_time)) }}
                                    @endif
                                @endif
                            </td>
                        @endif
                        <td>
                            @if(isset($employee->in_time))
                                @if($employee->in_time>$employee->max_entry_time)
                                    @php(
                                        $countLate++
                                    )
                                    <P style="color: #0A246A">Late</P>
                                @else
                                    <p style="color: #0b4d3f;">Present</p>
                                @endif
                            @else
                                @if(isset($employee->leave))
                                    <P style="color: #0A246A">On Leave</P>
                                @else
                                    <p style="color: #FF0000">Absent</p>
                                @endif

                            @endif

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div style="float:left;padding-top: 15px; text-align: left;font-size: 12px;">
                <span><b>Total Employee: </b> {{ $countAbsent+$countPresent+$totalL }}</span><br>
                <span><b>Total Present :</b> {{ ($countPresent) }}</span><br>
                <span><b>Total Absent :</b> {{ $countAbsent }}</span><br>
                <span><b>On Leave :</b> {{ $totalL }}</span><br>
                <span><b>Total Late :</b> {{ $countLate }}</span><br>

            </div>

        @else
            <hr>
            <h4 style="color:red;"><center> No Matched data found.</center></h4>
    @endif

</div>

</body>
</html>




