<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Date Wise Attendance Exception</title>
</head>
<body>

<div>
    <table>
        <thead>
        </thead>
        <tbody>
        <tr><td></td><td colspan="4" align="center"><h2><b>{{$companyInformation->company_name}}</b></h2></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_address1}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_email}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_phone}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center">Attendance exception report from <b>{{\Carbon\Carbon::parse($request->date)->format('d M Y')}} </b> to <b>{{\Carbon\Carbon::parse($request->endDate)->format('d M Y')}}</b></td></tr>

        </tbody>
    </table>
    @if(!empty($present))
        <table>
            <thead>
            <tr>
                <th align="center">ID</th>
                <th>Name</th>
                @if(!empty($request->coldesignation))
                    <th>Designation</th>
                @endif
                @if(!empty($request->coldepartment))
                    <th>Department</th>
                @endif
                @if(!empty($request->coljoiningdate))
                    <th>Joining Date</th>
                @endif
                @if(!empty($request->colgender))
                    <th>Gender</th>
                @endif
                @if(!empty($request->colunit))
                    <th>Unit</th>
                @endif
                @if(!empty($request->colfloor))
                    <th>Floor</th>
                @endif
                @if(!empty($request->colline))
                    <th>Line</th>
                @endif
                @if(!empty($request->colsection))
                    <th>Section</th>
                @endif
                @if(!empty($request->colstatus))
                    <th>Status</th>
                @endif
                <th>In Time</th>
                <th>Date</th>
                @if(!empty($request->collateTime))
                    <th>Late Time</th>
                @endif
                <th>Out Time</th>
            </tr>
            </thead>
            <tbody>
            @foreach($present as $employee)
                <tr>
                    <td align="center">{{$employee->employeeId}}</td>
                    <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>
                    @if(!empty($request->coldesignation))
                        <td>{{$employee->designation}}</td>
                    @endif
                    @if(!empty($request->coldepartment))
                        <td>{{$employee->departmentName}}</td>
                    @endif
                    @if(!empty($request->coljoiningdate))
                        <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('Y-m-d')}}</td>
                    @endif
                    @if(!empty($request->colgender))
                        <td>
                            @if($employee->empGenderId==1)
                                Male
                            @endif
                            @if($employee->empGenderId==2)
                                Female
                            @endif
                            @if($employee->empGenderId==3)
                                Other
                            @endif
                        </td>
                    @endif
                    @if(!empty($request->colunit))
                        <td>{{$employee->unitName}}</td>
                    @endif
                    @if(!empty($request->colfloor))
                        <td>{{$employee->floorName}}</td>
                    @endif
                    @if(!empty($request->colline))
                        <td>{{$employee->LineName}}</td>
                    @endif
                    @if(!empty($request->colsection))
                        <td>{{$employee->empSection}}</td>
                    @endif
                    @if(!empty($request->colstatus))
                        <td>
                            @if($employee->empAccStatus==1)
                                Active
                            @elseif($employee->empAccStatus==0)
                                Inactive
                            @endif
                        </td>
                    @endif
                    <td>
                        {{$employee->in_time}}
                    </td>
                    <td>
                        {{$employee->date}}
                    </td>
                    @if(!empty($request->collateTime))
                        <td>
                            @if($employee->in_time>$employee->max_entry_time)
                                <span>{{ gmdate('H:i:s', strtotime($employee->in_time) - strtotime($employee->max_entry_time)) }}</span>
                            @endif
                        </td>
                    @endif
                    <td>
                        <span style="color: #FF0000">Not Given</span>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>

        <div style="float:left;padding-top: 15px;font-size: 12px;">

            <span><b>Total Employee :</b> {{ ($countPresent) }}</span><br>
            {{--<span><b>Total Absent :</b> {{ $countAbsent }}</span><br>--}}
            {{--<span><b>Total Late Employee :</b> {{ $countPresent }}</span><br>--}}
        </div>

    @else
        <hr>
        <h4 style="color:red;"><center> No record found.</center></h4>
    @endif

</div>

</body>
</html>




