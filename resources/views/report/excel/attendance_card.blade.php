<?php
$b_time=0;
function roundTime($time){
    $hour=date('G',strtotime($time));
    $minute=date('i',strtotime($time));

    if($minute<25){
        $minute=0;
    }
    elseif ($minute>=25 &&$minute<55){
        $minute=.5;
    }
    else{
        $minute=0;
        $hour++;
    }
    $kind=number_format((float)($hour+$minute),1,'.','');
    return $kind;

}

function late_time($pretime,$time){
    $phour=date('G',strtotime($pretime));
    $pminute=date('i',strtotime($pretime));
    $hour=date('G',strtotime($time));
    $minute=date('i',strtotime($time));
    $thour=$phour+$hour;
    $tminute=$pminute+$minute;
    $h=floor($tminute/60);
    $tminute=sprintf("%02d",$tminute%60);
    $thour+=$h;
    $thour=sprintf("%02d",$thour);
    return "$thour:$tminute";

}

function end_time($time){
    $hour=date('G',strtotime($time));
    $minute=date('i',strtotime($time));
    $minute=sprintf("%02d",$minute+rand(0,20));
    return "$hour:$minute";

}

$join_less=0;

if($datas[0][0]->date_of_discontinuation!=null){
    if(\Carbon\Carbon::parse($datas[0][0]->date_of_discontinuation)->format('m-Y')==\Carbon\Carbon::parse($end_date)->format('m-Y')){
        $total_days=\Carbon\Carbon::parse($start_date)->diffInDays($datas[0][0]->date_of_discontinuation)+1;
        $l=1;
    }
    else{
        $l=2;
    }

}
else{
    $l=0;
}


if(\Carbon\Carbon::parse($datas[0][0]->empJoiningDate)->format('m-Y')==\Carbon\Carbon::parse($end_date)->format('m-Y'))
{
    $total_days=\Carbon\Carbon::parse($datas[0][0]->empJoiningDate)->diffInDays($end_date)+1;
    $k=1;

}
else{
    $k=0;
}



$absent=0;
$working_days=0;
$weekends=0;
$present=0;
$late=0;
$onleave=0;
$ot=0;
$late_time="00:00";

$attendance_settings=\Illuminate\Support\Facades\DB::table('attendance_setup')->leftJoin('employees','attendance_setup.id','=','employees.empShiftId')->where('employees.id','=',$emp_id)->first();

?>
        <!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Job Card</title>
</head>
<body>

<div>
    <table>
        <tbody>
        <tr><td></td><td colspan="4" align="center"><h2><b>{{$companyInformation->company_name}}</b></h2></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_address1}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"> Job Card Report Month From <b>{{\Carbon\Carbon::parse($start_date)->format('d-M-Y')}}</b> to <b>{{\Carbon\Carbon::parse($end_date)->format('d-M-Y')}} </b></td></tr>


        </tbody>
    </table>
    @if(isset($datas[0][0]))
        <table>
            <tr>
                <td><b>Employee ID :</b> {{$datas[0][0]->employeeId}}</td>
                <td><b>Name : </b> {{$datas[0][0]->empFirstName." ".$datas[0][0]->empLastName }}</td>
                <td><b>Joining Date :</b> {{\Carbon\Carbon::parse($datas[0][0]->empJoiningDate)->format('d-M-Y')}}</td>
            </tr>
            <tr>
                <td><b>Designation :</b> {{$datas[0][0]->designation}}</td>
                <td><b>Department : </b> {{$datas[0][0]->departmentName}}</td>
                <td><b> Section : </b> {{$datas[0][0]->empSection}}</td>
            </tr>

        </table>

        <table>
            <thead>
            <tr>
                <th>Date</th>
                <th>Day</th>
                <th>In Time </th>
                <th>Out Time</th>
                <th>OT</th>
                <th>Late Time</th>



                <th>Status</th>
                <th>Remarks</th>
            </tr>
            </thead>
            <tbody>
            @foreach($datas as $d)
                <?php
                if($k==1){
                    if(\Carbon\Carbon::parse($datas[0][0]->empJoiningDate)->subDay()->gte(\Carbon\Carbon::parse($d[1]))){
                        $j=1;
                        continue;
                    }
                    else{
                        $j=0;
                    }

                }
                else{
                    $j=0;
                }
                if($l==1){
                    if(\Carbon\Carbon::parse(\Carbon\Carbon::parse($d[1]))->subDay()->gte($datas[0][0]->date_of_discontinuation)){
                        $m=1;
                        continue;
                    }
                    else{
                        $m=0;
                    }

                }
                else{
                    $m=0;
                }

                $working_days++;
                $leave_check= explode('@',$d[2]);
                $holidayPurpose= explode('@',$d[2]);
                $regular_day=0;
                ?>
                <tr>
                    <td>{{\Carbon\Carbon::parse($d[1])->format('d-M-Y')}}</td>
                    <td>{{\Carbon\Carbon::parse($d[1])->format('l')}}</td>
                    <td>
                        @if(isset($d[0]->in_time))
                            <?php
                            $present++;
                            ?>

                            {{ date('G:i', strtotime($d[0]->in_time)) }}

                        @endif
                    </td>
                    <td>
                        @if(isset($d[0]->out_time))
                            @if($d[0]->in_time==$d[0]->out_time)
                                <span style="color: #FF0000;">Not Given</span>


                            @else

                                {{ gmdate('G:i', strtotime($d[0]->out_time)) }}

                            @endif

                        @endif
                    </td>
                   

                    <td>
                        @if(isset($d[0]->out_time) && $d[0]->out_time!=$d[0]->in_time)
                                @php
                                $rama_dan=DB::table('attendance_setup_ramadan')
                                ->where('shift_id',$attendance_settings->id)
                                ->where('ramadan_date',$d[0]->date)
                                ->first(); 
                                $break_date=DB::table('attendance_setup_break')
                                ->where('shift_id',$attendance_settings->id)
                                ->where('break_date',$d[0]->date)
                                ->first();
                                @endphp
                                
                                @if(isset($break_date->break_time))
                                @php $b_time=$break_date->break_time; @endphp
                               @endif
                               @if(isset($rama_dan->ramadan_date)==date('Y-m-d', strtotime($d[0]->date)))
                                @if($d[0]->out_time>$attendance_settings->exit_time_ramadan)
                                @if(\Carbon\Carbon::parse($d[1])->format('l')=="Friday")
                                    <?php
                                    $regular_day=\Illuminate\Support\Facades\DB::table('tbweekend_regular_day')->where('weekend_date','=',$d[1])->count();
                                    ?>
                                    @if(!$regular_day)
                                        <?php
                                        $ot+=roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time_ramadan)))+8-$b_time
                                        ?>
                                        {{ roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time_ramadan)))+8-$b_time }}
                                    @else
                                        <?php
                                        $ot+=roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time_ramadan)))-$b_time
                                        ?>
                                        {{ roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time_ramadan)))-$b_time }}
                                    @endif
                                @else
                                    <?php
                                    $ot+=roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time_ramadan)))-$b_time
                                    ?>
                                    {{ roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time_ramadan)))-$b_time }}
                                @endif
                               @else
                                @if(\Carbon\Carbon::parse($d[1])->format('l')=="Friday")
                                    <?php
                                    $regular_day=\Illuminate\Support\Facades\DB::table('tbweekend_regular_day')->where('weekend_date','=',$d[1])->count();
                                    ?>
                                    @if(!$regular_day)
                                        <?php
                                        $ot+=roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($d[0]->in_time)))-1-$b_time
                                        ?>
                                        {{roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($d[0]->in_time)))-1-$b_time}}
                                    @else
                                        0.0
                                    @endif
                                @else
                                    0.0
                                @endif
                            @endif
                            <!--- Ramadan end -->
                           @else 
                         <!--- general start -->
                            @if($d[0]->out_time>$attendance_settings->exit_time)
                            @if(\Carbon\Carbon::parse($d[1])->format('l')=="Friday")
                                <?php
                                $regular_day=\Illuminate\Support\Facades\DB::table('tbweekend_regular_day')->where('weekend_date','=',$d[1])->count();
                                ?>
                                @if(!$regular_day)
                                    <?php
                                    $ot+=roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time)))+8-$b_time
                                    ?>
                                    {{ roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time)))+8-$b_time }}
                                @else
                                    <?php
                                    $ot+=roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time)))-$b_time
                                    ?>
                                    {{ roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time)))-$b_time }}
                                @endif
                            @else
                                <?php
                                $ot+=roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time)))-$b_time
                                ?>
                                {{ roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time)))-$b_time }}
                            @endif
                           @else
                            @if(\Carbon\Carbon::parse($d[1])->format('l')=="Friday")
                                <?php
                                $regular_day=\Illuminate\Support\Facades\DB::table('tbweekend_regular_day')->where('weekend_date','=',$d[1])->count();
                                ?>
                                @if(!$regular_day)
                                    <?php
                                    $ot+=roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($d[0]->in_time)))-1-$b_time
                                    ?>
                                    {{roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($d[0]->in_time)))-1-$b_time}}
                                @else
                                    0.0
                                @endif
                            @else
                                0.0
                            @endif
                        @endif
                       <!--- general end -->

                        @endif
                        @endif
                        <!-- general overtime condition end -->
                    </td>

                    <td>
                        @if(isset($d[0]->in_time))

                        @php
                        $rama_dan=DB::table('attendance_setup_ramadan')
                        ->where('shift_id',$attendance_settings->id)
                        ->where('ramadan_date',$d[0]->date)
                        ->first(); 
                        @endphp
                      <!-- ramadan late condition start -->
                      @if(isset($rama_dan->ramadan_date)==date('Y-m-d', strtotime($d[0]->date)))
                            @if($d[0]->in_time>$attendance_settings->max_entry_time_ramadan)
                                <?php
                                $late_time=late_time($late_time,date('G:i', strtotime($d[0]->in_time) - strtotime($attendance_settings->max_entry_time_ramadan)));

                                ?>
                                {{ date('G:i', strtotime($d[0]->in_time) - strtotime($attendance_settings->max_entry_time_ramadan)) }}
                            @else
                                00:00
                            @endif
                            @else 
                      <!-- ramadan late condition end -->
                      <!-- general late condition end -->
                      @if($d[0]->in_time>$attendance_settings->max_entry_time)
                      <?php
                      $late_time=late_time($late_time,date('G:i', strtotime($d[0]->in_time) - strtotime($attendance_settings->max_entry_time)));
                      ?>
                      {{ date('G:i', strtotime($d[0]->in_time) - strtotime($attendance_settings->max_entry_time)) }}
                        @else
                            00:00
                        @endif
                      <!-- general late condition end -->
                          @endif
                        @endif
                    </td>



                    <td>
                        @if(isset($d[0]->in_time))
                            @if($d[0]->in_time>$attendance_settings->max_entry_time)
                                <?php $late++; ?>
                                <span>Late</span>
                            @else
                                <span>Present</span>
                            @endif
                        @else
                            @if($holidayPurpose[0]=='Holiday' || $leave_check[0]=='On Leave')
                                @if($leave_check[0]!='On Leave')
                                    <?php $working_days--; ?>
                                @else
                                    <?php $onleave++; ?>
                                @endif
                                @if($leave_check[0]=='On Leave')
                                    @if(isset($leave_check[1]))
                                        <span>{{$leave_check[1]}}</span>
                                    @endif
                                @elseif($holidayPurpose[0]=="Holiday")
                                    @if(isset($holidayPurpose[1]))
                                        <span>{{$holidayPurpose[1]}}</span>
                                    @endif

                                @else
                                    <span>{{$d[2]}}</span>
                                @endif
                            @else
                                @if(\Carbon\Carbon::parse($d[1])->format('l')=="Friday")
                                    <?php
                                    $regular_day=\Illuminate\Support\Facades\DB::table('tbweekend_regular_day')->where('weekend_date','=',$d[1])->count();
                                    ?>
                                    @if($regular_day && $l!=2)
                                        <?php
                                        $absent++;
                                        ?>
                                        <span style="color:#FF0000">Absent</span>
                                    @else
                                        <?php
                                        $weekends++;
                                        ?>
                                        <span>Weekend</span>
                                    @endif

                                @else
                                    @if($l!=2)
                                        <?php
                                        $absent++;
                                        ?>

                                        <span style="color:#FF0000">Absent</span>
                                    @endif

                                @endif

                            @endif

                        @endif
                    </td>
                    <td></td>

                </tr>
            @endforeach
            <tr style="font-weight: bold">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{$ot}} hour</td>

                <td>{{$late_time}}</td>
                <td></td>
                <td></td>

            </tr>
            </tbody>
        </table>

</div>


<div>

        <table>
            <tr>
                <td><b>Total Working Days: </b>{{ $total_days }} Days</td>
                <td><b>Holiday: </b>{{$total_holiday}} Days</td>
                <td><b>Total Present Days: </b> {{ $present }} Days</td>
            </tr>

            <tr>
                <td><b>Weekends: </b>{{$weekends}} Days</td>
                <td><b>Total Leave :</b> {{ ($onleave) }} Days</td>
                <td><b>Total Absent :</b> {{ $absent }} Days</td>
            </tr>

        </table>
</div>

@else
    <center><h4 style="color: #FF0000">No data found for the given date</h4></center>

@endif



</body>
</html>




