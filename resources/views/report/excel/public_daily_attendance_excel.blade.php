@php
    $out_time=\App\Http\Controllers\AttendanceController::out_time();
    $overtime_end=\App\Http\Controllers\OvertimeController::overtime_end();
    $late=0;
@endphp

        <!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Daily Attendance Report</title>
</head>
<body>
    <table>
        <thead>
        </thead>
        <tbody>
        <tr><td></td><td colspan="4" align="center"><h2><b>{{$companyInformation->company_name}}</b></h2></td></tr>
            <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_address1}}</b></td></tr>
            <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_email}}</b></td></tr>
            <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_phone}}</b></td></tr>
            <tr><td></td><td colspan="4" align="center">Attendance report for <b>{{\Carbon\Carbon::parse($request->date)->format('d M Y (l)')}} </b></td></tr>

        </tbody>
    </table>
    {{--<table>--}}
        {{--<tbody>--}}
            {{--<tr>--}}
            {{--<td colspan="3"><b>Total Employee: </b> {{$companyInformation->company_name}}</td>--}}
            {{--<td><b>Total Present :</b> {{$companyInformation->company_address1}}</td>--}}
            {{--<td><b>Total Absent :</b> {{$companyInformation->company_email}}</td></tr>--}}
            {{--<tr><td><b>On Leave :</b> {{$companyInformation->company_phone}}</td><br>--}}
            {{--<td><Attendance report for <b>{{\Carbon\Carbon::parse($request->date)->format('d M Y (l)')}} </b></tr>--}}
        {{--</tbody>--}}
    {{--</table>--}}

        @if(!empty($allData))
            <table>
                <thead>
                <tr>
                    {{--<th>SL</th>--}}
                    <th align="center">ID</th>
                    <th>Name</th>
                    @if(!empty($request->coldesignation))
                        <th>Designation</th>
                    @endif
                    @if(!empty($request->coldepartment))
                        <th>Department</th>
                    @endif
                    @if(!empty($request->coljoiningdate))
                        <th>Joining Date</th>
                    @endif
                    @if(!empty($request->colgender))
                        <th>Gender</th>
                    @endif
                    @if(!empty($request->colunit))
                        <th>Unit</th>
                    @endif
                    @if(!empty($request->colfloor))
                        <th>Floor</th>
                    @endif
                    @if(!empty($request->colline))
                        <th>Line</th>
                    @endif
                    @if(!empty($request->colsection))
                        <th>Section</th>
                    @endif
                    @if(!empty($request->colstatus))
                        <th>Status</th>
                    @endif
                    <th>In Time</th>
                    @if(!empty($request->collateTime))
                        <th>Late Time</th>
                    @endif
                    <th>Out Time</th>
                    @if(!empty($request->colOverTime))
                        <th>O.T.</th>
                    @endif
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @php $i=0 @endphp
                @foreach($allData as $employee)
                    <tr>
                        {{--<td valign="middle">{{sprintf('%02d', ++$i)}}</td>--}}
                        <td align="center">{{$employee->employeeId}}</td>
                        <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>
                        @if(!empty($request->coldesignation))
                            <td>{{$employee->designation}}</td>
                        @endif
                        @if(!empty($request->coldepartment))
                            <td>{{$employee->departmentName}}</td>
                        @endif
                        @if(!empty($request->coljoiningdate))
                            <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('Y-m-d')}}</td>
                        @endif
                        @if(!empty($request->colgender))
                            <td>
                                @if($employee->empGenderId==1)
                                    Male
                                @endif
                                @if($employee->empGenderId==2)
                                    Female
                                @endif
                                @if($employee->empGenderId==3)
                                    Other
                                @endif
                            </td>
                        @endif
                        @if(!empty($request->colunit))
                            <td>{{$employee->unitName}}</td>
                        @endif
                        @if(!empty($request->colfloor))
                            <td>{{$employee->floorName}}</td>
                        @endif
                        @if(!empty($request->colline))
                            <td>{{$employee->LineName}}</td>
                        @endif
                        @if(!empty($request->colsection))
                            <td>{{$employee->empSection}}</td>
                        @endif
                        @if(!empty($request->colstatus))
                            <td>
                                @if($employee->empAccStatus==1)
                                    Active
                                @elseif($employee->empAccStatus==0)
                                    Inactive
                                @endif
                            </td>
                        @endif
                        <td>
                            @if(isset($employee->in_time))
                                {{$employee->in_time}}
                            @endif
                        </td>

                        @if(!empty($request->collateTime))
                            <td>
                                @if(isset($employee->in_time))
                                    @if($employee->in_time>$employee->max_entry_time)
                                        {{--{{\Carbon\Carbon::now()->toDateTimeString()}}--}}
                                        {{--{{strtotime($employee->in_time)->diffInSeconds(strtotime($employee->max_entry_time))}}--}}
                                        {{--                                                    {{ \Carbon\Carbon::createFromFormat('H:i:s',strtotime($employee->in_time) - strtotime($employee->max_entry_time)) }}--}}
                                        {{--                                                    {{new \Carbon\Carbon($employee->in_time)->diff(new Carbon($attrec->in_time))->format('%h:%I') }}}}--}}

                                        {{ gmdate('H:i:s', (strtotime($employee->in_time) - strtotime($employee->max_entry_time)))}}

                                    @endif
                                @endif
                            </td>
                        @endif
                        <td>
                            @if(isset($employee->out_time))
                                @if($employee->in_time==$employee->out_time)
                                    <span class="red-text">Not Given</span>

                                @elseif($employee->out_time>$overtime_end)
                                    {{$overtime_end}}
                                @else
                                    {{$employee->out_time}}
                                @endif
                            @endif
                        </td>
                        @if(!empty($request->colOverTime))
                            <td>
                                @if(isset($employee->out_time))
                                    @if($employee->out_time>$employee->exit_time)
                                        @if(date('H:i:s', strtotime($employee->out_time))>date('H:i:s',strtotime($overtime_end)) )
                                            {{ date('H:i:s', strtotime($overtime_end) - strtotime($out_time)) }}
                                        @else
                                            {{ date('H:i:s', strtotime($employee->out_time) - strtotime($employee->exit_time)) }}
                                        @endif
                                    @endif
                                @endif
                            </td>
                        @endif
                        <td>
                            @if(isset($employee->in_time))
                                @if($employee->in_time>$lateTime)
                                    <?php $late++; ?>
                                    <P style="color: #0A246A">Late</P>
                                @else
                                    <p style="color: #0b4d3f;">Present</p>
                                @endif
                            @else
                                @if(isset($employee->Status))
                                    <P style="color: #0A246A">On Leave</P>
                                @else
                                    <p style="color: #FF0000">Absent</p>
                                @endif

                            @endif

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>


            <table>
                <tbody>
                    <tr><td><b>Total Employee: </b> {{ $countAbsent+$countPresent+$totalL }}</td>
                    <td><b>Total Present :</b> {{ ($countPresent) }}</td>
                    <td><b>Total Absent :</b> {{ $countAbsent }}</td></tr>
                    <tr><td><b>On Leave :</b> {{ $totalL }}</td><br>
                        <td><b>Total Late :</b> {{ $late }}</td><br></tr>
                </tbody>
            </table>



        @else
            <hr>
            <h4 style="color:red;"><center> No Matched data found.</center></h4>
    @endif

</body>
</html>




