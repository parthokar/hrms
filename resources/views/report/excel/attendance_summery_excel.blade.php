@php(
    $totalEmployee=\Illuminate\Support\Facades\DB::table('employees')->where('empAccStatus','=','1')->count()
)


        <!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Attendance Summery</title>
</head>
<body>

<div>
    <table>
        <tbody>
        <tr><td></td><td colspan="4" align="center"><h2><b>{{$companyInformation->company_name}}</b></h2></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_address1}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_email}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_phone}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center">Attendance summery from <b>{{\Carbon\Carbon::parse($request->date)->format('d M Y')}} </b> to <b> {{\Carbon\Carbon::parse($request->endDate)->format('d M Y')}} </b></td></tr>

        </tbody>
    </table>



    <center>

        @if(!empty($totalAttendance))
            <table>
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Day</th>
                    <th>Total Present</th>
                    <th>Total Late</th>
                    <th>Total Absent</th>
                    <th>On Leave</th>
                </tr>
                </thead>
                <tbody>
                @foreach($totalAttendance as $t)
                    <tr>
                        <td>{{\Carbon\Carbon::parse($t->date)->format('d M Y')}}</td>
                        <td>{{\Carbon\Carbon::parse($t->date)->format('l')}}</td>
                        @if($t->key==2)
                            <td style="color:#FF0000;">Weekend</td>
                            <td style="color:#FF0000;">Weekend</td>
                            <td style="color:#FF0000;">Weekend</td>
                            <td style="color:#FF0000;">Weekend</td>

                        @elseif($t->key==1)
                            <td style="color:#00aa00;">Festival Holiday</td>
                            <td style="color:#00aa00;">Festival Holiday</td>
                            <td style="color:#00aa00;">Festival Holiday</td>
                            <td style="color:#00aa00;">Festival Holiday</td>

                        @else
                            <td>{{$t->attendance}}</td>
                            <td>{{$t->late}}</td>
                            <td>{{$totalEmployee-($t->attendance+$t->leaves)}}</td>
                            <td>{{$t->leaves}}</td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <hr>
            <h4 style="color:red;"><center> No Matched data found.</center></h4>
    @endif

</div>

</body>
</html>




