@extends('layouts.master')
@section('title', 'Earn Leave Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 ><i class="fa fa-users "></i> <strong>Earn Leave Report</strong></h3>
                                
                            </div>
                        
                        </div>

                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-striped table-bordered">
                            <thead style="font-size:15px;font-weight:bold;">
                            <tr>
                                <th>id</th>
                                <th>Name</th>
                               
                                
                                @if(!empty($request->coldesignation))
                                <th>Designation</th>
                                @endif
                                <th>Total Earn Leave</th>
                                <th>Enjoyed</th>
                                <th>Available</th>
                                <th>Previous Month Salary</th>
                                <th>Per Day Salary</th>
                                <th>Payable Earn Leave Benefit</th>
                                @if(!empty($request->coldepartment))
                                <th>Department</th>
                                @endif
                                @if(!empty($request->coljoiningdate))
                                <th>Joining Date</th>
                                @endif
                                @if(!empty($request->colunit))
                                <th>Unit</th>
                                @endif
                                @if(!empty($request->colfloor))
                                <th>Floor</th>
                                @endif
                               
                                @if(!empty($request->colsection))
                                <th>Section</th>
                                @endif
                            
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($employee_benefit as $employee)
                            @php
                            $previousMonthDay=date("t", mktime(0,0,0, date("n") - 1));
                            $payPerDay=($employee->net_amount)/$previousMonthDay;
                            $totalBenefit=($employee->leave_available)*$payPerDay;
                           
                            @endphp
                            <tr>
                                <td>{{$employee->employeeId}}</td>
                                <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>
                                @if(!empty($request->coldesignation))
                                <td>{{$employee->designation}}</td>
                                @endif
                                <td>{{$employee->total_days}}</td>
                                <td>{{$employee->leave_taken}}</td>
                                <td>{{$employee->leave_available}}</td>
                                <td>{{number_format(($employee->net_amount))}} BDT</td>
                                <td>{{number_format(($payPerDay),2)}} BDT</td>
                                <td>{{number_format(($totalBenefit),2)}} BDT</td>
                               
                                @if(!empty($request->coldepartment))
                                <td>{{$employee->departmentName}}</td>
                                @endif
                                @if(!empty($request->coljoiningdate))
                                <td>{{date("d-M-Y",strtotime($employee->empJoiningDate))}}</td>
                                @endif
                            
                                @if(!empty($request->colunit))
                                    <td>{{$employee->unitName}}</td>
                                @endif
                                @if(!empty($request->colfloor))
                                    <td>{{$employee->floorName}}</td>
                                @endif
                               
                                @if(!empty($request->colsection))
                                <td>{{$employee->empSection}}</td>
                                @endif
                               
                            </tr>
                            @endforeach
                            @foreach($employee_without_leave_benefit as $leave)
                                        @php
                                        $previousMonthDay=date("t", mktime(0,0,0, date("n") - 1));
                                        $payPerDay=($leave->net_amount)/$previousMonthDay;
                                        @endphp
                                        <tr>
                                           
                                            <td>{{$leave->employeeId}}</td>
                                            <td>{{$leave->empFirstName}} {{$leave->empLastName}}</td>
                                            @if(!empty($request->coldesignation))
                                            <td>{{$leave->designation}}</td>
                                            @endif
                                            @foreach($earnLeave as $earn)
                                            @php 
                                            $totalBenefit=($earn->total_days)*$payPerDay;
                                            @endphp
                                            <td>{{$earn->total_days}}</td>
                                            <td>0</td>
                                            <td>{{$earn->total_days}}</td>
                                            <td>{{number_format(($leave->net_amount))}} BDT</td>
                                            <td>{{number_format(($payPerDay),2)}} BDT</td>
                                            <td>{{number_format(($totalBenefit),2)}} BDT</td>
                                        
                                            @endforeach
                                            @if(!empty($request->coldepartment))
                                            <td>{{$leave->departmentName}}</td>
                                            @endif
                                            @if(!empty($request->coljoiningdate))
                                            <td>{{date("d-M-Y",strtotime($leave->empJoiningDate))}}</td>
                                            @endif
                                        
                                            @if(!empty($request->colunit))
                                                <td>{{$leave->unitName}}</td>
                                            @endif
                                            @if(!empty($request->colfloor))
                                                <td>{{$leave->floorName}}</td>
                                            @endif
                                           
                                            @if(!empty($request->colsection))
                                            <td>{{$leave->empSection}}</td>
                                            @endif
                                        </tr>
                                           
                                    @endforeach
                          </tbody>
                        </table>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
  @include('include.copyright')
@endsection