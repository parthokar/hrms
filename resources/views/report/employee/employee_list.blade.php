@php
use App\Http\Controllers\dashboardcontroller;
@endphp
@extends('layouts.master')
@section('title', 'Employee List Report')
@section('content')
  <div class="page-content ">
      <div class="row panel"  style="border:1px solid #999">

            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Employee Report</b></h2>
                    <hr>
                </div>
            </div>


              {!! Form::open(['method'=>'POST','action'=>'ReportController@viewEmployeeListReport', 'target' => '_blank']) !!}
                <div class="col-xlg-7 col-md-10 col-md-offset-1 col-xlg-offset-2">

                    <div class="form-group">
                      <div class="input-form-gap"></div>
                        <label class="col-md-3">Unit<span class="clon">:</span></label>
                        <div class="col-md-9">
                            <select name="unitId" class="form-control"  data-search="true">
                              <option value="0">All</option>
                              @foreach($units as $unit)
                                <option value="{{$unit->id}}">{{$unit->name}}</option>
                              @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                      <div class="input-form-gap"></div>
                        <label class="col-md-3">Floor<span class="clon">:</span></label>
                         <div class="col-md-9">
                            <select name="floorId" class="form-control floor"  data-search="true">
                              <option value="0">All</option>
                              @foreach($floors as $floor)
                                <option value="{{$floor->id}}">{{$floor->floor}}</option>
                              @endforeach
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="input-form-gap"></div>
                        <label class="col-md-3">Line<span class="clon">:</span></label>
                         <div class="col-md-9">
                            <select class="form-control floor_lines"  data-search="true"></select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="input-form-gap"></div>
                        <label class="col-md-3">Section<span class="clon">:</span></label>
                        <div class="col-md-9">
                          <select name="sectionName" class="form-control"  data-search="true">
                            <option value="">All</option>
                            @foreach($sections as $section)
                              <option value="{{$section->empSection}}">{{$section->empSection}}</option>
                            @endforeach
                          </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="input-form-gap"></div>
                        <label class="col-md-3">Department<span class="clon">:</span></label>
                        <div class="col-md-9">
                          <select name="departmentId" class="form-control"  data-search="true">
                            <option value="0">All</option>
                            @foreach($departments as $department)
                              <option value="{{$department->id}}">{{$department->departmentName}}</option>
                            @endforeach
                          </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="input-form-gap"></div>
                        <label class="col-md-3">Designation<span class="clon">:</span></label>
                        <div class="col-md-9">
                          <select name="designationId" class="form-control"  data-search="true">
                            <option value="0">All</option>
                            @foreach($designations as $designation)
                              <option value="{{$designation->id}}">{{$designation->designation}}</option>
                            @endforeach
                          </select>
                        </div>
                    </div>

                    <div class="form-group">
                      <div class="input-form-gap"></div>
                        <label class="col-md-3">Skill/Process<span class="clon">:</span></label>
                        <div class="col-md-9">
                          <select name="skill_level" class="form-control"  data-search="true">
                            <option value="">All</option>
                            @foreach($skill_level as $sl)
                              <option value="{{$sl->skill_level}}">{{$sl->skill_level}}</option>
                            @endforeach
                          </select>
                        </div>
                    </div>

                    <div class="form-group">
                      <div class="input-form-gap"></div>
                        <label class="col-md-3">Gender<span class="clon">:</span></label>
                         <div class="col-md-9">
                            <select name="empGenderId" class="form-control"  data-search="true">
                              <option value="0">All</option>
                               <option value="1">Male</option>
                               <option value="2">Female</option>
                               <option value="3">Other</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="input-form-gap"></div>
                      <label class="col-md-3">Employee Status<span class="clon">:</span></label>
                      <div class="col-md-9">
                          <select class="form-control" name="accStatus">
                            <option selected value="1">Active</option>
                            <option value="0">InActive</option>
                          </select>
                      </div>
                    </div>

                    <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Sort By<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select class="form-control" name="reportType">
                          <option  value="employeeId">Employee ID</option>
                          <option  value="empFirstName">Employee name</option>
                          <option  value="empDesignationId">Designation</option>
                          <option  value="empDepartmentId">Department</option>
                          <option  value="line_id" selected="">Line </option>
                          <option  value="empSection">Section </option>
                        </select>
                    </div>
                  </div>

                    <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Sorting Order<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select class="form-control" name="reportOrder">
                          <option selected="" value="ASC">Ascending (ASC)</option>
                          <option value="DESC">Descending (DESC)</option>
                        </select>
                    </div>
                  </div>

                    <div class="form-group">
                      <div class="input-form-gap"></div>
                      <div class="input-form-gap"></div>
                      <label class="col-md-3">Printable Column<span class="clon">:</span></label>
                      <div class="col-md-9">
                        <div class="form-control">
                          
                          <input type="checkbox" checked name="coldepartment" value="1"> Department  &nbsp;&nbsp;
                          <input type="checkbox" checked name="coldesignation" value="1"> Designation  &nbsp;&nbsp;
                          <input type="checkbox" checked name="colgender" value="1"> Gender  &nbsp;&nbsp;
                          <input type="checkbox" name="coljoiningdate" value="1"> Joiningdate  &nbsp;&nbsp;
                          <input type="checkbox" name="colskill_level" value="1"> Skill/Process &nbsp;&nbsp;<br><br>
                          <input type="checkbox" name="colattstatus" value="1"> Attendance Status &nbsp;&nbsp;
                          <input type="checkbox" name="colunit" value="1"> Unit  &nbsp;&nbsp;
                          <input type="checkbox" name="colfloor" value="1"> Floor &nbsp;&nbsp;
                          <input type="checkbox" checked name="colline" value="1"> Line &nbsp;&nbsp;
                          <input type="checkbox" name="colsection" value="1"> Section &nbsp;&nbsp;
                          <input type="checkbox" name="colstatus" value="1"> Status &nbsp;&nbsp;
                          <input type="checkbox" name="coldob" value="1"> Date of Birth &nbsp;&nbsp;<br><br>
                          <input type="checkbox" name="colotstatus" value="1"> OT Status &nbsp;&nbsp;
                          <input type="checkbox" name="colsalary" value="1"> Salary &nbsp;&nbsp;
                          <input type="checkbox" name="colbank" value="1"> Bank A/C

                        </div>  
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="input-form-gap"></div>
                      <div class="input-form-gap"></div>
                      <label class="col-md-3">Additional Printable Column<span class="clon">:</span><br />(Only for Excel Export)</label>
                      <div class="col-md-9">
                        <div class="form-control">
                          
                          <input type="checkbox" name="colphone" value="1"> Phone Number &nbsp;&nbsp;
                          <input type="checkbox" name="colnid" value="1"> National ID  &nbsp;&nbsp;
                          <input type="checkbox" name="colCurrentAddress" value="1"> Current Address  &nbsp;&nbsp;
                          <input type="checkbox" name="colPermanentAddress" value="1"> Permanent Address  <br><br>
                          <input type="checkbox" name="colReligion" value="1"> Religion  &nbsp;&nbsp;
                          <input type="checkbox" name="colFatherName" value="1"> Father's Name &nbsp;&nbsp;
                          <input type="checkbox" name="colMotherName" value="1"> Mother's Name &nbsp;&nbsp;
                          <input type="checkbox" name="colBiometricID" value="1"> Biometric ID &nbsp;&nbsp;
                          <input type="checkbox" name="colPaymentMode" value="1"> Payment Mode &nbsp;&nbsp;
                            <br>
                            <br>
                          <input type="checkbox" name="colWorkingGroup" value="1"> Working Group  &nbsp;&nbsp;
                          <input type="checkbox" name="colBloodGroup" value="1"> Blood Group &nbsp;&nbsp;
                          <input type="checkbox" name="colCard" value="1"> Proxy ID &nbsp;&nbsp;
                          <input type="checkbox" name="colShift" value="1"> Work Shift &nbsp;&nbsp;
{{--                          <input type="checkbox" name="colbank" value="1"> Bank A/C--}}


                        </div>  
                      </div>
                    </div>
<!--                                     
                    <div class="form-group">
                      <div class="input-form-gap"></div>
                      <div class="input-form-gap"></div>
                      <label class="col-md-3">Page Size<span class="clon">:</span></label>
                      <div class="col-md-9">
                          <select class="form-control" name="pagesize">
                            <option selected value="A4">A4</option>
                            <option value="Legal">Legal</option>
                            <option value="Letter">Letter</option>
                          </select>
                      </div>
                    </div>
    
                    <div class="form-group">
                      <div class="input-form-gap"></div>
                      <label class="col-md-3">Page Orientation<span class="clon">:</span></label>
                      <div class="col-md-9">
                          <select class="form-control" name="pageOrientation">
                            <option selected value="Portrait">Portrait</option>
                            <option value="Landscape">Landscape</option>
                          </select>
                      </div>  
                    </div> -->
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <div class="form-group">
                    <div class="input-form-gap"></div>
                        <div class="input-form -gap"></div>
                        <label class="col-md-3">Excel Extension<span class="clon">:</span><br />(Only for Excel Export)</label>
                        <div class="col-md-9">
                            <select class="form-control" name="extensions">
                                <option selected value="xls">XLS</option>
                                <option value="xlsx">XLSX</option>
                                {{--<option value="csv">CSV</option>--}}
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                          <hr>
                          <button type="submit" value="Preview" name="viewType" class="btn btn-success"><i class="fa fa-list"></i> &nbsp;Preview</button>
                          <button type="submit" value="Generate PDF" name="viewType" class="btn btn-primary"><i class="fa fa-print"></i> &nbsp;Print </button>
                          <button type="submit" value="GenerateExcel" name="viewType" class="btn btn-blue"><i class="fa fa-file-excel-o"></i> &nbsp; Export as Excel </button>
                          <hr>
                        </div>
                    </div>

                </div>
                </div>
             {!! Form::close() !!}
      </div>
  </div>

<script>
  $(document).ready(function () {
      $("select.floor").change(function () {
          var floor= $(".floor option:selected").val();
          if(!floor){
              var rr='<select class="form-control floor_lines" name="line_id">'+
              '<option value="">All</option>'+

              '</select>';
              $('.floor_lines').html(rr);
          }
          else {
              $.ajax({
                  url: 'floor/line/' + floor,
                  method: 'get',
                  dataType: 'html',
                  success: function (response) {
                      $('.floor_lines').html(response);
                  }
              });
          }
      });
  });
</script>

@include('include.copyright')
@endsection