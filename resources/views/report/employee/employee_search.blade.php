@php
use App\Http\Controllers\dashboardcontroller;
@endphp
@extends('layouts.master')
@section('title', 'Employee Search')
@section('content')
    <div class="page-content ">
        <div class="row panel"  style="border:1px solid #999">

            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Search Employee</b></h2>
                    <hr>
                </div>
            </div>


            {!! Form::open(['method'=>'POST','action'=>'ReportController@showEmployee']) !!}
            <div class="col-xlg-7 col-md-10 col-md-offset-1 col-xlg-offset-2">

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Name or ID<span class="clon">:</span></label>
                    <div class="col-md-9">
                        {!! Form::select('empId',$employees,"Type Here...",['class'=>'form-control', 'id'=>"select2-limit", 'data-search'=>'true'])!!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-9 col-md-offset-3">
                        <hr>
                        <button type="submit" value="Preview" name="viewType" class="btn btn-success"><i class="fa fa-list"></i> &nbsp;Preview</button>
                        <hr>
                    </div>
                </div>

            </div>
        </div>
        {!! Form::close() !!}
    </div>

    @include('include.copyright')
@endsection