@php
    $total_amount=0;
    $total_total_hour=0;
@endphp

@extends('layouts.master')
@section('title', 'Extra Overtime Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel panel-default">
                <div class="panel-heading"><b>Extra Overtime Report</div>
                    <div class="panel-body">
                        <p class="pull-right"><button style="margin-top: 17px;" class="btn btn-info pull-right" id="pdf_btn" type="button" onclick="printDiv('print_area')"><i class="fa fa-print"></i> Print</button></p>


                        <div id="print_area">
                        <h3 class="text-center">{{$companyInformation->company_name}}</h3>
                        @if(isset($department))
                        <h4 class="text-center">Department:{{ $department->departmentName}}</h1>
                        @else 
                        <h4 class="text-center">Section:{{ $section->empSection}}</h1>    
                        @endif
                        <table class="table table-hover table-striped table-bordered">
                            <thead>
                            <tr>
                                <th scope="col">Sl No</th>
                                <th scope="col">Emp ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Designation</th>
                                <th scope="col">Basic</th>
                                <th scope="col">Gross Salary</th>
                                <th scope="col">O.T</th>
                                <th scope="col">O.T Rate</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Signature</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $order=0; $ot_hour=0; $total_ot=0; $total_amount=0; $total_basic=0; $total_gross=0; @endphp
                            @foreach($details as $extra)
                            @php

                               $ot_rate=$extra->basic_salary/104;
                               $ac_rate=round($ot_rate,2);
                               $total_hour=$extra->emp_hour+$extra->emp_minute+$extra->friday_hour;
                               if($total_hour==0){
                                continue;
                               }
                               $order++;
                               $total_ot+=$total_hour;
                               $total_basic+=$extra->basic_salary;
                               $total_gross+=$extra->total_salary;
                            @endphp
                            <tr>
                             <td>{{$order}}</td>
                             <td>{{$extra->employee_id}}</td>
                             <td>{{$extra->name}}</td>
                             <td>{{$extra->designation}}</td>
                             <td>{{$extra->basic_salary}}</td>
                             <td>{{$extra->total_salary}}</td>
                             <td>{{$total_hour}}</td>
                             <td>{{$ac_rate}}</td>
                             <td>
                                 @php
                                    $ot_amount=$ac_rate*$total_hour;
                                    $total_amount+=$ot_amount;
                                    $total_total_hour+=$total_hour;
                                 @endphp
                                 {{$ot_amount}}
                             </td>
                             <td></td>
                            </tr>
                          @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                  <td><b>Grand Total:</b></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                <td>{{number_format($total_basic)}}</td>
                                  <td>{{number_format($total_gross)}}</td>
                                  <td>{{$total_ot}}</td>
                                  <td></td>
                                  <td>{{round($total_amount)}}</td>
                                  <td></td>
                                </tr>
                              </tfoot>
                        </table>
                     </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
            function printDiv(divName) {
                var printContents = document.getElementById(divName).innerHTML;
                var originalContents = document.body.innerHTML;
                document.body.innerHTML = printContents;
                window.print();
                document.body.innerHTML = originalContents;
            }
        </script>
@include('include.copyright')
@endsection