@extends('layouts.master')
@section('title', 'Employee Salary Summary Report')
@section('content')
    <div class="page-content">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span style="font-size: 14px;font-weight: bold;">Salary Summery Report Yearly</span>
            </div>
            <div class="panel-body tbl_fonts">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Month</th>
                        <th>Basic</th>
                        <th>House</th>
                        <th>Medical</th>
                        <th>Transport</th>
                        <th>Food</th>
                        <th>Gross</th>
                        <th style="text-align: center;" colspan="2">Overtime</th>
                        <th>Gross Pay</th>
                        <th style="text-align: center;" colspan="3">Deduction</th>
                        <th style="text-align: center;" colspan="4">Bonus</th>
                        <th>Net</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($data)
                        @foreach($data as $item)
                            <tr>
                                <td>{{date('F-Y',strtotime($item->month))}}</td>
                                <td>{{$item->basic}} Taka</td>
                                <td>{{$item->house}} Taka</td>
                                <td>{{$item->medicals}} Taka</td>
                                <td>{{$item->transports}} Taka</td>
                                <td>{{$item->foods}} Taka</td>
                                <td>{{$item->gorss}} Taka</td>
                                <td>
                                    @if($item->overtimes=='')
                                        Hour: 0/Hr
                                    @else
                                        Hour: {{$item->overtimes}} /Hr
                                    @endif
                                </td>
                                <td>
                                    @if($item->ot_amounts=='')
                                        Amount: 0 Taka
                                    @else
                                        Amount: {{$item->ot_amounts}} Taka
                                    @endif
                                </td>
                                <td>{{$item->gross_pay}} Taka</td>
                                <td>
                                    @if($item->nor_deduction=='')
                                        Normal:0
                                    @else
                                        Normal:{{$item->nor_deduction}} Taka
                                    @endif
                                </td>
                                <td>
                                    @if($item->advanced_deduction=='')
                                        Advanced:0
                                    @else
                                        Advanced:{{$item->advanced_deduction}}
                                    @endif
                                </td>
                                <td>
                                    @if($item->a_deduction_amount=='')
                                        Absent:0
                                    @else
                                        Absent: {{$item->a_deduction_amount}} Taka
                                    @endif
                                </td>

                                <td>
                                    @if($item->att_bonus=='')
                                        Attendance: 0 Taka
                                    @else
                                        Attendance:: {{$item->att_bonus}} Taka
                                    @endif
                                </td>

                                <td>
                                    @if($item->festival_bonus=='')
                                        Festival: 0 Taka
                                    @else
                                        Festival: {{$item->festival_bonus}} Taka
                                    @endif
                                </td>
                                <td>
                                    @if($item->increment_bonus=='')
                                        Increment: 0 Taka
                                    @else
                                        Increment:  {{$item->increment_bonus}} Taka
                                    @endif
                                </td>
                                <td>
                                    @if($item->p_bonus=='')
                                        Production: 0 Taka
                                    @else
                                        Production: {{$item->p_bonus}} Taka
                                    @endif
                                </td>
                                <td>
                                    @if($item->net_total=='')
                                        Net: 0 Taka
                                    @else
                                        Net: {{$item->net_total}} Taka
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <span style="text-align: center; font-size: 12px;color:red;">Sorry no data found</span>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
    @include('include.copyright')
@endsection