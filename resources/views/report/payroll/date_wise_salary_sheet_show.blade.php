<!DOCTYPE html>
<html>
<head>
    <title>Salary Sheet Date Wise</title>
    <style>
        .company{
            text-align: center;
        }
        .company p{
            padding: 0;
            margin: 0;
            font-size: 17px;
        }
        .main_div{
            width:100%;
            margin: 5px;
        }
        .department_text{
            width: 10%;
            float: left;
        }
        .department_name{
            width: 10%;
            float: left;
        }
        .days_in_month{
            width: 20%;
            float: right;
        }
        .payment_date{
            width: 60%;
            float: left;
        }
        .section_line{
            width: 100%;
            float: left;
            margin-top: 5px;
            padding-bottom: 5px;
        }

        .salary_table{
            width: 100%;
            margin-top:15px;
        }

        .authority{
            width:100%;
            padding-top: 60px;
        }
        .prepared_by{
            width: 25%;
            float: left;
        }
        .audited_by{
            width: 25%;
            float: left;
        }
        .recomended_by{
            width: 25%;
            float: left;
        }
        .approved_by{
            width: 25%;
            float: left;
        }
        .table_1{

            border-collapse: collapse;
            width: 100%;
            font-size:12px;
        }

        .table_1 tr th,td{

            border: 1px solid black;
            text-align: center;
        }

        .table_lefttd{
            padding-left:5px;
            text-align: left !important;
        }
        .table2{
            width: 100%;
            font-size:12px;
        }

        .table2 tr>td{
            border: none;
            text-align: left;
        }

        @media print {
            .btn_hidden{
                display: none;
            }
            .page-break { page-break-after : always; }
            .table_1{
                width: 100%;
                font-size:12px;
            }

            .table_1 tr td{
                text-align: center;
            }

            .table_lefttd{
                text-align: left !important;
            }
        }


    </style>
</head>
<body>
<button style="margin-top: 17px;" class="btn btn-info btn_hidden" id="pdf_btn" type="button" onclick="printDiv('print_area')">Print</button>
<div id="print_area">
    <div class="company">
        <h3>{{$companyInformation->company_name}}</h3>
        <p>Pay Sheet for the date of</p>
        <p>
          {{$start}} to {{$end}}
        </p>
    </div>
    @php $order=0; @endphp
    @foreach($department as $datas)
        @php
            $dept_id=$datas->department_id;
         $employees=DB::table('tb_salary_history_installment')
         ->leftjoin('employees','tb_salary_history_installment.emp_id','=','employees.id')
         ->leftjoin('payroll_grade','tb_salary_history_installment.grade_id','=','payroll_grade.id')
         ->leftjoin('designations','tb_salary_history_installment.designation_id','=','designations.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('employees.employeeId as em_idss','tb_salary_history_installment.emp_id','tb_salary_history_installment.present','tb_salary_history_installment.weekend','tb_salary_history_installment.holiday','tb_salary_history_installment.total_payable_days','tb_salary_history_installment.basic_salary','tb_salary_history_installment.house_rant','tb_salary_history_installment.medical','tb_salary_history_installment.transport','tb_salary_history_installment.food','tb_salary_history_installment.gross','tb_salary_history_installment.leave','tb_salary_history_installment.total','tb_salary_history_installment.working_day','tb_salary_history_installment.absent','tb_salary_history_installment.gross_pay','tb_salary_history_installment.overtime','tb_salary_history_installment.overtime_rate','tb_salary_history_installment.overtime_amount','tb_salary_history_installment.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','tb_salary_history_installment.emp_id as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.employeeId')
         ->where('tb_salary_history_installment.start_date',$start)
         ->where('tb_salary_history_installment.end_date',$end)
         ->where('employees.date_of_discontinuation','=',null)
         ->where('tb_salary_history_installment.department_id','=',$dept_id)
         ->get();
        @endphp
        <div class="main_div  page-break ">
            <table class="table2">
                <tr>
                    <td>
                        <span>Department:</span>
                        <span><b>{{$datas->departmentName}}</b></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <span>Section/Line: <b>{{$datas->line_no}}</b></span>

                    </td>
                </tr>
            </table>
            <div class="salary_table">
                <table class="table_1">
                    <tr>
                        <th rowspan="2">SN</th>
                        <th rowspan="2">EmpID</th>
                        <th rowspan="2" class="table_lefttd">Name</th>
                        <th rowspan="2" class="table_lefttd">Designation</th>
                        <th rowspan="2" class="table_lefttd">Grade</th>
                        <th rowspan="2">Basic</th>
                        <th rowspan="2">House</th>
                        <th rowspan="2">Medical</th>
                        <th rowspan="2">Food</th>
                        <th rowspan="2">Transport</th>
                        <th rowspan="2">Gross</th>
                        <th colspan="4">Leave Day</th>
                        <th rowspan="2">WorkDays</th>
                        <th rowspan="2">Abs Days</th>
                        <th rowspan="2">Weekend</th>
                        <th rowspan="2">Holiday</th>
                        <th rowspan="2">Total Payable days</th>
                        <th rowspan="2">Gross Pay</th>
                        <th colspan="3">Overtime</th>
                        <th rowspan="2">Net wages</th>
                        <th rowspan="2">Signature</th>
                    </tr>
                    <tr>
                        <td>
                            CL
                        </td>
                        <td>
                            SL
                        </td>
                        <td>
                            EL
                        </td>
                        <td>
                            ML
                        </td>
                        <td>
                            Hrs
                        </td>
                        <td>
                            Rate
                        </td>
                        <td>
                            Amt
                        </td>
                    </tr>
                    @foreach($employees as $salary)
                        @php
                            $order++;
                            $test=$salary->emp_id;
                                  $leave_res=DB::SELECT("SELECT tb_salary_history_installment.emp_id as main_id,leave_of_employee.leave_type_id,leave_of_employee.total
                                  FROM tb_salary_history_installment
                                  LEFT JOIN leave_of_employee ON leave_of_employee.emp_id=tb_salary_history_installment.emp_id
                                  WHERE YEAR(leave_of_employee.month)='$yearss' AND MONTH(leave_of_employee.month)='$monthss' AND tb_salary_history_installment.dates='$month' AND tb_salary_history_installment.emp_id=$test");
                        @endphp
                        <tr>
                            <td>{{$order}}</td>
                            <td>{{$salary->em_idss}}</td>
                            <td class="table_lefttd">{{$salary->e_first_name}}</td>
                            <td class="table_lefttd">{{$salary->designation}}</td>
                            <td class="table_lefttd">{{$salary->grade_name}}</td>
                            <td>{{round($salary->basic_salary,0)}}</td>
                            <td>{{round($salary->house_rant,0)}}</td>
                            <td>{{round($salary->medical,0)}}</td>
                            <td>{{round($salary->food,0)}}</td>
                            <td>{{round($salary->transport,0)}}</td>
                            <td>{{round($salary->gross,0)}}</td>
                            @if($leave_res)
                                <td>
                                    @foreach($leave_res as $leave_resss)
                                        @if($leave_resss->leave_type_id==6)
                                            {{$leave_resss->total}}
                                        @endif
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($leave_res as $leave_resss)
                                        @if($leave_resss->leave_type_id==5)
                                            {{$leave_resss->total}}
                                        @endif
                                    @endforeach
                                </td>
                                <td>
                                    0
                                </td>
                                <td>
                                    0
                                </td>
                            @else
                                <td>
                                    0
                                </td>
                                <td>
                                    0
                                </td>
                                <td>
                                    0
                                </td>
                                <td>
                                    0
                                </td>
                            @endif

                            <td>
                                @if($salary->present=='')
                                    0
                                @else
                                    {{$salary->present}}
                                @endif
                            </td>
                            <td>
                                @if($salary->absent=='')
                                    0
                                @else
                                    {{$salary->absent}}
                                @endif
                            </td>
                            <td>{{$salary->weekend}}</td>
                            <td>
                                @if($salary->holiday=='')
                                    0
                                @else
                                    {{$salary->holiday}}
                                @endif
                            </td>
                            <td>{{$salary->total_payable_days}}</td>
                            <td>
                                @if($salary->gross_pay=='')
                                    0
                                @else
                                    {{round($salary->gross_pay,0)}}
                                @endif
                            </td>
                            <td>
                                @if($salary->overtime=='')
                                    0
                                @else
                                    {{$salary->overtime}}
                                @endif
                            </td>
                            <td>
                                @if($salary->overtime_rate=='')
                                    0
                                @else
                                    {{$salary->overtime_rate}}
                                @endif
                            </td>
                            <td>
                                @if($salary->overtime_amount=='')
                                    0
                                @else
                                    {{round($salary->overtime_amount,0)}}
                                @endif
                            </td>
                            <td>
                                {{round($salary->net_amount,0)}}
                            </td>
                            <td></td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    @endforeach
    <div class="authority">
        <div class="prepared_by">
            <span>Prepared By:</span>
        </div>
        <div class="audited_by">
            <span>Audited By:</span>
        </div>
        <div class="recomended_by">
            <span>Recommended By:</span>
        </div>
        <div class="approved_by">
            <span>Approved By:</span>
        </div>
    </div>
</div>

<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>
</body>
</html>