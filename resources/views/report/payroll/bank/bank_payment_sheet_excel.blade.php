


<!DOCTYPE html>
<html>
<head>
    <title>Bank Payment Sheet</title>
</head>
<body>
{{-- <button style="margin-top: 17px;" class="btn btn-info" id="pdf_btn" type="button" onclick="printDiv('print_area')"><i class="fa fa-print"></i> Print</button> --}}
<div id="print_area">
    <div class="main_div">
        <div class="salary_table">
            <table class="table_1">
                <thead>
                <tr style="text-align: center;">
                <tr><td></td><td colspan="5" align="center"><h4><b>{{$companyInformation->company_name}}</b></h4></td></tr>
                <tr><td></td><td colspan="5" align="center"><h4><b>Bank Payment Sheet For {{\Carbon\Carbon::parse($month)->format('F Y')}}</b></h4></td></tr>

                <tr>
                    <th>Sl No</th>
                    <th>Emp ID</th>
                    <th>Name</th>
                    <th>Designation</th>
                    <th>Section</th>
                    <th>Bank A/C</th>
                    <th>Total Amount</th>
                    <th>Remarks</th>
                </tr>
                </thead>
                <tbody>
                @php $order=0; $ot_hour=0; $total_ot=0; $total_amount=0; $basic=0; $gross=0; $ot_rates=0; @endphp
                @foreach($data as $key=>$d)
                    @php
                        $total_amount+=round($d->net_amount);
                    @endphp
                    <tr>
                        <td align="center">{{++$key}}</td>
                        <td>{{$d->employeeId}}</td>
                        <td>{{$d->empFirstName." ".$d->empLastName}}</td>
                        <td>{{$d->designation}}</td>
                        <td>{{$d->empSection}}</td>
                        <td>{{$d->bank_account}}</td>
                        <td>
                            {{round($d->net_amount)}}
                        </td>
                        <td></td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="6" align="right">Total:  </td>
                    <td align="right">
                        {{$total_amount}}
                    </td>
                    <td></td>
                </tr>
                <tr></tr>
                <tr></tr>
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
    </div>
</div>
</body>
</html>

