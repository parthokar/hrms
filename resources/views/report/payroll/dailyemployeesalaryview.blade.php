@extends('layouts.master')
@section('content')
    <div class="page-content">
        <button style="margin-top: 20px;" class="btn btn-info" type="button"  onclick="printDiv('payslip_id')"><i class="fa fa-print"></i> Print</button>
        <div id="payslip_id">
        <h4 style="text-align: center;font-weight: bold;">{{$companyInformation->company_name}}</h4>
        <p style="font-weight: bold;text-align: center;font-size: 16px;padding: 0 4px;">Daily Salary Information for the Date</p>
        <p style="text-align: center">{{$dates_show}}</p>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Line</th>
                            <th>Man Power</th>
                            <th>Salary</th>
                            {{--<th>2 Hours OT Hrs</th>--}}
                            {{--<th>2 hours OT Amt</th>--}}
                            <th>Total OT Hrs</th>
                            <th>Total OT Amt</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $total_man_power=0;$total_salary=0;$two_hoursss_ot=0;$two_hoursss_ot_amountsss=0;$total_ot_hoursss=0;$total_ot_amountsssss=0; @endphp
                        @foreach($select_salary as $salary_info)
                            @php $total_man_power+=$salary_info->man_power;$total_salary+=$salary_info->salary;$two_hoursss_ot+=$salary_info->two_hours_ot;$two_hoursss_ot_amountsss+=$salary_info->two_hours_ot_amount;$total_ot_hoursss+=$salary_info->total_ot_hour;$total_ot_amountsssss+=$salary_info->total_ot_amount; @endphp
                            <tr>
                                <td>
                                    @if($salary_info->line_no=='')
                                        No Line Found
                                        @else
                                        {{$salary_info->line_no}}
                                    @endif
                                </td>
                                <td>{{$salary_info->man_power}}</td>
                                <td>{{$salary_info->salary}}</td>
                                {{--<td>{{$salary_info->two_hours_ot}}</td>--}}
                                {{--<td>{{$salary_info->two_hours_ot_amount}}</td>--}}
                                <td>{{$salary_info->total_ot_hour}}</td>
                                <td>{{$salary_info->total_ot_amount}}</td>
                            </tr>
                         @endforeach
                  </tbody>
                        <tfoot>
                              <tr>
                                 <td>Grand Total:</td>
                                  <td>{{$total_man_power}}</td>
                                  <td>{{$total_salary}}</td>
                                  <td>{{$two_hoursss_ot}}</td>
                                  <td>{{$two_hoursss_ot_amountsss}}</td>
                            </tr>
                      </tfoot>
           </table>
        </div>
      </div>
    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
    @include('include.copyright')
@endsection