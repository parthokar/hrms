
<!DOCTYPE html>
<html>
<head>
    <title>Festival Bonus Sheet</title>
</head>
<body>
        {{-- <button style="margin-top: 17px;" class="btn btn-info" id="pdf_btn" type="button" onclick="printDiv('print_area')"><i class="fa fa-print"></i> Print</button> --}}
<div id="print_area">
        <div class="main_div">
            <div class="salary_table">
                <table class="table_1">
                    <thead>
                        <tr style="text-align: center;">
                            <tr><td></td><td colspan="16" align="center"><h4><b>{{$companyInformation->company_name}}</b></h4></td></tr>
                            <tr><td></td><td colspan="16" align="center"><p style="">{{$companyInformation->company_address1}} <b> 
                           
                           
                            </b> </p></td></tr>
                                <tr><td></td><td colspan="16" align="center"><p><b>Bonus For @if(isset($bonus_title)) {{$bonus_title->bonus_title}} @endif</b></p></td></tr>
                        </tr>
                        <tr></tr>
                        @php $order=0; $total_gross=0;$total_bonus=0;$total_payment=0; @endphp  
                   
                        <tr class="heading-top heading_top_1" style="border:0px;">
                            <th colspan="5" style="text-align:left;border:0px;">
                                <p >Section :@if(isset($section->section_id)){{$section->section_id}}@endif</p>   
                                <p></p>
                             </th>
                            
                            <th colspan="6" style="text-align:right;border:0px;">
                                <p>Payment Date:</p>
                            </th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>Employee ID</th>
                            <th>Name</th>
                            <th>Joining Date</th>
                            <th>Service Length</th>
                            <th>Designation</th>
                            <th>Gross Salary</th>
                            <th>Bonus Amount</th>
                            <th>Payment Amount</th>
                            @if($pp_mode==1)
                            <th>Payment Mode</th>
                            <th>Account No</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>  
                       
                        @foreach($data as $item)
                        @php 
                        $date1s = new DateTime($item->empJoiningDate);
                            $date2s = new DateTime($item->fest_date);
                            $intervals = $date1s->diff($date2s);
                            $year_condition=$intervals->y; 
                            $month_condition=$intervals->m;
                            $day_condition=$intervals->d;
                        $order++; $total_gross+=$item->emp_gross;$total_bonus+=$item->emp_bonus;$total_payment+=$item->emp_bonus; @endphp 
                            <tr>
                                <td>{{$order}}</td>
                                <td>{{$item->employeeId}}</td>
                                <td>{{$item->empFirstName}} {{$item->empLastName}} </td>
                                <td>{{date('d-M-Y',strtotime($item->empJoiningDate))}}</td>
                            <td>{{$year_condition}}Years {{$month_condition}}Month {{$day_condition}} Day</td>
                                <td>{{$item->designation}}</td>
                                <td>{{$item->emp_gross}}</td>
                                <td>{{$item->emp_bonus}}</td>
                                <td>{{$item->emp_bonus}}</td>
                                @if($pp_mode==1)
                                <td>{{$item->payment_mode}}</td>
                                <td>{{$item->bank_account}}</td>
                                @endif 
                            </tr>
                        @endforeach
                              
                                    <tr>
                                        <td>Total</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                      <td>{{$total_gross}}</td>
                                      <td>{{$total_bonus}}</td>
                                      <td>{{$total_payment}}</td>
                                      <td></td>
                                      <td></td>
                                      </tr>
                    <tr></tr>
                    <tr></tr>
                       {{-- <tr>
                            <td colspan="1"></td>
                            <td colspan="1"><p>Prepared By</p></td>
                            <td colspan="1"><p>Audited By</p></td>
                            <td colspan="3"><p>Recommended By</p></td>
                            <td colspan="4"><p>Director</p></td>
                        </tr> --}}
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</body>
</html>

