@extends('layouts.master')
@section('title', 'Festival Bonus Report')
@section('content')
    <div class="page-content">
        @if(Session::has('msg'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('msg') }}</p>
        @endif
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel panel-default">
                    <div class="panel-heading"><b>Festival Bonus Report</b></div>
                    <div class="panel-body">
                            <div class="col-md-12">
                             <h5>Bonus Report</h5>   
                            <div class="col-md-12">
                                <div class="form-group">
                                 <label>Select Type</label>
                                 <select id="ot_type" name="ot_type" class="form-control">
                                          <option value="">Select</option>
                                            <option value="1">Department Wise</option>
                                            <option value="2">Section Wise</option>
                                  </select>
                                </div>
                            </div>
                                <div class="col-md-12" id="department_type" style="display:none">
                                    {{Form::open(array('url' => '/report/employee/festival/department','method' => 'post','target'=>'_blank'))}}
                                    <div class="col-md-12">
                                        <label>Payment Mode</label>
                                        <select class="form-control" name="pp_mode">
                                            <option value="1">On<option>
                                            <option value="0">OFF<option>
                                        </select>
                                    </div>
                                       <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Select Payment Mode</label>
                                           <select class="form-control" name="p_mode">
                                               <option value="All">All</option> 
                                                    @foreach($payment_mode as $mode)
                                                        <option value="{{$mode->payment_mode}}">{{$mode->payment_mode}}</option> 
                                                    @endforeach 
                                           </select>
                                       </div>
                                    </div>
                                       <div class="col-md-3">
                                        <div class="form-group">
                                        <label>Select Department</label>
                                        <select class="form-control" name="dept_id" data-search="true">
                                                <option value="0">All</option>
                                                @foreach($department as $departments)
                                                   <option value="{{$departments->id}}">{{$departments->departmentName}}</option>
                                                @endforeach
                                        </select>
                                        </div>
                                       </div>
                                       <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="form-label">Select Title <span style="color: darkred;font-size: 16px;">*</span></label>
                                                <select class="form-control" name="title_id" data-search="true">
                                                    @foreach($title as $titles)
                                                       <option value="{{$titles->bonus_title}}">{{$titles->bonus_title}}</option>
                                                    @endforeach
                                            </select>
                                            </div>
                                        </div>
                                     

                                        <div class="col-md-12">
                                                <button type="submit" id="please_select_group_monthwise" name="salarymontwise" class="btn btn-info">Preview</button>
                                                <button type="submit" value="dept_excel" name="dept_excel" class="btn btn-success">Export Excel</button>
                                            </div>
                                         {{ Form::close() }}
                                   </div>

                                   <div class="col-md-12" id="section_type" style="display:none">
                                    {{Form::open(array('url' => '/report/employee/festival/section','method' => 'post','target'=>'_blank'))}}
                                    <div class="col-md-12">
                                        <label>Payment Mode</label>
                                        <select class="form-control" name="pp_mode">
                                            <option value="1">On<option>
                                            <option value="0">OFF<option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Select Payment Mode</label>
                                           <select class="form-control" name="p_mode">
                                               <option value="All">All</option> 
                                                    @foreach($payment_mode as $mode)
                                                        <option value="{{$mode->payment_mode}}">{{$mode->payment_mode}}</option> 
                                                    @endforeach 
                                           </select>
                                       </div>
                                    </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Select Section</label>
                                            <select class="form-control" name="sections_id" data-search="true">
                                                   <option value="als">All</option>
                                                    @foreach($section as $sections)
                                                       <option value="{{$sections->empSection}}">{{$sections->empSection}}</option>
                                                    @endforeach
                                            </select>
                                            </div>
                                           </div>
    
                                           <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-label">Select Title <span style="color: darkred;font-size: 16px;">*</span></label>
                                                    <select class="form-control" name="title_id" data-search="true">
                                                        @foreach($title as $titles)
                                                       <option value="{{$titles->bonus_title}}">{{$titles->bonus_title}}</option>
                                                       @endforeach
                                                </select>
                                                </div>
                                            </div>
                                     
                                            <input type="hidden" name="type" value="section_wise_data">
                                            <div class="col-md-12">
                                                    <button type="submit" id="please_select_group_monthwise" name="salarymontwise" class="btn btn-info">Preview</button>
                                                    <button type="submit" value="section_excel" name="section_excel" class="btn btn-success">Export Excel</button>
                                                </div>
                                             {{ Form::close() }}
                                       </div>
                                  </div>



                                  <div class="col-md-12">
                                    <h5>Bonus Summary Report</h5> 
                                    <div class="col-md-12">
                                        <div class="form-group">
                                         <label>Select Type</label>
                                         <select id="ot_types" name="ot_type" class="form-control">
                                                  <option value="">Select</option>
                                                    <option value="1">Department Wise</option>
                                                    <option value="2">Section Wise</option>
                                          </select>
                                        </div>
                                    </div>
                                        <div class="col-md-12" id="department_types" style="display:none">
                                            {{Form::open(array('url' => '/report/employee/festival/department/summary','method' => 'post','target'=>'_blank'))}}
                                               <div class="col-md-4">
                                                <div class="form-group">
                                                <label>Select Department</label>
                                                <select class="form-control" name="dept_id_summary" data-search="true">
                                                        <option value="dept_all">All</option>
                                                        @foreach($department as $departments)
                                                           <option value="{{$departments->id}}">{{$departments->departmentName}}</option>
                                                        @endforeach
                                                </select>
                                                </div>
                                               </div>
                                               <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label">Select Title <span style="color: darkred;font-size: 16px;">*</span></label>
                                                        <select class="form-control" name="title_id" data-search="true">
                                                            @foreach($title as $titles)
                                                            <option value="{{$titles->bonus_title}}">{{$titles->bonus_title}}</option>
                                                         @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            
                                                <div class="col-md-12">
                                                        <button type="submit" id="please_select_group_monthwise" name="salarymontwise" class="btn btn-info">Preview</button>
                                                        <button type="submit" value="dept_excel_summary" name="dept_excel_summary" class="btn btn-success">Export Excel</button>
                                                    </div>
                                                 {{ Form::close() }}
                                           </div>
        
                                           <div class="col-md-12" id="section_types" style="display:none">
                                            {{Form::open(array('url' => '/report/employee/festival/section/summary','method' => 'post','target'=>'_blank'))}}
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Select Section</label>
                                                    <select class="form-control" name="section_id_summary" data-search="true">
                                                           <option value="section_all">All</option>
                                                            @foreach($section as $sections)
                                                               <option value="{{$sections->empSection}}">{{$sections->empSection}}</option>
                                                            @endforeach
                                                    </select>
                                                    </div>
                                                   </div>
            
                                                   <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-label">Select Title <span style="color: darkred;font-size: 16px;">*</span></label>
                                                            <select class="form-control" name="title_id" data-search="true">
                                                                @foreach($title as $titles)
                                                                <option value="{{$titles->bonus_title}}">{{$titles->bonus_title}}</option>
                                                             @endforeach
                                                        </select>
                                                        </div>
                                                    </div>
                                               
                                                    <input type="hidden" name="type" value="section_wise_data">
                                                    <div class="col-md-12">
                                                            <button type="submit" id="please_select_group_monthwise" name="salarymontwise" class="btn btn-info">Preview</button>
                                                            <button type="submit" value="section_excel_summary" name="section_excel_summary" class="btn btn-success">Export Excel</button>
                                                        </div>
                                                     {{ Form::close() }}
                                               </div>
                                          </div>
                                     </div>
                               </div>
                          </div>
                     </div>
                </div>
           </div> 
    <script>
        $(document).ready(function(){


            $('#dept_year').datepicker({
                    changeYear: true,
                    dateFormat: "yy",
                    showButtonPanel: true,
                    currentText: "This Year",
                    onChangeMonthYear: function (year, inst) {
                        $(this).val($.datepicker.formatDate('yy', new Date(year, 1)));
                    },
                    onClose: function(dateText, inst) {
                        var year = $(".ui-datepicker-year :selected").val();
                        $(this).val($.datepicker.formatDate('yy', new Date(year,1)));
                    }
                }).focus(function () {
                    $(".ui-datepicker-calendar").hide();
                });


                $('#section_year').datepicker({
                    changeYear: true,
                    dateFormat: "yy",
                    showButtonPanel: true,
                    currentText: "This Year",
                    onChangeMonthYear: function (year, inst) {
                        $(this).val($.datepicker.formatDate('yy', new Date(year, 1)));
                    },
                    onClose: function(dateText, inst) {
                        var year = $(".ui-datepicker-year :selected").val();
                        $(this).val($.datepicker.formatDate('yy', new Date(year,1)));
                    }
                }).focus(function () {
                    $(".ui-datepicker-calendar").hide();
                });

                $('#dept_summary_year').datepicker({
                    changeYear: true,
                    dateFormat: "yy",
                    showButtonPanel: true,
                    currentText: "This Year",
                    onChangeMonthYear: function (year, inst) {
                        $(this).val($.datepicker.formatDate('yy', new Date(year, 1)));
                    },
                    onClose: function(dateText, inst) {
                        var year = $(".ui-datepicker-year :selected").val();
                        $(this).val($.datepicker.formatDate('yy', new Date(year,1)));
                    }
                }).focus(function () {
                    $(".ui-datepicker-calendar").hide();
                });

                
                $('#section_summary_year').datepicker({
                    changeYear: true,
                    dateFormat: "yy",
                    showButtonPanel: true,
                    currentText: "This Year",
                    onChangeMonthYear: function (year, inst) {
                        $(this).val($.datepicker.formatDate('yy', new Date(year, 1)));
                    },
                    onClose: function(dateText, inst) {
                        var year = $(".ui-datepicker-year :selected").val();
                        $(this).val($.datepicker.formatDate('yy', new Date(year,1)));
                    }
                }).focus(function () {
                    $(".ui-datepicker-calendar").hide();
                });

               

            


          $("#ot_type").change(function(){
            var select_type= $("#ot_type").val();
            if(select_type==1){
              $("#department_type").show();
              $("#month_wise").hide();
              $("#section_type").hide();
              $("#employee_type").hide();
            }

            if(select_type==2){
              $("#section_type").show();
              $("#month_wise").hide();
              $("#department_type").hide();
              $("#employee_type").hide();
            }

            if(select_type==3){
              $("#employee_type").show();
              $("#month_wise").hide();
              $("#department_type").hide();
              $("#section_type").hide();
            }

            if(select_type==4){
              $("#month_wise").show();
              $("#employee_type").hide();
              $("#department_type").hide();
              $("#section_type").hide();
            }
          });



          
          $("#ot_types").change(function(){
            var select_type= $("#ot_types").val();
            if(select_type==1){
              $("#department_types").show();
              $("#section_types").hide();
            }

            if(select_type==2){
              $("#section_types").show();
              $("#department_types").hide();
            }
          });





        });
    </script>
    @include('include.copyright')
@endsection
