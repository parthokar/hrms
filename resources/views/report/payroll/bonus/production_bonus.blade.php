@extends('layouts.master')
@section('title', 'Production Bonus Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel panel-default">
                    <div class="panel-heading"><b>Production Bonus Report</b></div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            {{Form::open(array('url' => '/report/production/bonus/employee','method' => 'post'))}}

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Select work Group</label>
                                    <select class="form-control" name="work_group" required>
                                        <option value="">Select</option>
                                        <option value="Staff">Staff</option>
                                        <option value="Worker">Worker</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Select Month</label>
                                    <div class="prepend-icon">
                                        <input type="text" name="year_bonus_month" id="year_bonus_month" autocomplete="off" class="form-control format_date">
                                        <i class="icon-calendar"></i>
                                    </div>
                                    <input type="hidden" id="year_bonus" value="{{URL::to('/report/employee/festival/bonus')}}">
                                </div>
                                <button type="submit"  id="year_bonus_report" class="btn btn-primary">Generate Report</button>
                            </div>
                            {{--<button type="submit"  class="btn btn-success">Download pdf</button>--}}
                            {{--<button type="button"  class="btn btn-primary">Download csv</button>--}}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#year_bonus_month').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm",
                showButtonPanel: true,
                currentText: "This Month",
                onChangeMonthYear: function (year, month, inst) {
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                },
                onClose: function(dateText, inst) {
                    var month = $(".ui-datepicker-month :selected").val();
                    var year = $(".ui-datepicker-year :selected").val();
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                }
            }).focus(function () {
                $(".ui-datepicker-calendar").hide();
            });
        });
    </script>
    @include('include.copyright')
@endsection
