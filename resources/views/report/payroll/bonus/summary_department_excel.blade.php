
<!DOCTYPE html>
<html>
<head>
    <title>Extra ot </title>
</head>
<body>
        {{-- <button style="margin-top: 17px;" class="btn btn-info" id="pdf_btn" type="button" onclick="printDiv('print_area')"><i class="fa fa-print"></i> Print</button> --}}
<div id="print_area">
        <div class="main_div">
            <div class="salary_table">
                <table class="table_1">
                    <thead>
                        <tr style="text-align: center;">
                            <tr><td></td><td colspan="16" align="center"><h4><b>{{$companyInformation->company_name}}</b></h4></td></tr>
                            <tr><td></td><td colspan="16" align="center"><p style="">{{$companyInformation->company_address1}} <b> 
                           
                           
                            </b> </p></td></tr>
                                <tr><td></td><td colspan="16" align="center"><p><b>Bonus For @if(isset($bonus_title)) {{$bonus_title->bonus_title}} @endif</b></p></td></tr>
                        </tr>
                        <tr></tr>
                   
                   
                        <tr>
                            <th>SL</th>
                            <th>Department</th>
                            <th>Man Power</th>
                            <th>Gross Salary</th>
                            <th>Bonus Amount</th>
                            <th>Payment Amount</th>
                        </tr>
                    </thead>
                    <tbody>  
                       
                        @php $order=0; $m_power=0; $total_gross=0;$total_bonus=0;$total_payment=0; @endphp    
                        @foreach($data as $item)
                        @php $order++; $m_power+=$item->man_power; $total_gross+=$item->tot_gross;$total_bonus+=$item->total;$total_payment+=$item->total; @endphp 
                            <tr>
                                <td>{{$order}}</td>
                                <td>{{$item->departmentName}}</td>
                                <td>{{$item->man_power}}</td>
                                <td>{{$item->tot_gross}}</td>
                                <td>{{$item->total}}</td>
                                <td>{{$item->total}}</td>
                            </tr>
                        @endforeach
                              
                                    <tr>
                                        <td>Total</td>
                                        <td></td>
                                        <td>{{$m_power}}</td>
                                        <td>{{number_format($total_gross)}}</td>
                                        <td>{{number_format($total_bonus)}}</td>
                                        <td>{{number_format($total_payment)}}</td>
                                        <td></td>
                                        <td></td>
                                      </tr>
                    <tr></tr>
                    <tr></tr>
                       {{-- <tr>
                            <td colspan="1"></td>
                            <td colspan="1"><p>Prepared By</p></td>
                            <td colspan="1"><p>Audited By</p></td>
                            <td colspan="3"><p>Recommended By</p></td>
                            <td colspan="4"><p>Director</p></td>
                        </tr> --}}
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</body>
</html>

