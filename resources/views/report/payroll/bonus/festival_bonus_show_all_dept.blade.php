<!DOCTYPE html>
<html>
<head>
    <title>Festival Bonus Sheet</title>
    <style>
        @page        {
            size: landscape;
        }

        @media print{@page {size: legal landscape}}

        .verticalTableHeader {
            text-align:center;
            white-space:nowrap;
            g-origin:50% 50%;
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            -o-transform: rotate(90deg);
            transform: rotate(90deg);

        }
        .verticalTableHeader p {
            margin:0 -100% ;
            display:inline-block;
        }
        .verticalTableHeader p:before{
            content:'';
            width:0;
            padding-top:110%;/* takes width as reference, + 10% for faking some extra padding */
            display:inline-block;
            vertical-align:middle;
        }
        .company{
            text-align: center;
        }
        .company p{
            padding: 0;
            margin: 0;
            font-size: 17px;
        }
        .main_div{
            width:100%;
            margin: 0px;
        }
        .salary_table{
            width: 100%;
            margin-top:8px;
        }
        .authority{
            width:100%;
            padding-top: 47px;
        }
        .prepared_by{
            width: 25%;
            float: left;
        }
        .audited_by{
            width: 25%;
            float: left;
        }
        .recomended_by{
            width: 25%;
            float: left;
        }
        .approved_by{
            width: 25%;
            float: left;
        }
        .table_1{
            border-collapse: collapse;
            width: 100%;
            font-size:12px;
        }

        .table_1 tr th,td{
            border: 1px solid black;
            text-align: center;
        }

        .table_lefttd{
            /*padding-left:5px;*/
            text-align: left !important;
        }
        .table2{
            width: 100%;
            font-size:12px;
        }

        .table2 tr>td{
            border: none;
            text-align: left;
        }
        .main_foter{
            border: 1px solid #000000;
            margin-top: -68px;
            /*padding-left: 290px;*/
            font-size: 14px;
            height: 24px;
            line-height: 24px;
        }
        .main_foter span{
            margin-right:6px;
        }

        tfoot{
            width: 100%;
        }


        tfoot tr td{

            width: 100%;
            clear: both;
            border:none;
            padding-top: 0px!important;
            color:#fff;
        }
        .authority span{
            border-top: 1px solid #fff;
            padding-top: 3px;
            padding-left: 15px;
            padding-right: 15px;
        }

        tfoot tr td .main_foter{
            text-align: left;
            border:none;
        }
        tfoot tr td .main_foter span{
            margin-right: 10px;
        }
        .company_heading_1 td{
            border: none;
            line-height: .7;
        }

        .company_heading_2 td{
            border:none;

        }
        tfoot td{
            border:none;
        }
        td.signature_td{
            /*width:70px;
            height:81px;*/
        }
        .dayofmonth{
            display: inline-block;
            padding-right: 40px;
        }
        .company_heading_1 td span{
            display: inline-block;
            margin-bottom: 5px;
        }
        .company_heading_1 h3{
            margin-bottom:2px;
            margin-top: 5px;
            font-size: 30px;
        }
        .company_heading_1 p {
            margin-top: 11px;
            font-size: 16px;
            text-align: center;
            margin-left: 58px;
        }
        .main_foter span:nth-child(1){

            padding-right: 17%;
        }
        .main_foter span:nth-child(2){
            font-size:12px;
        }
        .main_foter span:nth-child(3){
            font-size:12px;
        }
        .main_foter span:nth-child(4){
            font-size:12px;
        }
        .main_foter span:nth-child(5){
            font-size:12px;
        }
        .main_foter span:nth-child(6){
            font-size:12px;
        }
        .main_foter span:nth-child(7){
            font-size:12px;
        }
        .main_foter span:nth-child(8){
            font-size:12px;
            margin-left:6%;
        }
        .main_foter span:nth-child(9){
            font-size:12px;
            margin-left: 9%;
        }
        .main_foter span:nth-child(10){
            font-size:12px;
        }
        .ex_footer{
            opacity:0;
        }


        #signature_11{
            padding-left: 35px;
            padding-right: 35px;

        }
        .signature_td{
            width: 130px!important;
            height: 70px!important;
        }

        @media    print {
            .btn_hidden{
                display: none;
            }
            .main_div{page-break-after: always;}
            table { page-break-inside:auto; }
            tr    { page-break-inside:avoid; page-break-after:auto;}
            thead {display: table-header-group;}
            tbody { page-break-after:always;
                display: table-row-group;}
            tfoot {
                display: table-footer-group;
            }
            /*td.signature_td{
                width:70px;
                height:101px;
            }*/


            .ex_footer{
                opacity:1;
                position: fixed;
                bottom:0px;
                left:0;
                width:100%;
                /* padding-top:20px; */
                /* height:100px; */

            }
            .ex_footer .authority{
                padding-bottom: 0px;
                padding-top:20px;
                text-align:center;


            }
            .ex_footer .authority span{
                font-size:14px;
            }

            tfoot tr td .authority span{
                color:#fff;
                opacity:0;
            }
            tfoot tr td .authority span{
                border-top: 1px solid #fff;
                color:#fff;
                opacity:0;
            }

            .ex_footer tr td .authority span{
                color:#000;
            }
            .ex_footer .authority span{
                border-top: 1px solid #000;
                color:#000;
            }
            .signature_td{
                width: 130px!important;
                height: 85px!important;
            }

            .pageNumber1:after {
                counter-increment: page;
                content:"Page number: " counter(page);
                left: 0;
                top: 100%;
                white-space: nowrap;
                z-index: 20;
                -moz-border-radius: 5px;
                -moz-box-shadow: 0px 0px 4px #222;
                background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
                background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
            }
        }
    </style>
</head>
<body>
<div id="print_area">
    @php $order=0; @endphp
    @foreach($deprtment as  $deprtments)
    @php  $total_gross=0;$total_bonus=0;$total_payment=0; @endphp
    @if($p_mode=='All')
    @php      
    $data=DB::table('festival_bonus')
    ->join('employees','festival_bonus.emp_id','=','employees.id')
    ->leftjoin('designations','employees.empDesignationId','=','designations.id')
    ->select('festival_bonus.*','employees.empFirstName','employees.empLastName','employees.employeeId','employees.work_group','designations.designation','empJoiningDate','payment_mode','bank_account')
    ->where('dept_id',$deprtments->id)
    ->where('bonus_title',$title)
     ->where('emp_bonus','>',0)
    ->orderBy('employeeId','ASC')
    ->get();
    @endphp
      @else 
     @php   
    $data=DB::table('festival_bonus')
    ->join('employees','festival_bonus.emp_id','=','employees.id')
    ->leftjoin('designations','employees.empDesignationId','=','designations.id')
    ->select('festival_bonus.*','employees.empFirstName','employees.empLastName','employees.employeeId','employees.work_group','designations.designation','empJoiningDate','payment_mode','bank_account')
    ->where('dept_id',$deprtments->id)
    ->where('bonus_title',$title)
    ->where('emp_bonus','>',0)
    ->where('payment_mode','=',$p_mode)
    ->orderBy('employeeId','ASC')
    ->get();
     @endphp 
    @endif
        <div class="main_div">
            <div class="salary_table">
                <table class="table_1">
                    <thead>
                    <tr class="table_heading">
                    <tr style="text-align: center;" class="company_heading_1">
                        <td colspan="30">  <h3>{{$companyInformation->company_name}}</h3></td>
                    </tr>
                    <tr style="text-align: center;" class="company_heading_1">
                        <td colspan="30">  <h4>{{$companyInformation->company_address1}}</h4></td>
                    </tr>
                    <tr style="text-align: center;" class="company_heading_1">
                        <td colspan="5"  style="text-align: left;">
                            <span>Department:</span>
                            <span><b>{{$deprtments->departmentName}}</b></span><br>
                        </td>
                        <td colspan="14" >
                            <h2 style="margin-right:450px">Bonus For @if(isset($bonus_title)) {{$bonus_title->bonus_title}} @endif</h2>
                        </td>
                        <td colspan="11" style="text-align: left;">
                            <span class="pageNumber"></span><br>
                            <span class="dayofmonth">
                                            <b>
                                        
                                        </b>
                                        </span><br />
                                        <span style="margin-left:-262px;">Payment Date:</span>
                        </td>
                    </tr>
                    <tr>
                        <th rowspan="2">SL</th>
                        <th rowspan="2">Employee ID</th>
                        <th rowspan="2" width="14%">Name</th>
                        <th rowspan="2">Joining Date</th>
                        <th rowspan="2">Service Length</th>
                        <th rowspan="2">Designation</th>
                        <th rowspan="2">Gross Salary</th>
                        <th rowspan="2">Bonus Amount</th>
                        <th rowspan="2">Payment Amount</th>
                        @if($pp_mode==1)
                        <th rowspan="2">Payment Mode</th>
                        <th rowspan="2">Account No</th>
                        @endif
                        <th rowspan="2" id="signature_11">Signature</th>
                    </tr>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $item)
                        @php 
                        $date1s = new DateTime($item->empJoiningDate);
                        $date2s = new DateTime($item->fest_date);
                        $intervals = $date1s->diff($date2s);
                        $year_condition=$intervals->y; 
                        $month_condition=$intervals->m; 
                        $day_condition=$intervals->d; 
                     $order++; $total_gross+=$item->emp_gross;$total_bonus+=$item->emp_bonus;$total_payment+=$item->emp_bonus; @endphp 
                        <tr>
                            <td>{{$order}}</td>
                            <td>{{$item->employeeId}}</td>
                            <td>{{$item->empFirstName}} {{$item->empLastName}} </td>
                            <td>{{date('d-M-Y',strtotime($item->empJoiningDate))}}</td>
                            <td>{{$year_condition}}Years {{$month_condition}} Month {{$day_condition}} Day</td>
                            <td>{{$item->designation}}</td>
                            <td>{{number_format($item->emp_gross)}}</td>
                            <td>{{number_format($item->emp_bonus)}}</td>
                            <td>{{number_format($item->emp_bonus)}}</td>
                            @if($pp_mode==1)
                            <td>{{$item->payment_mode}}</td>
                            <td>{{$item->bank_account}}</td>
                            @endif 
                            <td class="signature_td"></td>
                        </tr>
                    @endforeach
                        <tr style="padding: 3px 3px;">
                            <td style="border-bottom: 1px solid #555;text-align: left;" colspan="5">Total:</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1"></td>
                            <td style="border-bottom: 1px solid #555;" colspan="1">{{number_format($total_gross)}}</td>
                            <td style="border-bottom: 1px solid #555;">{{number_format($total_bonus)}}</td>
                            <td style="border-bottom: 1px solid #555;">{{number_format($total_payment)}}</td>
                            @if($pp_mode==1)
                            <td style="border-bottom: 1px solid #555;"></td>
                            <td style="border-bottom: 1px solid #555;"></td>
                            @endif 
                            <td style="border-bottom: 1px solid #555;"></td>
                        </tr>
                        <tr></tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="31" style="padding-top:20px;border:none;border-top:4px solid #ffffff;" >
                            <div class="authority">
                                <div class="prepared_by">
                                    <span>Prepared By</span>
                                </div>
                                <div class="audited_by">
                                    <span>Audited By</span>
                                </div>
                                <div class="recomended_by">
                                    <span>Recommended By</span>
                                </div>
                                <div class="approved_by">
                                    <span>Director/MD</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    @endforeach
</div>
<div class="ex_footer">
    <tr>
        <td colspan="31" style="padding-top:20px;border:none;border-top:4px solid #ffffff;" >

            <div class="authority">
                <div class="prepared_by">
                    <span>Prepared By</span>
                </div>
                <div class="audited_by">
                    <span>Audited By</span>
                </div>
                <div class="recomended_by">
                    <span>Recommended By</span>
                </div>
                <div class="approved_by">
                    <span>Director/Md</span>
                </div>
            </div>
        </td>
    </tr>
</div>
<script>
     //window.print();
    // window.close();
</script>
</body>
</html>