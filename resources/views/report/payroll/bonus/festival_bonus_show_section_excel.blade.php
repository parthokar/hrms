{{-- <style>

    
    thead,
    tr,
    td,
    th {
        border: 1px solid #757575;
    }

    .heading-top {
        padding-top: 5px;
        padding-bottom: 5px;
    }

    .heading-top th {
        font-size: 13px;
        padding-top: 5px;
        padding-bottom: 5px;
    }

    .heading-top-middle {
        padding-top: 5px;
        padding-bottom: 5px;
    }

    .heading-top-middle th {
        font-size: 16px;
        text-align: center;
        padding-top: 5px;
        padding-bottom: 5px;
    }

    .table-content td {
        font-size: 14px;
    }

    tr.heading-top th {
        font-size: 11px;
    }
    .heading-bottom {
        border: 0px;
        font-size: 11px;
        border:none;
    }
    .heading-bottom td{
        border: 0px;
        border-color: #fff;
        padding-top: 6px;
        padding-bottom: 10px;
    }

    .table_info_data tr td{
        height: 65px;
    }
    .table_info_data tr{
        height:45px;
    }
    tr.heading_top_1{
        border:0px;
    }
    tr.heading_top_1 th{
        border:0px;
    }
    .table_head{
        
        border-top: 0px;
        border-left:0px;
       
    }
    .footer_part{
        opacity: 0;
        display: block;
    }
    tfoot{
        opacity: 1;
    }
    .table_1{
        border-collapse: collapse;
    }
    .footer_part p{
            width: 25%;
            float: left;
            text-align: center;
            padding-top: 40px;
    }
    .c_name{
        font-size: 24px;
        margin-top:10px;
    }
    .c_address{
        text-align: center;
        margin-bottom: 6px;
        margin-top: 7px;
    }
    .c_time{
        text-align: center;
        font-size: 22px;
    }
    .heading_top_1 p{
        font-size: 16px;
        margin-bottom: 5px;
        margin-top: 5px;
    }
   

    @page  {
            size: landscape;
            
        }

      	

    @media print{
        table { page-break-inside:auto; }
        tr    { page-break-inside:avoid; page-break-after:auto;}
        thead {display: table-header-group;}
        tbody { page-break-after:always;
            display: table-row-group;}
        tfoot {
            display: table-footer-group;
           
        }
        .table_head{
            
            border-right:0px;
        }
        tbody{
            border-bottom: 0px;
        }
        tfoot{
            border-top:0px;
            border: none;
            padding-top: 10px;
            opacity: 0;
        }
        .table_info_data tr:last-child td{
            border-bottom:none;
        }

        .footer_part{ 
            opacity: 0;
            display: block;
            padding-top: 40px;
        }
       
        .footer-space{
            height:20px;
        }
        .footer_part p{
            width: 25%;
            float: left;
            text-align: center;
        }
    }
</style> --}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="fMlB0wRKfUGpCcLII5pcNpvL52VEcxIIDsiQ2cru">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="FEITS">
    <title>Festival Bonus </title> 
</head>
<body class="fixed-topbar fixed-sidebar theme-sdtl color-default">
    <section>
        <div class="main-content">
            <div class="page-content ">
                <div class="container-fluid">
                <div class="" >
                    <div>
                        <table class="table_1" style="width:100%;text-align:center;">
                            <thead class="table_head">
                                <tr  class="heading_top_1" style="border:0px;">
                                    <th colspan="9"  style="border:0px;">
                                        <div class="">
                                            <div class="text-center" style="margin-bottom: 10px;">
                                                <b class="c_name"> {{$companyInformation->company_name}}</b> 
                                                <p  class="c_address" > <b>{{$companyInformation->company_address1}}</b> </p>
                                                <span class="c_time" ><b>Bonus For @if(isset($bonus_title)) {{$bonus_title->bonus_title}}-{{date('Y',strtotime($bonus_title->month))}} @endif <span style="text-transform: uppercase; margin-left:6px;"></span> </b></span>
                                                
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                                <tr class="heading-top heading_top_1" style="border:0px;">
                                    <th colspan="4" style="text-align:left;border:0px;">
                                    <p >Section :{{$section->section_id}}</p>   
                                        <p></p>
                                     </th>
                                    
                                    <th colspan="5" style="text-align:right;border:0px;">
                                        <p>Payment Date:</p>
                                    </th>
                                </tr>
                                <tr class="heading-top-middle">
                                    <th  rowspan="2">SL</th>
                                    <th rowspan="2">Employee ID</th>
                                    <th rowspan="2">Name</th>
                                    <th rowspan="2">Joining Date</th>
                                    <th rowspan="2">Designation</th>
                                    <th rowspan="2">Gross Salary</th>
                                    <th rowspan="2">Bonus Amount</th>
                                    <th rowspan="2">Payment Amount</th>
                                </tr>
                            </thead>
                            <tbody class="table-content table_info_data" >
                                @php $order=0; @endphp    
                                    @foreach($data as $item)
                                    @php $order++; @endphp 
                                        <tr>
                                            <td>{{$order}}</td>
                                            <td>{{$item->employeeId}}</td>
                                            <td>{{$item->empFirstName}} {{$item->empLastName}} </td>
                                            <td>{{date('d-M-Y',strtotime($item->empJoiningDate))}}</td>
                                            <td>{{$item->designation}}</td>
                                            <td>{{$item->emp_gross}}</td>
                                            <td>{{$item->emp_bonus}}</td>
                                            <td>{{$item->emp_bonus}}</td>
                                        </tr>
                                    @endforeach
                            </tbody>
                        </table>
                        <div class="footer_part">
                            <p >Prepared By</p>
                            <p>Checked By</p>
                            <p>Manager Account</p>
                            <p>Managing Director</p>
                        </div>
                    </div>
                </div>
            </div> 
            </div>
        </div>
    </section>
  
</body>
</html>
