
<!DOCTYPE html>
<html>
<head>
    <title>Salary Summary</title>
</head>
<body>
<table>
    <thead>
    <tr>
    <tr style="text-align: center;">
    <tr><td></td><td colspan="16" style="text-align:center;"><h2><b>{{$companyInformation->company_name}}</b></h2></td></tr>
    <tr><td></td><td colspan="16" style="text-align:center;"><b>{{$companyInformation->company_address1}}</b></td></tr>
    <tr><td></td><td colspan="16" style="text-align:center;"><p><b>Salary Summary {{date('F-Y',strtotime($_POST['salary_summery_monthsss']))}}</b></p></td></tr>
    <tr><td></td></tr>
    </tr>
    <tr>
        <th>_ SN _</th>
        <th>
                @if(isset($dept_id))
                Department
                @endif
                @if(isset($section_id))
                 Section 
                @endif
                @if(isset($monthly_data))
                Department
                @endif
                @if(isset($all_dept))
                 Department
                @endif
                @if(isset($all_section))
                Section 
                @endif
                @if(isset($all_group))
                WORK GROUP
                @endif
                @if(isset($group))
                WORK GROUP
                @endif
        </th>
        <th>_ WORKER _</th>
        <th>BASIC</th>
        <th>HOUSE</th>
        <th>MEDICAL</th>
        <th>FOOD</th>
        <th>TA/DA</th>
        <th>GROSS </th>
        <th>GROSS PAY</th>
        <th>ABS</th>
        <th> ADV </th>
        <th> OT </th>
        <th> OT AMT </th>
        <th> ATT BONUS </th>
        <th> SP ALLOW </th>
        <th> NET </th>
    </tr>
    </tr>
    </thead>
    <tbody>
    @php $m_power=0;$basic_total=0;$house_total=0;$total_medicals=0; $i=0;
                            $total_foods=0;$total_transports=0;$total_gorss=0;$total_gross_pay=0;
                            $abs_deduction_amount=0;$total_advance_deduction=0;$total_overtime=0;
                            $total_ot_amount=0;$total_att_bonus=0;$total_increment_bonus=0;$total_net_amount=0;
                            $actual_resigned=0;$actual_lefty=0;$total_basic_resigned=0;$total_house_resigned=0;$total_medical_resigned=0;
                            $total_food_resigned=0;$total_transport_resigned=0;$total_gross_resigned=0;$total_grospay_resigned=0;
                            $total_abs_resigned=0;$total_adv_resigned=0;$total_ot_resigned=0;
                            $total_otamt_resigned=0;$total_attbonus_resigned=0;$total_secial_resigned=0;
                            $total_net_resigned=0;$total_basic_lefty=0;$total_house_lefty=0;
                            $total_medical_lefty=0;$total_food_lefty=0;$total_transport_lefty=0;
                            $total_gross_lefty=0;$total_grospay_lefty=0;$total_abs_lefty=0;
                            $total_adv_lefty=0;$total_ot_lefty=0;$total_otamt_lefty=0;
                            $total_attbonus_lefty=0;$total_secial_lefty=0;$total_net_lefty=0;
                            $order_test=0;
    @endphp

    @foreach($data as $summery)
        @php $order_test++; @endphp
        <tr>
            <td style="text-align:center;">{{$order_test}}</td>
            <td style="text-align:left;">
                    @if(isset($dept_id))
                    {{$summery->departmentName}}
                    @endif
                    @if(isset($section_id))
                    {{$summery->section_id}}
                    @endif
                    @if(isset($monthly_data))
                     {{$summery->departmentName}}
                    @endif
                    @if(isset($all_dept))
                     {{$summery->departmentName}}
                    @endif

                    @if(isset($all_section))
                    {{$summery->section_id}}
                    @endif
                    @if(isset($all_group))
                    {{$summery->work_group}}
                    @endif
                    @if(isset($group))
                    {{$summery->work_group}}
                    @endif
            </td>
            <td style="text-align:left;">@php $t=$summery->worker;  @endphp {{$summery->worker}} @php $m_power+=$summery->worker; @endphp</td>
            <td style="text-align:left;">{{(round($summery->basic))}} @php $basic_total+=$summery->basic; @endphp</td>
            <td style="text-align:left;">{{(round($summery->house))}} @php $house_total+=$summery->house; @endphp</td>
            <td style="text-align:left;">{{(round($summery->medicals))}} @php $total_medicals+=$summery->medicals; @endphp</td>
            <td style="text-align:left;">{{(round($summery->foods))}} @php $total_foods+=$summery->foods; @endphp</td>
            <td style="text-align:left;">{{(round($summery->transports))}} @php $total_transports+=$summery->transports; @endphp</td>
            <td style="text-align:left;">{{(round($summery->gorss))}} @php $total_gorss+=$summery->gorss; @endphp</td>
            <td style="text-align:left;">{{(round($summery->gross_pay))}} @php $total_gross_pay+=$summery->gross_pay; @endphp</td>
            <td style="text-align:left;">{{(round($summery->a_deduction_amount))}} @php $abs_deduction_amount+=$summery->a_deduction_amount; @endphp</td>
            <td style="text-align:left;">@if($summery->advanced_deduction=='') 0 @else {{(round($summery->advanced_deduction))}} @endif @php $total_advance_deduction+=$summery->advanced_deduction; @endphp</td>
            <td style="text-align:left;">{{($summery->overtimes)}} @php $total_overtime+=$summery->overtimes; @endphp</td>
            <td style="text-align:left;">{{(round($summery->ot_amounts))}} @php $total_ot_amount+=$summery->ot_amounts; @endphp</td>
            <td style="text-align:left;">{{(round($summery->attendance_bonus))}} @php $total_att_bonus+=$summery->attendance_bonus; @endphp</td>
            <td style="text-align:left;">@if($summery->total_increment=='') 0 @else {{(round($summery->total_increment))}} @endif @php $total_increment_bonus+=$summery->total_increment; @endphp</td>
            <td style="text-align:left;">{{(round($summery->net_total))}} @php $total_net_amount+=$summery->net_total; @endphp</td>
        </tr>
    @endforeach
    @foreach($resigned as $r)
        @php $rt=$order_test+1; @endphp
        @php
            $actual_resigned+=$r->worker;$total_basic_resigned+=$r->basic;$total_house_resigned+=$r->house;
            $total_medical_resigned+=$r->medicals;$total_food_resigned+=$r->foods;
            $total_transport_resigned+=$r->transports;$total_gross_resigned+=$r->gorss;
            $total_grospay_resigned+=$r->gross_pay;$total_abs_resigned+=$r->a_deduction_amount;
            $total_adv_resigned+=$r->advanced_deduction;$total_ot_resigned+=$r->overtimes;
            $total_otamt_resigned+=$r->ot_amounts;$total_attbonus_resigned+=$r->attendance_bonus;
            $total_secial_resigned+=$r->total_increment;$total_net_resigned+=$r->net_total;
        @endphp
        <tr>
            <td style="text-align:center;">{{$rt}}</td>
            <td style="text-align:left;">Grand Total Resigned:</td>
            <td style="text-align:left;">{{($r->worker)}}</td>
            <td style="text-align:left;">{{($r->basic)}}</td>
            <td style="text-align:left;">{{($r->house)}}</td>
            <td style="text-align:left;">{{($r->medicals)}}</td>
            <td style="text-align:left;">{{($r->foods)}}</td>
            <td style="text-align:left;">{{($r->transports)}}</td>
            <td style="text-align:left;">{{($r->gorss)}}</td>
            <td style="text-align:left;">{{($r->gross_pay)}}</td>
            <td style="text-align:left;">{{($r->a_deduction_amount)}}</td>
            <td style="text-align:left;">{{($r->advanced_deduction)}}</td>
            <td style="text-align:left;">{{($r->overtimes)}}</td>
            <td style="text-align:left;">{{($r->ot_amounts)}}</td>
            <td style="text-align:left;">{{($r->attendance_bonus)}}</td>
            <td style="text-align:left;">{{($r->total_increment)}}</td>
            <td style="text-align:left;">{{($r->net_total)}}</td>
        </tr>
        <tr></tr>
    @endforeach

    <tr>
        <td style="text-align:center;" colspan="2">Total:</td>
        <td style="text-align:left;">{{($m_power+$actual_resigned)}}</td>
        <td style="text-align:left;">{{($basic_total+$total_basic_resigned)}}</td>
        <td style="text-align:left;">{{($house_total+$total_house_resigned)}}</td>
        <td style="text-align:left;">{{($total_medicals+$total_medical_resigned)}}</td>
        <td style="text-align:left;">{{($total_foods+$total_food_resigned)}}</td>
        <td style="text-align:left;">{{($total_transports+$total_transport_resigned)}}</td>
        <td style="text-align:left;">{{($total_gorss+$total_gross_resigned)}}</td>
        <td style="text-align:left;">{{($total_gross_pay+$total_grospay_resigned)}}</td>
        <td style="text-align:left;">{{($abs_deduction_amount+$total_abs_resigned)}}</td>
        <td style="text-align:left;">{{($total_advance_deduction+$total_adv_resigned)}}</td>
        <td style="text-align:left;">{{($total_overtime+$total_ot_resigned)}}</td>
        <td style="text-align:left;">{{($total_ot_amount+$total_otamt_resigned)}}</td>
        <td style="text-align:left;">{{($total_att_bonus+$total_attbonus_resigned)}}</td>
        <td style="text-align:left;">{{($total_increment_bonus+$total_secial_resigned)}}</td>
        <td style="text-align:left;">{{($total_net_amount+$total_net_resigned)}}</td>
    </tr>



    @foreach($lefty as $l)
        @php
            $actual_lefty+=$l->worker;$total_basic_lefty+=$l->basic;$total_house_lefty+=$l->house;
            $total_medical_lefty+=$l->medicals;$total_food_lefty+=$l->foods;$total_transport_lefty+=$l->transports;
            $total_gross_lefty+=$l->gorss;$total_grospay_lefty+=$l->gross_pay;$total_abs_lefty+=$l->a_deduction_amount;
            $total_adv_lefty+=$l->advanced_deduction;$total_ot_lefty+=$l->overtimes;$total_otamt_lefty+=$l->ot_amounts;
            $total_attbonus_lefty+=$l->attendance_bonus;$total_secial_lefty+=$l->total_increment;
            $total_net_lefty+=$l->net_total;$lt=$rt+1;
        @endphp
        <tr style="text-align:left;">
            <td style="text-align:center;"> {{ $lt}} </td>
            <td style="text-align:left;">Grand Total Lefty:</td>
            <td style="text-align:left;">{{($l->worker)}}</td>
            <td style="text-align:left;">{{($l->basic)}}</td>
            <td style="text-align:left;">{{($l->house)}}</td>
            <td style="text-align:left;">{{($l->medicals)}}</td>
            <td style="text-align:left;">{{($l->foods)}}</td>
            <td style="text-align:left;">{{($l->transports)}}</td>
            <td style="text-align:left;">{{($l->gorss)}}</td>
            <td style="text-align:left;">{{($l->gross_pay)}}</td>
            <td style="text-align:left;">{{($l->a_deduction_amount)}}</td>
            <td style="text-align:left;">{{($l->advanced_deduction)}}</td>
            <td style="text-align:left;">{{($l->overtimes)}}</td>
            <td style="text-align:left;">{{($l->ot_amounts)}}</td>
            <td style="text-align:left;">{{($l->attendance_bonus)}}</td>
            <td style="text-align:left;">{{($l->total_increment)}}</td>
            <td style="text-align:left;">{{($l->net_total)}}</td>
        </tr>
        <tr></tr>
    @endforeach
    <tr>
        <td style="text-align:left;" colspan="2">Grand Total:</td>

        <td style="text-align:left;">{{($m_power+$actual_resigned+$actual_lefty)}}</td>
        <td style="text-align:left;">{{($basic_total+$total_basic_resigned+$total_basic_lefty)}}</td>
        <td style="text-align:left;">{{($house_total+$total_house_resigned+$total_house_lefty)}}</td>
        <td style="text-align:left;">{{($total_medicals+$total_medical_resigned+$total_medical_lefty)}}</td>
        <td style="text-align:left;">{{($total_foods+$total_food_resigned+$total_food_lefty)}}</td>
        <td style="text-align:left;">{{($total_transports+$total_transport_resigned+$total_transport_lefty)}}</td>
        <td style="text-align:left;">{{($total_gorss+$total_gross_resigned+$total_gross_lefty)}}</td>
        <td style="text-align:left;">{{($total_gross_pay+$total_grospay_resigned+$total_grospay_lefty)}}</td>
        <td style="text-align:left;">{{($abs_deduction_amount+$total_abs_resigned+$total_abs_lefty)}}</td>
        <td style="text-align:left;">{{($total_advance_deduction+$total_adv_resigned+$total_adv_lefty)}}</td>
        <td style="text-align:left;">{{($total_overtime+$total_ot_resigned+$total_ot_lefty)}}</td>
        <td style="text-align:left;">{{($total_ot_amount+$total_otamt_resigned+$total_otamt_lefty)}}</td>
        <td style="text-align:left;">{{($total_att_bonus+$total_attbonus_resigned+$total_attbonus_lefty)}}</td>
        <td style="text-align:left;">{{($total_increment_bonus+$total_secial_resigned+$total_secial_lefty)}}</td>
        <td style="text-align:left;">{{($total_net_amount+$total_net_resigned+$total_net_lefty)}}</td>
    </tr>
    <tr></tr>
    <tr></tr>

    <tr>
        <td colspan="2"></td>
        <td colspan="3"><p>Prepared By</p></td>
        <td colspan="3"><p>Audited By</p></td>
        <td colspan="3"><p>Recommended By</p></td>
        <td colspan="3"><p>Approved By</p></td>

    </tr>
    </tbody>
    <tfoot>
    </tfoot>
</table>
</div>
</div>
</body>
</html>
