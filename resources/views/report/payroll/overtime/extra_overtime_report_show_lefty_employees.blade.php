<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Extra Overtime Report</title>
    <style>

        /*@page {*/
        /*    size: landscape;*/
        /*}*/

        /*@media print{@page {size: legal landscape}}*/


        table{
            border-collapse: collapse;
        }
        thead,
        tr,
        td,
        th {
            font-size: 13px;
            border: 1px solid #000;

        }

        .heading-top {
            padding-top: 5px;
            padding-bottom: 5px;
        }

        .heading-top th {
            font-size: 13px;
            padding-top: 5px;
            padding-bottom: 5px;
        }

        .heading-top-middle {
            padding-top: 5px;
            padding-bottom: 5px;
        }

        .heading-top-middle th {
            font-size: 13px;
            text-align: center;
            padding-top: 2px;
            padding-bottom: 3px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .table-content td {
            font-size: 11px;
        }


        .heading-top th:first-child{
            border-right:0px;
        }
        .heading-top th:nth-child(2){
            border-left:0px;
            border-right:0px;
        }
        .heading-top th:nth-child(3 ){
            border-left:0px;
        }
        tr.heading-top th {
            font-size: 11px;
        }
        .heading-bottom {
            /*border: 0px;*/
            font-size: 11px;
            /*border:none;*/
        }
        .heading-bottom td{
            /*border: 0px;*/
            /*border-color: #fff;*/
            padding-top: 2px;
            padding-bottom: 2px;
            font-weight: bold;
        }

        .table_info_data tr td{
            padding-left: 10px;
            padding-right: 10px;

        }
        .table_info_data tr{
            height:23px;
        }
        tr.heading_top_1{
            border:0px;
        }
        tr.heading_top_1 th{
            border:0px;
        }
        .table_head{

            border-top: 0px;
            border-left:0px;

        }
        .footer_part{
            opacity: 0;
            display: none;
        }
        tfoot{
            opacity: 1;
        }
        .footer-space{
            padding-top: 20px;
        }

        .footer-part p{
            border-top: 1px solid #8a8585;
            text-align: center;
            padding-top: 10px;
            padding-left: 10px;
            padding-right: 10px;
            margin-top: 75px;
            width: 190px;
        }
        .heading_top_1 p {
            margin-top: 3px;
            margin-bottom: 3px;
        }
        .top_h {
            text-align: center;
        }
        .n_h{
            width:130px;
        }
        .d_h{
            width: 110px;
        }
        th.n_h_1{
            width: 130px;
        }
        th.d_h_1{
            width:110px;
        }
        a{
            text-decoration: none;
            color:#000000;
        }
        .authority div{
            width: 18%;
            margin-right: 5%;
            text-align: center;
            /*border-top: 1px solid #777171;*/
            padding-top: 1px;
            margin-top: 30px;
            margin-left: 2%;
        }
        .ex-footer{
            margin-top: 10px;
        }
        .main_table{
            width: 100%;
            border: 0px;
            border-top:1px solid #ddd ;
        }
        thead{
            border: 0px;
        }
        thead tr td{
            border: none!important;
        }
        .main_table{
            border: 0px!important;
        }
        thead tr td{
            text-align: center;
        }
        thead p{
            margin-bottom: 5px;
            margin-top: 5px;
        }
        thead h3{
            margin-bottom: 2px;
            margin-top: 5px;
        }
        thead h4{
            margin-bottom: 2px;
            margin-top: 5px;
        }
        tbody tr td{
            padding-left: 7px;
            padding-right: 7px;
            padding-top: 2px;
            padding-bottom: 2px;
        }

        @media print{

            table { page-break-inside:auto; }
            tr    { page-break-inside:avoid; page-break-after:auto;}
            thead {display: table-header-group;}
            tbody { page-break-after:always;
                display: table-row-group;}
            tfoot {
                display: table-footer-group;

            }
            th.n_h_1 {
                width: 28%!important;
            }
        }
    </style>
</head>
<body>
@php
    $total_amount=0;
    $total_total_hour=0;
@endphp
<div class="page-content">
    <div class="row">
        <div class="col-md-12 portlets">
            <div class="panel panel-default">

                <div class="panel-body">




                        <table class="table table-hover table-striped table-bordered main_table" >

                            <thead>

                            <tr style="border: 0px;">
                                <td colspan="30" tyle="border: 0px; text-align:center;" >
                                    <p><b>Extra Overtime Report Lefty</b></p>
                                 <h3 class="text-center">{{$companyInformation->company_name}}</h3>

                                    @if(isset($department))
                                        <h4 class="text-center">Department:{{ $department->departmentName}}</h4>
                                    @endif


                                    @if(isset($section))
                                        <h4 class="text-center">Section:{{ $section->empSection}}</h4>
                                    @endif
                                    <p class="text-center" style="padding-bottom: 30px;"><b>Extra O.T sheet of 
                                        @if(isset($start_month)) ({{date('m/d/Y',strtotime($start_month))}} to {{date('m/d/Y',strtotime($end_month))}}) @else  {{$request->start_date}} to {{$request->end_date}}  @endif
                                      
                                    </b>
                                </p>

                            </tr>





                            <tr>
                                <th style="padding-left: 20px" scope="col">Sl No</th>
                                <th scope="col">Emp ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Designation</th>
                                <th scope="col">Basic</th>
                                <th scope="col">Gross</th>
                                <th scope="col">Overtime (Hr)</th>
                                <th scope="col">Rate</th>
                                <th scope="col">Amount</th>
                                @if($p_select==1)
                                <th scope="col">Payment Mode</th>
                                <th scope="col">Account No</th>
                                @endif
                                <th scope="col">Signature</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $order=0; $ot_hour=0; $total_ot=0; $total_amount=0; $basic=0; $gross=0; $ot_rates=0; @endphp
                            @foreach($extraotsss as $extra)
                                @php

                                    $ot_rate=$extra->basic_salary/104;
                                    $ac_rate=round($ot_rate,2);
                                    $total_hour=$extra->total_hour+$extra->total_minute+$extra->friday_hour;
                                    if($total_hour==0){
                                     continue;
                                    }
                                    $order++;
                                    $total_ot+=$total_hour;
                                    $ot_rates+=$ac_rate;
                                @endphp
                                <tr>
                                    <td style="padding-left: 25px" align="center">{{$order}}</td>
                                    <td>{{$extra->employee_id}}</td>
                                    <td style="width: 200px; height: 30px;">{{$extra->name}}</td>
                                    <td>{{$extra->designation}}</td>
                                    <td>{{$extra->basic_salary}}</td>
                                    <td>{{$extra->total_salary}}</td>
                                    <td>{{$total_hour}}</td>
                                    <td>{{$ac_rate}}</td>
                                    <td>
                                        @php
                                            $ot_amount=$ac_rate*$total_hour;
                                            $total_amount+=$ot_amount;
                                            $basic+=$extra->basic_salary;
                                            $gross+=$extra->total_salary;
                                        @endphp
                                        {{round($ot_amount,0)}}
                                    </td>
                                @if($p_select==1)
                                <td>{{$extra->payment_mode}}</td>
                                <td>{{$extra->bank_account}}</td>
                                @endif 
                                    <td width="200px"></td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr style="border: none;">
                                <td colspan="30" style="border: none;" class="border_none">
                                    <div class="ex_footer_1">
                                        <div class="authority" style="display: flex;">
                                            <div class="prepared_by" style="width: 22%;text-align: center">
                                                <span style="display: inline-block;
            border-top: 1px solid #3a3a3a;
            padding: 6px 22px;
            margin-top: 30px;">Prepared By</span>
                                            </div>
                                            <div class="audited_by" style="width: 22%;text-align: center"">
                                            <span style="display: inline-block;
            border-top: 1px solid #3a3a3a;
            padding: 6px 22px;
            margin-top: 30px;">Audited By</span>
                                        </div>
                                        <div class="recomended_by" style="width: 22%;text-align: center"">
                                        <span style="display: inline-block;
            border-top: 1px solid #3a3a3a;
            padding: 6px 22px;
            margin-top: 30px;">Recommended By</span>
                                    </div>
                                    <div class="approved_by" style="width: 22%;text-align: center"">
                                    <span style="display: inline-block;
            border-top: 1px solid #3a3a3a;
            padding: 6px 22px;
            margin-top: 30px;">Director</span>
                    </div>
                </div>


            </div>
            </td>
            </tr>
            </tfoot>

            <tr>
                <td>Total:</td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{$basic}}</td>
                <td>{{$gross}}</td>
                <td>{{$total_ot}}</td>
                <td>{{round($ot_rates,2)}}</td>
                <td>{{number_format($total_amount)}}</td>
                <td></td>
            </tr>

            </table>

        </div>
    </div>
</div>
</div>
</div>
</div>



</body>
</html>