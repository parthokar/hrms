
<!DOCTYPE html>
<html>
<head>
    <title>Extra Overtime</title>
</head>
<body>
        
<div id="print_area">
        <div class="main_div">
            <div class="salary_table">
                <table class="table_1">
                    <thead>
                        <tr style="text-align: center;">
                            <tr><td></td><td colspan="16" align="center"><h4><b>{{$companyInformation->company_name}}</b></h4></td></tr>
                            <tr><td></td><td colspan="16" align="center"><p style=""> <b>O.T Extra Summary for the month of</b> </p></td></tr>
                                <tr><td></td><td colspan="16" align="center"><p><b>{{$monthss}}</b></p></td></tr>
                        </tr>
                        <tr></tr>
                        <tr>
                               <th>SL</th>
                                @if(isset($section_workar))
                                <th>SECTION</th>
                                @else 
                                <th>DEPARTMENT</th>
                                @endif
                                <th>MAN POWER</th>
                                <th>BASIC</th>
                                <th>GROSS</th>
                                <th>OVERTIME (HOUR)</th>
                                <th>AMOUNT</th>
                                <th>REMARKS</th>
                        </tr>
                    </thead>
                    <tbody>  

                        @php $order=0; $man_power=0; $basic=0; $gross=0; $overtime=0; $total_amounts=0; @endphp
                        @foreach($total as $totals)
                        @php $order++; @endphp
                        <tr>
                        <td>{{$order}}</td>
                        <td style="text-align:left">
                            @if(isset($section_workar))
                              {{$section->empSection}}
                              @else 
                             {{$department->departmentName}}
                            @endif
                        </td>
                        <td style="text-align:left">
                                @if(isset($section_workar))
                                    @php $worker=DB::table('tb_total_present')->count();  @endphp
                                    @else 
                                    @php $worker=DB::table('tb_total_present')->count(); @endphp
                                 @endif     
                                 {{$worker}} @php $man_power+=$worker; @endphp
                            </td>
                            <td style="text-align:left">
                                @foreach($total_basic_gross as $test)
                                {{$test->total_basic}} @php $basic+=$test->total_basic; @endphp
                                  @php 
                                    $total_employee=DB::table('tb_total_present')->count();
                                    $multiply=$total_employee*104;
                                    $ot_rate=$test->total_basic/$multiply;
                                    $actual_ot_rate=round($ot_rate);
                                 @endphp
                                @endforeach
                            </td>
                            <td style="text-align:left">
                                @foreach($total_basic_gross as $test)
                                {{$test->total_gross}}  @php $gross+=$test->total_gross; @endphp
                                @endforeach
                            </td>
                            <td style="text-align:left">{{$totals->total_hour+$totals->friday_hour+$totals->total_minute}} @php $total_ot=$totals->total_hour+$totals->friday_hour+$totals->total_minute; $overtime+=$total_ot; @endphp</td>
                            <td style="text-align:left">{{$totals->total_amount}} @php $total_amounts+=$totals->total_amount; @endphp</td>
                                    <td></td> 
                                    </tr>
                                    @endforeach

                                    @php $lworker=0; $lbasic=0; $lgross=0; $lot=0; $lotamount=0; $rworker=0; $rbasic=0; $rgross=0; $rot=0; $rotamount=0;@endphp
                                    @foreach($total_resigned as $resigneds)
                                    @php $rworker+=$resigneds->worker; $rbasic+=$resigneds->basic; $rgross+=$resigneds->gross; $rot+=$resigneds->total_ot; $rotamount+=$resigneds->ot_amount; @endphp
                                       <tr>
                                               <td colspan="2">Resigned:</td>
                                               <td style="text-align:left">{{$resigneds->worker}}</td>
                                               <td style="text-align:left">{{$resigneds->basic}}</td>
                                               <td style="text-align:left">{{$resigneds->gross}}</td>
                                               <td style="text-align:left">{{$resigneds->total_ot}}</td>
                                               <td style="text-align:left">{{round($resigneds->ot_amount)}}</td>
                                               <td></td>
                                           </tr>
                                       @endforeach

                                       @foreach($total_lefty as $lefty)
                                       @php $lworker+=$lefty->worker; $lbasic+=$lefty->basic; $lgross+=$lefty->gross; $lot+=$lefty->total_ot; $lotamount+=$lefty->ot_amount; @endphp
                                       <tr>
                                               <td colspan="2">Lefty:</td>
                                               <td style="text-align:left">{{$lefty->worker}}</td>
                                               <td style="text-align:left">{{$lefty->basic}}</td>
                                               <td style="text-align:left">{{$lefty->gross}}</td>
                                               <td style="text-align:left">{{$lefty->total_ot}}</td>
                                               <td style="text-align:left">{{round($lefty->ot_amount)}}</td>
                                               <td></td>
                                       </tr> 
                                       @endforeach
                                   <tr>
                                       <td colspan="2">Grand Total:</td>
                                       <td style="text-align:left">{{$man_power+$rworker+$lworker}}</td>
                                       <td style="text-align:left">{{$basic+$rbasic+$lbasic}}</td>
                                       <td style="text-align:left">{{$gross+$rgross+$lgross}}</td>
                                       <td style="text-align:left">{{$overtime+$rot+$lot}}</td>
                                       <td style="text-align:left">{{$total_amounts+$rotamount+$lotamount}}</td>
                                       <td></td>
                                   </tr> 
                          <tr></tr>
                         <tr></tr>
                       <tr>
                            <td colspan="1"></td>
                            <td colspan="1"><p>Prepared By</p></td>
                            <td colspan="1"><p>Audited By</p></td>
                            <td colspan="3"><p>Recommended By</p></td>
                            <td colspan="4"><p>Approved By</p></td>
                        </tr>
                    </tbody>
                    <tfoot>
                </table>
            </div>
        </div>
    </div>
</body>
</html>

