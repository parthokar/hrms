<style>

    table{
        border-collapse: collapse;
    }
    thead,
    tr,
    td,
    th {
        border: 1px solid #757575;

    }

    .heading-top {
        padding-top: 5px;
        padding-bottom: 5px;
    }

    .heading-top th {
        font-size: 13px;
        padding-top: 5px;
        padding-bottom: 5px;
    }

    .heading-top-middle {
        padding-top: 5px;
        padding-bottom: 5px;
    }

    .heading-top-middle th {
        font-size: 13px;
    text-align: center;
    padding-top: 2px;
    padding-bottom: 3px;
    padding-left: 10px;
    padding-right: 10px;
    }

    .table-content td {
        font-size: 11px;
    }


    .heading-top th:first-child{
        border-right:0px;
    }
    .heading-top th:nth-child(2){
        border-left:0px;
        border-right:0px;
    }
    .heading-top th:nth-child(3 ){
        border-left:0px;
    }
    tr.heading-top th {
        font-size: 11px;
    }
    .heading-bottom {
        /*border: 0px;*/
        font-size: 11px;
        /*border:none;*/
    }
    .heading-bottom td{
        /*border: 0px;*/
        /*border-color: #fff;*/
        padding-top: 2px;
        padding-bottom: 2px;
        font-weight: bold;
    }

    .table_info_data tr td{
        padding-left: 10px;
        padding-right: 10px;

    }
    .table_info_data tr{
        height:23px;
    }
    tr.heading_top_1{
        border:0px;
    }
    tr.heading_top_1 th{
        border:0px;
    }
    .table_head{

        border-top: 0px;
        border-left:0px;

    }
    .footer_part{
        opacity: 0;
        display: none;
    }
    tfoot{
        opacity: 1;
    }
    .footer-space{
        padding-top: 20px;
    }

    .footer-part p{
        border-top: 1px solid #8a8585;
        text-align: center;
        padding-top: 10px;
        padding-left: 10px;
        padding-right: 10px;
        margin-top: 75px;
        width: 190px;
    }
    .heading_top_1 p {
        margin-top: 3px;
        margin-bottom: 3px;
    }
    .top_h {
        text-align: center;
    }
    .n_h{
        width:130px;
    }
    .d_h{
        width: 110px;
    }
    th.n_h_1{
        width: 130px;
    }
    th.d_h_1{
        width:110px;
    }
    a{
        text-decoration: none;
        color:#000000;
    }
    .authority div{
        width: 18%;
        margin-right: 5%;
        text-align: center;
        border-top: 1px solid #777171;
        padding-top: 1px;
        margin-top: 40px;
        margin-left: 2%;
    }
    .ex-footer{
        margin-top: 10px;
    }
    @page  {
        size: portrait;

    }
    @media print{

        table { page-break-inside:auto; }
        tr    { page-break-inside:avoid; page-break-after:auto;}
        thead {display: table-header-group;}
        tbody { page-break-after:always;
            display: table-row-group;}
        tfoot {
            display: table-row-group;

        }
        th.n_h_1 {
            width: 28%!important;
        }
    }
</style>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="fMlB0wRKfUGpCcLII5pcNpvL52VEcxIIDsiQ2cru">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="admin-themes-lab">
    <meta name="author" content="FEITS">
    <title>
        Extra Ot Summary </title>
</head>
<body class="fixed-topbar fixed-sidebar theme-sdtl color-default">
<section>
    <div class="main-content">
        <div class="page-content ">
            <div class="container-fluid">
                    <p class="pull-right"><button style="margin-top: 17px;" class="btn btn-info pull-right" id="pdf_btn" type="button" onclick="printDiv('print_area')"><i class="fa fa-print"></i> Print</button></p>
                <div class="" >
                    <div id="print_area">
                        <table class="thikana" style="width:100%;text-align:center;">
                            <thead class="table_head" style="border:0px!important;">
                                <tr  class="heading_top_1" style="border:0px;">
                                    <th colspan="19"  style="border:0px;">
                                        <div class="col-xlg-12 col-lg-12  col-sm-12" style="margin-bottom: 20px;">
                                            <div class="top_h" style="margin-bottom: 1px; ">
                                                <b>FIN BANGLA APPARELS LTD.</b>
                                                <p style=""> <b>O.T Extra Summary from {{\Carbon\Carbon::parse($request->start)->format('d-m-Y')}} to {{\Carbon\Carbon::parse($request->end)->format('d-m-Y')}} </b> </p>

                                            </div>
                                        </div>
                                    </th>
                                </tr>
                                <tr class="heading-top-middle">
                                    <th rowspan="2">SL</th>
                                    <th rowspan="2">DEPARTMENT</th>
                                    <th rowspan="2">MAN POWER</th>
                                    <th rowspan="2">BASIC</th>
                                    <th rowspan="2" class="n_h_1">GROSS</th>
                                    <th rowspan="2" class="d_h_1">OVERTIME (HOUR)</th>
                                    <th rowspan="2">AMOUNT</th>
                                    <th rowspan="2">REMARKS</th>
                                </tr>
                            </thead>
                            <tbody class="table-content table_info_data">

                                    @php $order=0; $basic=0; $gross=0;$overtime=0; $worker=0; $total_amounts=0; $man_power=0; @endphp
                                    @foreach($total as $totals)
                                    @php 
                                    $order++; $lefty=$order+1;
                                    $data=DB::SELECT("SELECT SUM(total_present) as t_basic,SUM(total_absent) as t_gross FROM `tb_total_present` WHERE emp_id=$totals->department");
                                    $worker=DB::table('tb_total_present')->where('emp_id',$totals->department)->count();
                                    @endphp
                                    
                                    @php  $overtime+=$totals->total_hour+$totals->friday_hour+$totals->total_minute @endphp
                                    <tr>
                                    <td>{{$order}}</td>
                                    <td>
                                            {{-- {{$totals->departmentName}} --}}
                                          <a target="_blank" href="{{url('department/employee/ot/amount/details/'.base64_encode($totals->department))}}">{{$totals->departmentName}}</a>
                                    </td>
                                    <td>    
                                     {{$worker}}
                                     @php $man_power+=$worker;  $lefty=$man_power+1; $resigned=$lefty+1; @endphp
                                    </td>
                                        <td>
                                             @foreach( $data as $test)
                                                    {{number_format($test->t_basic)}}
                                                   @php $basic+=$test->t_basic; @endphp  
                                             @endforeach
                                        </td>
                                        <td>
                                         @foreach( $data as $test)
                                                {{number_format($test->t_gross)}}
                                                @php $gross+=$test->t_gross; @endphp  
                                         @endforeach
                                        </td>
                                        <td>{{$totals->total_hour+$totals->friday_hour+$totals->total_minute}}</td>
                                    <td>{{round($totals->total_amount)}} @php $total_amounts+=$totals->total_amount; @endphp </td>
                                    <td></td> 
                                    </tr>
                                    @endforeach
                            </tbody>

                            <tfoot>
                                @php $l_worker=0;
                                     $r_worker=0; 
                                     $l_basic=0;
                                     $r_basic=0;
                                     $l_gross=0;
                                     $r_gross=0;
                                     $l_ot=0;
                                     $r_ot=0;
                                     $l_ot_amt=0;
                                     $r_ot_amt=0;
                                @endphp


                            @foreach($total_resigned as $resigneds)
                            @php $r_worker+=$resigneds->worker;  $r_basic+=$resigneds->basic; $r_gross+=$resigneds->gross; $r_ot+=$resigneds->total_ot; $r_ot_amt+=$resigneds->ot_amount; @endphp
                                <tr class="heading-bottom" style=" font-size:11px;">
                                    <td>{{$order+1}} @php $leftys=$order+1; @endphp</td>
                                    <td><b>Resigned:</b></td>
                                    <td>{{$resigneds->worker}}</td>
                                    <td>{{$resigneds->basic}}</td>
                                    <td>{{$resigneds->gross}}</td>
                                    <td>{{$resigneds->total_ot}}</td>
                                    <td>{{round($resigneds->ot_amount)}}</td>
                                    <td></td>
                                </tr>
                            @endforeach

                              @foreach($total_lefty as $lefty)
                              @php $l_worker+=$lefty->worker;  $l_basic+=$lefty->basic; $l_gross+=$lefty->gross; $l_ot+=$lefty->total_ot; $l_ot_amt+=$lefty->ot_amount; @endphp
                                <tr class="heading-bottom" style=" font-size:11px;">
                                    <td>{{$leftys+1}} </td>
                                    <td><b>Lefty:</b></td>
                                    <td>{{$lefty->worker}}</td>
                                    <td>{{$lefty->basic}}</td>
                                    <td>{{$lefty->gross}}</td>
                                    <td>{{$lefty->total_ot}}</td>
                                    <td>{{round($lefty->ot_amount)}}</td>
                                    <td></td>
                                </tr>
                            @endforeach

                    

                                <tr class="heading-bottom" style=" font-size:11px;">
                                    <td colspan="2"><b>Grand Total:</b></td>
                                    <td>{{$man_power+$l_worker+$r_worker}}</td>
                                    <td>{{number_format($basic+$l_basic+$r_basic)}}</td>
                                    <td>{{number_format($gross+$l_gross+$r_gross)}}</td>
                                    <td>{{$overtime+$l_ot+$r_ot}}</td>
                                    <td>{{number_format($total_amounts+$l_ot_amt+$r_ot_amt)}}</td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                        <div class="ex-footer">
                            <div class="authority" style="display:flex;">
                                <div class="prepared_by">
                                    <span>Prepared By</span>
                                </div>
                                <div class="audited_by">
                                    <span>Audited By</span>
                                </div>
                                <div class="recomended_by">
                                    <span>Recommended By</span>
                                </div>
                                <div class="approved_by">
                                    <span>Approved By</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</section>
<script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
</body>
</html>
