@extends('layouts.master')
@section('title', 'Lefty Employee Extra Overtime Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel panel-default">
                    <div class="panel-heading"><b>Lefty Employee Extra Overtime Report</b></div>
                    <div class="panel-body">

                        <div class="col-md-12">

                            <div class="col-md-12">
                                <div class="form-group">
                                 <label>Select Type</label>
                                 <select id="ot_type" name="ot_type" class="form-control">
                                          <option value="">Select</option>
                                            <option value="1">Department Wise</option>
                                            <option value="2">Section Wise</option>
                                            <option value="3">Employee Wise</option>
                                            <option value="4">Month Wise</option>
                                  </select>
                                </div>
                            </div>
                                   <div class="col-md-12" id="department_type" style="display:none">
                                        {!! Form::open(['method'=>'POST','action'=>'datesalarycontroller@extraovertimeReportshowLeftyEmployee', 'target' => '_blank']) !!}
                                       <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Select Payment type</label>
                                            <select class="form-control" name="payment_id">
                                                <option value="0" selected>All</option>
                                                <option value="1">Bank</option>
                                                <option value="2">bKash</option>
                                                <option value="3">Cash</option>
                                            </select>
                                        </div>
                                       </div>

                                       <div class="col-md-3">
                                        <div class="form-group">
                                        <label>Select Department</label>
                                        <select class="form-control" name="department_id" data-search="true">
                                                @foreach($department as $departments)
                                                   <option value="{{$departments->id}}">{{$departments->departmentName}}</option>
                                                @endforeach
                                        </select>
                                        </div>
                                       </div>

                                       <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Start date</label>
                                               <input type="text" class="form-control date-picker" name="start_date" autocomplete="off" required>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                               <label>End date</label>
                                               <input type="text" class="form-control date-picker" name="end_date" autocomplete="off" required>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Payment Mode</label>
                                                <select class="form-control" name="payment_select">
                                                    <option value="1">On</option>
                                                    <option value="0">Off</option>
                                                </select>
                                            </div> 
                                        </div>

                                        <input type="hidden" name="type" value="dept_wise_data">
                                       
                                         <div class="col-md-12">
                                                <button type="submit" target="_blank" value="preview" name="submit" class="btn btn-success"><i class="fa fa-search"></i>&nbsp; Preview</button>
                                                <button type="submit" target="_blank" value="dept_excel" name="dept_excel" class="btn btn-info"><i class="fa fa-download"></i>&nbsp; Generate Excel</button>
                                         </div> 
                                         {{ Form::close() }}
                                   </div>
                                      <div class="col-md-12" id="section_type" style="display:none">
                                            {!! Form::open(['method'=>'POST','action'=>'datesalarycontroller@extraovertimeReportshowLeftyEmployee', 'target' => '_blank']) !!}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Select Payment type</label>
                                                    <select class="form-control" name="payment_id">
                                                        <option value="0" selected>All</option>
                                                        <option value="1">Bank</option>
                                                        <option value="2">bKash</option>
                                                        <option value="3">Cash</option>
                                                    </select>
                                                </div>
                                            </div>   
                                               <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Select Section</label>
                                                <select class="form-control" name="section_id" data-search="true">
                                                        @foreach($section as $sections)
                                                           <option value="{{$sections->empSection}}">{{$sections->empSection}}</option>
                                                        @endforeach
                                                </select>
                                                </div>
                                               </div>

                                               <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Start date</label>
                                                       <input type="text" class="form-control date-picker" name="start_date" autocomplete="off" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                       <label>End date</label>
                                                       <input type="text" class="form-control date-picker" name="end_date" autocomplete="off" required>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Payment Mode</label>
                                                        <select class="form-control" name="payment_select">
                                                            <option value="1">On</option>
                                                            <option value="0">Off</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <input type="hidden" name="type" value="section_wise_data">
                                                
                                                <div class="col-md-12">
                                                        <button type="submit" target="_blank" value="preview" name="submit" class="btn btn-success"><i class="fa fa-search"></i>&nbsp; Preview</button>
                                                        <button type="submit" target="_blank" value="section_excel" name="section_excel" class="btn btn-info"><i class="fa fa-download"></i>&nbsp; Generate Excel</button>
                                                 </div> 
                                                 {{ Form::close() }}
                                           </div>
                                   </div>
                                   <div class="col-md-12" id="employee_type" style="display:none">
                                        {!! Form::open(['method'=>'POST','action'=>'datesalarycontroller@extraovertimeReportshowLeftyEmployee', 'target' => '_blank']) !!}
                                             <div class="col-md-3">
                                                <div class="form-group" style="display:none">
                                                    <label>Select Payment type</label>
                                                    <select class="form-control" name="payment_id">
                                                        <option value="0" selected>All</option>
                                                        <option value="1">Bank</option>
                                                        <option value="2">bKash</option>
                                                        <option value="3">Cash</option>
                                                    </select>
                                                </div>
                                                 <div class="form-group">
                                                   <label>Select Employee</label>  
                                                <select class="form-control" name="emp_id" data-search="true">
                                                        @foreach($employee as $employees)
                                                           <option value="{{$employees->id}}">({{$employees->employeeId}}) {{$employees->empFirstName}} {{$employees->empLastName}}</option>
                                                        @endforeach
                                                </select>
                                                 </div>
                                               </div>
        
                                               <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Start date</label>
                                                       <input type="text" class="form-control date-picker" name="start_date" autocomplete="off" required>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                       <label>End date</label>
                                                       <input type="text" class="form-control date-picker" name="end_date" autocomplete="off" required>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="type" value="employee_wise_data">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Payment Mode</label>
                                                        <select class="form-control" name="payment_select">
                                                            <option value="1">On</option>
                                                            <option value="0">Off</option>
                                                        </select>
                                                    </div> 
                                                </div>
                                                <div class="col-md-12">
                                                        <button type="submit" target="_blank" value="preview" name="submit" class="btn btn-success"><i class="fa fa-search"></i>&nbsp; Preview</button>
                                                        <button type="submit" target="_blank" value="emp_excel" name="emp_excel" class="btn btn-info"><i class="fa fa-download"></i>&nbsp; Generate Excel</button>
                                                 </div> 
                                                {{ Form::close() }}
                                     </div>




                                     <div class="col-md-12" id="month_type" style="display:none">
                                        {!! Form::open(['method'=>'POST','action'=>'datesalarycontroller@extraovertimeReportshowLeftyEmployee', 'target' => '_blank']) !!}
                                             <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Select Payment type</label>
                                                    <select class="form-control" name="payment_id">
                                                        <option value="0" selected>All</option>
                                                        <option value="1">Bank</option>
                                                        <option value="2">bKash</option>
                                                        <option value="3">Cash</option>
                                                    </select>
                                                </div>
                                            </div>
                                                <div class="col-md-4">
                                                 <div class="form-group">
                                                    <label>Select Month</label>  
                                                    <input type="text" class="form-control" id="ot_month" name="ot_month" autocomplete="off" required>
                                                 </div>
                                                </div>

                                                <input type="hidden" name="type" value="month_wise_data">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Payment Mode</label>
                                                        <select class="form-control" name="payment_select">
                                                            <option value="1">On</option>
                                                            <option value="0">Off</option>
                                                        </select>
                                                    </div> 
                                                </div>
                                                <div class="col-md-12">
                                                        <button type="submit" target="_blank" value="preview" name="submit" class="btn btn-success"><i class="fa fa-search"></i>&nbsp; Preview</button>
                                                        <button type="submit" target="_blank" value="month_excel" name="month_excel" class="btn btn-info"><i class="fa fa-download"></i>&nbsp; Generate Excel</button>
                                                 </div> 
                                         {{ Form::close() }}
                                     </div>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
          $("#ot_type").change(function(){
            var select_type= $("#ot_type").val();
            if(select_type==1){
              $("#department_type").show();
              $("#section_type").hide();
              $("#employee_type").hide();
              $("#month_type").hide();
            }

            if(select_type==2){
              $("#section_type").show();
              $("#department_type").hide();
              $("#employee_type").hide();
              $("#month_type").hide();
            }

            if(select_type==3){
              $("#employee_type").show();
              $("#department_type").hide();
              $("#section_type").hide();
              $("#month_type").hide();
            }

            if(select_type==4){
              $("#month_type").show();
              $("#employee_type").hide();
              $("#department_type").hide();
              $("#section_type").hide();
            }
          });



          $('#ot_month').datepicker({ 
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm-dd",
                showButtonPanel: true,
                currentText: "This Month",
                onChangeMonthYear: function (year, month, inst) {
                    $(this).val($.datepicker.formatDate('yy-mm-dd', new Date(year, month - 1, 1)));
                },
                onClose: function(dateText, inst) {
                    var month = $(".ui-datepicker-month :selected").val();
                    var year = $(".ui-datepicker-year :selected").val();
                    $(this).val($.datepicker.formatDate('yy-mm-dd', new Date(year, month, 1)));
                }
            }).focus(function () {
                $(".ui-datepicker-calendar").hide();
            });



        });
    </script>
    @include('include.copyright')
@endsection