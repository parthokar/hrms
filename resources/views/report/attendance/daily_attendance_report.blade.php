@extends('layouts.master')
@section('title', 'Daily Attendace Report')
@section('content')
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>

        @elseif(Session::has('error'))
            <p id="alert_message" class="alert alert-warning">{{ Session::get('error') }}</p>

        @endif
        <div class="panel" style="border-radius:4px">
        <div class="panel-header">
        <h2><strong>Daily Attendance</strong> Sheet</h2>
        </div>
        <div class="panel-content">
        {!! Form::label('date', 'Select a date: ') !!}
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <input type="text" class="form-control" id="date" name="date"/>
                </div>
            </div>
        </div>
        <hr>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Full Name</th>
                <th>Employee Id</th>
                <th>Designation</th>
                <th>Date</th>
                <th>In time</th>
                <th>Out Time</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody id="attendance_display">
            </tbody>
        </table>
        </div>
        </div>
        @include('attendance.modal.showAttendance')
    </div>

    <script>
        $(function() {
            var dateVariable;
            var data;
            $("#date").datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: function(dateText) {
                    dateVariable = dateText;
//                    alert(dateVariable);
//                    alert($("#date").val())
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url:'data/'+dateVariable,
                        method:'POST',
                        dataType: 'json',
                        success: function (response) {
                            $.each(response, function (i, item) {

//                                console.log(typeof item.in_time);
                                if(typeof item.in_time === "undefined"){
                                    item.in_time="";
                                    late="<p class='red-text'>Absent</p>";
                                }

                                else if(item.in_time>'{{ \App\Http\Controllers\AttendanceController::late_time() }}')
                                {
                                    late="<p class='late-text'>Late</p>";
                                }
                                else{
                                    late="<p class='safe-text'>Present</p>";
                                }
                                if(typeof item.out_time==='undefined'){
                                    item.out_time='';
                                }
                                else if(item.in_time===item.out_time)
                                {
                                    item.out_time='<p class="red-text">Not Given</p>';
                                }
                                if(typeof item.date==='undefined'){
                                    item.date="<p class='red-text'>"+dateVariable+"</p>"
                                }

                                data+="<tr><td>"+item.empFirstName+" "+item.empLastName+"</td> <td>"+
                                    item.employeeId+"</td> <td>"+item.designation+"</td> <td>"+item.date+
                                    "</td> <td>"+item.in_time+"</td> <td>"+item.out_time+"</td> <td>"+late+
                                    "</td> <td><a href='"+window.location.href+"/"+ item.id+"/"+dateVariable+"' class='btn btn-default btn-sm show-attendance-add'><i class='fa fa-plus'></i></a> </td></tr>";


                            })
                            $('#attendance_display').html(data);
                            data="";

                        },
                        error:function () {

                        }

                    });
                }

            });

            $('table #attendance_display').on('click','.show-attendance-add',function (event) {
                event.preventDefault();
                var url=$(this).attr('href');


                $.ajax({
                    url:url,
                    method:'GET',
                    dataType:'html',
                    success:function (response) {
                        $('#show-attendance-form').html(response)


                    }
                });


                $('#showAttendance').modal('show');

            })

        });
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 8000);

    </script>
    @include('include.copyright')
@endsection