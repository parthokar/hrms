@extends('layouts.master')
@section('title', 'Daily Absent Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-md-8">
                                <h3 ><i class="fa fa-users "></i> Absent Report For <strong>{{ \Carbon\Carbon::parse($request->date)->format('d M Y (l)') }}</strong></h3>

                            </div>

                            {!! Form::open(['target'=>'_blank','method'=>'post']) !!}
                            <div class="col-md-4" style="margin-top:8px;">
                                <input type="hidden" name="reportType" value="{{$request->reportType}}">
                                <input type="hidden" name="date" value="{{$request->date}}">
                                <input type="hidden" name="unitId" value="{{$request->unitId}}">
                                <input type="hidden" name="floorId" value="{{$request->floorId}}">
                                <input type="hidden" name="line_id" value="{{$request->line_id}}">
                                <input type="hidden" name="departmentId" value="{{$request->departmentId}}">
                                <input type="hidden" name="designationId" value="{{$request->designationId}}">
                                <input type="hidden" name="empGenderId" value="{{$request->empGenderId}}">
                                <input type="hidden" name="accStatus" value="{{$request->accStatus}}">
                                <input type="hidden" name="coldesignation" value="1">
                                <input type="hidden" name="colcontinueabsent" value="1">
                                @if(isset($request->colPhone))
                                    <input type="hidden" name="colPhone" value="{{$request->colPhone}}">
                                @endif
                                @if(isset($request->sectionName[0]))
                                    @for($i=0;$i<count($request->sectionName);$i++)
                                        @if(isset($request->sectionName[$i]))
                                            <input type="hidden" name="sectionName[]" value="{{$request->sectionName[$i]}}">
                                        @endif
                                    @endfor
                                @endif
                                <input type="hidden" name="pageOrientation" value="{{$request->pageOrientation}}">
                                <input type="hidden" name="pagesize" value="{{$request->pagesize}}">
                                <input type="hidden" name="extensions" value="{{$request->extensions}}">
                                <button type="submit" value="PDF Department Wise" name="viewType" class="btn btn-primary btn-round btn-sm"  style="float:right;" ><i class="fa fa-file-pdf-o"></i> &nbsp Download PDF</button>
                                {{--                                <a href="#" data-toggle="modal" data-target="#search_employee" class="btn btn-success btn-round btn-sm"  style="float:right;" ><i class="fa fa-search"></i> Search Employee </a>--}}
                            </div>
                            {!! Form::close() !!}

                        </div>

                    </div>

                    <div class="panel-content pagination2 table-responsive">

                        @foreach($departments as $line)
                            <?php
                            $query1=$query;
                            $query1.=" AND departments.id='".$line->id."' GROUP BY employees.id ORDER BY attendance.date DESC";

                            $absent=DB::select($query1);
                            $countAbsent=count($absent);

                            ?>
                            @if($countAbsent!=0)
                                <h4><b>Department: {{$line->departmentName}}</b></h4>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Emp. ID</th>
                                        <th>Name</th>
                                        @if(!empty($request->coldesignation))
                                            <th>Designation</th>
                                        @endif
                                        @if(!empty($request->coldepartment))
                                            <th>Department</th>
                                        @endif
                                        @if(!empty($request->coljoiningdate))
                                            <th>Joining Date</th>
                                        @endif
                                        @if(!empty($request->colgender))
                                            <th>Gender</th>
                                        @endif
                                        @if(!empty($request->colunit))
                                            <th>Unit</th>
                                        @endif
                                        @if(!empty($request->colfloor))
                                            <th>Floor</th>
                                        @endif
                                        @if(!empty($request->colline))
                                            <th>Line</th>
                                        @endif
                                        @if(!empty($request->colPhone))
                                            <th>Contact No.</th>
                                        @endif
                                        @if(!empty($request->colsection))
                                            <th>Section</th>
                                        @endif
                                        @if(!empty($request->colstatus))
                                            <th>Status</th>
                                        @endif
                                        @if(!empty($request->colcontinueabsent))
                                            <th>Last Attend Date</th>
                                            <th>Continue Absent</th>
                                        @endif
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach( $absent as $key=> $employee)
                                        <tr>
                                            <td>
                                                <b><a href="{{route('employee_profile.show',base64_encode($employee->id))}}" target="_blank">{{$employee->employeeId}}</a></b>
                                            </td>
                                            <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>
                                            @if(!empty($request->coldesignation))
                                                <td>{{$employee->designation}}</td>
                                            @endif
                                            @if(!empty($request->coldepartment))
                                                <td>{{$employee->departmentName}}</td>
                                            @endif
                                            @if(!empty($request->coljoiningdate))
                                                <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('Y-m-d')}}</td>
                                            @endif
                                            @if(!empty($request->colgender))
                                                <td>
                                                    @if($employee->empGenderId==1)
                                                        Male
                                                    @endif
                                                    @if($employee->empGenderId==2)
                                                        Female
                                                    @endif
                                                    @if($employee->empGenderId==3)
                                                        Other
                                                    @endif
                                                </td>
                                            @endif
                                            @if(!empty($request->colunit))
                                                <td>{{$employee->unitName}}</td>
                                            @endif
                                            @if(!empty($request->colfloor))
                                                <td>{{$employee->floorName}}</td>
                                            @endif
                                            @if(!empty($request->colline))
                                                <td>{{$employee->LineName}}</td>
                                            @endif
                                            @if(!empty($request->colPhone))
                                                <td>{{$employee->empPhone}}</td>
                                            @endif
                                            @if(!empty($request->colsection))
                                                <td>{{$employee->empSection}}</td>
                                            @endif
                                            @if(!empty($request->colstatus))
                                                <td>
                                                    @if($employee->empAccStatus==1)
                                                        Active
                                                    @elseif($employee->empAccStatus==0)
                                                        Inactive
                                                    @endif
                                                </td>
                                            @endif
                                            @if(!empty($request->colcontinueabsent))
                                                <?php
                                                $start_date=\Carbon\Carbon::parse($employee->date);
                                                if(\Carbon\Carbon::parse($request->date)>$start_date){
                                                    $diff=\Carbon\Carbon::parse($request->date)->diffInDays($start_date);
                                                }
                                                else{
                                                    $diff=0;
                                                }
                                                ?>
                                                <td>{{\Carbon\Carbon::parse($employee->date)->format('d M Y')}}</td>
                                                <td> {{ $diff }}</td>
                                            @endif
                                            <td><span class="red-text">Absent</span></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif

                        @endforeach


                        <hr>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection