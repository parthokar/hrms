@extends('layouts.master')
@section('title', 'Daily Missing Attendance Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 ><i class="fa fa-users "></i> Missing Attendance For <strong>{{ \Carbon\Carbon::parse($request->date)->format('d M Y (l)') }}</strong></h3>

                            </div>

                        </div>

                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-dynamic table-hover table-striped table-bordered">
                            <thead style="font-size:15px;font-weight:bold;">
                            <tr>
                                <th>Name</th>
                                <th>id</th>
                                @if(!empty($request->coldesignation))
                                    <th>Designation</th>
                                @endif
                                @if(!empty($request->coldepartment))
                                    <th>Department</th>
                                @endif
                                @if(!empty($request->coljoiningdate))
                                    <th>Joining Date</th>
                                @endif
                                @if(!empty($request->colgender))
                                    <th>Gender</th>
                                @endif
                                @if(!empty($request->colunit))
                                    <th>Unit</th>
                                @endif
                                @if(!empty($request->colfloor))
                                    <th>Floor</th>
                                @endif
                                @if(!empty($request->colline))
                                    <th>Line</th>
                                @endif
                                @if(!empty($request->colsection))
                                    <th>Section</th>
                                @endif
                                @if(!empty($request->colstatus))
                                    <th>Status</th>
                                @endif
                                <th>In Time</th>
                                @if(!empty($request->collateTime))
                                    <th>Late Time</th>
                                @endif
                                <th>Out Time</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($present as $employee)
                                <tr>
                                    <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>
                                    <td>
                                        <b><a href="{{route('employee_profile.show',base64_encode($employee->id))}}" target="_blank">{{$employee->employeeId}}</a></b>
                                    </td>
                                    @if(!empty($request->coldesignation))
                                        <td>{{$employee->designation}}</td>
                                    @endif
                                    @if(!empty($request->coldepartment))
                                        <td>{{$employee->departmentName}}</td>
                                    @endif
                                    @if(!empty($request->coljoiningdate))
                                        <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('Y-m-d')}}</td>
                                    @endif
                                    @if(!empty($request->colgender))
                                        <td>
                                            @if($employee->empGenderId==1)
                                                Male
                                            @endif
                                            @if($employee->empGenderId==2)
                                                Female
                                            @endif
                                            @if($employee->empGenderId==3)
                                                Other
                                            @endif
                                        </td>
                                    @endif
                                    @if(!empty($request->colunit))
                                        <td>{{$employee->unitName}}</td>
                                    @endif
                                    @if(!empty($request->colfloor))
                                        <td>{{$employee->floorName}}</td>
                                    @endif
                                    @if(!empty($request->colline))
                                        <td>{{$employee->LineName}}</td>
                                    @endif
                                    @if(!empty($request->colsection))
                                        <td>{{$employee->empSection}}</td>
                                    @endif
                                    @if(!empty($request->colstatus))
                                        <td>
                                            @if($employee->empAccStatus==1)
                                                Active
                                            @elseif($employee->empAccStatus==0)
                                                Inactive
                                            @endif
                                        </td>
                                    @endif
                                    <td>

                                        {{$employee->in_time}}

                                    </td>
                                    <td>
                                        <span class="red-text">Not Given</span>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                        <hr>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection