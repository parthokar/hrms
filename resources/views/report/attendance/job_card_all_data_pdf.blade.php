<?php
use Illuminate\Support\Facades\DB;

function roundTime($time){
    $hour=date('G',strtotime($time));
    $minute=date('i',strtotime($time));

    if($minute<25){
        $minute=0;
    }
    elseif ($minute>=25 &&$minute<55){
        $minute=.5;
    }
    else{
        $minute=0;
        $hour++;
    }
    $kind=number_format((float)($hour+$minute),1,'.','');
    return $kind;
}
function late_time($pretime,$time){
    $phour=date('G',strtotime($pretime));
    $pminute=date('i',strtotime($pretime));
    $hour=date('G',strtotime($time));
    $minute=date('i',strtotime($time));
    $thour=$phour+$hour;
    $tminute=$pminute+$minute;
    $h=floor($tminute/60);
    $tminute=sprintf("%02d",$tminute%60);
    $thour+=$h;
    $thour=sprintf("%02d",$thour);
    return "$thour:$tminute";
}
$festivalLeave = DB::table('tb_festival_leave')->get();
$abs=0;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Attendance Card</title>
    <style>
        #employeeDetails{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 90%;
            font-size: 11px;
            margin:0px auto;
            margin-top: 15px;
        }
        #employeeDetails td, #employeeDetails th {
            /*border: 1px solid #ddd;*/
            font-size: 12px;
        }
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }
        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: center !important;
        }
        #customers th {
            text-align: left;
            padding: 5px;
        }
        table td {
            padding: 2px;
            margin: 0;
        }
        .reportHeaderArea{
            text-align: center;
        }
        .reportHeader{
            line-height: 4px;
        }
        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }
        .reportHeaderCompany{
            font-size: 18px !important;
        }
        #cardFooter{
            border-collapse: collapse;
            font-size:11px;
            width:70%;
            margin:0px auto;
            margin-top:15px;
            /*float:right; */
        }
        .reportDateRange{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif  !important;
            font-size: 12px !important;
            font-weight: bold;
        }
        #cardFooter td {
            text-align: left !important;
        }
        @media print{
            .print_div{page-break-after: always;}
            table { page-break-inside:auto; }
            tr       { page-break-inside:avoid; page-break-after:auto;}
            thead {display: table-header-group;}
            tbody { page-break-after:always;
                display: table-row-group;}
        }
    </style>
</head>
<body>

@if(count($employee))
    @foreach($employee as $emp)
        <?php
        $approvedLeave = DB::table('tb_leave_application')
            ->leftJoin('tb_leave_type','tb_leave_application.leave_type_id','=','tb_leave_type.id')
            ->where(['employee_id' => $emp->id, 'status' => 1])
            ->select('tb_leave_application.*','tb_leave_type.leave_type')
            ->get();
        if(\Carbon\Carbon::parse($emp->empJoiningDate)->format('m-Y')==\Carbon\Carbon::parse($request->end_date)->format('m-Y'))
        {
            $start=\Carbon\Carbon::parse($emp->empJoiningDate)->toDateString();
            $total_days=\Carbon\Carbon::parse($emp->empJoiningDate)->diffInDays($request->end_date)+1;
            $k=1;
        }
        else{
            $start=$request->start_date;
            $k=0;
        }

        if(\Carbon\Carbon::parse($emp->date_of_discontinuation)->format('m-Y')==\Carbon\Carbon::parse($request->end_date)->format('m-Y'))
        {
            $end=\Carbon\Carbon::parse($emp->date_of_discontinuation)->toDateString();
        }
        else{
            $end=$request->end_date;
        }
        $absent=0;
        $working_days=0;
        $weekends=0;
        $present=0;
        $late=0;
        $holiday=0;
        $onleave=0;
        $ot=0;
        $late_time="00:00";
        $dates ='';
        $datas ='';
        ?>
        <div class="container print_div">
            <div class="reportHeaderArea">
                <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
                <p class="reportHeader">{{$companyInformation->company_address1}}</p>
                <p class="reportDateRange">Job Card Report Month From <b>{{\Carbon\Carbon::parse($request->start_date)->format('d-M-Y')}}</b> to <b>{{\Carbon\Carbon::parse($request->end_date)->format('d-M-Y')}} </b></p>
            </div>
            <table id="employeeDetails" >
                <tr>
                    <td style="text-align:left" width="35%"><b>Employee ID :</b> {{$emp->employeeId}}</td>
                    <td style="text-align:left" width="35%"><b>Name : </b> {{$emp->empFirstName." ".$emp->empLastName }}</td>
                    <td style="text-align:left" width="30%"><b>Joining Date :</b> {{\Carbon\Carbon::parse($emp->empJoiningDate)->format('d-M-Y')}}</td>
                </tr>
                <tr>
                    <td style="text-align:left" width="35%"><b>Designation :</b> {{$emp->designation}}</td>
                    <td style="text-align:left" width="35%"><b>Department : </b> {{$emp->departmentName}}</td>
                    <td style="text-align:left" width="30%"><b> Section : </b> {{$emp->empSection}}</td>
                </tr>
            </table>
            <table id='customers' style="margin-top:15px;font-size:11px;" border="1px">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Day</th>
                    <th>In Time </th>
                    <th>Out Time</th>
                    @if($emp->empOTStatus==1)
                        <th>OT</th>
                    @endif
                    <th>Late Time</th>
                    <th>Status</th>
                    <th>Remarks</th>
                </tr>
                </thead>
                <tbody>
                @for($i = \Carbon\Carbon::createFromFormat('Y-m-d', $start); $i->lte(\Carbon\Carbon::createFromFormat('Y-m-d', $end)); $i=$i->addDay())
                    <?php
                    $key=0;
                    $fes=0;
                    $lev=0;
                    $dates = $i->format('Y-m-d');
                    $timestamp = strtotime($dates);
                    $dept_holiday=DB::table('emp_wise_holiday')
                    ->where('emp_id',$emp->id)
                    ->get();
                    $day = date('l', $timestamp);
                    foreach ($festivalLeave as $fe){
                        $fesDayStart=strtotime($fe->start_date);
                        $fesDayEnd=strtotime($fe->end_date);
                        if($timestamp >=$fesDayStart && $timestamp<=$fesDayEnd){
                            $fes=1;
                            $fesPurpose=$fe->purpose;
                            break;
                        }
                    }
                   // department wise holiday
                    foreach($dept_holiday as $d_holi){
                        $fesDayStart=strtotime($d_holi->holiday_date);
                        $fesDayEnd=strtotime($d_holi->holiday_end_date);
                        if($timestamp >=$fesDayStart && $timestamp<=$fesDayEnd){
                            $fes=1;
                            $fesPurpose=$d_holi->perpose;
                            break;
                        }
                    }
                    // department wise holiday
                    foreach ($approvedLeave as $al){
                        if($timestamp >=strtotime($al->leave_starting_date) && $timestamp<=strtotime($al->leave_ending_date))
                        {
                            // return $al->leave_type_id;
                            $lev=1;
                            $leave_name=$al->leave_type;
                            break;
                        }
                    }
                    if ($lev != 1) {
                        if ($fes != 1) {
                            $data = DB::select("SELECT attendance.*, employees.empFirstName, employees.empLastName,employees.empSection, employees.date_of_discontinuation, employees.empJoiningDate, employees.employeeId from
                        `attendance` inner join `employees` on `attendance`.`emp_id` = `employees`.`id` where employees.id =" . $emp->id . " and date='$dates'");

                            if (count($data)) {
                                $datas = array_merge($data, [$dates, $abs]);
                            } else {
                                $data = [$emp];
                                $datas = array_merge($data, [$dates, ++$abs]);
                            }
                        } else {
                            $data = [$emp];
                            $datas = array_merge($data, [$dates, "Holiday@$fesPurpose"]);
                        }
                    } else {
                        $data = [$emp];
                        $datas = array_merge($data, [$dates, "On Leave@$leave_name"]);
                    }
                    $working_days++;
                    $leave_check= explode('@',$datas[2]);
                    $holidayPurpose= explode('@',$datas[2]);
                    $regular_day=0;
                    ?>
                    <tr>
                        <td>{{\Carbon\Carbon::parse($i)->format('d-M-Y')}}</td>
                        <td>{{\Carbon\Carbon::parse($i)->format('l')}}</td>
                        <td>
                            @if(isset($datas[0]->in_time))
                                <?php
                                $present++;
                                ?>
                                {{ date('G:i', strtotime($datas[0]->in_time)) }}
                            @endif
                        </td>
                        <td>
                            @if(isset($datas[0]->out_time))
                                @if(date('h:i:a',strtotime($datas[0]->in_time))==date('h:i:a',strtotime($datas[0]->out_time)))
                                  <span style="color: #FF0000;">Not Given</span>
                                @else 
                                {{date('G:i', strtotime($datas[0]->out_time))}}
                                @endif 
                             @endif
                         </td>

                        @if($emp->empOTStatus==1)
                        <td>
                            @if(isset($datas[0]->out_time) && date('h:i:a',strtotime($datas[0]->out_time))!=date('h:i:a',strtotime($datas[0]->in_time)))
                                @if($datas[0]->out_time>$datas[0]->exit_times)
                                    @if(\Carbon\Carbon::parse($datas[1])->format('l')=="Friday")
                                        <?php
                                        $regular_day=\Illuminate\Support\Facades\DB::table('tbweekend_regular_day')->where('weekend_date','=',$datas[1])->count();
                                        ?>
                                        @if(!$regular_day)
                                            <?php
                                            $ot+=roundTime(date('G:i', strtotime($datas[0]->out_time) - strtotime($datas[0]->exit_times)))+8
                                            ?>
                                            {{ roundTime(date('G:i', strtotime($datas[0]->out_time) - strtotime($datas[0]->exit_times)))+8 }}
                                        @else
                                            <?php
                                            $ot+=roundTime(date('G:i', strtotime($datas[0]->out_time) - strtotime($datas[0]->exit_times)))
                                            ?>
                                            {{ roundTime(date('G:i', strtotime($datas[0]->out_time) - strtotime($datas[0]->exit_times))) }}
                                        @endif
                                    @else
                                        <?php
                                        $ot+=roundTime(date('G:i', strtotime($datas[0]->out_time) - strtotime($datas[0]->exit_times)))
                                        ?>
                                        {{ roundTime(date('G:i', strtotime($datas[0]->out_time) - strtotime($datas[0]->exit_times))) }}
                                    @endif
                                @else
                                    @if(\Carbon\Carbon::parse($datas[1])->format('l')=="Friday")
                                        <?php
                                        $regular_day=\Illuminate\Support\Facades\DB::table('tbweekend_regular_day')->where('weekend_date','=',$datas[1])->count();
                                        ?>
                                        @if(!$regular_day)
                                            <?php
                                            $ot+=roundTime(date('G:i', strtotime($datas[0]->out_time) - strtotime($datas[0]->in_time)))-1
                                            ?>
                                            {{roundTime(date('G:i', strtotime($datas[0]->out_time) - strtotime($datas[0]->in_time)))-1}}
                                        @else
                                            0.0
                                        @endif
                                    @else
                                        0.0
                                    @endif
                                @endif
                            @endif
                        </td>
                    @endif
                    <td>
                        @if(isset($datas[0]->in_time))
                            @if($datas[0]->in_time>$datas[0]->max_entry)
                                <?php
                                $late_time=late_time($late_time,date('G:i', strtotime($datas[0]->in_time) - strtotime($datas[0]->max_entry)));

                                ?>
                                {{ date('G:i', strtotime($datas[0]->in_time) - strtotime($datas[0]->max_entry)) }}
                            @else
                                00:00
                            @endif
                        @endif
                    </td>


                        <td>
                            @if(isset($datas[0]->in_time))
                                @if($datas[0]->in_time>$datas[0]->max_entry)
                                    <?php $late++; ?>
                                    <span>Late</span>
                                @else
                                    <span>Present</span>
                                @endif
                            @else
                                @if($holidayPurpose[0]=='Holiday' || $leave_check[0]=='On Leave')
                                    @if($leave_check[0]!='On Leave')
                                        <?php $working_days--; ?>
                                    @else
                                        <?php $onleave++; ?>
                                    @endif
                                    @if($leave_check[0]=='On Leave')
                                        @if(isset($leave_check[1]))
                                            <span>{{$leave_check[1]}}</span>
                                        @endif
                                    @else
                                        <?php
                                        ?>
                                        <span>
                                        <?php
                                            $holiday++;
                                            ?>
                                            @if(isset($holidayPurpose[1]))
                                                <span>{{$holidayPurpose[1]}}</span>
                                            @endif
                                    </span>
                                    @endif
                                @else
                                    @if(\Carbon\Carbon::parse($datas[1])->format('l')=="Friday")
                                        <?php
                                        $regular_day=\Illuminate\Support\Facades\DB::table('tbweekend_regular_day')->where('weekend_date','=',$datas[1])->count();
                                        ?>
                                        @if($regular_day)
                                            <?php
                                            $absent++;
                                            ?>
                                            <span style="color:#FF0000">Absent</span>
                                        @else
                                            <?php
                                            $weekends++;
                                            ?>
                                            <span>Weekend</span>
                                        @endif
                                    @else
                                        <?php
                                        $absent++;
                                        ?>
                                        <span style="color:#FF0000">Absent</span>
                                    @endif
                                @endif
                            @endif
                        </td>
                        <td></td>
                    </tr>
                @endfor
                <tr style="font-weight: bold">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    @if($emp->empOTStatus==1)
                        <td>{{$ot}} hour</td>
                    @endif
                    <td>{{$late_time}}</td>
                    <td></td>
                    <td></td>
                </tr>
                </tbody>
            </table>
            <div>
                <center>
                    <table id="cardFooter">
                        <tr>
                            <td width="50%"><b>Total Days: </b>{{ $total_days }} Days</td>
                        </tr>
                        <tr>
                            {{--$working_days--}}
                            <td width="50%"><b>Holiday: </b>{{$holiday}} Days</td>
                        </tr>
                        <tr>
                            {{--$working_days--}}
                            <td width="50%"><b>Weekends: </b>{{$weekends}} Days</td>
                        </tr>
                        <tr>
                            <td  width="50%"><b>Total Present Days: </b> {{ $present }} Days</td>
                        </tr>
                        <tr>
                            <td class="padTop10"><b>Total Leave :</b> {{ ($onleave) }} Days</td></tr>
                        <tr>
                            <td class="padTop10"><b>Total Absent :</b> {{ $absent }} Days</td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    @endforeach
@else
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportDateRange">Job Card Report Month From <b>{{\Carbon\Carbon::parse($request->start_date)->format('d-M-Y')}}</b> to <b>{{\Carbon\Carbon::parse($request->end_date)->format('d-M-Y')}} </b></p>
    </div>

    <h3 style="color: red; text-align: center"> No Employee Found </h3>
@endif
</body>
</html>

<script>
    window.print();
</script>






