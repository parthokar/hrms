<?php
$finalTotal=0;
$finalPresent=0;
$finalLate=0;


?>

<!DOCTYPE html>
<html>
<head>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 5px;
            text-align: left;
        }
        @media print {
            table { page-break-inside:auto;
                padding-bottom: 10px;
                margin-bottom: 10px;
            }

            /*tr    { page-break-inside:avoid; page-break-after:auto;}*/
            /*thead {display: table-header-group;}*/
            /*tbody { page-break-after:always;*/
                /*display: table-row-group;}*/
        }
    </style>
    <title>Summary Report on Date</title>
</head>
<body>
<div style="text-align: center">
    <h2>{{$companyInformation->company_name}}</h2>
    <p>Summary Report on Date</p>
    <p>{{\Illuminate\Support\Carbon::parse($request->date)->format('j M Y')}}</p>
</div>
<p style="text-align: right;">Print Date:{{\Carbon\Carbon::now('Asia/Dhaka')->toDateTimeString()}}</p>

<table style="width:100%;">

    <thead>
    <tr>
        <th>Department</th>
        <th>Designations</th>
        <th>A</th>
        <th>L</th>
        <th>P</th>
        <th>T</th>
    </tr>
    </thead>
    <tbody>
    @foreach($departments as $d)
        <?php
            $finalDepartmentTotal=0;
            $finalDepartmentPresent=0;
            $finalDepartmentLate=0;

            $employeesDesignation=\Illuminate\Support\Facades\DB::table('employees')
                ->leftJoin('designations','employees.empDesignationId','=','designations.id')->where('empDepartmentId','=',$d->id)->distinct('employees.empDesignationId')->get(['designations.id','designations.designation']);
            $c=count($employeesDesignation);
        ?>
        <td rowspan="{{$c+2}}">{{$d->departmentName}}</td>

        @foreach($employeesDesignation as $key=>$designation)
            <?php
            $date=\Illuminate\Support\Carbon::parse($request->date)->toDateString();

            $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('empDepartmentId', '=', $d->id)->where('empDesignationId','=',$designation->id)->count();
            $finalDepartmentTotal+=$total;

            $totalP=DB::table('employees')->join('attendance','employees.id','=','attendance.emp_id')->where('employees.empAccStatus','=','1')->where(['attendance.date'=>$date])->where('empDepartmentId', '=', $d->id)->where('empDesignationId','=',$designation->id)->count();
//            $totalL=DB::table('employees')->join('attendance','employees.id','=','attendance.emp_id')->leftJoin('attendance_setup','employees.empShiftId','=','attendance_setup.id')->where('employees.empAccStatus','=','1')->where('attendance.in_time','>','attendance_setup.max_entry_time')->where(['attendance.date'=>"$date"])->where('empDepartmentId', '=', $d->id)->where('empDesignationId','=',$designation->id)->count();
            $tl=DB::select("select count(*) as aggregate from `employees` inner join `attendance` on `employees`.`id` = `attendance`.`emp_id` left join `attendance_setup` on `employees`.`empShiftId` = `attendance_setup`.`id` where `employees`.`empAccStatus` = 1 and `attendance`.`in_time` > attendance_setup.max_entry_time and `attendance`.`date` = '$date' and `empDepartmentId` = $d->id and `empDesignationId` = $designation->id");
            $totalL=$tl[0]->aggregate;
            $finalDepartmentPresent+=$totalP;
            $finalDepartmentLate+=$totalL;
            $finalTotal+=$total;
            $finalPresent+=$totalP;
            $finalLate+=$totalL;
            ?>
            <tr>


                <td>{{$designation->designation}}</td>
                <td>{{$total-$totalP}}</td>
                <td>{{$totalL}}</td>
                <td>{{$totalP-$totalL}}</td>
                <td>{{$total}}</td>
            </tr>
        @endforeach
        <tr style="background: #d1d4d4; ">
            <td><b>Total</b></td>
            <td><b>{{$finalDepartmentTotal-$finalDepartmentPresent}}</b></td>
            <td><b>{{$finalDepartmentLate}}</b></td>
            <td><b>{{$finalDepartmentPresent-$finalDepartmentLate}}</b></td>
            <td><b>{{$finalDepartmentTotal}}</b></td>

        </tr>
    @endforeach
    <tr style="background: #f7f7f7;">
        <td><b>Total</b></td>
        <td></td>
        <td><b>{{$finalTotal-$finalPresent}}</b></td>
        <td><b>{{$finalLate}}</b></td>
        <td><b>{{$finalPresent-$finalLate}}</b></td>
        <td><b>{{$finalTotal}}</b></td>

    </tr>
    </tbody>
</table>

</body>
</html>
