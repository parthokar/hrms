@extends('layouts.master')
@section('title', 'Job Card All')
@section('content')
    <div class="page-content ">
        <div class="row panel"  style="border:1px solid #999">

            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Job Card All(3-Shift)</b></h2>
                    <hr>
                </div>
            </div>
            @if(Session::has('message'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('message')}}</p>
            @endif


            {!! Form::open(['method'=>'POST','url'=>'/report/attendance/card_all/view/three/shift/data','target'=>'_blank']) !!}
            <div class="col-xlg-7 col-md-10 col-md-offset-1 col-xlg-offset-2">

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Start Date<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <input type="text" name="start_date" class="date-picker form-control" value="{{\Carbon\Carbon::now()->format('m/d/Y')}}" required placeholder="Select Start date...">
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">End Date<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <input type="text" name="end_date" class="date-picker form-control" value="{{\Carbon\Carbon::now()->format('m/d/Y')}}" required placeholder="Select End date...">
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Unit<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select name="unitId" class="form-control"  data-search="true">
                            <option value="0">All</option>
                            @foreach($units as $unit)
                                <option value="{{$unit->id}}">{{$unit->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Floor<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select name="floorId" class="form-control floor"  data-search="true">
                            <option value="0">All</option>
                            @foreach($floors as $floor)
                                <option value="{{$floor->id}}">{{$floor->floor}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Section<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select name="sectionName" class="form-control"  data-search="true">
                            <option value="0">All</option>
                            @foreach($sections as $section)
                                <option value="{{$section->empSection}}">{{$section->empSection}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Department<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select name="departmentId" class="form-control"  data-search="true">
                            <option value="0">All</option>
                            @foreach($departments as $department)
                                <option value="{{$department->id}}">{{$department->departmentName}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


{{--                <div class="form-group">--}}
{{--                    <div class="input-form-gap"></div>--}}
{{--                    <label class="col-md-3">Designation<span class="clon">:</span></label>--}}
{{--                    <div class="col-md-9">--}}
{{--                        <select name="designationId" class="form-control"  data-search="true">--}}
{{--                            <option value="0">All</option>--}}
{{--                            @foreach($designations as $designation)--}}
{{--                                <option value="{{$designation->id}}">{{$designation->designation}}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="form-group">--}}
{{--                    <div class="input-form-gap"></div>--}}
{{--                    <label class="col-md-3">Gender<span class="clon">:</span></label>--}}
{{--                    <div class="col-md-9">--}}
{{--                        <select name="empGenderId" class="form-control"  data-search="true">--}}
{{--                            <option value="0">All</option>--}}
{{--                            <option value="1">Male</option>--}}
{{--                            <option value="2">Female</option>--}}
{{--                            <option value="3">Other</option>--}}
{{--                        </select>--}}
{{--                    </div>--}}
{{--                </div>--}}

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Employee Status<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select class="form-control" name="accStatus">
                            <option selected value="1">Active</option>
                            <option value="0">InActive</option>
                        </select>
                    </div>

                    <div class="form-group padding-bottom-30-percent">
                        <div class="col-md-9 col-md-offset-3">
                            <hr>
                            <input type="submit" value="Preview" name="viewType" class="btn btn-success margin-top-10">

                            <button type="submit" value="Generate PDF" name="viewType" class="btn btn-primary margin-top-10"><i class="fa fa-file-pdf-o"></i> &nbsp; Generate PDF </button>


                            {{--<input type="reset" value="Clear " class="btn btn-warning margin-top-10">--}}
                        </div>
                    </div>

                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $("select.floor").change(function () {
                var floor= $(".floor option:selected").val();
                if(!floor){
                    var rr='<select class="form-control floor_lines" name="line_id">'+
                        '<option value="">All</option>'+

                        '</select>';
                    $('.floor_lines').html(rr);
                }
                else {
                    $.ajax({
                        url: 'floor/line/' + floor,
                        method: 'get',
                        dataType: 'html',
                        success: function (response) {
                            $('.floor_lines').html(response);
                        }
                    });
                }
            });
        });

        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>

    @include('include.copyright')
@endsection