@php
    use App\Http\Controllers\dashboardcontroller;
@endphp
@extends('layouts.master')
@section('title', 'Daily Attendance Report')
@section('content')
    <div class="page-content ">
        <div class="row panel"  style="border:1px solid #999">

            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Daily Attendance Report</b></h2>
                    <hr>
                </div>
            </div>


            {!! Form::open(['method'=>'POST','action'=>'ReportController@public_view_report_daily_attendance','target'=>'_blank']) !!}
            <div class="col-xlg-7 col-md-10 col-md-offset-1 col-xlg-offset-2">
                <div>
                    @if(Session::has('message'))
                        <p id="alert_message" class="alert alert-danger">{{Session::get('message')}}</p>
                    @elseif(Session::has('success'))
                        <p id="alert_message" class="alert alert-success">{{Session::get('success')}}</p>
                    @endif
                </div>


                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Select a Date<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <input type="text" name="date" class="date-picker form-control" value="{{\Carbon\Carbon::now()->format('m/d/Y')}}" required placeholder="Select a date...">
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Unit<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select name="unitId" class="form-control"  data-search="true">
                            <option value="0">All</option>
                            @foreach($units as $unit)
                                <option value="{{$unit->id}}">{{$unit->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Floor<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select name="floorId" class="form-control floor"  data-search="true">
                            <option value="0">All</option>
                            @foreach($floors as $floor)
                                <option value="{{$floor->id}}">{{$floor->floor}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Line<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select class="form-control floor_lines"  data-search="true"></select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Section<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select name="sectionName" class="form-control"  data-search="true">
                            <option value="">All</option>
                            @foreach($sections as $section)
                                <option value="{{$section->empSection}}">{{$section->empSection}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Department<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select name="departmentId" class="form-control"  data-search="true">
                            <option value="0">All</option>
                            @foreach($departments as $department)
                                <option value="{{$department->id}}">{{$department->departmentName}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Designation<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select name="designationId" class="form-control"  data-search="true">
                            <option value="0">All</option>
                            @foreach($designations as $designation)
                                <option value="{{$designation->id}}">{{$designation->designation}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Gender<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select name="empGenderId" class="form-control"  data-search="true">
                            <option value="0">All</option>
                            <option value="1">Male</option>
                            <option value="2">Female</option>
                            <option value="3">Other</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Employee Status<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select class="form-control" name="accStatus">
                            <option selected value="1">Active</option>
                            <option value="0">InActive</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <div class="input-form-gap"></div>
                        <div class="input-form-gap"></div>
                        <label class="col-md-3">Printable Column<span class="clon">:</span></label>
                        <div class="col-md-9">
                            <div class="form-control">

                                <input type="checkbox" checked name="coldepartment" value="1"> Department  &nbsp;&nbsp;
                                <input type="checkbox" checked name="coldesignation" value="1"> Designation  &nbsp;&nbsp;
                                <input type="checkbox" name="colgender" value="1"> Gender  &nbsp;&nbsp;
                                <input type="checkbox" checked name="collateTime" value="1"> Late Time &nbsp;&nbsp;
                                <input type="checkbox" name="colOverTime" value="1"> Over Time  &nbsp;&nbsp;<br><br>
                                <input type="checkbox" name="coljoiningdate" value="1"> Joiningdate
                                <input type="checkbox" name="colunit" value="1"> Unit  &nbsp;&nbsp;
                                <input type="checkbox" name="colfloor" value="1"> Floor &nbsp;&nbsp;
                                <input type="checkbox" name="colline" value="1"> Line &nbsp;&nbsp;
                                <input type="checkbox" name="colsection" value="1"> Section &nbsp;&nbsp;
                                <input type="checkbox" name="colstatus" value="1"> Status &nbsp;&nbsp;
                                <input type="checkbox" name="colshift" value="1"> Work Group &nbsp;&nbsp;
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-form-gap"></div>
                        <div class="input-form-gap"></div>
                        <label class="col-md-3">Page Size<span class="clon">:</span></label>
                        <div class="col-md-9">
                            <select class="form-control" name="pagesize">
                                <option selected value="A4">A4</option>
                                <option value="Legal">Legal</option>
                                <option value="Letter">Letter</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-form-gap"></div>
                        <label class="col-md-3">Page Orientation<span class="clon">:</span></label>
                        <div class="col-md-9">
                            <select class="form-control" name="pageOrientation">
                                <option selected value="Portrait">Portrait</option>
                                <option value="Landscape">Landscape</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-form-gap"></div>
                        <label class="col-md-3">Excel Extension<span class="clon">:</span></label>
                        <div class="col-md-9">
                            <select class="form-control" name="extensions">
                                <option selected value="xls">XLS</option>
                                <option value="xlsx">XLSX</option>
                                {{--<option value="csv">CSV</option>--}}
                            </select>
                        </div>
                    </div>

                    <div class="form-group padding-bottom-30-percent">
                        <div class="col-md-9 col-md-offset-3">
                            <hr>
                            <input type="submit" value="Preview" name="viewType" class="btn btn-success margin-top-10">


                            <span class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Generate/Export
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><button type="submit" value="Generate PDF" name="viewType" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i> &nbsp; Generate PDF </button></li>
                                    <li><button type="submit" value="Generate Excel" name="viewType" class="btn btn-blue"><i class="fa fa-file-excel-o"></i> &nbsp; Generate Excel </button></li>
                                </ul>
                            </span>

                            {{--<input type="reset" value="Clear" class="btn btn-warning margin-top-10">--}}
                        </div>
                    </div>

                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $("select.floor").change(function () {
                var floor= $(".floor option:selected").val();
                if(!floor){
                    var rr='<select class="form-control floor_lines" name="line_id">'+
                        '<option value="">All</option>'+

                        '</select>';
                    $('.floor_lines').html(rr);
                }
                else {
                    $.ajax({
                        url: 'floor/line/' + floor,
                        method: 'get',
                        dataType: 'html',
                        success: function (response) {
                            $('.floor_lines').html(response);
                        }
                    });
                }
            });
        });


        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);

    </script>

    @include('include.copyright')
@endsection