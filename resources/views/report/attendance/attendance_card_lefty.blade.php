@extends('layouts.master')
@section('title', 'Attendance Job Card')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3><i class="fa fa-table"></i> <strong>Employee Job </strong> Card Lefty</h3>
                    </div>
                    <div class="panel-content">
                        @if(Session::has('message'))
                            <p id="alert_message" class="alert alert-danger">{{Session::get('message')}}</p>
                        @endif
                        <div class="row" id="get-form">
                            {{Form::open(array('url' => 'attendance/card/pdf','method' => 'get', 'target'=>'_blank'))}}
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="required form-label">Start Date</label>
                                    <div class="prepend-icon">
                                        <input type="text" name="start_date" autocomplete="off" id="start_date" class="date-picker form-control" placeholder="Select a date..." required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="required form-label">End Date</label>
                                    <div class="prepend-icon">
                                        <input type="text" name="end_date" autocomplete="off" id="end_date" class="date-picker form-control" placeholder="Select a date..." required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label class="form-label">Select Employee: </label>
                                    <select name="emp_id" class="form-control" onchange="checkEmployee(this)" data-search="true">
                                        @foreach($employees as $emp)
                                            <option value="{{$emp->id}}">{{$emp->empFirstName." (".$emp->employeeId.")"}}</option>
                                        @endforeach
                                    </select>


                                </div>
                            </div>

                            <div class="col-md-7">
                                <div class="form-group">
                                    <label class="form-label">Excel Extension: </label>
                                    <div class="append-icon">
                                        <div class="option-group">
                                            <select class="form-control" name="extensions">
                                                <option selected value="xls">XLS</option>
                                                <option value="xlsx">XLSX</option>
                                            </select>
                                        </div>
                                    </div>


                                </div>
                            </div>


                            <div class="col-md-12">
                                <button type="button" id="attendance_report" class="btn btn-primary">Generate Report</button>
                                <button type="submit" id="attendance-report-pdf" value="Download PDF English" name="viewType" class="btn btn-dark">Download PDF (English)</button>
{{--                                <button type="submit" id="attendance-report-pdf" value="Download PDF Bangla" name="viewType" class="btn btn-dark">Download PDF (Bengali)</button>--}}
                                <button type="submit" id="attendance-report-pdf" value="Download Excel" name="viewType" class="btn btn-success">Download Excel</button>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="report-url_hidden_id" value="{{URL::to('/attendance/report/card')}}">
                    {{ Form::close() }}
                </div>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Employee</th>
                        <th>Employee Id</th>
                        <th>Designation</th>
                        <th>Date</th>
                        <th>In time</th>
                        <th>Out Time</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody id="attendance_display">
                    </tbody>
                </table>
            </div>
        </div>

        <div id="last-word" style="font-weight: bold; font-size: 16px; float: right; color: #5b5b5b;">


        </div>
    </div>
    <script>

        $(document).ready(function(){

            $('#attendance-report-pdf').click(function () {
                if($('#empId').val()===null){
                    alert('Select employee.');
                    return false;
                }
            });


            $("#attendance_report").click(function(){
                var countAbsent=0;
                var countHoliday=0;
                var countDay=0;
                var countLeave=0;
                var countLate=0;
                var data='';
                var attendance_data = '';
                var url=$("#report-url_hidden_id").val();
                var form=$('#get-form').find('form');
                var lateTime= "{{ \App\Http\Controllers\AttendanceController::late_time() }}";
//                alert(lateTime);

                if($('#start_date').val()==''){
                    alert('Select starting date.');
                    return
                }
                if($('#end_date').val()==''){
                    alert('Select ending date.');
                    return
                }
                if($('#empId').val()===null){
                    alert('Select employee.');
                    return

                }
                var late="";
                {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    })
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: form.serialize(),
                        dataType: "json",
                        success: function (data) {
                            $.each(data, function (i, item) {
                                if(typeof (item.empFirstName)==='undefined' && typeof (item.empLastName)==='undefined' && typeof (item.employeeId)==='undefined'){
                                    if(item[2]==='weekend' || item[2]==='Festival Holiday' || item[2]==='On Leave'){
                                        countHoliday++;
                                        attendance_data += '<tr>  <td>' + item[0].empFirstName + " " + item[0].empLastName +
                                            '</td>  <td>' + item[0].employeeId + '</td> <td>' + item[0].designation +
                                            '</td> <td>' + item["1"] + '</td> <td></td> <td> </td> <td class="">'+item['2']+'</td> </tr>';
                                        if(item[2]==='On Leave'){
                                            countHoliday--;
                                            countLeave++;
                                            countDay++;

                                        }

                                    }
                                    else if(typeof (item[0].in_time)==='undefined' && typeof (item[0].out_time)==='undefined'){
                                        countAbsent++;
                                        countDay++;
                                        attendance_data += '<tr>  <td>' + item[0].empFirstName + " " + item[0].empLastName +
                                            '</td>  <td>' + item[0].employeeId + '</td> <td>' + item[0].designation +
                                            '</td> <td>' + item["1"] + '</td> <td></td> <td> </td> <td class="red-text">Absent</td> </tr>';

                                    }

                                    else {
                                        countDay++;
                                        if (item[0].in_time > lateTime) {
                                            late = "Late";
                                            countLate++;
                                        }
                                        else {
                                            late = "";
                                        }
                                        if (item[0].in_time === item[0].out_time) {
                                            item[0].out_time = '<p class="red-text">Not Given</p>';
                                        }
                                        attendance_data += '<tr>  <td>' + item[0].empFirstName + " " + item[0].empLastName +
                                            '</td>  <td>' + item[0].employeeId + '</td> <td>' + item[0].designation +
                                            '</td> <td>' + item[0].date + '</td> <td>' + item[0].in_time + '</td> <td>' +
                                            item[0].out_time + '</td> <td class="late-text">' + late + '</td> </tr>';

                                    }


                                }
                                else {
                                    if (item.in_time > '09:15:00') {
                                        late = "Present";
                                    }
                                    else {
                                        late = "";
                                    }
                                    if (item.in_time === item.out_time) {
                                        item.out_time = '<p class="red-text">Not Given</p>';
                                    }
                                    attendance_data += '<tr>  <td>' + item.empFirstName + " " + item.empLastName +
                                        '</td>  <td>' + item.employeeId + '</td> <td>' + item.designation +
                                        '</td> <td>' + item.date + '</td> <td>' + item.in_time + '</td> <td>'
                                        + item.out_time + '</td> <td class="late-text">' + late + '</td> </tr>';
                                }
                            });
                            var workingDays="<p>Total Working Days: <span style='color: #7b0f7b;'>"+ countDay+"</span></p> <p>Total Absent: <span style='color: #e01b1b'>"+ countAbsent+"</span> Day/Days</p>";
                            $('#attendance_display').html(attendance_data);
                            $('#last-word').html(workingDays);
                        },
                        error: function (data) {
//                            console.log('Error:', data);
                        }
                    });
                }
            });
        });
    </script>
    @include('include.copyright')
@endsection