@extends('layouts.master')
@section('title', 'Vacancy List Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3><i class="fa fa-table"></i> <strong>Vacancy </strong> List</h3>
                    </div>
                    
                    <div class="panel-content">
                            <a href="{{url('report/recruitment')}}" class="btn btn btn btn-default"><i class="fa fa-arrow-left"> Back</i></a>
                        <div class="row">
                        
                         {{Form::open(array('url' => '/report/vacancy/vacancyList','method' => 'post','target'=>"_blank"))}}
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="required form-label">Date From</label>
                                    <div class="prepend-icon">
                                        <input type="text" id="leave_start_date" name="vacancy_start" class="date-picker form-control" placeholder="Select a date..." required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="required form-label">Date To</label>
                                    <div class="prepend-icon">
                                        <input type="text" id="leave_end" name="vacancy_end" class="date-picker form-control" placeholder="Select a date..." required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="leave_report_url_hidden_id" value="{{URL::to('/leave/report/show')}}">
                            <hr>
                             <input type="submit" value="Preview" name="viewType" class="btn btn-success margin-top-10">
        
                          <span class="dropdown">
                              <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Generate PDF
                              <span class="caret"></span></button>
                              <ul class="dropdown-menu">
                                  <li><button type="submit" value="pdfenglish" name="viewType" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i> &nbsp; English </button></li>
                                  <li><button type="submit" value="pdfbangla" name="viewType" class="btn btn-blue"><i class="fa fa-file-excel-o"></i> &nbsp; বাংলা </button></li>
                              </ul>
                          </span>
                          <hr>
                          <hr>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection