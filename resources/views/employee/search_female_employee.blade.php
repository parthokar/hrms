@extends('layouts.master')
@section('title', 'Search Female Employee')
@section('content')
{{ Html::script('hrm_script/js/bootstrap3-typeahead.js') }}
<div class="page-content">
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 ><i class="fa fa-search "></i>Male Employee<strong> Search </strong> Results</h3>
                        </div>

                        <div class="col-md-6" style="margin-top:8px;">
                            <a href="{{url('/female_employee')}}" class="btn btn-primary btn-round btn-sm"  style="float:right;" ><i class="fa fa-female"></i> Female Employee List </a>
                            <a href="#"  data-toggle="modal" data-target="#search_employee" class="btn btn-success btn-round btn-sm"  style="float:right;" ><i class="fa fa-search"></i> Search Employee </a>
                        </div>
                    </div>

                </div>
                <div class="panel-content pagination2 table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Employee Id</th>
                                <th>Designation</th>
                                <th>Department</th>
                                <th>Unit</th>
                                <th>Section</th>
                                <th>Joining Date</th>
                                <th>Gender</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($employees as $employee)
                            <tr>
                                <td>{{$employee->empFirstName ." ".$employee->empLastName}}</td>
                                <td>{{$employee->employeeId}}</td>
                                <td>{{$employee->designation}}</td>
                                <td>{{$employee->departmentName}}</td>
                                <td>{{$employee->unitName}}</td>
                                <td>{{$employee->empSection}}</td>
                                <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('Y-m-d')}}</td>
                                <td>
                                    @if($employee->empGenderId==1)
                                    Male
                                    @endif
                                    @if($employee->empGenderId==2)
                                    Female
                                    @endif
                                    @if($employee->empGenderId==3)
                                    Other
                                    @endif
                                </td>

                                <td>
                                    <a href="{{route('employee.show',$employee->id)}}" target="_blank" title="View Employee Details" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="search_employee" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Search Employee</strong></h4>
            </div>
            <div class="modal-body" style="border-top:1px solid #ddd;">
             <form action="{{url('/female_employee_search')}}" method="POST">
                {{ csrf_field() }}

                <div class="form-group">
                  <label for="employeeInfo">Keyword</label>
                  <input name="keyword" placeholder="Write search keyword here." id="employeeInfo" class="form-control" />
                </div>
            <hr>
            <div class="form-group">
                <button type="submit" class="btn btn-success" ><i class="fa fa-search"></i> Search</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>

</div>
</div>


@include('include.copyright')
@endsection