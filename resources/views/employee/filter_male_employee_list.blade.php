@extends('layouts.master')
@section('title', 'Male Employee List')
@section('content')
{{ Html::script('hrm_script/js/bootstrap3-typeahead.js') }}
<div class="page-content">
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 ><i class="fa fa-users "></i> Male <strong>Employee </strong> List</h3>
                            
                        </div>
                        <div class="col-md-6" style="margin-top:8px;">
                            <a href="{{url('/male_employee')}}" class="btn btn-primary btn-round btn-sm"  style="float:right;" ><i class="fa fa-male"></i> Male Employee List </a>
                            <a href="#"  data-toggle="modal" data-target="#filter" class="btn btn-danger btn-round btn-sm"  style="float:right;" ><i class="fa fa-filter"></i> Filter </a>
                        </div>


                    </div>

                </div>
                <div class="panel-content pagination2 table-responsive">
                    <table class="table table-hover table-dynamic table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Employee Id</th>
                                <th>Designation</th>
                                <th>Department</th>
                                <th>Unit</th>
                                <th>Section</th>
                                <th>Joining Date</th>
                                <th>Gender</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($employees as $employee)
                            <tr>
                                <td>{{$employee->empFirstName ." ".$employee->empLastName}}</td>
                                <td>{{$employee->employeeId}}</td>
                                <td>{{$employee->designation}}</td>
                                <td>{{$employee->departmentName}}</td>
                                <td>{{$employee->unitName}}</td>
                                <td>{{$employee->empSection}}</td>
                                <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('Y-m-d')}}</td>
                                <td>
                                    @if($employee->empGenderId==1)
                                    Male
                                    @endif
                                    @if($employee->empGenderId==2)
                                    Female
                                    @endif
                                    @if($employee->empGenderId==3)
                                    Other
                                    @endif
                                </td>

                                <td>
                                    <a href="{{route('employee.show',$employee->id)}}" target="_blank" title="View Employee Details" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="filter" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Filter Data</strong></h4>
            </div>
            <div class="modal-body" style="border-top:1px solid #ddd;">
             <form action="{{url('/male_employee_filter')}}" method="POST">
                {{ csrf_field() }}

                <div class="form-group">
                  <label for="departmentId">Employee Department</label>
                  <select name="departmentId" id="departmentId" class="form-control" >
                    <option value="">All Department</option>
                    @foreach($departments as $department)

                    <?php if(isset($request)&&!empty($request)){ ?>
                    <option <?php if($request->departmentId==$department->id){ echo "selected"; } ?> value="{{$department->id}}">{{$department->departmentName}}</option>
                    <?php }else{ ?>
                    <option value="{{$department->id}}">{{$department->departmentName}}</option>
                    <?php } ?>

                    @endforeach
                </select>
            </div>
            <div class="form-group">
              <label for="designationId">Employee Designation</label>
              <select name="designationId" id="designationId" class="form-control" >
                <option value="">All Designation</option>
                @foreach($designations as $d)

                <?php if(isset($request)&&!empty($request)){ ?>
                <option <?php if($request->designationId==$d->id){ echo "selected"; } ?> value="{{$d->id}}">{{$d->designation}}</option>
                <?php }else{ ?>
                <option value="{{$d->id}}">{{$d->designation}}</option>
                <?php } ?>
                @endforeach
                </select>
            </div>

            <div class="form-group">
              <label for="unitId">Unit</label>
              <select name="unitId" id="unitId" class="form-control" >
                <option value="">All Unit</option>
                @foreach($units as $unit)

                <?php if(isset($request)&&!empty($request)){ ?>
                <option <?php if($request->unitId==$unit->id){ echo "selected"; } ?> value="{{$unit->id}}">{{$unit->name}}</option>
                <?php }else{ ?>
                <option value="{{$unit->id}}">{{$unit->name}}</option>
                <?php } ?>
                @endforeach
                </select>
            </div>

            <div class="form-group">
              <label for="empSection">Employee Section</label>

                <?php if(isset($request)&&!empty($request)){ ?>
                    <input name="empSection" value="{{$request->empSection}}" id="empSection" class="form-control" >
                <?php }else{ ?>
                    <input name="empSection" placeholder="Employee Section" id="empSection" class="form-control" >
                <?php } ?>
        </div>

        <hr>
        <div class="form-group">
            <button type="submit" class="btn btn-danger" ><i class="fa fa-filter"></i> Filter</button>
            <button type="button" class="btn btn-default" data-dismiss="modal"> Close</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>

</div>
</div>


<script type="text/javascript">

    var url = "{{ route('employee.section.autocomplete.ajax') }}";

    $('#empSection').typeahead({

        source:  function (query, process) {

            return $.get(url, { query: query }, function (data) {

                return process(data);

            });

        }

    });

</script>
@include('include.copyright')
@endsection