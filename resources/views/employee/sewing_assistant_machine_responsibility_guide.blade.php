<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
                        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
                    'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
<!doctype html>
<html lang="en">
    <head>

        <meta charset="UTF-8">
        <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        <title>সহকারী সেলাই মেশিন অপারেটরের দায়িত্ব ও কর্তব্য</title>
        <style>
            @page { sheet-size: A4; }
            body{
                font-family: 'bangla', sans-serif;
                font-size: 17px;

            }
            p{  
                line-height: 1px;
            }

            .basicInfo{
                width: 100%;
            }

            .basicInfo td{
                height: 22px;
            }


            .reportHeaderArea{
                text-align: center;
            }
            .reportHeader p{
                line-height: 9px;
                padding-top: -10px;
            }
            .container{
                text-align: left;
            }

            .salary{
                text-align:left;
                width: 95%;
            }

            .head{
                font-size: 14px;
                font-weight: bold;
                text-align:left;
            }

            .appApply{
                text-align:left;
                font-size:17px;
                padding-top: 15px; 
                padding-bottom: 35px;
            }

            .content p{
                line-height: 15px;
            }

            .niyogkar p{
                line-height: 15px;
            }

            .boldText{
                font-weight: bold !important;
                font-size: 16px !important;
            }

            .reportHeaderCompany{
                font-size: 35px !important;
                padding: 0px 0px;
                padding-bottom: -18px;
            }
            .h1pad{
                padding-bottom: -10px;
            }
            #table1{
                width: 100%;
            }
            #table1 th,td{
                border:1px;
                border-collapse: collapse;
            }
            
            .basicInfo{
                font-size: 16px;
            }
            .basicInfo th,td{
                border:none;
            }

            li{
                padding: 3px 0px;
            }
            ul{
                padding: 0px 0px;
            }

        </style>
    </head>
    <body>

        <div class="container">
            <div class="reportHeaderArea">
                <h1 class="reportHeaderCompany">{{$companyInformation->bComName}}</h1>
                <p class="reportHeader">{{$companyInformation->bComAddress}}</p>
                <hr>
            </div>
            <div>
                <h3 style="text-decoration: underline;text-align: center;">সহকারী সেলাই মেশিন অপারেটরের দায়িত্ব ও কর্তব্য</h3>
            </div>
            <div style="text-align: center">
                @foreach($employee as $emp)
                <table class="basicInfo" >
                    <tr>
                        <td>নামঃ <span class="boldText"><?php if(!empty($emp->empBnFullName)){echo $emp->empBnFullName;}else{ echo $emp->empFirstName ." ".$emp->empLastName; } ?></span></td>
                        <td>কার্ড নং:  <span class="boldText"><?php if(!empty($emp->employeeBnId)){echo $emp->employeeBnId;}else{ echo $emp->employeeId; } ?></span></td>
                        <td>সেকশনঃ <span class="boldText"><?php if(!empty($emp->empSection)){echo $emp->empSection;}else{ echo ""; } ?></span></td>
                        <td style="font-size:16px;">ইউনিট নামঃ <span class="boldText"> <?php if(!empty($emp->bnName)){echo $emp->bnName;}else{ echo $emp->unitName; } ?></span></td>
                    </tr>

                </table>
                @endforeach

                <div class="appApply">
                   <ul style="list-style-type: none;">
                        <li>১. যথা সময়ে কর্মস্থলে উপস্থিত হওয়া।</li>
                        <li>২. কারখানায় আসা ও যাওয়ার সময় সঠিকভাবে আইডি কার্ড পাঞ্চ করে হাজিরা নিশ্চিত করা।</li>
                        <li>৩. কম্পানির যাবতীয় আইন কানুন মেনে চলা।</li>
                        <li>৪. সেলাই কাজে অপারেটরকে সর্বত্র সহযোগিতা করা।</li>
                        <li>৫. তৈরি পোশাকের অপ্রয়োজনীয়/বাড়তি সুতা কেটে পরিষ্কার করা। </li>
                        <li>৬. নাম্বার ও সাজ অনুসারে মিলানো।</li>
                        <li>৭. প্রয়োজন বিশেষে চিহ্নিত করা।</li>
                        <li>৮. ক্রমানুসারে বান্ডেল করা।</li>
                        <li>৯. প্রদত্ত টার্গেট পূরণ করা।</li>
                        <li>১০. কোন ধরণের সমস্যা মনে হলে সাথে সাথেই অপারেটর এবঙ সুপারভাইজরকে অবগত করতে হবে।</li>
                        <li>১১. উৎপাদিত পণ্যের গুণগত মান ঠিক রাখা।</li>
                        <li>১২. তৈরি পোশাক বা এর অংশ পরবর্তী প্রসেসে পৌছে দেওয়া।</li>
                        <li>১৩. আইডি কার্ড ও মাক্স পরিধান করে কাজ করা।</li>
                        <li>১৪. সিজার, কাটার্‌ ভোমর যথাযথ স্থানে বেধে কাজ করা।</li>
                        <li>১৫. ফ্লোরে যাতায়াতের পথ প্রতিবন্ধকতামুক্ত রাখা। </li>
                        <li>১৬. ইলেকট্রিক বোর্ড, ফায়ার এক্সটিংগুশার, ফাস্ট এইড বক্সের সামনে প্রতিবন্ধকতামুক্ত রাখা।</li>
                        <li>১৭. ঊর্ধ্বতনের নির্দেশ মোতাবেক কাজ করা।</li>

                   </ul>
                </div>
                <table width="100%" style="padding-top: 30px;">
                    <tr>
                        <td>-------------------- <br > কর্তৃপক্ষ <br> পিনাকী গ্রুপ</td>
                        <td style="text-align: right;">--------------------------------------------- <br >সহকারী সেলাই মেশিন অপারেটরের স্বাক্ষর </td>
                    </tr>
                </table>
                
            </div>
        </div>

    </body>
</html>
