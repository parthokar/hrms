@extends('layouts.master')
@section('title', 'Delete Employee List')
@section('content')
    {{ Html::script('hrm_script/js/bootstrap3-typeahead.js') }}
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 ><i class="fa fa-users "></i>Delete <strong>Employee </strong> List</h3>

                            </div>
{{--                            <div class="col-md-6" style="margin-top:8px;">--}}
{{--                                <a href="#"  data-toggle="modal" data-target="#filter" class="btn btn-danger btn-round btn-sm"  style="float:right;" ><i class="fa fa-filter"></i> Filter </a>--}}
{{--                                <a href="#"  data-toggle="modal" data-target="#search_employee" class="btn btn-success btn-round btn-sm"  style="float:right;" ><i class="fa fa-search"></i> Search Employee </a>--}}
{{--                            </div>--}}


                        </div>

                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-striped table-bordered table-dynamic">
                            <thead>
                            <tr>
                                <th>Employee Id</th>
                                <th>Name</th>
                                <th>Designation</th>
                                <th>Department</th>
                                <th>Unit</th>
                                <th>Section</th>
                                <th>Joining Date</th>
                                <th>Discontinue Date</th>
                                <th>Gender</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($employees as $employee)
                                <tr>
                                    <td>{{$employee->employeeId}}</td>
                                    <td>{{$employee->empFirstName ." ".$employee->empLastName}}</td>
                                    <td>{{$employee->designation}}</td>
                                    <td>{{$employee->departmentName}}</td>
                                    <td>{{$employee->unitName}}</td>
                                    <td>{{$employee->empSection}}</td>
                                    <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('d-M-Y')}}</td>
                                    <td>{{\Carbon\Carbon::parse($employee->date_of_discontinuation)->format('d-M-Y')}}</td>
                                    <td>
                                        @if($employee->empGenderId==1)
                                            Male
                                        @endif
                                        @if($employee->empGenderId==2)
                                            Female
                                        @endif
                                        @if($employee->empGenderId==3)
                                            Other
                                        @endif
                                    </td>

                                    <td>
                                        <a href="{{route('employee.employee_confirm_delete',base64_encode($employee->id))}}" title="View Employee Details" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">

        var url = "{{ route('employee.section.autocomplete.ajax') }}";

        $('#empSection').typeahead({

            source:  function (query, process) {

                return $.get(url, { query: query }, function (data) {

                    return process(data);

                });

            }

        });

    </script>
    @include('include.copyright')
@endsection