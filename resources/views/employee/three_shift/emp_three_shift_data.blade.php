@extends('layouts.master')
@section('title', 'Manage Employee 3 Shifts')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
    
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-gavel"></i> <strong>Manage Employee 3 Shift</strong></h3>
                    </div>
                    <div class="panel-content">
                        <div class="panel-content well">
                        <h3><i class="fa fa-plus-square"></i> <b>Manage Employee 3 Shift</b></h3><hr>
                           <h4 class="text-center">Employee Information</h4>
                            <p style="font-size:20px" class="text-center">Id:{{$employee->employeeId}}</p>
                            <p style="font-size:20px" class="text-center">Name:{{$employee->empFirstName}}</p>
                           <form action="{{route('three_shift_emp_store')}}" method="post">
                            @csrf
                            <input type="hidden" name="emp_id" value="{{$employee->id}}">
                            <table style="width:100%" class="table table-border">
                                <tr>
                                  <th>Date</th>
                                  <th>Shift</th>
                                  <th>Weekend</th>
                                </tr>
                               <?php for($i=$start;$i<=$end;$i++){ ?>
                               <input type="hidden" name="shift_date[]" value="{{$i}}">
                                <tr>
                                <td>{{$i}} ->  {{date('l',strtotime($i))}}</td>
                                    <td>
                                     <select name="emp_shift[]" class="form-control">
                                        <option value="">Select</option> 
                                        @foreach($shifts as $shift)
                                          <option value="{{$shift->id}}">{{$shift->shiftName}}</option>
                                        @endforeach
                                     </select>
                                    </td>
                                    <td>
                                        <select name="weekend[]" class="form-control">
                                            <option value="">Select</option> 
                                              <option value="w">Weekend</option>
                                         </select>
                                    </td>
                                  </tr>
                               <?php } ?> 
                              </table> 
                            <button style="margin-left:15px" type="submit" class="btn btn-success"><i class="fa fa-save"></i>Save Information</button>                   
                            </form>
                        </div>
                        <hr>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection