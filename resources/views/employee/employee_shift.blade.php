@extends('layouts.master')
@section('title', 'Manage Employee Shift')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('success'))
        <p id="alert_message" class="alert alert-success">{{Session::get('success')}}</p>
        @endif
        @if(Session::has('error'))
            <p id="alert_message" class="alert alert-danger">{{Session::get('error')}}</p>
        @endif
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-gavel"></i> <strong>Employee Shift</strong></h3>
                    </div>
                    <div class="panel-content">
                        <div class="panel-content well">
                        <h3><i class="fa fa-plus-square"></i> <b>Employee Shift</b></h3><hr>



                        <form action="{{url('employees/shift/department/update')}}" method="post">
                           @csrf 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pwd">Select Department:</label>
                                    <select class="form-control" name="dept_id" autocomplete="off" data-search="true" required>
                                        <option value="">Select Department</option>
                                            @foreach($department as $departments)
                                               <option value="{{ $departments->id }}">{{$departments->departmentName}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pwd">Select Shift:</label>
                                    <select class="form-control" name="shift_id" autocomplete="off" data-search="true" required>
                                        <option value="">Select Shift</option>
                                            @foreach($shift as $shifts)
                                            <option value="{{ $shifts->id }}">{{$shifts->shiftName}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                            <button style="margin-left:15px" type="submit" class="btn btn-success"><i class="fa fa-save"></i>Update</button>                   
                        </form>


                        <form action="{{url('employees/shift/section/update')}}" method="post">
                            @csrf 
                             <div class="col-md-6">
                                 <div class="form-group">
                                     <label for="pwd">Select Section:</label>
                                     <select class="form-control" name="section_id" autocomplete="off" data-search="true" required>
                                         <option value="">Select Section</option>
                                             @foreach($section as $sections)
                                             <option value="{{$sections->empSection}}">{{$sections->empSection}}</option>
                                             @endforeach
                                     </select>
                                 </div>
                             </div>
                             <div class="col-md-6">
                                 <div class="form-group">
                                     <label for="pwd">Select Shift:</label>
                                     <select class="form-control" name="shift_id" autocomplete="off" data-search="true" required>
                                         <option value="">Select Shift</option>
                                             @foreach($shift as $shifts)
                                             <option value="{{ $shifts->id }}">{{$shifts->shiftName}}</option>
                                             @endforeach
                                     </select>
                                 </div>
                             </div>
                        
                             <button style="margin-left:15px" type="submit" class="btn btn-success"><i class="fa fa-save"></i>Update</button>                   
                         </form>
                    
                        {{-- <form action="{{route('emp_wise_shift')}}" method="post">
                            @csrf 
                             <div class="col-md-3">
                                 <div class="form-group">
                                     <label for="pwd">Select Employee:</label>
                                     <select class="form-control" name="emp_idss" autocomplete="off" data-search="true" required>
                                         <option value="">Select Employee</option>
                                             @foreach($employee as $employees)
                                             <option value="{{ $employees->id }}" {{ (collect(old('emp_idss'))->contains($employees->id)) ? 'selected':'' }}>{{$employees->empFirstName}} {{$employees->employeeId}}</option>
                                             @endforeach
                                     </select>
                                 </div>
                             </div>
                             <div class="col-md-3">
                                 <div class="form-group">
                                     <label for="pwd">Select Shift:</label>
                                     <select class="form-control" name="shift_idss" autocomplete="off" data-search="true" required>
                                         <option value="">Select Shift</option>
                                             @foreach($shift as $shifts)
                                             <option value="{{ $shifts->id }}" {{ (collect(old('shift_idss'))->contains($shifts->id)) ? 'selected':'' }}>{{$shifts->shiftName}}</option>
                                             @endforeach
                                     </select>
                                 </div>
                             </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    <label for="pwd">Start Date:</label>
                                <input type="text" class="form-control date-picker" value="{{old('start_datess')}}" name="start_datess" autocomplete="off" required>
                                    
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="pwd">End Date:</label>
                                    <input type="text" class="form-control date-picker" value="{{old('end_datess')}}" name="end_datess" autocomplete="off" required>
                                </div>
                            </div>
                             <button style="margin-left:15px" type="submit" class="btn btn-success"><i class="fa fa-check"></i>Assign Shift</button>                   
                         </form>
                        <hr> --}}



                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection

