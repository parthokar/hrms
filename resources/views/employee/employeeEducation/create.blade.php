@extends('layouts.master')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong>Employee</strong> Education Info</h2>
                    </div>
                    <div class="panel-body bg-white">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="modal-body">
                                    {!! Form::open(['method'=>'POST','action'=>['EmployeeEducationInfoController@store'],'files'=>true]) !!}
                                    {{--{!! Form::hidden('emp_id',$emp_id) !!}--}}

                                    <div class="form-group">
                                        {!! Form::label('empExamTitle','Examination Name:') !!}
                                        {!! Form::text('empExamTitle',null,['class'=>'form-control','required type'=>'text', 'placeholder'=>'Enter Examination Name']) !!}

                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('empInstitution','Institution:') !!}
                                        {!! Form::text('empInstitution',null,['class'=>'form-control','required type'=>'text', 'placeholder'=>'Enter InstitutionName']) !!}

                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('empResult','Exam Result:') !!}
                                        {!! Form::text('empResult',null,['class'=>'form-control','required type'=>'text', 'placeholder'=>'Enter Result']) !!}

                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('empScale','Scale:') !!}
                                        {!! Form::text('empScale',null,['class'=>'form-control','required type'=>'text', 'placeholder'=>'Enter Grade Scale']) !!}

                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('empPassYear','Passing Year:') !!}
                                        {!! Form::text('empPassYear',null,['class'=>'form-control','required type'=>'text', 'placeholder'=>'Enter Passing Year']) !!}

                                    </div>


                                    <div class="form-group">
                                        {!! Form::label('empCertificate','Attachment:') !!}
                                        {!! Form::file('empCertificate', null, ['class'=>'form-control-file','required type'=>'text']) !!}
                                    </div>




                                </div>
                                <div class="modal-footer">
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="Submit" class="btn btn-primary">Add New</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






@include('include.copyright')

@endsection