<div class="modal fade" id="newTrainingHistory" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Add New Training</strong></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'POST','action'=>['EmployeeTrainingController@store'],'files'=>true]) !!}
                {!! Form::hidden('emp_id',$emp_id) !!}
                <div class="form-group">
                    {!! Form::label('training_name','Training Name:', ['class'=>'required']) !!}
                    <input class="form-control" required="" type="text" placeholder="Enter Training Name" name="training_name" id="training_name">

                </div>
                <div class="form-group">
                    {!! Form::label('training_institution','Institution/Company:', ['class'=>'required']) !!}
                    <input class="form-control" type="text" required="" placeholder="Enter Training Institution" name="training_institution" id="training_institution">

                </div>

                <div class="form-group">
                    {!! Form::label('training_start','Start Date:',['class'=>'required']) !!}
                    {!! Form::text('training_start',null,['class'=>'date-picker form-control', 'required'=>'', 'placeholder'=>'Enter Start Date']) !!}

                </div>

                <div class="form-group">
                    {!! Form::label('training_end','Training End:',['class'=>'required']) !!}
                    {!! Form::text('training_end',null,['class'=>'date-picker form-control', 'required'=>'', 'placeholder'=>'Enter Leaving Date']) !!}

                </div>

                <div class="form-group">
                    <label class="form-label">Description</label>
                    <textarea class="form-control" placeholder="Enter Description" name="training_description" id="training_description"></textarea>

                </div>

                <div class="form-group">
                    {!! Form::label('training_attachment','Attachment:') !!}
                    {!! Form::file('training_attachment',null,['class'=>'form-control-file','type'=>'text']) !!}

                </div>

            </div>
            <div class="modal-footer">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"> Close</button>
                    <button type="Submit" class="btn btn-primary"><i class="fa fa-save"></i> &nbsp; Save </button>
                </div>

                {!! Form::close() !!}
            </div>

        </div>
    </div>
</div>