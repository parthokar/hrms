<div class="modal fade" id="editPhoto" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Change Profile Picture</strong></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'PATCH','action'=>['EmployeeController@UpdatePhoto',$employee->id],'files'=>true]) !!}

                <div class="form-group">
                    {!! Form::label('empPhoto','Picture:') !!}
                    {!! Form::file('empPhoto', null, ['class'=>'form-control-file','required type'=>'text']) !!}

                </div>

            </div>
            <div class="modal-footer">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="Submit" class="btn btn-primary">Update Photo</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>