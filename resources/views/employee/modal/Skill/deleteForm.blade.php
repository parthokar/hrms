{!! Form::open(['method'=>'DELETE','action'=>['SkillTestController@destroy',$id]]) !!}

<p>You sure you want to delete <strong>{{ $we->	skill_name }}</strong> record ??</p>

<div class="modal-footer">
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="Submit" id='update-working-experience' class="btn btn-primary">Delete</button>
    </div>

</div>


{!! Form::close() !!}