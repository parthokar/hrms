<div class="modal fade" id="{{'educationDelete'.$ec->id}}" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Confirm Delete</strong></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'DELETE','action'=>['EmployeeEducationInfoController@destroy',$ec->id],'files'=>true]) !!}
                <table class="table table-striped">
                    <div class="form-group">
                        <tr>
                            <td>{!! Form::label('empExamTitle','Examination Name:') !!}</td>
                            <td>{!! Form::label('empExamTitle',$ec->empExamTitle) !!}</td>
                        </tr>
                    </div>

                    <div class="form-group">
                        <tr>
                            <td>{!! Form::label('empInstitution','Institution Name:') !!}</td>
                            <td>{!! Form::label('empInstitution',$ec->empInstitution) !!}</td>
                        </tr>
                    </div>

                    <div class="form-group">
                        <tr>
                            <td>{!! Form::label('empResult','Examination Result:') !!}</td>
                            <td>{!! Form::label('empResult',$ec->empResult) !!}</td>
                        </tr>
                    </div>
                    <div class="form-group">
                        <tr>
                            <td>{!! Form::label('empScale','Scale:') !!}</td>
                            <td>{!! Form::label('empScale',$ec->empScale) !!}</td>
                        </tr>
                    </div>
                    <div class="form-group">
                        <tr>
                            <td>{!! Form::label('empPassYear','Passing Year:') !!}</td>
                            <td>{!! Form::label('empPassYear',$ec->empPassYear) !!}</td>
                        </tr>
                    </div>

                    <div class="form-group">
                        <tr>
                            <td>{!! Form::label('empCertificate','Attachment:') !!}</td>

                            <td>
                                @if($ec->empCertificate)
                                    Available

                                @endif
                                @if(!$ec->empCertificate)
                                    Not Available

                                @endif
                            </td>
                        </tr>


                    </div>

                </table>






            </div>
            <div class="modal-footer">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="Submit" class="btn btn-primary">Confirm Delete</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>