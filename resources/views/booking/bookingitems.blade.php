@extends('layouts.master')
@section('title', 'Item List')
@section('content')
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('delete'))

            <p id="alert_message" class="alert alert-danger">{{Session::get('delete')}}</p>

        @endif
        @if(Session::has('edit'))

            <p id="alert_message" class="alert alert-info">{{Session::get('edit')}}</p>

        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-lg-6">
                                <h3><i class="fa fa-lock"></i> <strong>Booking Item </strong> List</h3>
                            </div>
                            <div class="col-lg-6" style="text-align: right;">
                                <a data-toggle="modal" data-target="#create" class="btn btn-primary btn-round btn-sm"><i class="fa fa-plus"></i>Add New</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-bordered table-dynamic">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Created By</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=0; @endphp
                            @foreach($booking_items as $bi)
                                <tr>
                                    <td>{{++$i}}</td>
                                    <td>{{$bi->biName}}</td>
                                    <td>{{$bi->biDescription}}</td>
                                    <td>{{$bi->created_by}}</td>
                                    <td>
                                        <a data-toggle="modal" title="Edit" data-target="#edit{{$bi->id}}" class="btn btn-default btn-sm"><i class="fa fa-edit"></i></a>
                                        <a data-toggle="modal" title="Delete Data" data-target="#delete{{$bi->id}}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>

                                <!-- Edit Modal -->
                                <div class="modal fade" id="edit{{$bi->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Update Info</strong></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                {!! Form::open(['method'=>'PATCH', 'action'=>['BookingController@update', $bi->id]]) !!}
                                                 <div class="form-group">
                                                  <label for="biName"> Name</label>
                                                  <input type="text" value="{{$bi->biName}}" name="biName" class="form-control" required="" placeholder="Name" >
                                                </div>
                                                <div class="form-group">
                                                  <label for="biDescription"> Description</label>
                                                  <textarea name="biDescription" class="form-control" placeholder="Description" >{{$bi->biDescription}}</textarea>
                                                </div>
                                                <div class="modal-footer">
                                                    <hr>
                                                    <button type="Submit" class="btn btn-primary">Update Information</button>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>

                                                {!! Form::close() !!}
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                {{--@endif--}}

                                <!--End of Edit Model-->





                                <!-- Delete Modal -->
                                <div class="modal fade" id="delete{{$bi->id}}" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Confirm Delete</strong></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Do you want to delete <strong>{{$bi->biName}}?</strong></p>
                                            </div>
                                            <div class="modal-footer">
                                                {!! Form::open(['method'=>'DELETE', 'action'=>['BookingController@destroy', $bi->id]]) !!}
                                                <button type="submit" class="btn btn-danger">Confirm</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--Delete Modal Ended-->

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!-- Add New Modal -->
    <div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><strong>New Booking Item Information</strong>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST', 'action'=>'BookingController@store']) !!}
                    <div class="form-group">
                      <label for="biName"> Name</label>
                      <input type="text" name="biName" class="form-control" required="" placeholder="Name" >
                    </div>
                    <div class="form-group">
                      <label for="biDescription"> Description</label>
                      <textarea name="biDescription" class="form-control" placeholder="Description" ></textarea>
                    </div>
                    <div class="modal-footer">
                        <hr>
                        <button type="Submit" class="btn btn-primary">Save Information</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>


    @include('include.copyright')
@endsection