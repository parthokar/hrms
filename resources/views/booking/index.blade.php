@extends('layouts.master')
@section('title', 'Booking List')
@section('content')
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('delete'))

            <p id="alert_message" class="alert alert-danger">{{Session::get('delete')}}</p>

        @endif
        @if(Session::has('edit'))

            <p id="alert_message" class="alert alert-info">{{Session::get('edit')}}</p>

        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-lg-6">
                                <h3><i class="fa fa-lock"></i> <strong>Booking </strong> List</h3>
                            </div>
                            <div class="col-lg-6" style="text-align: right;">
                                
                            </div>
                        </div>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-bordered table-dynamic">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Booked By</th>
                                <th width="20%">Purpose</th>
                                <th>Date</th>
                                <th>Starting Time</th>
                                <th>Ending Time</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=0; @endphp
                            @foreach($booking_history as $bh)
                                <tr>
                                    <td>{{++$i}}</td>
                                    <td>{{$bh->biName}}</td>
                                    <td>{{$bh->bookedBy}}</td>
                                    <td>{{$bh->purpose}}</td>
                                    <td>{{\Carbon\Carbon::parse($bh->biDate)->format('d-M-Y')}}</td>
                                    <td>{{date('h:i:s a',strtotime($bh->biStartTime))}}</td>
                                    <td>{{date('h:i:s a',strtotime($bh->biEndTime))}}</td>
                                    <td><?php if($bh->status==0){ echo "<span style='color:green;'>Active</span>"; }
                                    if($bh->status==1){ echo "<span style='color:red;'>Cancelled</span>"; } ?></td>
                                    <td><!-- 
                                        <a data-toggle="modal" title="Edit" data-target="#edit{{$bh->id}}" class="btn btn-default btn-sm"><i class="fa fa-edit"></i></a> -->
                                        <?php if($bh->status==0){ ?>
                                            <a data-toggle="modal" title="Cancel Booking Request" data-target="#delete{{$bh->id}}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                        <?php } ?>
                                    </td>
                                </tr>


                                <!-- Delete Modal -->
                                <div class="modal fade" id="delete{{$bh->id}}" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Confirm Cancel</strong></h5>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Do you want to cancel this booking?</strong></p>
                                                {!! Form::open(['method'=>'POST', 'route'=>['booking.booking_cancel']]) !!}
                                                <input type="hidden" name="id" value="{{$bh->id}}">
                                                <button type="submit" class="btn btn-danger">Confirm Cancel</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                {!! Form::close() !!}
                                            </div>
                                            
                                            <div class="modal-footer">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--Delete Modal Ended-->
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>


    @include('include.copyright')
@endsection