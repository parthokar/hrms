<div class="panel">
    <div class="panel-header">
        <h3><i class="fa fa-table"></i> <strong>Application </strong> List</h3>
    </div>
    <div class="panel-content pagination2 table-responsive">
        <table class="table table-hover table-dynamic">
            <thead>
            <tr>
                <th>id</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Position</th>
                <th>Submitted Date</th>
                <th>Interview Status</th>
                <th>Interview Date</th>
                <th>Priority</th>
                {{--<th>Joining Date</th>--}}
                {{--<th>Status</th>--}}
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($applications as $a)
                <tr>
                    <td>{{$a->id}}</td>
                    <td>{{$a->name}}</td>
                    <td>{{$a->phone}}</td>
                    <td>{{$a->vacTitle}}</td>
                    <td>{{\Carbon\Carbon::parse($a->submittedDate)->format('Y-m-d')}}</td>
                    <td>{{$a->interviewStatus}}</td>
                    <td>{{\Carbon\Carbon::parse($a->interviewDate)->format('Y-m-d')}}</td>
                    <td>{{$a->priorityStatus}}</td>
                    <td>
                        <a href="{{route('application.details',$a->id)}}" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>
                        {{--<button type="button" class="btn btn-info btn-xs show-edit-modal" data-toggle="modal" data-target="edit{{$a->id}}">Open</button>--}}
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>