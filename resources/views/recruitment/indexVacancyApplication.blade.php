@extends('layouts.master')
@section('title', 'Vacancy Application')
@section('content')
    <div class="page-content">
        @if(Session::has('store'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('store') }}</p>
        @endif
        @if(Session::has('del'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('del') }}</p>
        @endif
        {{--@elseif(Session::has('file_size'))--}}
            {{--<p id="alert_message" class="alert alert-danger">{{ Session::get('file_size') }}</p>--}}
        {{--@endif--}}
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-table"></i> <strong>Application </strong> List</h3>
                    </div>
                    <a href="{{route('application.create')}}" class="btn custom-btn btn-xs ">Add New</a>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Position</th>
                                <th>Submitted Date</th>
                                <th>Interview Status</th>
                                <th>Interview Date</th>
                                <th>Priority</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($applications as $a)
                                <tr>
                                    <td>{{$a->id}}</td>
                                    <td>{{$a->name}}</td>
                                    <td>{{$a->phone}}</td>
                                    <td>{{$a->vacTitle}}</td>
                                    <td>{{\Carbon\Carbon::parse($a->submittedDate)->format('Y-m-d')}}</td>
                                    <td>{{$a->interviewStatus}}</td>
                                    <td>@if($a->interviewDate!=null)
                                        {{ \Carbon\Carbon::parse($a->interviewDate)->format('Y-m-d')}}
                                    @endif
                                    </td>
                                    <td>{{$a->priorityStatus}}</td>
                                    <td>
                                        <a href="{{route('application.details',$a->id)}}" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-modal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        {!! Form::open(['method'=>'POST','action'=>'RecruitmentController@storeVacancyApplication','files'=>true]) !!}

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="required form-group">
                                    {!! Form::label('name','Name',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        <input type="text" name="name" class="form-control" placeholder="Enter Name..." required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('phone','Phone Number',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        <input type="text" name="phone" class="form-control" placeholder="Enter Phone Number">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="required form-label">Applying For </label>
                                    <div class="option-group">
{{--                                        {!! Form::select('vacancy_id',[''=>'Select Vacancy Position']+ $vacancies,null,['class'=>'form-control','required'=>'']) !!}--}}

                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="required form-label">Submitted Date</label>
                                    <div class="prepend-icon">
                                        <input type="text" name="submittedDate" class="date-picker form-control" value="{{\Carbon\Carbon::now()->format('m/d/Y')}}" required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="required form-label">Attachment (Max Size:5mb)</label>
                                    {!! Form::file('attachment', ['class'=>'form-control','required'=>'']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Interview Status</label>
                                    <div class="option-group">
                                        <select id="language" onchange="interviewCheck(this);" name="interviewStatus" class="form-control">
                                            <option value="">Select Status</option>
                                            <option value="Accepted">Accepted</option>
                                            <option value="Rejected">Rejected</option>

                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div id="interviewDate" class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label">Interview Date</label>
                                    <div class="prepend-icon">
                                        <input type="text" name="interviewDate" class="date-picker form-control" placeholder="Select a date...">
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div id="conditionDisplay">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Interview Details</label>
                                        <div class="append-icon">
                                            <textarea name="interviewNote" class="form-control" placeholder="Enter the interview details..." rows="4"></textarea>
                                            <i class="icon-lock"></i>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">


                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Priority</label>
                                        <div class="option-group">
                                            <select id="language" name="priorityStatus" class="form-control">
                                                <option value="">Select Priority</option>
                                                <option value="High">High</option>
                                                <option value="Regular">Regular</option>
                                                <option value="Low">Low</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Job Status</label>
                                        <div class="option-group">
                                            <select id="language" name="jobStatus" class="form-control">
                                                <option value="">Select Status</option>
                                                <option value="Confirm">Confirm</option>
                                                <option value="WaitingList">Waiting List</option>
                                                <option value="Reject">Reject</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>



                        <div class="text-center  m-t-20">
                            <button type="submit" class="btn btn-embossed btn-primary">Save</button>
                            <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0">Cancel</button>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>

        </div>
    </div>



    <script>
//        $('.show-edit-modal').click(function (event){
//            event.preventDefault();
//            $('#edit-modal').modal('show');
//
//        });


        setTimeout(function() {
            $('#alert_message').fadeOut('fast');

        }, 5000);
    </script>


    @include('include.copyright')
@endsection