{!! Form::open(['method'=>'PATCH','action'=>['TrainingController@update_history',1],'files'=>true]) !!}
{{Form::hidden('training_id_fixed',$training_id)}}
{{Form::hidden('training_starting_date_fixed',$training->training_starting_date)}}
{{Form::hidden('training_ending_date_fixed',$training->training_ending_date)}}
<div class="form-group">
    {!! Form::label('training_id','Training Name:') !!}
    <div class="option-group">
        {!! Form::select('training_id',[''=>'Select training']+ $trainings,$training_id,['class'=>'form-control','required'=>''])!!}
    </div>

</div>
<div class="form-group">
    {!! Form::label('training_starting_date','Training Starting Date:') !!}
    <input class="form-control" required="" value="{{$training->training_starting_date}}" type="date" placeholder="Enter Training Starting Date" name="training_starting_date" id="training_starting_date">

</div>

<div class="form-group">
    {!! Form::label('training_ending_date','Training Ending Date:') !!}
    <input class="form-control" required="" value="{{$training->training_ending_date}}" type="date" placeholder="Enter Training Ending Date" name="training_ending_date" id="training_ending_date">

</div>

<div class="modal-footer">
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="Submit" class="btn btn-primary">Update</button>
    </div>
</div>

{!! Form::close() !!}