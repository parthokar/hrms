

<div class="row">
    <div class="col-sm-12">
        <div class="required form-group">
            {!! Form::label('name','Name',['class'=>'control-label']) !!}
            <div class="append-icon">
                <input type="text" name="name" value="{{$unit->name}}" class="form-control" placeholder="Enter Name..." required disabled>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="form-group">
            {!! Form::label('phone_no','Phone Number',['class'=>'control-label']) !!}
            <div class="append-icon">
                <input type="text" name="phone_no" value="{{ $unit->phone_no }}" class="form-control" placeholder="Enter Phone Number" required disabled>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="form-group">
            {!! Form::label('email','Email Address',['class'=>'control-label']) !!}
            <div class="append-icon">
                <input type="text" name="email" value="{{$unit->email}}" class="form-control" placeholder="Enter Email Address" required disabled>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label class="control-label">Address</label>
            <div class="append-icon">
                <textarea name="address" class="form-control" placeholder="Enter the interview details..." rows="4" required disabled> {{$unit->address}}</textarea>
                <i class="icon-lock"></i>
            </div>
        </div>
    </div>

</div>


<div class="text-center  m-t-20">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>

