{!! Form::open(['method'=>'PATCH','action'=>['LineController@update',$line->id],'files'=>true]) !!}

<div class="row">
    <div class="col-sm-12">
        <div class="required form-group">
            {!! Form::label('line','Line Number',['class'=>'control-label']) !!}
            <div class="append-icon">
                <input type="text" name="line_no" value="{{$line->line_no}}" class="form-control" placeholder="Enter Name..." required>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="required form-group">
            <div class="form-group">
                {!! Form::label('floor_id','Floor:') !!}
                {!! Form::select('floor_id',[''=>'Select Floor']+$floors,$line->floor_id,['class'=>'form-control','required'=>'', 'data-search'=>'true']) !!}

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label class="control-label">Description</label>
            <div class="append-icon">
                <textarea name="lineDescription" class="form-control" placeholder="Enter Line Description..." rows="4"> {{$line->lineDescription}}</textarea>
                <i class="icon-lock"></i>
            </div>
        </div>
    </div>

</div>



<div class="text-center  m-t-20">
    <button type="submit" id="update-unit" class="btn btn-embossed btn-primary">Update Info</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>

{!! Form::close() !!}