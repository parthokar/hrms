@extends('layouts.master')
@section('title', 'Attendance Bonus Category List')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">

        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{Session::get('message')}}</p>
        @endif
        @if(Session::has('fmessage'))
            <p id="alert_message" class="alert alert-danger">{{Session::get('message')}}</p>
        @endif

        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-list"></i><strong> Attendance Bonus Category List</strong></h3>
                    </div>
                    <div class="panel-content">
                        <div class="panel-content well">
                        <h3><i class="fa fa-plus-square"></i> <b>New Bonus Category</b></h3><hr>
                            {{Form::open(array('url' => 'attendance/bonus/store','method' => 'post'))}}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="pwd">Title:</label>
                                        <input type="text" name="bTitle" class="form-control form-white" placeholder="Title" required maxlength="20">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="pwd">Bonus Amount:</label>
                                        <input type="number" step="any" name="bAmount" class="form-control form-white" placeholder="Bonus Amount" required >
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Create</button>
                                    </div> 
                                </div>
                            </div>                           
                            {{ Form::close() }}
                        </div>
                        <hr>
                        <table class="table-responsive table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Title</th>
                                <th>Amount</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=0; @endphp
                            @foreach($data as $ab)
                            <tr>
                                <td>{{++$i}}</td>
                                <td>{{$ab->bTitle}}</td>
                                <td>{{$ab->bAmount}}</td>
                                <td width="25%"><a data-toggle="modal" data-target="#{{$ab->id}}" class="btn btn-success btn-sm" type="submit"><i class="fa fa-edit"></i></a>
                                <a data-toggle="modal" data-target="#delete{{$ab->id}}" class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i></a></td>
                            </tr>
                            <!-- Modal -->
                            <div class="modal fade" id="{{$ab->id}}" role="dialog">
                                <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Update</h4>
                                        </div>
                                        <div class="modal-body">
                                            {{Form::open(array('url' => 'attendance/bonus/update','method' => 'post'))}}
                                            <div class="form-group">
                                                <label for="bTitle">Title</label>
                                                <input type="text" name="bTitle" class="form-control form-white" required value="{{$ab->bTitle}}" placeholder="Title" >
                                            </div>
                                             <div class="form-group">
                                                <label for="bAmount">Amount</label>
                                                <input type="number" step="any" name="bAmount" class="form-control form-white" required value="{{$ab->bAmount}}" id="bAmount" >
                                            </div>

                                            <input type="hidden" name="bId" value="{{$ab->id}}">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="delete{{$ab->id}}" role="dialog">
                                <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Remove Attendance Bonus Information </h4>
                                        </div>
                                        <div class="modal-body">
                                            
                                            {{Form::open(array('url' => 'attendance/bonus/delete','method' => 'post'))}}
                                             <div class="form-group">
                                                <h3><b>Are you sure to remove {{$ab->bTitle}}?</b></h3>
                                            </div>
                                            <hr>
                                            <input type="hidden" name="bId" value="{{$ab->id}}">
                                            <button type="submit" class="btn btn-danger">Remove Shift</button>
                                            <button  class="btn btn-default" type="button" class="close" data-dismiss="modal">Close</button>
                                            
                                            {{ Form::close() }}

                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection