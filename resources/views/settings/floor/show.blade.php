<div class="row">
    <div class="col-sm-12">
        <div class="required form-group">
            {!! Form::label('floor','Floor',['class'=>'control-label']) !!}
            <div class="append-icon">
                <input type="text" name="floor" value="{{$floor->floor}}" class="form-control" placeholder="Enter Name..." required disabled>
            </div>
        </div>
    </div>
</div>


<div class="text-center  m-t-20">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>

