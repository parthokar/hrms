{!! Form::open(['method'=>'DELETE','action'=>['FloorController@destroy',$floor->id]]) !!}

<div class="container">
    <p>You sure you want to delete <strong>{{ $floor->floor }}</strong> ??</p>
</div>


<div class="text-center  m-t-20">
    <button type="submit" id="update-unit" class="btn btn-embossed btn-danger">Confirm Delete</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>

{!! Form::close() !!}