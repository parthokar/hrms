{!! Form::open(['method'=>'PATCH','action'=>['FloorController@update',$floor->id]]) !!}

<div class="row">
    <div class="col-sm-12">
        <div class="required form-group" style="color:#555;">
            {!! Form::label('line','Line Number',['class'=>'control-label']) !!}
            <div class="append-icon">
                <input type="text" name="floor" value="{{$floor->floor}}" class="form-control" placeholder="Enter floor..." required>
            </div>
        </div>
    </div>
</div>


<div class="text-center  m-t-20">
    <button type="submit" id="update-unit" class="btn btn-embossed btn-primary">Update Info</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>

{!! Form::close() !!}