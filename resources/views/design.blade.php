<!DOCTYPE html>
<html>
<head>
    <title>Page Title</title>
    <style>
        .company{
            text-align: center;
        }
        .company p{
            padding: 0;
            margin: 0;
            font-size: 22px;
        }
        .main_div{
            width:100%;
            margin: 5px;
        }
        .department_text{
            width: 10%;
            float: left;
        }
        .department_name{
            width: 10%;
            float: left;
        }
        .days_in_month{
            width: 20%;
            float: right;
        }
        .payment_date{
            width: 60%;
            float: left;
        }
        .section_line{
            width: 100%;
            float: left;
            margin-top: 5px;
        }

        .salary_table{
            width: 100%;
            margin-top:51px;
        }

        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th{
            font-size: 16px;
        }
        td{
            font-size: 14px;
            padding: 16px 3px;
        }
        .authority{
            width:100%;
            padding-top: 60px;
        }
        .prepared_by{
           width: 25%;
           float: left;
        }
        .audited_by{
            width: 25%;
            float: left;
        }
        .recomended_by{
            width: 25%;
            float: left;
        }
        .approved_by{
            width: 25%;
            float: left;
        }
    </style>
</head>
<body>
<button style="margin-top: 17px;" class="btn btn-info" id="pdf_btn" type="button" onclick="printDiv('print_area')">Print</button>
       <div id="print_area">
          <div class="company">
              <h3>{{$companyInformation->company_name}}</h3>
              <p>Pay Sheet for the month of</p>
              <p>November-2018</p>
          </div>

          <div class="main_div">
              <div class="department_text">
                  <span>Department:</span>
              </div>

              <div class="department_name">
                  <span>Swing</span>
              </div>

              <div class="days_in_month">
                  <span>Days In Month: 31</span>
              </div>

              <div class="payment_date">
                  <span>Payment Date: <?php echo date('m-d-Y'); ?></span>
              </div>

              <div class="section_line">
                  <span>Section: Section 07</span>
              </div>
          </div>



           <div class="salary_table">
               <table>
                   <tr>
                       <th>SN</th>
                       <th>EmpID</th>
                       <th>Name</th>
                       <th>Designation</th>
                       <th>Grade</th>
                       <th>Basic</th>
                       <th>House</th>
                       <th>Medical</th>
                       <th>Food</th>
                       <th>Transport</th>
                       <th>Gross</th>
                       <th colspan="4">Leave</th>
                       <th>Workdays</th>
                       <th>Absent</th>
                       <th colspan="2">Deduction</th>
                       <th>Gross Pay</th>
                       <th colspan="3">Overtime</th>
                       <th>Att Bonus</th>
                       <th>Special Allow</th>
                       <th>Net wages</th>
                       <th>Signature</th>
                   </tr>
                   <tr>
                       <td>77</td>
                       <td>8132432432</td>
                       <td>8132432432</td>
                       <td>8132432432</td>
                       <td>8132432432</td>
                       <td>8132432432</td>
                       <td>8132423432432432</td>
                       <td>81324324324</td>
                       <td>81324234324</td>
                       <td>81324324324</td>
                       <td>81324324324</td>
                       <td>CL:2</td>
                       <td>SL:3</td>
                       <td>EL:4</td>
                       <td>ML:1</td>
                       <td>25</td>
                       <td>20</td>
                       <td>Adv:32423432</td>
                       <td>Absent:32432432</td>
                       <td>3423432432</td>
                       <td>Hrs:20</td>
                       <td>Rat:3456</td>
                       <td>Amt:325324534</td>
                       <td>500</td>
                       <td>100</td>
                       <td>12000</td>
                       <td></td>
                   </tr>

               </table>
           </div>

           <div class="authority">
               <div class="prepared_by">
                   <span>Prepared By:</span>
               </div>
               <div class="audited_by">
                   <span>Audited By:</span>
               </div>
               <div class="recomended_by">
                   <span>Recommended By:</span>
               </div>
               <div class="approved_by">
                   <span>Approved By:</span>
               </div>
           </div>
       </div>

          <script>
              function printDiv(divName) {
                  var printContents = document.getElementById(divName).innerHTML;
                  var originalContents = document.body.innerHTML;
                  document.body.innerHTML = printContents;
                  window.print();
                  document.body.innerHTML = originalContents;
              }
          </script>
</body>
</html>