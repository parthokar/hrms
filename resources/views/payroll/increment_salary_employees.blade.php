@extends('layouts.master')
@section('title', 'Increment Employee Salary')
@section('content')
          <div class="page-content">
              @if(Session::has('increment_bonus_add'))
                  <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('increment_bonus_add') }}</p>
              @endif
              @if(Session::has('increment_bonus_delete'))
                      <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('increment_bonus_delete') }}</p>
               @endif
              {{Form::open(array('url' => 'employee/year/search','method' => 'post'))}}
              <div class="panel panel-default">
                    <div class="panel-heading">
                        Search Incrementable Employees
                    </div>
                        <div class="panel-body">


                          <div class="col-md-6">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <div class="form-group">
                                          <label>Select Month</label>
                                          <input type="text" class="form-control" name="incr_employees_month" id="year_bonus_month" placeholder="Select Month" autocomplete="off" required>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="form-label">Select Work Group</label>
                                      <select class="form-control" name="work_group" required>
                                          <option value="Worker">Worker</option>
                                          <option value="Staff">Staff</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
                              </div>
                          </div>
                            {{ Form::close() }}

                            {{Form::open(array('url' => 'employee/year/next/increment/search','method' => 'post'))}}
                            <div class="col-md-6">
                                <label>Next Increment</label>
                                <select id="next_increment_submission" class="form-control" name="check_increment">
                                    <option>Select</option>
                                    @foreach($next_increment as $increment)
                                       <option value="{{$increment->increment_month}}">{{date('F-Y',strtotime($increment->increment_month))}}</option>
                                    @endforeach
                                </select>
                            </div>
                            {{ Form::close() }}
                    </div>
              </div>
          </div>
          <script>
              $(document).ready(function() {
                  $('#year_bonus_month').datepicker({
                      changeMonth: true,
                      changeYear: true,
                      dateFormat: "yy-mm-dd",
                      showButtonPanel: true,
                      currentText: "This Month",
                      onChangeMonthYear: function (year, month, inst) {
                          $(this).val($.datepicker.formatDate('yy-mm-dd', new Date(year, month - 1, 1)));
                      },
                      onClose: function(dateText, inst) {
                          var month = $(".ui-datepicker-month :selected").val();
                          var year = $(".ui-datepicker-year :selected").val();
                          $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                      }
                  }).focus(function () {
                      $(".ui-datepicker-calendar").hide();
                  });
              });
          </script>

          <script>
              $(document).ready(function() {
                  $("#next_increment_submission").change(function(){
                      $(this).closest('form').attr('target', '_blank').submit();
                  });
                  setTimeout(function() {
                      $('#alert_message').fadeOut('fast');
                  }, 5000);
              });
          </script>
    @include('include.copyright')
@endsection