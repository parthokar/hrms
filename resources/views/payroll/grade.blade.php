@extends('layouts.master')
@section('title', 'New Grade')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
          @if(Session::has('message'))
              <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
           @endif
                <div class="panel">
                    <div class="panel-header">
                          <div class="row">
                            <div class="col-md-4">
                                <h3>New Pay<strong>roll/Grade</strong></h3>
                            </div>
                            <div class="col-md-8" style="margin-top:8px;">
                               <a href="{{url('/grade')}}" class="btn btn-success btn-sm pull-right"> <i class="fa fa-eye"></i> View Grade List</a>
                            </div>
                          </div>
                    </div>
                       <div class="panel-content">
                            {{Form::open(array('url' => 'payroll_grade_store','method' => 'post'))}}
                                    <div class="form-group">
                                        <label for="gradename">Grade Name</label>
                                        <input type="text" name="grade_name" class="form-control form-white" id="gradename" placeholder="Enter Grade Name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="basicamount">Basic</label>
                                        <input onkeyup="house_rant()" type="number" name="basic_amount" step="any" class="form-control form-white" id="basicamount" placeholder="Amount" required>
                                    </div>
                                   <div class="form-group">
                                       <label for="rantamount">House Rent</label>
                                       <input type="text" name="house_amount"  class="form-control form-white" id="rantamount" value=""   placeholder="Amount" required>
                                   </div>
                                   <div class="form-group">
                                       <label for="medicalamount">Medical</label>
                                       <input type="number" name="medical_amount" step="any" class="form-control form-white" id="medicalamount" placeholder="Amount" required>
                                   </div>
                                   <div class="form-group">
                                       <label for="transportamount">Transportation</label>
                                       <input type="number" name="transport_amount" step="any" class="form-control form-white" id="transportamount" placeholder="Amount" required>
                                   </div>
                                   <div class="form-group">
                                       <label for="foodamount">Food</label>
                                       <input type="number" name="food_amount" step="any" class="form-control form-white" id="foodamount" placeholder="Amount" required>
                                   </div>
                                   <div class="form-group">
                                       <label for="othersamount">Others</label>
                                       <input type="number" name="others_amount" step="any" class="form-control form-white" id="othersamount" placeholder="Amount" required>
                                   </div>
                                  <input type="hidden" name="user_name" value="{{auth::user()->name}}">
                                  <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                             {{ Form::close() }}
                         </div>
                    </div>
             </div>
    <script>
        function house_rant(){
            var basic=$("#basicamount").val();
            var house_rant=basic/100*50;
            var house_rat_value=document.getElementById("rantamount").value=house_rant.toFixed();
        }
    </script>
    @include('include.copyright')
@endsection