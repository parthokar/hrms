@extends('layouts.master')
@section('title', 'Employee Salary')
@section('content')
    <style>
        .pagination li a {
            color: #A2A2A2;
            font-size: 12px;
            background: darkblue;
            color: #ffffff;
        }

        .pagination{
            padding: 18px;
            border: 1px solid #9292b1;
        }
    </style>
    <script>
    setTimeout(function() {
    $('#alert_message').fadeOut('fast');
    }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('message'))
        <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-table"></i> <strong>Employee </strong>Total Gross</h3>
                    </div>
              

                    <div class="col-md-3"></div>
                <form method="POST" action="{{url('employee/search/')}}">
                    @csrf 
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Enter Employee Id or Name</label>
                            <input type="text" class="form-control" name="employee" autocomplete="off" required>
                        </div>
                        <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
                    </div>
                </form>
                    <div class="col-md-3"></div>
            

                    <div class="panel-content table-responsive">
                        <table class="table table-hover table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>EmpID</th>
                                <th>Employee</th>
                                <th>Designation</th>
                                <th>Grade</th>
                                <th>Basic</th>
                                <th>Gross</th>
                                <th width="15%">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $userSalary)
                                    <tr>
                                        <td><a href="{{route('employee_profile.show',base64_encode($userSalary->emp_id))}}" target="_blank"><b>{{$userSalary->employeeId}}</b></a></td>
                                        <td>{{$userSalary->empFirstName}}</td>
                                        <td>{{$userSalary->designation}}</td>
                                        <td>{{$userSalary->grade_name}}</td>
                                        <td>{{$userSalary->basic_salary}}</td>
                                        
                                        <td>{{sprintf('%0.2f', $userSalary->total_employee_salary)}}</td>
                                            
                                        <td>
                                            <a data-toggle="modal" data-target="#{{$userSalary->id}}" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>
                                            <button  id="salary_grade_modal" value="{{$userSalary->id}}" class="btn btn-success btn-sm open_modal"><i class="fa fa-edit"></i></button>

                                        </td>
                                    </tr>
                                <!-- Modal -->
                                <div class="modal fade" id="{{$userSalary->id}}" role="dialog" style="margin-bottom: 40px;">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title"><b>Salary Details</b></h4>
                                            </div>
                                            <h4 class="text-center" style="margin-top:20px;"><b>Employee: {{$userSalary->empFirstName}} </b></h4>
                                            <hr>
                                            <div class="modal-body">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pwd">Employee ID:</label>
                                                        <input type="text" name="" class="form-control form-white" value="{{$userSalary->employeeId}}" readonly >
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pwd">Employee:</label>
                                                        <input type="text" name="" class="form-control form-white" value="{{$userSalary->empFirstName}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pwd">Grade:</label>
                                                        <input type="text" name="" class="form-control form-white" value="{{$userSalary->grade_name}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pwd">Basic:</label>
                                                        <input type="text" name="" class="form-control form-white" value="{{$userSalary->basic_salary}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pwd">House rant:</label>
                                                        <input type="text" name="" class="form-control form-white" value="{{$userSalary->house_rant}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pwd">Medical Allowance:</label>
                                                        <input type="text" name="" class="form-control form-white" value="{{$userSalary->medical}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pwd">Transportation Allowance:</label>
                                                        <input type="text" name="" class="form-control form-white" value="{{$userSalary->transport}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pwd">Food Allowance:</label>
                                                        <input type="text" name="" class="form-control form-white" value="{{$userSalary->food}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pwd">Other:</label>
                                                        <input type="text" name="" class="form-control form-white" value="{{$userSalary->other}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <hr>
                                                    <h3  style="color:red"><b>Gross Salary: {{$userSalary->total_employee_salary}} Taka</b></h3>
                                                    
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Modal data edit -->
                                <div class="modal fade" id="employeesalarymodaledit" role="dialog" style="margin-bottom: 40px;">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title"><b>Salary Update</b></h4>
                                            </div>
                                            <h4 class="text-center" style="margin-top: 20px;"><b>Employee: <span id="emp_name" class="text-center"></span> </b></h4>

                                            
                                            <div class="modal-body">
                                                {{Form::open(array('url' => 'salary_up','method' => 'post'))}}
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="pwd">Select Grade:</label>
                                                        <select id="grade_employee_select" name="grade_employee_select" class="form-control">
                                                            <option>Select Grade</option>
                                                              @foreach($grade_name as $grade)
                                                              <option value="{{$grade->id}}">{{$grade->grade_name}}</option>
                                                              @endforeach
                                                         </select>
                                                    </div>
                                                    <input type="hidden" name="salary_grade_url" id="salary_grade_url" value="{{URL::to('/employee/salary/grade/modals')}}">
                                                    <input type="hidden" name="grade_url" id="grade_url" value="{{URL::to('/employee/grade/salary/update')}}">
                                                    <input type="hidden" id="emp_idsss" name="emp_ac_id" value="">
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pwd">Current Grade:</label>
                                                        <input type="text" class="form-control form-white" value="{{$userSalary->grade_name}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pwd">Selected Grade:</label>
                                                        <input type="text" id="current_grade" name="current_grade" class="form-control form-white" value="" readonly>
                                                        <input type="hidden" name="emp_grade_id" id="current_grade_id" value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    
                                                    <div class="form-group">
                                                        <label for="gross"><b>Gross Salary</b></label>
                                                        <input type="number" onkeyup="recalculatesalary()" step="any" name="gross" class="form-control form-white" id="gross" placeholder="Amount" required >
                                                    </div>

                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pwd">Basic:</label>
                                                        <input type="text" readonly id="basic" name="basic" class="form-control form-white" value="" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pwd">House rant:</label>
                                                        <input type="text" readonly id="house_rant" name="house_rant" class="form-control form-white" value="" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pwd">Medical Allowance:</label>
                                                        <input type="text" readonly id="medical" name="medical" class="form-control form-white" value="" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pwd">Transportation Allowance:</label>
                                                        <input type="text" readonly id="transport" name="transport" class="form-control form-white" value="" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pwd">Food Allowance:</label>
                                                        <input type="text" readonly id="foods" name="foods" class="form-control form-white" value="" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pwd">Other:</label>
                                                        <input type="text" readonly id="others" name="others" class="form-control form-white" value="" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <p>Total Gross:<span id="total_gross_salary" style="color:red"></span>  Taka</p>
                                                </div>
                                                <div class="col-md-12">
                                                    <hr>
                                                    <button onclick="return confirm('are you sure?')" type="submit" class="btn btn-success">Update</button>
                                                    <input type="hidden" id="actual_id" name="salary_hidden_id" value="">
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                            </div>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).on('click','.open_modal',function(){
             var id = $(this).val();
             var url = $('#salary_grade_url').val();
             var urlid=url+'/'+id;
             var formData = {
                id: $(this).val(),
             }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $.ajax({
                type: "GET",
                url: urlid,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    $('#employeesalarymodaledit').modal('show');
                    $.each(data, function (i, item) {
                          var empid=document.getElementById('emp_idsss').value= item.emp_id;
                          var acid=document.getElementById('actual_id').value= item.default_id;
                          var gradeid=document.getElementById('current_grade_id').value= item.grades_id;
                          var empname=document.getElementById('emp_name').innerHTML=item.empFirstName;
                          var gradename=document.getElementById('current_grade').value= item.grade_name;
                          var basic=document.getElementById('basic').value= item.basic_salary;
                          var house=document.getElementById('house_rant').value= item.house_rant;
                          var medical=document.getElementById('medical').value= item.medical;
                          var transport=document.getElementById('transport').value= item.transport;
                          var food=document.getElementById('foods').value= item.food;
                          var others=document.getElementById('others').value= item.default_others;
                          var total_salary =document.getElementById('total_gross_salary').innerHTML= item.total_employee_salary;
                          document.getElementById('gross').value= item.total_employee_salary;
                     });
                },
                error: function (data) {
                    console.log('Error:', data);
                }
             });
            });
    </script>
    <script>
        $(document).ready(function(){
            $("#grade_employee_select").change(function(){
                var url = $('#grade_url').val();
                var id=$("#grade_employee_select").val();
                var urlid=url+'/'+id;

                  var formData = {
                    id: $('#grade_employee_select').val(),
                  }
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    })
                    $.ajax({
                        type: "GET",
                        url: urlid,
                        data: formData,
                        dataType: 'json',
                        success: function (data) {
                            $.each(data, function (i, item) {
                                  var gradeid=document.getElementById('current_grade_id').value= item.id;
                                  var gradename=document.getElementById('current_grade').value= item.grade_name;
                                  var basic=document.getElementById('basic').value= item.basic_salary;
                                  var house=document.getElementById('house_rant').value= item.house_rant;
                                  var medical=document.getElementById('medical').value= item.medical;
                                  var transport=document.getElementById('transport').value= item.transport;
                                  var food=document.getElementById('foods').value= item.food;
                                  var others=document.getElementById('others').value= item.others;
                                  var total_gross=  (parseFloat(basic)+parseFloat(house)+parseFloat(medical)+parseFloat(transport)+parseFloat(food)+parseFloat(others));
                                  document.getElementById('total_gross_salary').innerHTML=total_gross;
                                  document.getElementById('gross').value=total_gross;

                            });
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
               });
           });


            function recalculatesalary(){
                var gross=parseFloat($("#gross").val());
                $("#medical").val('600.00');
                $("#transport").val('350.00');
                $("#foods").val('900.00');
                $("#other").val('0.00');

                var medical=parseFloat($("#medical").val());
                var transport=parseFloat($("#transport").val());
                var food=parseFloat($("#foods").val());
                var other=parseFloat($("#others").val());
                var deduction = (medical+transport+food+other);
                var basic = ((gross-deduction)/1.5);
                $("#basic").val(basic.toFixed());
                var house = ((basic/100)*50);
                $("#house_rant").val(house.toFixed());
                document.getElementById('total_gross_salary').innerHTML= gross;
            }
    </script>
    @include('include.copyright')
@endsection