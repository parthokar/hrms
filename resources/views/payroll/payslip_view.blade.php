@extends('layouts.master')
@section('title', 'Payslip')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
        @endif


        @if(Session::has('attendancebonus'))
                <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('attendancebonus') }}</p>
        @endif

        <ul class="nav nav-tabs">
            <li><a data-toggle="tab" href="#home">BONUS</a></li>
            <li><a data-toggle="tab" href="#menu1">PAYSLIP</a></li>
        </ul>
        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <h3>SELECT BONUS TYPE</h3>
                <ul class="nav nav-tabs">
                    <li><a data-toggle="tab" href="#normal_bonus">One year Bonus</a></li>
                    <li><a data-toggle="tab" href="#attendance">Attendance Bonus</a></li>
                </ul>
                <div class="tab-content">
                    <div id="normal_bonus" class="tab-pane fade">
                        <h3>One Year Employee Bonus</h3>
                        {{Form::open(array('url' => 'employee/bonus/store','method' => 'post'))}}
                        <div class=" pagination2">
                        <table id="example" class="table table-hover table-dynamic">
                        <thead>
                        <tr>
                        <th>Order</th>
                        <th>Employee</th>
                        <th>Designation</th>
                        <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $order=0; @endphp
                        @foreach($data as $employee)
                        @php $order++; @endphp
                        <tr>
                        <td>{{$order}}</td>
                        <td>{{$employee->empFirstName}}  {{$employee->empLastName}}</td>
                        <td>{{$employee->designation}}</td>
                        <td><input type="number"  name="bonus_amount_employees[]" id="bonus_amount_employees[]" class="form-control form-white" placeholder="Amount"></td>
                        <input type="hidden" id="emp_ids[]" name="emp_ids[]" value="{{$employee->id}}">

                        </tr>
                        @endforeach
                        </tbody>
                        </table>
                        <button  type="submit" class="btn btn-success">Save</button>
                        </div>
                        {{ Form::close() }}
                    </div>
                    <div id="attendance" class="tab-pane fade">
                        <h3>Attendance Bonus Of Employee</h3>
                        {{Form::open(array('url' => 'attendancebonus','method' => 'post'))}}
                        <div class=" pagination2">
                        <table id="example" class="table table-hover table-dynamic">
                        <thead>
                        <tr>
                        <th>Order</th>
                        <th>Employee</th>
                        <th>Designation</th>
                        <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $order=0; @endphp
                        @foreach($data as $employee)
                        @php $order++; @endphp
                        <tr>
                        <td>{{$order}}</td>
                        <td>{{$employee->empFirstName}}  {{$employee->empLastName}}</td>
                        <td>{{$employee->designation}}</td>
                        <td><input type="number"  name="bonus_amount_employees[]" id="bonus_amount_employees[]" class="form-control form-white" placeholder="Amount"></td>
                        <input type="hidden" id="emp_ids[]" name="emp_ids[]" value="{{$employee->id}}">
                        <input type="hidden" id="url" name="hiddenurl" value="{{URL::to('/overtime_store')}}">
                        </tr>
                        @endforeach
                        </tbody>
                        </table>
                        <button  type="submit" class="btn btn-success">Save</button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
            <div id="menu1" class="tab-pane fade">
                <h3>PAYSLIP</h3>
                <p>You have successfully completed all steps.</p>
                <a href="{{url('/report/payslip')}}" class="btn btn-success">Generate Payslip</a>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection