<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OvertimeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_overtime')->insert([
            'min_overtime'=>'18:30',
            'max_overtime'=>'20:00'
        ]);
    }
}
