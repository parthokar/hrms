<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WeekLeaveTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $insertData=[
            [
                'id'=>1,
                'day-id'=>1,
                'day'=>'Sunday',
                'status'=>0,
            ],
            [
                'id'=>2,
                'day-id'=>2,
                'day'=>'Monday',
                'status'=>0,
            ],
            [
                'id'=>3,
                'day-id'=>3,
                'day'=>'Tuesday',
                'status'=>0,
            ],
            [
                'id'=>4,
                'day-id'=>4,
                'day'=>'Wednesday',
                'status'=>0,
            ],
            [
                'id'=>5,
                'day-id'=>5,
                'day'=>'Thursday',
                'status'=>0,
            ],
            [
                'id'=>6,
                'day-id'=>6,
                'day'=>'Friday',
                'status'=>1,
            ],
            [
                'id'=>7,
                'day-id'=>7,
                'day'=>'Saturday',
                'status'=>0,
            ],
        ];
        DB::table('week_leave')->insert($insertData);
    }
}
