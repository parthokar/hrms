<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LeaveTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $insertData=[
            [
                'id'=>1,
                'leave_type'=>"Maternity Leave",
                'total_days'=>120,
                'created_by'=>"Developer",
		        'modified_by'=>"Developer",
            ],
 	        [
                'id'=>4,
                'leave_type'=>"Earn Leave",
                'total_days'=>10,
                'created_by'=>"Developer",
                'modified_by'=>"Developer",
            ],
        ];
        DB::table('tb_leave_type')->insert($insertData);
    }
}
