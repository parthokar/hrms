<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompanyInformationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbcompany_information')->insert([
            'company_name'=>"FEITS IT. Solution",
            'company_phone'=>"123456789",
            'company_email'=>"company@email.com",
            'company_address1'=>"Uttora, Dhaka",
            'company_address2'=>"Bangladesh",
        ]);
    }
}
