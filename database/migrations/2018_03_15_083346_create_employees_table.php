<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('empFirstName','30');
            $table->string('empLastName','30')->nullable()->default("");
            $table->string('employeeId','30');
            $table->index('employeeId');
            $table->integer('empDesignationId');
            $table->index('empDesignationId');
            $table->tinyInteger('empGenderId');
            $table->index('empGenderId');
            $table->tinyInteger('empRole');
            $table->string('empPassword','60');
            $table->tinyInteger('empAccStatus')->default(1);
            $table->tinyInteger('empShiftId')->nullable();
            $table->tinyInteger('empAttBonusId')->nullable();
            $table->tinyInteger('empOTStatus')->nullable();
            $table->index('empAccStatus');
            $table->string('empEmail','50')->unique()->nullable();
            $table->string('empPhone','15')->nullable();
            $table->text('empParAddress')->nullable();
            $table->text('empCurrentAddress')->nullable();
            $table->string('empFatherName','50')->nullable();
            $table->string('empMotherName','50')->nullable();
            $table->tinyInteger('empDepartmentId');
            $table->index('empDepartmentId');
            $table->date('empJoiningDate');
            $table->index('empJoiningDate');
            $table->date('empDOB')->nullable();
            $table->string('empPhoto','150')->nullable();
            $table->tinyInteger('empMaritalStatusId');
            $table->string('empEcontactName','50')->nullable();
            $table->string('emergencyPhone','20')->nullable();
            $table->text('emergencyAddress')->nullable();
            $table->string('empReligion','30')->nullable();
            $table->string('empNid','50')->nullable();
            $table->tinyInteger('empNationalityId')->nullable();
            $table->string('empGlobalId','50')->nullable();
            $table->string('empSection')->nullable();
            $table->string('empBloodGroup','4')->nullable();
            $table->string('empCardNumber','20')->unique()->nullable();
            $table->index('empCardNumber');
            $table->integer('unit_id')->nullable();
            $table->index('unit_id');
            $table->integer('line_id')->nullable();
            $table->index('line_id');
            $table->integer('floor_id')->nullable();
            $table->index('floor_id');
            $table->string('work_group',40)->nullable();
            $table->string('salary_type',40)->nullable();
            $table->string('bank_account',120)->nullable();
            $table->text('bank_info')->nullable();
            $table->string('payment_mode',60)->nullable();
            $table->date('date_of_discontinuation')->nullable();
            $table->text('reason_of_discontinuation')->nullable();
            $table->string('emergency_contact_relation',60)->nullable();
            $table->string('create_by',50)->nullable();
            $table->string('modified_by',50)->nullable();
            $table->string('skill_level','90')->nullable();
            $table->string('reference_type',60)->default('None');
            $table->string('empBnFullName')->nullable();
            $table->string('employeeBnId')->nullable();
            $table->string('empBnFatherName')->nullable();
            $table->string('empBnMotherName')->nullable();
            $table->text('reference_description')->nullable();
            $table->text('empBnCurrentAddress')->nullable();
            $table->text('empBnParAddress')->nullable();
            $table->string('empBnEduQuality')->nullable();
            $table->string('empPreExperience')->nullable();
            $table->string('empBnWorkingType')->nullable();
            $table->string('empChildrenNumber')->nullable();
            $table->string('empBoyNumber')->nullable();
            $table->string('empGirlNumber')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
