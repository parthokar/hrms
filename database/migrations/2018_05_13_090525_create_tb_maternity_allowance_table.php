<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbMaternityAllowanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_maternity_allowance', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_id');
            $table->index('employee_id');
            $table->date('leave_starting_date');
            $table->index('leave_starting_date');
            $table->date('leave_ending_date');
            $table->index('leave_ending_date');
            $table->string('previous_three_month_salary');
            $table->string('total_working_days');
            $table->string('total_payable_amount');
            $table->string('per_instalment');
            $table->string('other2');
            $table->string('other3');
            $table->timestamps();
            $table->date('first_installment_pay')->nullable();
            $table->date('second_installment_pay')->nullable();
            $table->string('approved_by')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_maternity_allowance');
    }
}
