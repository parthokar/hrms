<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbhrmsActivationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbhrms_activation', function (Blueprint $table) {
            $table->increments('id');
            $table->string('trackId')->nullable();
            $table->string('haOption')->nullable();
            $table->string('haValue')->nullable();
            $table->string('haSDate')->nullable();
            $table->string('haEDate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbhrms_activation');
    }
}
