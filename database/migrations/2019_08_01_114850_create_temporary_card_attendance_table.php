<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemporaryCardAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temporary_card_attendance', function (Blueprint $table) {
            $table->increments('id');
            $table->string('empCardNumber','20');
            $table->index('empCardNumber');
            $table->string('in_time','30')->nullable();
            $table->index('in_time');
            $table->string('out_time','30')->nullable();
            $table->index('out_time');
            $table->timestamp('date');
            $table->index('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temporary_card_attendance');
    }
}
