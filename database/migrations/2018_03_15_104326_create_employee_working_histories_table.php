<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeWorkingHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_working_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id');
            $table->index('emp_id');
            $table->string('empCompanyName','190');
            $table->string('empJobTile','190');
            $table->timestamp('empJoiningDate')->nullable();
            $table->timestamp('empLeaveDate')->nullable();
            $table->text('empWHDescription')->nullable();
            $table->text('empWHattachment')->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('modified_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_working_histories');
    }
}
